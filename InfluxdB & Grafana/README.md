

```
https://community.grafana.com/t/is-there-a-way-to-restore-an-accidentally-deleted-dashboard/23839
there is no way to get it back unless you have a copy of the database from when it was still not deleted or have exported the dashboard earlier.
```


---
## install

##### InfluxdB

```
$ scp influxdb-1.8.3.x86_64.rpm grafanax01:/tmp

$ yum localinstall influxdb-1.8.3.x86_64.rpm
$ systemctl start influxdb
$ systemctl status influxdb
```


```
influx <<EOF

create database njmon

EOF

##alt + D

influx <<EOF

show databases

EOF
```


```
bin]# ./influxd

 8888888           .d888 888                   8888888b.  888888b.
   888            d88P"  888                   888  "Y88b 888  "88b
   888            888    888                   888    888 888  .88P
   888   88888b.  888888 888 888  888 888  888 888    888 8888888K.
   888   888 "88b 888    888 888  888  Y8bd8P' 888    888 888  "Y88b
   888   888  888 888    888 888  888   X88K   888    888 888    888
   888   888  888 888    888 Y88b 888 .d8""8b. 888  .d88P 888   d88P
 8888888 888  888 888    888  "Y88888 888  888 8888888P"  8888888P"

2022-02-10T09:54:49.596989Z     info    InfluxDB starting       {"log_id": "0Z_lkpV0000", "version": "1.8.3", "branch": "1.8", "commit": "563e6c3d1a7a2790763c6289501095dbec19244e"}
2022-02-10T09:54:49.597012Z     info    Go runtime      {"log_id": "0Z_lkpV0000", "version": "go1.13.8", "maxprocs": 2}
run: open server: listen: listen tcp 127.0.0.1:8088: bind: address already in use

```

sudo netstat -ltnp | grep :

lsof -i:8086

kill -9 <process id>


$ ps aux | grep kubectl
$ kill -9 process_number_here


```
cat /lib/systemd/system/influxdb.service
# If you modify this, please also make sure to edit init.sh

[Unit]
Description=InfluxDB is an open-source, distributed, time series database
Documentation=https://docs.influxdata.com/influxdb/
After=network-online.target

[Service]
User=influxdb
Group=influxdb
LimitNOFILE=65536
EnvironmentFile=-/etc/default/influxdb
ExecStart=/usr/bin/influxd -config /etc/influxdb/influxdb.conf $INFLUXD_OPTS
KillMode=control-group
Restart=on-failure

[Install]
WantedBy=multi-user.target
Alias=influxd.service

```


journalctl -u influxdb



##### Telegraf

```
telegraf]# influx
Connected to http://localhost:8086 version 1.8.3
InfluxDB shell version: 1.8.3
> create user telegraf with password 'password123!'
> show users
user     admin
----     -----
telegraf false
> show GRANTS for telegraf
database privilege
-------- ---------
> grant ALL on telegraf to telegraf
> show GRANTS for telegraf
database privilege
-------- ---------
telegraf ALL PRIVILEGES
> quit
```


systemctl restart telegraf

```
# influx
Connected to http://localhost:8086 version 1.8.3
InfluxDB shell version: 1.8.3
> use telegraf
Using database telegraf

> show measurements
name: measurements
name
----
cpu
disk
diskio
kernel
mem
processes
swap
system
> quit

```
after go to grafana connection influxdB


##### Grafana

```
scp grafana-7.3.6-1.x86_64.rpm grafanax01:/tmp
wget <rpm package url>
sudo yum localinstall <local rpm package>
yum localinstall grafana-7.3.6-1.x86_64.rpm

sudo yum install <rpm package url>
```



```
sudo systemctl daemon-reload
sudo systemctl start grafana-server
sudo systemctl status grafana-server

sudo systemctl enable grafana-server
```



```
$ hostname
grafanax01
```


http://grafanax01:3000/

user-passwords firsly; admin , admin

and change your password for admin super user

after that go to configuration -> Data source -> select influxdB


finally fill in the blank for u'r options

---


```
$ influx
$ create database telegraf
$ create user “telegraf” with password ‘Password123!’ with all privileges
$ exit
```

ERR: error authorizing query: no user provided

->>>http-auth setup. -->> # command line

In grafana;

You can create auto dashboard with uid or download - import json data.

> Dashboards -> Manage -> Import -> Upload Json file

How can i snapshot in grafana? 



like that [snapshot vmware-vsphere](https://snapshot.raintank.io/dashboard/snapshot/GjIn7BFdZvImyQbJKB7aCb4F7zM7knFT?orgId=2 )



#grafana mssql connection examples querry


```
SELECT
  $__timeEpoch(<time_column>),
  <value column> as value,
  <series name column> as metric
FROM
  <table name>
WHERE
  $__timeFilter(time_column)
ORDER BY
  <time_column> ASC

Time series:
- return column named time (in UTC), as a unix time stamp or any sql native date data type. You can use the macros below.
- any other columns returned will be the time point values.
Optional:
  - return column named metric to represent the series name.
  - If multiple value columns are returned the metric column is used as prefix.
  - If no column named metric is found the column name of the value column is used as series name

Resultsets of time series queries need to be sorted by time.

Table:
- return any set of columns

Macros:
- $__time(column) -> column AS time
- $__timeEpoch(column) -> DATEDIFF(second, '1970-01-01', column) AS time
- $__timeFilter(column) -> column BETWEEN '2017-04-21T05:01:17Z' AND '2017-04-21T05:01:17Z'
- $__unixEpochFilter(column) -> column >= 1492750877 AND column <= 1492750877
- $__unixEpochNanoFilter(column) ->  column >= 1494410783152415214 AND column <= 1494497183142514872
- $__timeGroup(column, '5m'[, fillvalue]) -> CAST(ROUND(DATEDIFF(second, '1970-01-01', column)/300.0, 0) as bigint)*300.
     by setting fillvalue grafana will fill in missing values according to the interval
     fillvalue can be either a literal value, NULL or previous; previous will fill in the previous seen value or NULL if none has been seen yet
- $__timeGroupAlias(column, '5m'[, fillvalue]) -> CAST(ROUND(DATEDIFF(second, '1970-01-01', column)/300.0, 0) as bigint)*300 AS [time]
- $__unixEpochGroup(column,'5m') -> FLOOR(column/300)*300
- $__unixEpochGroupAlias(column,'5m') -> FLOOR(column/300)*300 AS [time]

Example of group by and order by with $__timeGroup:
SELECT
  $__timeGroup(date_time_col, '1h') AS time,
  sum(value) as value
FROM yourtable
GROUP BY $__timeGroup(date_time_col, '1h')
ORDER BY 1

Or build your own conditionals using these macros which just return the values:
- $__timeFrom() ->  '2017-04-21T05:01:17Z'
- $__timeTo() ->  '2017-04-21T05:01:17Z'
- $__unixEpochFrom() -> 1492750877
- $__unixEpochTo() -> 1492750877
- $__unixEpochNanoFrom() ->  1494410783152415214
- $__unixEpochNanoTo() ->  1494497183142514872
```


[grafana-dashboard](https://grafana.com/grafana/dashboards/928/reviews)

[Ex](https://www.youtube.com/watch?v=FrqeG-IajWM)

[auto dashboards](https://www.youtube.com/watch?v=BkdKLZNHjgY)

