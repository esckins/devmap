vsphere -->> search hosts --->> snapshots --> take snapshots

return to back -->> revert & power on server 



#####

```
su

cd /usr/lib/systemd/system/
cp grafana-server.service grafana-server.service.bckp
vi grafana-server.service
```

```
P.S.: For some weird reason, I also had to change permissions on the grafana.ini since the syslog ("vi /var/log/syslog") showed:
lvl=crit msg="failed to parse \"/etc/grafana/grafana.ini\": open /etc/grafana/grafana.ini: permission denied"
So I ran:
chmod g+rw /etc/grafana/grafana.ini
```


access grafana from a browser

to 443 so users don’t need to type in the port in the URL

`setcap 'cap_net_bind_service=+ep' /usr/sbin/grafana-server`


chmod g+rw /var/log/grafana/grafana.log



cd /usr/lib/systemd/system/

[Unit]
Description=Grafana instance
Documentation=http://docs.grafana.org
Wants=network-online.target
After=network-online.target
After=postgresql.service mariadb.service mysqld.service

[Service]
EnvironmentFile=/etc/sysconfig/grafana-server
User=grafana
Group=grafana
Type=notify
Restart=on-failure
WorkingDirectory=/usr/share/grafana
RuntimeDirectory=grafana
RuntimeDirectoryMode=0750
ExecStart=/usr/sbin/grafana-server                                                  \
                            --config=${CONF_FILE}                                   \
                            --pidfile=${PID_FILE_DIR}/grafana-server.pid            \
                            --packaging=rpm                                         \
                            cfg:default.paths.logs=${LOG_DIR}                       \
                            cfg:default.paths.data=${DATA_DIR}                      \
                            cfg:default.paths.plugins=${PLUGINS_DIR}                \
                            cfg:default.paths.provisioning=${PROVISIONING_CFG_DIR}

LimitNOFILE=10000
TimeoutStopSec=20

[Install]
WantedBy=multi-user.target

systemctl status grafana-server.service -l



systemctl daemon-reload
