```

---
# rpm -Uvh grafana-enterprise-8.4.3-1.x86_64.rpm
warning: grafana-enterprise-8.4.3-1.x86_64.rpm: Header V4 RSA/SHA256 Signature, key ID 24098cb6: NOKEY
Preparing...                          ################################# [100%]
        package grafana-enterprise-8.4.3-1.x86_64 is already installed
---

cd /var/lib/grafana/plugins/

unzip grafana-clock-panel-1.3.0.any.zip


rm -rf grafana-clock-panel-1.3.0.any.zip

systemctl restart grafana-server.service


---
Image Render ınstall but dependency packet must be installed.

I manually installed the plugin by hand:
grafana@dashboards-01-inf-ops.c.gitlab-ops.internal:/etc/grafana$ grafana-cli plugins install grafana-image-renderer
because there was no existing capability in the chef recipe to add plugins and I didn't have the immediate time to go down that path.
We should strongly consider automating the installation of that plugin (and any others we need) so we don't lose them again after future upgrades.


Also had to install some additional packages, found from ldd /var/lib/grafana/plugins/grafana-image-renderer/chrome-linux/chrome after graphs still didn't render:
libx11-xcb1, libxcomposite1, libxcursor1, libxdamage1, libgtk-3-0 (highly likely some dependency chain includes the prior ones), libnss3, libxss1, libasound2


```

[plugins](https://grafana.com/grafana/plugins/?pg=hp&plcmt=lt-box-plugins)

https://developpaper.com/install-x-artifact-to-make-your-grafana-kanban-cool/
