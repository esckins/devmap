1-Install new instance of Grafana
2-Install used plugin on new server
3-Stop Grafana service on source and destination server
4-Copy /var/lib/grafana/grafana.db from old to new server
5-Check /etc/grafana/grafana.ini
6-Restart Grafana
7-Regular connection to the grafana url
8-Dashboard, datasource, users, psw, team,… are the same
