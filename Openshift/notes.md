```
https://github.com/OpenShiftDemos/openshift-ops-workshops/tree/ocp4-prod/support
http://ocp-ocs-admins-labguides.6923.rh-us-east-1.openshiftapps.com/workshop/environment
```


####################################################################################################################



```
watch "oc get machinesets -n openshift-machine-api | egrep 'NAME|workerocs'"


oc get machines -n openshift-machine-api | egrep 'NAME|workerocs'


oc get machinesets -n openshift-machine-api | grep -v infra


oc get nodes -l node-role.kubernetes.io/worker -l '!node-role.kubernetes.io/infra','!node-role.kubernetes
.io/master'



oc create namespace openshift-storage
oc label namespace openshift-storage "openshift.io/cluster-monitoring=true"
```




####################################################################################################################


```
kind: StatefulSet
apiVersion: apps/v1
metadata:
  annotations:
    prometheus-operator-input-hash: '13857664929274331522'
  resourceVersion: '22513'
  name: prometheus-k8s
  uid: e87b0d84-9f01-4b94-9d71-ba927a172a03
  creationTimestamp: '2023-01-20T00:26:57Z'
  generation: 1
  managedFields:
    - manager: operator
      operation: Update
      apiVersion: apps/v1
      time: '2023-01-20T00:26:57Z'
      fieldsType: FieldsV1
      fieldsV1:
        'f:metadata':
          'f:annotations':
            .: {}
            'f:prometheus-operator-input-hash': {}
          'f:labels':
            .: {}
            'f:app.kubernetes.io/component': {}
            'f:app.kubernetes.io/name': {}
            'f:app.kubernetes.io/part-of': {}
            'f:app.kubernetes.io/version': {}
            'f:operator.prometheus.io/name': {}
            'f:operator.prometheus.io/shard': {}
            'f:prometheus': {}
          'f:ownerReferences':
            .: {}
            'k:{"uid":"bfeacecb-1c46-40c5-88e5-d47f3c23ffa3"}': {}
        'f:spec':
          'f:podManagementPolicy': {}
          'f:replicas': {}
          'f:revisionHistoryLimit': {}
          'f:selector': {}
          'f:serviceName': {}
          'f:template':
            'f:metadata':
              'f:annotations':
                .: {}
                'f:kubectl.kubernetes.io/default-container': {}
                'f:target.workload.openshift.io/management': {}
              'f:labels':
                'f:operator.prometheus.io/shard': {}
                'f:operator.prometheus.io/name': {}
                'f:app.kubernetes.io/managed-by': {}
                'f:app': {}
                'f:app.kubernetes.io/name': {}
                .: {}
                'f:app.kubernetes.io/part-of': {}
                'f:app.kubernetes.io/version': {}
                'f:app.kubernetes.io/instance': {}
                'f:app.kubernetes.io/component': {}
                'f:prometheus': {}
            'f:spec':
              'f:volumes':
                'k:{"name":"secret-prometheus-k8s-tls"}':
                  .: {}
                  'f:name': {}
                  'f:secret':
                    .: {}
                    'f:defaultMode': {}
                    'f:secretName': {}
                'k:{"name":"config"}':
                  .: {}
                  'f:name': {}
                  'f:secret':
                    .: {}
                    'f:defaultMode': {}
                    'f:secretName': {}
                'k:{"name":"secret-metrics-client-certs"}':
                  .: {}
                  'f:name': {}
                  'f:secret':
                    .: {}
                    'f:defaultMode': {}
                    'f:secretName': {}
                'k:{"name":"config-out"}':
                  .: {}
                  'f:emptyDir': {}
                  'f:name': {}
                'k:{"name":"metrics-client-ca"}':
                  .: {}
                  'f:configMap':
                    .: {}
                    'f:defaultMode': {}
                    'f:name': {}
                  'f:name': {}
                .: {}
                'k:{"name":"configmap-kubelet-serving-ca-bundle"}':
                  .: {}
                  'f:configMap':
                    .: {}
                    'f:defaultMode': {}
                    'f:name': {}
                  'f:name': {}
                'k:{"name":"configmap-serving-certs-ca-bundle"}':
                  .: {}
                  'f:configMap':
                    .: {}
                    'f:defaultMode': {}
                    'f:name': {}
                  'f:name': {}
                'k:{"name":"secret-prometheus-k8s-proxy"}':
                  .: {}
                  'f:name': {}
                  'f:secret':
                    .: {}
                    'f:defaultMode': {}
                    'f:secretName': {}
                'k:{"name":"secret-prometheus-k8s-thanos-sidecar-tls"}':
                  .: {}
                  'f:name': {}
                  'f:secret':
                    .: {}
                    'f:defaultMode': {}
                    'f:secretName': {}
                'k:{"name":"prometheus-k8s-rulefiles-0"}':
                  .: {}
                  'f:configMap':
                    .: {}
                    'f:defaultMode': {}
                    'f:name': {}
                  'f:name': {}
                'k:{"name":"tls-assets"}':
                  .: {}
                  'f:name': {}
                  'f:secret':
                    .: {}
                    'f:defaultMode': {}
                    'f:secretName': {}
                'k:{"name":"secret-kube-etcd-client-certs"}':
                  .: {}
                  'f:name': {}
                  'f:secret':
                    .: {}
                    'f:defaultMode': {}
                    'f:secretName': {}
                'k:{"name":"prometheus-k8s-db"}':
                  .: {}
                  'f:emptyDir': {}
                  'f:name': {}
                'k:{"name":"secret-kube-rbac-proxy"}':
                  .: {}
                  'f:name': {}
                  'f:secret':
                    .: {}
                    'f:defaultMode': {}
                    'f:secretName': {}
                'k:{"name":"secret-prometheus-k8s-htpasswd"}':
                  .: {}
                  'f:name': {}
                  'f:secret':
                    .: {}
                    'f:defaultMode': {}
                    'f:secretName': {}
                'k:{"name":"secret-grpc-tls"}':
                  .: {}
                  'f:name': {}
                  'f:secret':
                    .: {}
                    'f:defaultMode': {}
                    'f:secretName': {}
                'k:{"name":"prometheus-trusted-ca-bundle"}':
                  .: {}
                  'f:configMap':
                    .: {}
                    'f:defaultMode': {}
                    'f:items': {}
                    'f:name': {}
                    'f:optional': {}
                  'f:name': {}
                'k:{"name":"web-config"}':
                  .: {}
                  'f:name': {}
                  'f:secret':
                    .: {}
                    'f:defaultMode': {}
                    'f:secretName': {}
              'f:containers':
                'k:{"name":"config-reloader"}':
                  'f:image': {}
                  'f:volumeMounts':
                    .: {}
                    'k:{"mountPath":"/etc/prometheus/config"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                    'k:{"mountPath":"/etc/prometheus/config_out"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                    'k:{"mountPath":"/etc/prometheus/rules/prometheus-k8s-rulefiles-0"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                  'f:terminationMessagePolicy': {}
                  .: {}
                  'f:resources':
                    .: {}
                    'f:requests':
                      .: {}
                      'f:cpu': {}
                      'f:memory': {}
                  'f:args': {}
                  'f:command': {}
                  'f:env':
                    .: {}
                    'k:{"name":"POD_NAME"}':
                      .: {}
                      'f:name': {}
                      'f:valueFrom':
                        .: {}
                        'f:fieldRef': {}
                    'k:{"name":"SHARD"}':
                      .: {}
                      'f:name': {}
                      'f:value': {}
                  'f:terminationMessagePath': {}
                  'f:imagePullPolicy': {}
                  'f:name': {}
                'k:{"name":"kube-rbac-proxy"}':
                  'f:image': {}
                  'f:volumeMounts':
                    .: {}
                    'k:{"mountPath":"/etc/kube-rbac-proxy"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                    'k:{"mountPath":"/etc/tls/private"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                  'f:terminationMessagePolicy': {}
                  .: {}
                  'f:resources':
                    .: {}
                    'f:requests':
                      .: {}
                      'f:cpu': {}
                      'f:memory': {}
                  'f:args': {}
                  'f:terminationMessagePath': {}
                  'f:imagePullPolicy': {}
                  'f:ports':
                    .: {}
                    'k:{"containerPort":9092,"protocol":"TCP"}':
                      .: {}
                      'f:containerPort': {}
                      'f:name': {}
                      'f:protocol': {}
                  'f:name': {}
                'k:{"name":"kube-rbac-proxy-thanos"}':
                  'f:image': {}
                  'f:volumeMounts':
                    .: {}
                    'k:{"mountPath":"/etc/tls/client"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/etc/tls/private"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                  'f:terminationMessagePolicy': {}
                  .: {}
                  'f:resources':
                    .: {}
                    'f:requests':
                      .: {}
                      'f:cpu': {}
                      'f:memory': {}
                  'f:args': {}
                  'f:env':
                    .: {}
                    'k:{"name":"POD_IP"}':
                      .: {}
                      'f:name': {}
                      'f:valueFrom':
                        .: {}
                        'f:fieldRef': {}
                  'f:terminationMessagePath': {}
                  'f:imagePullPolicy': {}
                  'f:ports':
                    .: {}
                    'k:{"containerPort":10902,"protocol":"TCP"}':
                      .: {}
                      'f:containerPort': {}
                      'f:name': {}
                      'f:protocol': {}
                  'f:name': {}
                'k:{"name":"prom-label-proxy"}':
                  .: {}
                  'f:args': {}
                  'f:image': {}
                  'f:imagePullPolicy': {}
                  'f:name': {}
                  'f:resources':
                    .: {}
                    'f:requests':
                      .: {}
                      'f:cpu': {}
                      'f:memory': {}
                  'f:terminationMessagePath': {}
                  'f:terminationMessagePolicy': {}
                'k:{"name":"prometheus"}':
                  'f:image': {}
                  'f:volumeMounts':
                    'k:{"mountPath":"/etc/prometheus/rules/prometheus-k8s-rulefiles-0"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                    'k:{"mountPath":"/etc/prometheus/secrets/kube-rbac-proxy"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/etc/prometheus/secrets/prometheus-k8s-proxy"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/etc/prometheus/config_out"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/etc/prometheus/certs"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                    .: {}
                    'k:{"mountPath":"/etc/prometheus/secrets/kube-etcd-client-certs"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/etc/prometheus/web_config/web-config.yaml"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                      'f:subPath': {}
                    'k:{"mountPath":"/prometheus"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                    'k:{"mountPath":"/etc/prometheus/configmaps/kubelet-serving-ca-bundle"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/etc/prometheus/configmaps/serving-certs-ca-bundle"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/etc/prometheus/secrets/prometheus-k8s-tls"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/etc/prometheus/secrets/metrics-client-certs"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/etc/pki/ca-trust/extracted/pem/"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/etc/prometheus/secrets/prometheus-k8s-thanos-sidecar-tls"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                  'f:terminationMessagePolicy': {}
                  .: {}
                  'f:resources':
                    .: {}
                    'f:requests':
                      .: {}
                      'f:cpu': {}
                      'f:memory': {}
                  'f:args': {}
                  'f:readinessProbe':
                    .: {}
                    'f:exec':
                      .: {}
                      'f:command': {}
                    'f:failureThreshold': {}
                    'f:periodSeconds': {}
                    'f:successThreshold': {}
                    'f:timeoutSeconds': {}
                  'f:terminationMessagePath': {}
                  'f:imagePullPolicy': {}
                  'f:name': {}
                'k:{"name":"prometheus-proxy"}':
                  'f:image': {}
                  'f:volumeMounts':
                    .: {}
                    'k:{"mountPath":"/etc/pki/ca-trust/extracted/pem/"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/etc/proxy/htpasswd"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                    'k:{"mountPath":"/etc/proxy/secrets"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                    'k:{"mountPath":"/etc/tls/private"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                  'f:terminationMessagePolicy': {}
                  .: {}
                  'f:resources':
                    .: {}
                    'f:requests':
                      .: {}
                      'f:cpu': {}
                      'f:memory': {}
                  'f:args': {}
                  'f:env':
                    .: {}
                    'k:{"name":"HTTPS_PROXY"}':
                      .: {}
                      'f:name': {}
                    'k:{"name":"HTTP_PROXY"}':
                      .: {}
                      'f:name': {}
                    'k:{"name":"NO_PROXY"}':
                      .: {}
                      'f:name': {}
                  'f:terminationMessagePath': {}
                  'f:imagePullPolicy': {}
                  'f:ports':
                    .: {}
                    'k:{"containerPort":9091,"protocol":"TCP"}':
                      .: {}
                      'f:containerPort': {}
                      'f:name': {}
                      'f:protocol': {}
                  'f:name': {}
                'k:{"name":"thanos-sidecar"}':
                  'f:image': {}
                  'f:volumeMounts':
                    .: {}
                    'k:{"mountPath":"/etc/tls/grpc"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                  'f:terminationMessagePolicy': {}
                  .: {}
                  'f:resources':
                    .: {}
                    'f:requests':
                      .: {}
                      'f:cpu': {}
                      'f:memory': {}
                  'f:args': {}
                  'f:env':
                    .: {}
                    'k:{"name":"POD_IP"}':
                      .: {}
                      'f:name': {}
                      'f:valueFrom':
                        .: {}
                        'f:fieldRef': {}
                  'f:terminationMessagePath': {}
                  'f:imagePullPolicy': {}
                  'f:ports':
                    .: {}
                    'k:{"containerPort":10901,"protocol":"TCP"}':
                      .: {}
                      'f:containerPort': {}
                      'f:name': {}
                      'f:protocol': {}
                    'k:{"containerPort":10902,"protocol":"TCP"}':
                      .: {}
                      'f:containerPort': {}
                      'f:name': {}
                      'f:protocol': {}
                  'f:name': {}
              'f:dnsPolicy': {}
              'f:priorityClassName': {}
              'f:serviceAccount': {}
              'f:restartPolicy': {}
              'f:schedulerName': {}
              'f:nodeSelector': {}
              'f:terminationGracePeriodSeconds': {}
              'f:initContainers':
                .: {}
                'k:{"name":"init-config-reloader"}':
                  'f:image': {}
                  'f:volumeMounts':
                    .: {}
                    'k:{"mountPath":"/etc/prometheus/config"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                    'k:{"mountPath":"/etc/prometheus/config_out"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                    'k:{"mountPath":"/etc/prometheus/rules/prometheus-k8s-rulefiles-0"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                  'f:terminationMessagePolicy': {}
                  .: {}
                  'f:resources':
                    .: {}
                    'f:requests':
                      .: {}
                      'f:cpu': {}
                      'f:memory': {}
                  'f:args': {}
                  'f:command': {}
                  'f:env':
                    .: {}
                    'k:{"name":"POD_NAME"}':
                      .: {}
                      'f:name': {}
                      'f:valueFrom':
                        .: {}
                        'f:fieldRef': {}
                    'k:{"name":"SHARD"}':
                      .: {}
                      'f:name': {}
                      'f:value': {}
                  'f:terminationMessagePath': {}
                  'f:imagePullPolicy': {}
                  'f:name': {}
              'f:serviceAccountName': {}
              'f:securityContext':
                .: {}
                'f:fsGroup': {}
                'f:runAsNonRoot': {}
                'f:runAsUser': {}
              'f:affinity':
                .: {}
                'f:podAntiAffinity':
                  .: {}
                  'f:preferredDuringSchedulingIgnoredDuringExecution': {}
          'f:updateStrategy':
            'f:type': {}
    - manager: kube-controller-manager
      operation: Update
      apiVersion: apps/v1
      time: '2023-01-20T00:27:06Z'
      fieldsType: FieldsV1
      fieldsV1:
        'f:status':
          'f:currentReplicas': {}
          'f:currentRevision': {}
          'f:updatedReplicas': {}
          'f:readyReplicas': {}
          'f:replicas': {}
          'f:availableReplicas': {}
          'f:collisionCount': {}
          'f:observedGeneration': {}
          'f:updateRevision': {}
      subresource: status
  namespace: openshift-monitoring
  ownerReferences:
    - apiVersion: monitoring.coreos.com/v1
      kind: Prometheus
      name: k8s
      uid: bfeacecb-1c46-40c5-88e5-d47f3c23ffa3
      controller: true
      blockOwnerDeletion: true
  labels:
    app.kubernetes.io/component: prometheus
    app.kubernetes.io/name: prometheus
    app.kubernetes.io/part-of: openshift-monitoring
    app.kubernetes.io/version: 2.29.2
    operator.prometheus.io/name: k8s
    operator.prometheus.io/shard: '0'
    prometheus: k8s
spec:
  replicas: 2
  selector:
    matchLabels:
      app: prometheus
      app.kubernetes.io/instance: k8s
      app.kubernetes.io/managed-by: prometheus-operator
      app.kubernetes.io/name: prometheus
      operator.prometheus.io/name: k8s
      operator.prometheus.io/shard: '0'
      prometheus: k8s
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: prometheus
        app.kubernetes.io/part-of: openshift-monitoring
        app.kubernetes.io/instance: k8s
        app.kubernetes.io/version: 2.29.2
        prometheus: k8s
        app.kubernetes.io/component: prometheus
        operator.prometheus.io/shard: '0'
        app.kubernetes.io/managed-by: prometheus-operator
        app.kubernetes.io/name: prometheus
        operator.prometheus.io/name: k8s
      annotations:
        kubectl.kubernetes.io/default-container: prometheus
        target.workload.openshift.io/management: '{"effect": "PreferredDuringScheduling"}'
    spec:
      nodeSelector:
        kubernetes.io/os: linux
      restartPolicy: Always
      initContainers:
        - resources:
            requests:
              cpu: 100m
              memory: 50Mi
          terminationMessagePath: /dev/termination-log
          name: init-config-reloader
          command:
            - /bin/prometheus-config-reloader
          env:
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: metadata.name
            - name: SHARD
              value: '0'
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: config
              mountPath: /etc/prometheus/config
            - name: config-out
              mountPath: /etc/prometheus/config_out
            - name: prometheus-k8s-rulefiles-0
              mountPath: /etc/prometheus/rules/prometheus-k8s-rulefiles-0
          terminationMessagePolicy: FallbackToLogsOnError
          image: >-
            quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:fefe5b4b9f77d57043b24cb3d9113b39926542cf64c122adbaa2316073dd8218
          args:
            - '--watch-interval=0'
            - '--listen-address=:8080'
            - '--config-file=/etc/prometheus/config/prometheus.yaml.gz'
            - >-
              --config-envsubst-file=/etc/prometheus/config_out/prometheus.env.yaml
            - '--watched-dir=/etc/prometheus/rules/prometheus-k8s-rulefiles-0'
      serviceAccountName: prometheus-k8s
      schedulerName: default-scheduler
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 100
              podAffinityTerm:
                labelSelector:
                  matchLabels:
                    app.kubernetes.io/component: prometheus
                    app.kubernetes.io/name: prometheus
                    app.kubernetes.io/part-of: openshift-monitoring
                    prometheus: k8s
                namespaces:
                  - openshift-monitoring
                topologyKey: kubernetes.io/hostname
      terminationGracePeriodSeconds: 600
      securityContext:
        runAsUser: 65534
        runAsNonRoot: true
        fsGroup: 65534
      containers:
        - resources:
            requests:
              cpu: 70m
              memory: 1Gi
          readinessProbe:
            exec:
              command:
                - sh
                - '-c'
                - >-
                  if [ -x "$(command -v curl)" ]; then exec curl
                  http://localhost:9090/-/ready; elif [ -x "$(command -v wget)"
                  ]; then exec wget -q -O /dev/null
                  http://localhost:9090/-/ready; else exit 1; fi
            timeoutSeconds: 3
            periodSeconds: 5
            successThreshold: 1
            failureThreshold: 120
          terminationMessagePath: /dev/termination-log
          name: prometheus
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: prometheus-trusted-ca-bundle
              readOnly: true
              mountPath: /etc/pki/ca-trust/extracted/pem/
            - name: config-out
              readOnly: true
              mountPath: /etc/prometheus/config_out
            - name: tls-assets
              readOnly: true
              mountPath: /etc/prometheus/certs
            - name: prometheus-k8s-db
              mountPath: /prometheus
            - name: prometheus-k8s-rulefiles-0
              mountPath: /etc/prometheus/rules/prometheus-k8s-rulefiles-0
            - name: web-config
              readOnly: true
              mountPath: /etc/prometheus/web_config/web-config.yaml
              subPath: web-config.yaml
            - name: secret-kube-etcd-client-certs
              readOnly: true
              mountPath: /etc/prometheus/secrets/kube-etcd-client-certs
            - name: secret-prometheus-k8s-tls
              readOnly: true
              mountPath: /etc/prometheus/secrets/prometheus-k8s-tls
            - name: secret-prometheus-k8s-proxy
              readOnly: true
              mountPath: /etc/prometheus/secrets/prometheus-k8s-proxy
            - name: secret-prometheus-k8s-thanos-sidecar-tls
              readOnly: true
              mountPath: /etc/prometheus/secrets/prometheus-k8s-thanos-sidecar-tls
            - name: secret-kube-rbac-proxy
              readOnly: true
              mountPath: /etc/prometheus/secrets/kube-rbac-proxy
            - name: secret-metrics-client-certs
              readOnly: true
              mountPath: /etc/prometheus/secrets/metrics-client-certs
            - name: configmap-serving-certs-ca-bundle
              readOnly: true
              mountPath: /etc/prometheus/configmaps/serving-certs-ca-bundle
            - name: configmap-kubelet-serving-ca-bundle
              readOnly: true
              mountPath: /etc/prometheus/configmaps/kubelet-serving-ca-bundle
          terminationMessagePolicy: FallbackToLogsOnError
          image: >-
            quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:bd8457a514f36dc65e31fc50eceaa101eb70464f4d2720760cc26351fe102294
          args:
            - '--web.console.templates=/etc/prometheus/consoles'
            - '--web.console.libraries=/etc/prometheus/console_libraries'
            - '--config.file=/etc/prometheus/config_out/prometheus.env.yaml'
            - '--storage.tsdb.path=/prometheus'
            - '--storage.tsdb.retention.time=15d'
            - '--web.enable-lifecycle'
            - >-
              --web.external-url=https://prometheus-k8s-openshift-monitoring.apps.cluster-qds87.qds87.sandbox2488.opentlc.com/
            - '--web.route-prefix=/'
            - '--web.listen-address=127.0.0.1:9090'
            - '--web.config.file=/etc/prometheus/web_config/web-config.yaml'
        - resources:
            requests:
              cpu: 1m
              memory: 10Mi
          terminationMessagePath: /dev/termination-log
          name: config-reloader
          command:
            - /bin/prometheus-config-reloader
          env:
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: metadata.name
            - name: SHARD
              value: '0'
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: config
              mountPath: /etc/prometheus/config
            - name: config-out
              mountPath: /etc/prometheus/config_out
            - name: prometheus-k8s-rulefiles-0
              mountPath: /etc/prometheus/rules/prometheus-k8s-rulefiles-0
          terminationMessagePolicy: FallbackToLogsOnError
          image: >-
            quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:fefe5b4b9f77d57043b24cb3d9113b39926542cf64c122adbaa2316073dd8218
          args:
            - '--listen-address=localhost:8080'
            - '--reload-url=http://localhost:9090/-/reload'
            - '--config-file=/etc/prometheus/config/prometheus.yaml.gz'
            - >-
              --config-envsubst-file=/etc/prometheus/config_out/prometheus.env.yaml
            - '--watched-dir=/etc/prometheus/rules/prometheus-k8s-rulefiles-0'
        - resources:
            requests:
              cpu: 1m
              memory: 25Mi
          terminationMessagePath: /dev/termination-log
          name: thanos-sidecar
          env:
            - name: POD_IP
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: status.podIP
          ports:
            - name: http
              containerPort: 10902
              protocol: TCP
            - name: grpc
              containerPort: 10901
              protocol: TCP
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: secret-grpc-tls
              mountPath: /etc/tls/grpc
          terminationMessagePolicy: FallbackToLogsOnError
          image: >-
            quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:8268b6cdf5c4f054f77c075e7ce864812e7069ecb4706657ece35e0e1040e452
          args:
            - sidecar
            - '--prometheus.url=http://localhost:9090/'
            - '--tsdb.path=/prometheus'
            - '--grpc-address=[$(POD_IP)]:10901'
            - '--http-address=127.0.0.1:10902'
            - '--grpc-server-tls-cert=/etc/tls/grpc/server.crt'
            - '--grpc-server-tls-key=/etc/tls/grpc/server.key'
            - '--grpc-server-tls-client-ca=/etc/tls/grpc/ca.crt'
        - resources:
            requests:
              cpu: 1m
              memory: 20Mi
          terminationMessagePath: /dev/termination-log
          name: prometheus-proxy
          env:
            - name: HTTP_PROXY
            - name: HTTPS_PROXY
            - name: NO_PROXY
          ports:
            - name: web
              containerPort: 9091
              protocol: TCP
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: secret-prometheus-k8s-tls
              mountPath: /etc/tls/private
            - name: secret-prometheus-k8s-proxy
              mountPath: /etc/proxy/secrets
            - name: secret-prometheus-k8s-htpasswd
              mountPath: /etc/proxy/htpasswd
            - name: prometheus-trusted-ca-bundle
              readOnly: true
              mountPath: /etc/pki/ca-trust/extracted/pem/
          terminationMessagePolicy: FallbackToLogsOnError
          image: >-
            quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:ca501a8b8967d45fcb3759d802ec4c99be8c2f5038b7503f9affed6d8ab56d65
          args:
            - '-provider=openshift'
            - '-https-address=:9091'
            - '-http-address='
            - '-email-domain=*'
            - '-upstream=http://localhost:9090'
            - '-openshift-service-account=prometheus-k8s'
            - '-openshift-sar={"resource": "namespaces", "verb": "get"}'
            - >-
              -openshift-delegate-urls={"/": {"resource": "namespaces", "verb":
              "get"}}
            - '-tls-cert=/etc/tls/private/tls.crt'
            - '-tls-key=/etc/tls/private/tls.key'
            - >-
              -client-secret-file=/var/run/secrets/kubernetes.io/serviceaccount/token
            - '-cookie-secret-file=/etc/proxy/secrets/session_secret'
            - '-openshift-ca=/etc/pki/tls/cert.pem'
            - '-openshift-ca=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt'
            - '-htpasswd-file=/etc/proxy/htpasswd/auth'
        - resources:
            requests:
              cpu: 1m
              memory: 15Mi
          terminationMessagePath: /dev/termination-log
          name: kube-rbac-proxy
          ports:
            - name: tenancy
              containerPort: 9092
              protocol: TCP
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: secret-prometheus-k8s-tls
              mountPath: /etc/tls/private
            - name: secret-kube-rbac-proxy
              mountPath: /etc/kube-rbac-proxy
          terminationMessagePolicy: FallbackToLogsOnError
          image: >-
            quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:0c304c11986790f446604319dfc29db494b15f5f9fa13f2c436688ec29c92dbe
          args:
            - '--secure-listen-address=0.0.0.0:9092'
            - '--upstream=http://127.0.0.1:9095'
            - '--config-file=/etc/kube-rbac-proxy/config.yaml'
            - '--tls-cert-file=/etc/tls/private/tls.crt'
            - '--tls-private-key-file=/etc/tls/private/tls.key'
            - >-
              --tls-cipher-suites=TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305
            - '--logtostderr=true'
            - '--v=10'
        - name: prom-label-proxy
          image: >-
            quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:d77f233e2a0c891f971926fab9fac8c8fee1ce90d8e26286d41f3baeb1b07126
          args:
            - '--insecure-listen-address=127.0.0.1:9095'
            - '--upstream=http://127.0.0.1:9090'
            - '--label=namespace'
          resources:
            requests:
              cpu: 1m
              memory: 15Mi
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: FallbackToLogsOnError
          imagePullPolicy: IfNotPresent
        - resources:
            requests:
              cpu: 1m
              memory: 10Mi
          terminationMessagePath: /dev/termination-log
          name: kube-rbac-proxy-thanos
          env:
            - name: POD_IP
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: status.podIP
          ports:
            - name: thanos-proxy
              containerPort: 10902
              protocol: TCP
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: secret-prometheus-k8s-thanos-sidecar-tls
              mountPath: /etc/tls/private
            - name: metrics-client-ca
              readOnly: true
              mountPath: /etc/tls/client
          terminationMessagePolicy: FallbackToLogsOnError
          image: >-
            quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:0c304c11986790f446604319dfc29db494b15f5f9fa13f2c436688ec29c92dbe
          args:
            - '--secure-listen-address=[$(POD_IP)]:10902'
            - '--upstream=http://127.0.0.1:10902'
            - '--tls-cert-file=/etc/tls/private/tls.crt'
            - '--tls-private-key-file=/etc/tls/private/tls.key'
            - >-
              --tls-cipher-suites=TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305
            - '--allow-paths=/metrics'
            - '--logtostderr=true'
            - '--client-ca-file=/etc/tls/client/client-ca.crt'
      serviceAccount: prometheus-k8s
      volumes:
        - name: config
          secret:
            secretName: prometheus-k8s
            defaultMode: 420
        - name: tls-assets
          secret:
            secretName: prometheus-k8s-tls-assets
            defaultMode: 420
        - name: config-out
          emptyDir: {}
        - name: prometheus-k8s-rulefiles-0
          configMap:
            name: prometheus-k8s-rulefiles-0
            defaultMode: 420
        - name: web-config
          secret:
            secretName: prometheus-k8s-web-config
            defaultMode: 420
        - name: secret-kube-etcd-client-certs
          secret:
            secretName: kube-etcd-client-certs
            defaultMode: 420
        - name: secret-prometheus-k8s-tls
          secret:
            secretName: prometheus-k8s-tls
            defaultMode: 420
        - name: secret-prometheus-k8s-proxy
          secret:
            secretName: prometheus-k8s-proxy
            defaultMode: 420
        - name: secret-prometheus-k8s-thanos-sidecar-tls
          secret:
            secretName: prometheus-k8s-thanos-sidecar-tls
            defaultMode: 420
        - name: secret-kube-rbac-proxy
          secret:
            secretName: kube-rbac-proxy
            defaultMode: 420
        - name: secret-metrics-client-certs
          secret:
            secretName: metrics-client-certs
            defaultMode: 420
        - name: configmap-serving-certs-ca-bundle
          configMap:
            name: serving-certs-ca-bundle
            defaultMode: 420
        - name: configmap-kubelet-serving-ca-bundle
          configMap:
            name: kubelet-serving-ca-bundle
            defaultMode: 420
        - name: prometheus-k8s-db
          emptyDir: {}
        - name: secret-prometheus-k8s-htpasswd
          secret:
            secretName: prometheus-k8s-htpasswd
            defaultMode: 420
        - name: metrics-client-ca
          configMap:
            name: metrics-client-ca
            defaultMode: 420
        - name: secret-grpc-tls
          secret:
            secretName: prometheus-k8s-grpc-tls-d56r7lb05466j
            defaultMode: 420
        - name: prometheus-trusted-ca-bundle
          configMap:
            name: prometheus-trusted-ca-bundle-2rsonso43rc5p
            items:
              - key: ca-bundle.crt
                path: tls-ca-bundle.pem
            defaultMode: 420
            optional: true
      dnsPolicy: ClusterFirst
      priorityClassName: system-cluster-critical
  serviceName: prometheus-operated
  podManagementPolicy: Parallel
  updateStrategy:
    type: RollingUpdate
  revisionHistoryLimit: 10
status:
  observedGeneration: 1
  availableReplicas: 2
  updateRevision: prometheus-k8s-6ff9d9d66f
  currentRevision: prometheus-k8s-6ff9d9d66f
  currentReplicas: 2
  updatedReplicas: 2
  replicas: 2
  collisionCount: 0
  readyReplicas: 2


```

####################################################################################################################




























####################################################################################################################

```
kind: ConfigMap
apiVersion: v1
metadata:
  name: prometheus-k8s-rulefiles-0
  namespace: openshift-monitoring
  uid: 440e1103-3d61-40a4-a396-ccd2534a4e95
  resourceVersion: '22554'
  creationTimestamp: '2023-01-20T00:27:11Z'
  labels:
    managed-by: prometheus-operator
    prometheus-name: k8s
  ownerReferences:
    - apiVersion: monitoring.coreos.com/v1
      kind: Prometheus
      name: k8s
      uid: bfeacecb-1c46-40c5-88e5-d47f3c23ffa3
      controller: true
      blockOwnerDeletion: true
  managedFields:
    - manager: operator
      operation: Update
      apiVersion: v1
      time: '2023-01-20T00:27:11Z'
      fieldsType: FieldsV1
      fieldsV1:
        'f:data':
          'f:openshift-monitoring-prometheus-operator-rules.yaml': {}
          'f:openshift-cluster-samples-operator-samples-operator-alerts.yaml': {}
          'f:openshift-kube-apiserver-operator-kube-apiserver-operator.yaml': {}
          'f:openshift-cluster-node-tuning-operator-node-tuning-operator.yaml': {}
          'f:openshift-cluster-machine-approver-machineapprover-rules.yaml': {}
          'f:openshift-monitoring-kube-state-metrics-rules.yaml': {}
          'f:openshift-monitoring-prometheus-k8s-thanos-sidecar-rules.yaml': {}
          'f:openshift-dns-operator-dns.yaml': {}
          'f:openshift-kube-apiserver-kube-apiserver-requests.yaml': {}
          .: {}
          'f:openshift-monitoring-cluster-monitoring-operator-prometheus-rules.yaml': {}
          'f:openshift-kube-apiserver-kube-apiserver-slos-basic.yaml': {}
          'f:openshift-kube-apiserver-api-usage.yaml': {}
          'f:openshift-operator-lifecycle-manager-olm-alert-rules.yaml': {}
          'f:openshift-kube-apiserver-audit-errors.yaml': {}
          'f:openshift-kube-controller-manager-operator-kube-controller-manager-operator.yaml': {}
          'f:openshift-ovn-kubernetes-master-rules.yaml': {}
          'f:openshift-ingress-operator-ingress-operator.yaml': {}
          'f:openshift-multus-prometheus-k8s-rules.yaml': {}
          'f:openshift-monitoring-alertmanager-main-rules.yaml': {}
          'f:openshift-machine-api-machine-api-operator-prometheus-rules.yaml': {}
          'f:openshift-monitoring-prometheus-k8s-prometheus-rules.yaml': {}
          'f:openshift-kube-apiserver-kube-apiserver-slos-extended.yaml': {}
          'f:openshift-monitoring-telemetry.yaml': {}
          'f:openshift-cluster-version-cluster-version-operator.yaml': {}
          'f:openshift-monitoring-kubernetes-monitoring-rules.yaml': {}
          'f:openshift-machine-config-operator-machine-config-daemon.yaml': {}
          'f:openshift-etcd-operator-etcd-prometheus-rules.yaml': {}
          'f:openshift-monitoring-node-exporter-rules.yaml': {}
          'f:openshift-monitoring-thanos-querier.yaml': {}
          'f:openshift-marketplace-marketplace-alert-rules.yaml': {}
          'f:openshift-image-registry-image-registry-operator-alerts.yaml': {}
          'f:openshift-cloud-credential-operator-cloud-credential-operator-alerts.yaml': {}
          'f:openshift-ovn-kubernetes-networking-rules.yaml': {}
          'f:openshift-kube-apiserver-cpu-utilization.yaml': {}
          'f:openshift-kube-scheduler-operator-kube-scheduler-operator.yaml': {}
        'f:metadata':
          'f:labels':
            .: {}
            'f:managed-by': {}
            'f:prometheus-name': {}
          'f:ownerReferences':
            .: {}
            'k:{"uid":"bfeacecb-1c46-40c5-88e5-d47f3c23ffa3"}': {}
data:
  openshift-kube-apiserver-kube-apiserver-slos-extended.yaml: |
    groups:
    - name: kube-apiserver-slos-extended
      rules:
      - alert: KubeAPIErrorBudgetBurn
        annotations:
          description: The API server is burning too much error budget. This alert fires
            when too many requests are failing with high latency. Use the 'API Performance'
            monitoring dashboards to narrow down the request states and latency. The 'etcd'
            monitoring dashboards also provides metrics to help determine etcd stability
            and performance.
          summary: The API server is burning too much error budget.
        expr: |
          sum(apiserver_request:burnrate1d) > (3.00 * 0.01000)
          and
          sum(apiserver_request:burnrate2h) > (3.00 * 0.01000)
        for: 1h
        labels:
          long: 1d
          namespace: openshift-kube-apiserver
          severity: warning
          short: 2h
      - alert: KubeAPIErrorBudgetBurn
        annotations:
          description: The API server is burning too much error budget. This alert fires
            when too many requests are failing with high latency. Use the 'API Performance'
            monitoring dashboards to narrow down the request states and latency. The 'etcd'
            monitoring dashboards also provides metrics to help determine etcd stability
            and performance.
          summary: The API server is burning too much error budget.
        expr: |
          sum(apiserver_request:burnrate3d) > (1.00 * 0.01000)
          and
          sum(apiserver_request:burnrate6h) > (1.00 * 0.01000)
        for: 3h
        labels:
          long: 3d
          namespace: openshift-kube-apiserver
          severity: warning
          short: 6h
    - name: kube-apiserver.rules
      rules:
      - expr: |
          # error
          label_replace(
            sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",code=~"5.."}[2h]))
          / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[2h])))
          , "type", "error", "_none_", "")
          or
          # resource-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource"}[2h]))
            -
              (sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource",le="0.1"}[2h])) or vector(0))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[2h])))
          , "type", "slow-resource", "_none_", "")
          or
          # namespace-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace"}[2h]))
            - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace",le="0.5"}[2h]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[2h])))
          , "type", "slow-namespace", "_none_", "")
          or
          # cluster-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",scope="cluster"}[2h]))
              - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",scope="cluster",le="5"}[2h]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[2h])))
          , "type", "slow-cluster", "_none_", "")
        labels:
          verb: read
        record: apiserver_request:burnrate2h
      - expr: |
          # error
          label_replace(
            sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",code=~"5.."}[1d]))
          / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[1d])))
          , "type", "error", "_none_", "")
          or
          # resource-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource"}[1d]))
            -
              (sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource",le="0.1"}[1d])) or vector(0))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[1d])))
          , "type", "slow-resource", "_none_", "")
          or
          # namespace-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace"}[1d]))
            - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace",le="0.5"}[1d]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[1d])))
          , "type", "slow-namespace", "_none_", "")
          or
          # cluster-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",scope="cluster"}[1d]))
              - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",scope="cluster",le="5"}[1d]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[1d])))
          , "type", "slow-cluster", "_none_", "")
        labels:
          verb: read
        record: apiserver_request:burnrate1d
      - expr: |
          # error
          label_replace(
            sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",code=~"5.."}[3d]))
          / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[3d])))
          , "type", "error", "_none_", "")
          or
          # resource-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource"}[3d]))
            -
              (sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource",le="0.1"}[3d])) or vector(0))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[3d])))
          , "type", "slow-resource", "_none_", "")
          or
          # namespace-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace"}[3d]))
            - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace",le="0.5"}[3d]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[3d])))
          , "type", "slow-namespace", "_none_", "")
          or
          # cluster-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",scope="cluster"}[3d]))
              - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",scope="cluster",le="5"}[3d]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[3d])))
          , "type", "slow-cluster", "_none_", "")
        labels:
          verb: read
        record: apiserver_request:burnrate3d
      - expr: |
          (
            (
              # too slow
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[1d]))
              -
              sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",le="1"}[1d]))
            )
            +
            sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",code=~"5.."}[1d]))
          )
          /
          sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[1d]))
        labels:
          verb: write
        record: apiserver_request:burnrate1d
      - expr: |
          (
            (
              # too slow
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[2h]))
              -
              sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",le="1"}[2h]))
            )
            +
            sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",code=~"5.."}[2h]))
          )
          /
          sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[2h]))
        labels:
          verb: write
        record: apiserver_request:burnrate2h
      - expr: |
          (
            (
              # too slow
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[3d]))
              -
              sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",le="1"}[3d]))
            )
            +
            sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",code=~"5.."}[3d]))
          )
          /
          sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[3d]))
        labels:
          verb: write
        record: apiserver_request:burnrate3d
  openshift-kube-apiserver-api-usage.yaml: |
    groups:
    - name: pre-release-lifecycle
      rules:
      - alert: APIRemovedInNextReleaseInUse
        annotations:
          message: Deprecated API that will be removed in the next version is being used.
            Removing the workload that is using the {{ $labels.group }}.{{ $labels.version
            }}/{{ $labels.resource }} API might be necessary for a successful upgrade
            to the next cluster version. Refer to `oc get apirequestcounts {{ $labels.resource
            }}.{{ $labels.version }}.{{ $labels.group }} -o yaml` to identify the workload.
        expr: |
          group(apiserver_requested_deprecated_apis{removed_release="1.23"}) by (group,version,resource) and (sum by(group,version,resource) (rate(apiserver_request_total{system_client!="kube-controller-manager",system_client!="cluster-policy-controller"}[4h]))) > 0
        for: 1h
        labels:
          namespace: openshift-kube-apiserver
          severity: info
      - alert: APIRemovedInNextEUSReleaseInUse
        annotations:
          message: Deprecated API that will be removed in the next EUS version is being
            used. Removing the workload that is using the {{ $labels.group }}.{{ $labels.version
            }}/{{ $labels.resource }} API might be necessary for a successful upgrade
            to the next EUS cluster version. Refer to `oc get apirequestcounts {{ $labels.resource
            }}.{{ $labels.version }}.{{ $labels.group }} -o yaml` to identify the workload.
        expr: |
          group(apiserver_requested_deprecated_apis{removed_release=~"1\\.2[123]"}) by (group,version,resource) and (sum by(group,version,resource) (rate(apiserver_request_total{system_client!="kube-controller-manager",system_client!="cluster-policy-controller"}[4h]))) > 0
        for: 1h
        labels:
          namespace: openshift-kube-apiserver
          severity: info
  openshift-kube-apiserver-kube-apiserver-slos-basic.yaml: |
    groups:
    - name: kube-apiserver-slos-basic
      rules:
      - alert: KubeAPIErrorBudgetBurn
        annotations:
          description: The API server is burning too much error budget. This alert fires
            when too many requests are failing with high latency. Use the 'API Performance'
            monitoring dashboards to narrow down the request states and latency. The 'etcd'
            monitoring dashboards also provides metrics to help determine etcd stability
            and performance.
          summary: The API server is burning too much error budget.
        expr: |
          sum(apiserver_request:burnrate1h) > (14.40 * 0.01000)
          and
          sum(apiserver_request:burnrate5m) > (14.40 * 0.01000)
        for: 2m
        labels:
          long: 1h
          namespace: openshift-kube-apiserver
          severity: critical
          short: 5m
      - alert: KubeAPIErrorBudgetBurn
        annotations:
          description: The API server is burning too much error budget. This alert fires
            when too many requests are failing with high latency. Use the 'API Performance'
            monitoring dashboards to narrow down the request states and latency. The 'etcd'
            monitoring dashboards also provides metrics to help determine etcd stability
            and performance.
          summary: The API server is burning too much error budget.
        expr: |
          sum(apiserver_request:burnrate6h) > (6.00 * 0.01000)
          and
          sum(apiserver_request:burnrate30m) > (6.00 * 0.01000)
        for: 15m
        labels:
          long: 6h
          namespace: openshift-kube-apiserver
          severity: critical
          short: 30m
    - name: kube-apiserver.rules
      rules:
      - expr: |
          # error
          label_replace(
            sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",code=~"5.."}[5m]))
          / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[5m])))
          , "type", "error", "_none_", "")
          or
          # resource-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource"}[5m]))
            -
              (sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource",le="0.1"}[5m])) or vector(0))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[5m])))
          , "type", "slow-resource", "_none_", "")
          or
          # namespace-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace"}[5m]))
            - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace",le="0.5"}[5m]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[5m])))
          , "type", "slow-namespace", "_none_", "")
          or
          # cluster-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",scope="cluster"}[5m]))
              - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",scope="cluster",le="5"}[5m]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[5m])))
          , "type", "slow-cluster", "_none_", "")
        labels:
          verb: read
        record: apiserver_request:burnrate5m
      - expr: |
          # error
          label_replace(
            sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",code=~"5.."}[30m]))
          / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[30m])))
          , "type", "error", "_none_", "")
          or
          # resource-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource"}[30m]))
            -
              (sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource",le="0.1"}[30m])) or vector(0))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[30m])))
          , "type", "slow-resource", "_none_", "")
          or
          # namespace-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace"}[30m]))
            - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace",le="0.5"}[30m]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[30m])))
          , "type", "slow-namespace", "_none_", "")
          or
          # cluster-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",scope="cluster"}[30m]))
              - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",scope="cluster",le="5"}[30m]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[30m])))
          , "type", "slow-cluster", "_none_", "")
        labels:
          verb: read
        record: apiserver_request:burnrate30m
      - expr: |
          # error
          label_replace(
            sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",code=~"5.."}[1h]))
          / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[1h])))
          , "type", "error", "_none_", "")
          or
          # resource-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource"}[1h]))
            -
              (sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource",le="0.1"}[1h])) or vector(0))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[1h])))
          , "type", "slow-resource", "_none_", "")
          or
          # namespace-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace"}[1h]))
            - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace",le="0.5"}[1h]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[1h])))
          , "type", "slow-namespace", "_none_", "")
          or
          # cluster-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",scope="cluster"}[1h]))
              - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",scope="cluster",le="5"}[1h]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[1h])))
          , "type", "slow-cluster", "_none_", "")
        labels:
          verb: read
        record: apiserver_request:burnrate1h
      - expr: |
          # error
          label_replace(
            sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",code=~"5.."}[6h]))
          / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[6h])))
          , "type", "error", "_none_", "")
          or
          # resource-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource"}[6h]))
            -
              (sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="resource",le="0.1"}[6h])) or vector(0))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[6h])))
          , "type", "slow-resource", "_none_", "")
          or
          # namespace-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace"}[6h]))
            - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec",scope="namespace",le="0.5"}[6h]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET",subresource!~"proxy|log|exec"}[6h])))
          , "type", "slow-namespace", "_none_", "")
          or
          # cluster-scoped latency
          label_replace(
            (
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"LIST|GET",scope="cluster"}[6h]))
              - sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET",scope="cluster",le="5"}[6h]))
            ) / scalar(sum(rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[6h])))
          , "type", "slow-cluster", "_none_", "")
        labels:
          verb: read
        record: apiserver_request:burnrate6h
      - expr: |
          (
            (
              # too slow
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[1h]))
              -
              sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",le="1"}[1h]))
            )
            +
            sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",code=~"5.."}[1h]))
          )
          /
          sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[1h]))
        labels:
          verb: write
        record: apiserver_request:burnrate1h
      - expr: |
          (
            (
              # too slow
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[30m]))
              -
              sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",le="1"}[30m]))
            )
            +
            sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",code=~"5.."}[30m]))
          )
          /
          sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[30m]))
        labels:
          verb: write
        record: apiserver_request:burnrate30m
      - expr: |
          (
            (
              # too slow
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[5m]))
              -
              sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",le="1"}[5m]))
            )
            +
            sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",code=~"5.."}[5m]))
          )
          /
          sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[5m]))
        labels:
          verb: write
        record: apiserver_request:burnrate5m
      - expr: |
          (
            (
              # too slow
              sum(rate(apiserver_request_duration_seconds_count{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[6h]))
              -
              sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",le="1"}[6h]))
            )
            +
            sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE",code=~"5.."}[6h]))
          )
          /
          sum(rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[6h]))
        labels:
          verb: write
        record: apiserver_request:burnrate6h
      - expr: |
          sum by (code,resource) (rate(apiserver_request_total{job="apiserver",verb=~"LIST|GET"}[5m]))
        labels:
          verb: read
        record: code_resource:apiserver_request_total:rate5m
      - expr: |
          sum by (code,resource) (rate(apiserver_request_total{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[5m]))
        labels:
          verb: write
        record: code_resource:apiserver_request_total:rate5m
      - expr: |
          histogram_quantile(0.99, sum by (le, resource) (rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"LIST|GET"}[5m]))) > 0
        labels:
          quantile: "0.99"
          verb: read
        record: cluster_quantile:apiserver_request_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.99, sum by (le, resource) (rate(apiserver_request_duration_seconds_bucket{job="apiserver",verb=~"POST|PUT|PATCH|DELETE"}[5m]))) > 0
        labels:
          quantile: "0.99"
          verb: write
        record: cluster_quantile:apiserver_request_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.99, sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",subresource!="log",verb!~"LIST|WATCH|WATCHLIST|DELETECOLLECTION|PROXY|CONNECT"}[5m])) without(instance, pod))
        labels:
          quantile: "0.99"
        record: cluster_quantile:apiserver_request_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.9, sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",subresource!="log",verb!~"LIST|WATCH|WATCHLIST|DELETECOLLECTION|PROXY|CONNECT"}[5m])) without(instance, pod))
        labels:
          quantile: "0.9"
        record: cluster_quantile:apiserver_request_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.5, sum(rate(apiserver_request_duration_seconds_bucket{job="apiserver",subresource!="log",verb!~"LIST|WATCH|WATCHLIST|DELETECOLLECTION|PROXY|CONNECT"}[5m])) without(instance, pod))
        labels:
          quantile: "0.5"
        record: cluster_quantile:apiserver_request_duration_seconds:histogram_quantile
  openshift-operator-lifecycle-manager-olm-alert-rules.yaml: |
    groups:
    - name: olm.csv_abnormal.rules
      rules:
      - alert: CsvAbnormalFailedOver2Min
        annotations:
          message: Failed to install Operator {{ $labels.name }} version {{ $labels.version
            }}. Reason-{{ $labels.reason }}
        expr: csv_abnormal{phase=~"^Failed$"}
        for: 2m
        labels:
          namespace: '{{ $labels.namespace }}'
          severity: warning
      - alert: CsvAbnormalOver30Min
        annotations:
          message: Failed to install Operator {{ $labels.name }} version {{ $labels.version
            }}. Phase-{{ $labels.phase }} Reason-{{ $labels.reason }}
        expr: csv_abnormal{phase=~"(^Replacing$|^Pending$|^Deleting$|^Unknown$)"}
        for: 30m
        labels:
          namespace: '{{ $labels.namespace }}'
          severity: warning
    - name: olm.installplan.rules
      rules:
      - alert: InstallPlanStepAppliedWithWarnings
        annotations:
          message: The API server returned a warning during installation or upgrade of
            an operator. An Event with reason "AppliedWithWarnings" has been created with
            complete details, including a reference to the InstallPlan step that generated
            the warning.
        expr: sum(increase(installplan_warnings_total[5m])) > 0
        labels:
          severity: warning
  openshift-machine-config-operator-machine-config-daemon.yaml: |
    groups:
    - name: mcd-reboot-error
      rules:
      - alert: MCDRebootError
        annotations:
          message: Reboot failed on {{ $labels.node }} , update may be blocked
        expr: |
          mcd_reboot_err > 0
        labels:
          severity: critical
    - name: mcd-drain-error
      rules:
      - alert: MCDDrainError
        annotations:
          message: 'Drain failed on {{ $labels.node }} , updates may be blocked. For more
            details:  oc logs -f -n {{ $labels.namespace }} {{ $labels.pod }} -c machine-config-daemon'
        expr: |
          mcd_drain_err > 0
        labels:
          severity: warning
    - name: mcd-pivot-error
      rules:
      - alert: MCDPivotError
        annotations:
          message: 'Error detected in pivot logs on {{ $labels.node }} '
        expr: |
          mcd_pivot_err > 0
        labels:
          severity: warning
    - name: mcd-kubelet-health-state-error
      rules:
      - alert: KubeletHealthState
        annotations:
          message: Kubelet health failure threshold reached
        expr: |
          mcd_kubelet_state > 2
        labels:
          severity: warning
    - name: system-memory-exceeds-reservation
      rules:
      - alert: SystemMemoryExceedsReservation
        annotations:
          message: System memory usage of {{ $value | humanize }} on {{ $labels.node }}
            exceeds 95% of the reservation. Reserved memory ensures system processes can
            function even when the node is fully allocated and protects against workload
            out of memory events impacting the proper functioning of the node. The default
            reservation is expected to be sufficient for most configurations and should
            be increased (https://docs.openshift.com/container-platform/latest/nodes/nodes/nodes-nodes-managing.html)
            when running nodes with high numbers of pods (either due to rate of change
            or at steady state).
        expr: |
          sum by (node) (container_memory_rss{id="/system.slice"}) > ((sum by (node) (kube_node_status_capacity{resource="memory"} - kube_node_status_allocatable{resource="memory"})) * 0.95)
        for: 15m
        labels:
          severity: warning
    - name: master-nodes-high-memory-usage
      rules:
      - alert: MasterNodesHighMemoryUsage
        annotations:
          message: Memory usage of {{ $value | humanize }} on {{ $labels.node }} exceeds
            90%. Master nodes starved of memory could result in degraded performance of
            the control plane.
        expr: |
          ((sum(node_memory_MemTotal_bytes AND on (instance) label_replace( kube_node_role{role="master"}, "instance", "$1", "node", "(.+)" )) - sum(node_memory_MemFree_bytes + node_memory_Buffers_bytes + node_memory_Cached_bytes AND on (instance) label_replace( kube_node_role{role="master"}, "instance", "$1", "node", "(.+)" ))) / sum(node_memory_MemTotal_bytes AND on (instance) label_replace( kube_node_role{role="master"}, "instance", "$1", "node", "(.+)" )) * 100) > 90
        for: 15m
        labels:
          severity: warning
  openshift-etcd-operator-etcd-prometheus-rules.yaml: |
    groups:
    - name: etcd
      rules:
      - alert: etcdMembersDown
        annotations:
          description: 'etcd cluster "{{ $labels.job }}": members are down ({{ $value
            }}).'
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-etcd-operator/etcdMembersDown.md
          summary: etcd cluster members are down.
        expr: |
          max without (endpoint) (
            sum without (instance) (up{job=~".*etcd.*"} == bool 0)
          or
            count without (To) (
              sum without (instance) (rate(etcd_network_peer_sent_failures_total{job=~".*etcd.*"}[120s])) > 0.01
            )
          )
          > 0
        for: 10m
        labels:
          severity: critical
      - alert: etcdNoLeader
        annotations:
          description: 'etcd cluster "{{ $labels.job }}": member {{ $labels.instance }}
            has no leader.'
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-etcd-operator/etcdNoLeader.md
          summary: etcd cluster has no leader.
        expr: |
          etcd_server_has_leader{job=~".*etcd.*"} == 0
        for: 1m
        labels:
          severity: critical
      - alert: etcdGRPCRequestsSlow
        annotations:
          description: 'etcd cluster "{{ $labels.job }}": 99th percentile of gRPC requests
            is {{ $value }}s on etcd instance {{ $labels.instance }}.'
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-etcd-operator/etcdGRPCRequestsSlow.md
          summary: etcd grpc requests are slow
        expr: |
          histogram_quantile(0.99, sum(rate(grpc_server_handling_seconds_bucket{job=~".*etcd.*", grpc_type="unary"}[5m])) without(grpc_type))
          > 0.15
        for: 10m
        labels:
          severity: critical
      - alert: etcdMemberCommunicationSlow
        annotations:
          description: 'etcd cluster "{{ $labels.job }}": member communication with {{
            $labels.To }} is taking {{ $value }}s on etcd instance {{ $labels.instance
            }}.'
          summary: etcd cluster member communication is slow.
        expr: |
          histogram_quantile(0.99, rate(etcd_network_peer_round_trip_time_seconds_bucket{job=~".*etcd.*"}[5m]))
          > 0.15
        for: 10m
        labels:
          severity: warning
      - alert: etcdHighNumberOfFailedProposals
        annotations:
          description: 'etcd cluster "{{ $labels.job }}": {{ $value }} proposal failures
            within the last 30 minutes on etcd instance {{ $labels.instance }}.'
          summary: etcd cluster has high number of proposal failures.
        expr: |
          rate(etcd_server_proposals_failed_total{job=~".*etcd.*"}[15m]) > 5
        for: 15m
        labels:
          severity: warning
      - alert: etcdHighFsyncDurations
        annotations:
          description: 'etcd cluster "{{ $labels.job }}": 99th percentile fsync durations
            are {{ $value }}s on etcd instance {{ $labels.instance }}.'
          summary: etcd cluster 99th percentile fsync durations are too high.
        expr: |
          histogram_quantile(0.99, rate(etcd_disk_wal_fsync_duration_seconds_bucket{job=~".*etcd.*"}[5m]))
          > 0.5
        for: 10m
        labels:
          severity: warning
      - alert: etcdHighFsyncDurations
        annotations:
          description: 'etcd cluster "{{ $labels.job }}": 99th percentile fsync durations
            are {{ $value }}s on etcd instance {{ $labels.instance }}.'
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-etcd-operator/etcdHighFsyncDurations.md
        expr: |
          histogram_quantile(0.99, rate(etcd_disk_wal_fsync_duration_seconds_bucket{job=~".*etcd.*"}[5m]))
          > 1
        for: 10m
        labels:
          severity: critical
      - alert: etcdHighCommitDurations
        annotations:
          description: 'etcd cluster "{{ $labels.job }}": 99th percentile commit durations
            {{ $value }}s on etcd instance {{ $labels.instance }}.'
          summary: etcd cluster 99th percentile commit durations are too high.
        expr: |
          histogram_quantile(0.99, rate(etcd_disk_backend_commit_duration_seconds_bucket{job=~".*etcd.*"}[5m]))
          > 0.25
        for: 10m
        labels:
          severity: warning
      - alert: etcdBackendQuotaLowSpace
        annotations:
          description: 'etcd cluster "{{ $labels.job }}": database size exceeds the defined
            quota on etcd instance {{ $labels.instance }}, please defrag or increase the
            quota as the writes to etcd will be disabled when it is full.'
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-etcd-operator/etcdBackendQuotaLowSpace.md
          summary: etcd cluster database size exceeds the defined quota.
        expr: |
          (etcd_mvcc_db_total_size_in_bytes/etcd_server_quota_backend_bytes)*100 > 95
        for: 10m
        labels:
          severity: critical
      - alert: etcdExcessiveDatabaseGrowth
        annotations:
          description: 'etcd cluster "{{ $labels.job }}": Observed surge in etcd writes
            leading to 50% increase in database size over the past four hours on etcd
            instance {{ $labels.instance }}, please check as it might be disruptive.'
          summary: etcd cluster database size increased by 50 percent over the past four
            hours.
        expr: |
          increase(((etcd_mvcc_db_total_size_in_bytes/etcd_server_quota_backend_bytes)*100)[240m:1m]) > 50
        for: 10m
        labels:
          severity: warning
    - name: openshift-etcd.rules
      rules:
      - alert: etcdHighNumberOfLeaderChanges
        annotations:
          description: 'etcd cluster "{{ $labels.job }}": {{ $value }} leader changes
            within the last 15 minutes. Frequent elections may be a sign of insufficient
            resources, high network latency, or disruptions by other components and should
            be investigated.'
          summary: etcd cluster has high number of leader changes.
        expr: |
          increase((max without (instance) (etcd_server_leader_changes_seen_total{job=~".*etcd.*"}) or 0*absent(etcd_server_leader_changes_seen_total{job=~".*etcd.*"}))[15m:1m]) >= 5
        for: 5m
        labels:
          severity: warning
      - alert: etcdInsufficientMembers
        annotations:
          description: etcd is reporting fewer instances are available than are needed
            ({{ $value }}). When etcd does not have a majority of instances available
            the Kubernetes and OpenShift APIs will reject read and write requests and
            operations that preserve the health of workloads cannot be performed. This
            can occur when multiple control plane nodes are powered off or are unable
            to connect to each other via the network. Check that all control plane nodes
            are powered on and that network connections between each machine are functional.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-etcd-operator/etcdInsufficientMembers.md
          summary: etcd is reporting that a majority of instances are unavailable.
        expr: sum(up{job="etcd"} == bool 1 and etcd_server_has_leader{job="etcd"} == bool
          1) without (instance,pod) < ((count(up{job="etcd"}) without (instance,pod) +
          1) / 2)
        for: 3m
        labels:
          severity: critical
  openshift-monitoring-alertmanager-main-rules.yaml: |
    groups:
    - name: alertmanager.rules
      rules:
      - alert: AlertmanagerFailedReload
        annotations:
          description: Configuration has failed to load for {{ $labels.namespace }}/{{
            $labels.pod}}.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/AlertmanagerFailedReload.md
          summary: Reloading an Alertmanager configuration has failed.
        expr: |
          # Without max_over_time, failed scrapes could create false negatives, see
          # https://www.robustperception.io/alerting-on-gauges-in-prometheus-2-0 for details.
          max_over_time(alertmanager_config_last_reload_successful{job="alertmanager-main",namespace="openshift-monitoring"}[5m]) == 0
        for: 10m
        labels:
          severity: critical
      - alert: AlertmanagerMembersInconsistent
        annotations:
          description: Alertmanager {{ $labels.namespace }}/{{ $labels.pod}} has only
            found {{ $value }} members of the {{$labels.job}} cluster.
          summary: A member of an Alertmanager cluster has not found all other cluster
            members.
        expr: |
          # Without max_over_time, failed scrapes could create false negatives, see
          # https://www.robustperception.io/alerting-on-gauges-in-prometheus-2-0 for details.
            max_over_time(alertmanager_cluster_members{job="alertmanager-main",namespace="openshift-monitoring"}[5m])
          < on (namespace,service) group_left
            count by (namespace,service) (max_over_time(alertmanager_cluster_members{job="alertmanager-main",namespace="openshift-monitoring"}[5m]))
        for: 15m
        labels:
          severity: warning
      - alert: AlertmanagerFailedToSendAlerts
        annotations:
          description: Alertmanager {{ $labels.namespace }}/{{ $labels.pod}} failed to
            send {{ $value | humanizePercentage }} of notifications to {{ $labels.integration
            }}.
          summary: An Alertmanager instance failed to send notifications.
        expr: |
          (
            rate(alertmanager_notifications_failed_total{job="alertmanager-main",namespace="openshift-monitoring"}[5m])
          /
            rate(alertmanager_notifications_total{job="alertmanager-main",namespace="openshift-monitoring"}[5m])
          )
          > 0.01
        for: 5m
        labels:
          severity: warning
      - alert: AlertmanagerClusterFailedToSendAlerts
        annotations:
          description: The minimum notification failure rate to {{ $labels.integration
            }} sent from any instance in the {{$labels.job}} cluster is {{ $value | humanizePercentage
            }}.
          summary: All Alertmanager instances in a cluster failed to send notifications
            to a critical integration.
        expr: |
          min by (namespace,service, integration) (
            rate(alertmanager_notifications_failed_total{job="alertmanager-main",namespace="openshift-monitoring", integration=~`.*`}[5m])
          /
            rate(alertmanager_notifications_total{job="alertmanager-main",namespace="openshift-monitoring", integration=~`.*`}[5m])
          )
          > 0.01
        for: 5m
        labels:
          severity: warning
      - alert: AlertmanagerClusterFailedToSendAlerts
        annotations:
          description: The minimum notification failure rate to {{ $labels.integration
            }} sent from any instance in the {{$labels.job}} cluster is {{ $value | humanizePercentage
            }}.
          summary: All Alertmanager instances in a cluster failed to send notifications
            to a non-critical integration.
        expr: |
          min by (namespace,service, integration) (
            rate(alertmanager_notifications_failed_total{job="alertmanager-main",namespace="openshift-monitoring", integration!~`.*`}[5m])
          /
            rate(alertmanager_notifications_total{job="alertmanager-main",namespace="openshift-monitoring", integration!~`.*`}[5m])
          )
          > 0.01
        for: 5m
        labels:
          severity: warning
      - alert: AlertmanagerConfigInconsistent
        annotations:
          description: Alertmanager instances within the {{$labels.job}} cluster have
            different configurations.
          summary: Alertmanager instances within the same cluster have different configurations.
        expr: |
          count by (namespace,service) (
            count_values by (namespace,service) ("config_hash", alertmanager_config_hash{job="alertmanager-main",namespace="openshift-monitoring"})
          )
          != 1
        for: 20m
        labels:
          severity: warning
      - alert: AlertmanagerClusterDown
        annotations:
          description: '{{ $value | humanizePercentage }} of Alertmanager instances within
            the {{$labels.job}} cluster have been up for less than half of the last 5m.'
          summary: Half or more of the Alertmanager instances within the same cluster
            are down.
        expr: |
          (
            count by (namespace,service) (
              avg_over_time(up{job="alertmanager-main",namespace="openshift-monitoring"}[5m]) < 0.5
            )
          /
            count by (namespace,service) (
              up{job="alertmanager-main",namespace="openshift-monitoring"}
            )
          )
          >= 0.5
        for: 5m
        labels:
          severity: warning
  openshift-monitoring-prometheus-k8s-prometheus-rules.yaml: |
    groups:
    - name: prometheus
      rules:
      - alert: PrometheusBadConfig
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} has failed to
            reload its configuration.
          summary: Failed Prometheus configuration reload.
        expr: |
          # Without max_over_time, failed scrapes could create false negatives, see
          # https://www.robustperception.io/alerting-on-gauges-in-prometheus-2-0 for details.
          max_over_time(prometheus_config_last_reload_successful{job=~"prometheus-k8s|prometheus-user-workload"}[5m]) == 0
        for: 10m
        labels:
          severity: warning
      - alert: PrometheusNotificationQueueRunningFull
        annotations:
          description: Alert notification queue of Prometheus {{$labels.namespace}}/{{$labels.pod}}
            is running full.
          summary: Prometheus alert notification queue predicted to run full in less than
            30m.
        expr: |
          # Without min_over_time, failed scrapes could create false negatives, see
          # https://www.robustperception.io/alerting-on-gauges-in-prometheus-2-0 for details.
          (
            predict_linear(prometheus_notifications_queue_length{job=~"prometheus-k8s|prometheus-user-workload"}[5m], 60 * 30)
          >
            min_over_time(prometheus_notifications_queue_capacity{job=~"prometheus-k8s|prometheus-user-workload"}[5m])
          )
        for: 15m
        labels:
          severity: warning
      - alert: PrometheusErrorSendingAlertsToSomeAlertmanagers
        annotations:
          description: '{{ printf "%.1f" $value }}% errors while sending alerts from Prometheus
            {{$labels.namespace}}/{{$labels.pod}} to Alertmanager {{$labels.alertmanager}}.'
          summary: Prometheus has encountered more than 1% errors sending alerts to a
            specific Alertmanager.
        expr: |
          (
            rate(prometheus_notifications_errors_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m])
          /
            rate(prometheus_notifications_sent_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m])
          )
          * 100
          > 1
        for: 15m
        labels:
          severity: warning
      - alert: PrometheusNotConnectedToAlertmanagers
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} is not connected
            to any Alertmanagers.
          summary: Prometheus is not connected to any Alertmanagers.
        expr: |
          # Without max_over_time, failed scrapes could create false negatives, see
          # https://www.robustperception.io/alerting-on-gauges-in-prometheus-2-0 for details.
          max_over_time(prometheus_notifications_alertmanagers_discovered{job=~"prometheus-k8s|prometheus-user-workload"}[5m]) < 1
        for: 10m
        labels:
          severity: warning
      - alert: PrometheusTSDBReloadsFailing
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} has detected {{$value
            | humanize}} reload failures over the last 3h.
          summary: Prometheus has issues reloading blocks from disk.
        expr: |
          increase(prometheus_tsdb_reloads_failures_total{job=~"prometheus-k8s|prometheus-user-workload"}[3h]) > 0
        for: 4h
        labels:
          severity: warning
      - alert: PrometheusTSDBCompactionsFailing
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} has detected {{$value
            | humanize}} compaction failures over the last 3h.
          summary: Prometheus has issues compacting blocks.
        expr: |
          increase(prometheus_tsdb_compactions_failed_total{job=~"prometheus-k8s|prometheus-user-workload"}[3h]) > 0
        for: 4h
        labels:
          severity: warning
      - alert: PrometheusNotIngestingSamples
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} is not ingesting
            samples.
          summary: Prometheus is not ingesting samples.
        expr: |
          (
            rate(prometheus_tsdb_head_samples_appended_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m]) <= 0
          and
            (
              sum without(scrape_job) (prometheus_target_metadata_cache_entries{job=~"prometheus-k8s|prometheus-user-workload"}) > 0
            or
              sum without(rule_group) (prometheus_rule_group_rules{job=~"prometheus-k8s|prometheus-user-workload"}) > 0
            )
          )
        for: 10m
        labels:
          severity: warning
      - alert: PrometheusDuplicateTimestamps
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} is dropping {{
            printf "%.4g" $value  }} samples/s with different values but duplicated timestamp.
          summary: Prometheus is dropping samples with duplicate timestamps.
        expr: |
          rate(prometheus_target_scrapes_sample_duplicate_timestamp_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m]) > 0
        for: 1h
        labels:
          severity: warning
      - alert: PrometheusOutOfOrderTimestamps
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} is dropping {{
            printf "%.4g" $value  }} samples/s with timestamps arriving out of order.
          summary: Prometheus drops samples with out-of-order timestamps.
        expr: |
          rate(prometheus_target_scrapes_sample_out_of_order_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m]) > 0
        for: 1h
        labels:
          severity: warning
      - alert: PrometheusRemoteStorageFailures
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} failed to send
            {{ printf "%.1f" $value }}% of the samples to {{ $labels.remote_name}}:{{
            $labels.url }}
          summary: Prometheus fails to send samples to remote storage.
        expr: |
          (
            (rate(prometheus_remote_storage_failed_samples_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m]) or rate(prometheus_remote_storage_samples_failed_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m]))
          /
            (
              (rate(prometheus_remote_storage_failed_samples_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m]) or rate(prometheus_remote_storage_samples_failed_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m]))
            +
              (rate(prometheus_remote_storage_succeeded_samples_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m]) or rate(prometheus_remote_storage_samples_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m]))
            )
          )
          * 100
          > 1
        for: 15m
        labels:
          severity: warning
      - alert: PrometheusRemoteWriteBehind
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} remote write is
            {{ printf "%.1f" $value }}s behind for {{ $labels.remote_name}}:{{ $labels.url
            }}.
          summary: Prometheus remote write is behind.
        expr: |
          # Without max_over_time, failed scrapes could create false negatives, see
          # https://www.robustperception.io/alerting-on-gauges-in-prometheus-2-0 for details.
          (
            max_over_time(prometheus_remote_storage_highest_timestamp_in_seconds{job=~"prometheus-k8s|prometheus-user-workload"}[5m])
          - ignoring(remote_name, url) group_right
            max_over_time(prometheus_remote_storage_queue_highest_sent_timestamp_seconds{job=~"prometheus-k8s|prometheus-user-workload"}[5m])
          )
          > 120
        for: 15m
        labels:
          severity: info
      - alert: PrometheusRemoteWriteDesiredShards
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} remote write desired
            shards calculation wants to run {{ $value }} shards for queue {{ $labels.remote_name}}:{{
            $labels.url }}, which is more than the max of {{ printf `prometheus_remote_storage_shards_max{instance="%s",job=~"prometheus-k8s|prometheus-user-workload"}`
            $labels.instance | query | first | value }}.
          summary: Prometheus remote write desired shards calculation wants to run more
            than configured max shards.
        expr: |
          # Without max_over_time, failed scrapes could create false negatives, see
          # https://www.robustperception.io/alerting-on-gauges-in-prometheus-2-0 for details.
          (
            max_over_time(prometheus_remote_storage_shards_desired{job=~"prometheus-k8s|prometheus-user-workload"}[5m])
          >
            max_over_time(prometheus_remote_storage_shards_max{job=~"prometheus-k8s|prometheus-user-workload"}[5m])
          )
        for: 15m
        labels:
          severity: warning
      - alert: PrometheusRuleFailures
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} has failed to
            evaluate {{ printf "%.0f" $value }} rules in the last 5m.
          summary: Prometheus is failing rule evaluations.
        expr: |
          increase(prometheus_rule_evaluation_failures_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m]) > 0
        for: 15m
        labels:
          severity: warning
      - alert: PrometheusMissingRuleEvaluations
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} has missed {{
            printf "%.0f" $value }} rule group evaluations in the last 5m.
          summary: Prometheus is missing rule evaluations due to slow rule group evaluation.
        expr: |
          increase(prometheus_rule_group_iterations_missed_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m]) > 0
        for: 15m
        labels:
          severity: warning
      - alert: PrometheusTargetLimitHit
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} has dropped {{
            printf "%.0f" $value }} targets because the number of targets exceeded the
            configured target_limit.
          summary: Prometheus has dropped targets because some scrape configs have exceeded
            the targets limit.
        expr: |
          increase(prometheus_target_scrape_pool_exceeded_target_limit_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m]) > 0
        for: 15m
        labels:
          severity: warning
      - alert: PrometheusLabelLimitHit
        annotations:
          description: Prometheus {{$labels.namespace}}/{{$labels.pod}} has dropped {{
            printf "%.0f" $value }} targets because some samples exceeded the configured
            label_limit, label_name_length_limit or label_value_length_limit.
          summary: Prometheus has dropped targets because some scrape configs have exceeded
            the labels limit.
        expr: |
          increase(prometheus_target_scrape_pool_exceeded_label_limits_total{job=~"prometheus-k8s|prometheus-user-workload"}[5m]) > 0
        for: 15m
        labels:
          severity: warning
      - alert: PrometheusTargetSyncFailure
        annotations:
          description: '{{ printf "%.0f" $value }} targets in Prometheus {{$labels.namespace}}/{{$labels.pod}}
            have failed to sync because invalid configuration was supplied.'
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/PrometheusTargetSyncFailure.md
          summary: Prometheus has failed to sync targets.
        expr: |
          increase(prometheus_target_sync_failed_total{job=~"prometheus-k8s|prometheus-user-workload"}[30m]) > 0
        for: 5m
        labels:
          severity: critical
  openshift-monitoring-thanos-querier.yaml: |
    groups:
    - name: thanos-query
      rules:
      - alert: ThanosQueryHttpRequestQueryErrorRateHigh
        annotations:
          description: Thanos Query {{$labels.job}} is failing to handle {{$value | humanize}}%
            of "query" requests.
          summary: Thanos Query is failing to handle requests.
        expr: |
          (
            sum by (job, namespace) (rate(http_requests_total{code=~"5..", job="thanos-querier", handler="query"}[5m]))
          /
            sum by (job, namespace) (rate(http_requests_total{job="thanos-querier", handler="query"}[5m]))
          ) * 100 > 5
        for: 1h
        labels:
          severity: warning
      - alert: ThanosQueryHttpRequestQueryRangeErrorRateHigh
        annotations:
          description: Thanos Query {{$labels.job}} is failing to handle {{$value | humanize}}%
            of "query_range" requests.
          summary: Thanos Query is failing to handle requests.
        expr: |
          (
            sum by (job, namespace) (rate(http_requests_total{code=~"5..", job="thanos-querier", handler="query_range"}[5m]))
          /
            sum by (job, namespace) (rate(http_requests_total{job="thanos-querier", handler="query_range"}[5m]))
          ) * 100 > 5
        for: 1h
        labels:
          severity: warning
      - alert: ThanosQueryGrpcServerErrorRate
        annotations:
          description: Thanos Query {{$labels.job}} is failing to handle {{$value | humanize}}%
            of requests.
          summary: Thanos Query is failing to handle requests.
        expr: |
          (
            sum by (job, namespace) (rate(grpc_server_handled_total{grpc_code=~"Unknown|ResourceExhausted|Internal|Unavailable|DataLoss|DeadlineExceeded", job="thanos-querier"}[5m]))
          /
            sum by (job, namespace) (rate(grpc_server_started_total{job="thanos-querier"}[5m]))
          * 100 > 5
          )
        for: 1h
        labels:
          severity: warning
      - alert: ThanosQueryGrpcClientErrorRate
        annotations:
          description: Thanos Query {{$labels.job}} is failing to send {{$value | humanize}}%
            of requests.
          summary: Thanos Query is failing to send requests.
        expr: |
          (
            sum by (job, namespace) (rate(grpc_client_handled_total{grpc_code!="OK", job="thanos-querier"}[5m]))
          /
            sum by (job, namespace) (rate(grpc_client_started_total{job="thanos-querier"}[5m]))
          ) * 100 > 5
        for: 1h
        labels:
          severity: warning
      - alert: ThanosQueryHighDNSFailures
        annotations:
          description: Thanos Query {{$labels.job}} have {{$value | humanize}}% of failing
            DNS queries for store endpoints.
          summary: Thanos Query is having high number of DNS failures.
        expr: |
          (
            sum by (job, namespace) (rate(thanos_query_store_apis_dns_failures_total{job="thanos-querier"}[5m]))
          /
            sum by (job, namespace) (rate(thanos_query_store_apis_dns_lookups_total{job="thanos-querier"}[5m]))
          ) * 100 > 1
        for: 1h
        labels:
          severity: warning
  openshift-marketplace-marketplace-alert-rules.yaml: |
    groups:
    - name: marketplace.community_operators.rules
      rules:
      - alert: CommunityOperatorsCatalogError
        annotations:
          message: Default OperatorHub source "community-operators" is in Non-Ready state
            for more than 10 mins.
        expr: catalogsource_ready{name="community-operators",exported_namespace="openshift-marketplace"}
          == 0
        for: 10m
        labels:
          severity: warning
    - name: marketplace.certified_operators.rules
      rules:
      - alert: CertifiedOperatorsCatalogError
        annotations:
          message: Default OperatorHub source "certified-operators" is in Non-Ready state
            for more than 10 mins.
        expr: catalogsource_ready{name="certified-operators",exported_namespace="openshift-marketplace"}
          == 0
        for: 10m
        labels:
          severity: warning
    - name: marketplace.redhat_operators.rules
      rules:
      - alert: RedhatOperatorsCatalogError
        annotations:
          message: Default OperatorHub source "redhat-operators" is in Non-Ready state
            for more than 10 mins.
        expr: catalogsource_ready{name="redhat-operators",exported_namespace="openshift-marketplace"}
          == 0
        for: 10m
        labels:
          severity: warning
    - name: marketplace.redhat_marketplace.rules
      rules:
      - alert: RedhatMarketplaceCatalogError
        annotations:
          message: Default OperatorHub source "redhat-marketplace" is in Non-Ready state
            for more than 10 mins.
        expr: catalogsource_ready{name="redhat-marketplace",exported_namespace="openshift-marketplace"}
          == 0
        for: 10m
        labels:
          severity: warning
  openshift-image-registry-image-registry-operator-alerts.yaml: |
    groups:
    - name: ImageRegistryOperator
      rules:
      - alert: ImageRegistryStorageReconfigured
        annotations:
          message: |
            Image Registry Storage configuration has changed in the last 30
            minutes. This change may have caused data loss.
        expr: increase(image_registry_operator_storage_reconfigured_total[30m]) > 0
        labels:
          severity: warning
  openshift-ovn-kubernetes-networking-rules.yaml: |
    groups:
    - name: cluster-network-operator-ovn.rules
      rules:
      - alert: NodeWithoutOVNKubeNodePodRunning
        annotations:
          summary: All Linux nodes should be running an ovnkube-node pod, {{ $labels.node
            }} is not.
        expr: |
          (kube_node_info unless on(node) (kube_pod_info{namespace="openshift-ovn-kubernetes",pod=~"ovnkube-node.*"}
          or kube_node_labels{label_kubernetes_io_os="windows"})) > 0
        for: 20m
        labels:
          severity: warning
      - alert: NetworkPodsCrashLooping
        annotations:
          summary: Pod {{ $labels.namespace}}/{{ $labels.pod}} ({{ $labels.container }})
            is restarting {{ printf "%.2f" $value }} times / 5 minutes.
        expr: |
          rate(kube_pod_container_status_restarts_total{namespace="openshift-ovn-kubernetes"}[15m]) * 60 * 5 > 0
        for: 1h
        labels:
          severity: warning
  openshift-kube-scheduler-operator-kube-scheduler-operator.yaml: |
    groups:
    - name: cluster-version
      rules:
      - alert: KubeSchedulerDown
        annotations:
          message: KubeScheduler has disappeared from Prometheus target discovery.
        expr: |
          absent(up{job="scheduler"} == 1)
        for: 15m
        labels:
          severity: critical
    - name: scheduler-legacy-policy-deprecated
      rules:
      - alert: SchedulerLegacyPolicySet
        annotations:
          message: The scheduler is currently configured to use a legacy scheduler policy
            API. Use of the policy API is deprecated and removed in 4.10.
        expr: |
          cluster_legacy_scheduler_policy > 0
        for: 60m
        labels:
          severity: warning
  openshift-cluster-version-cluster-version-operator.yaml: |
    groups:
    - name: cluster-version
      rules:
      - alert: ClusterVersionOperatorDown
        annotations:
          description: The operator may be down or disabled. The cluster will not be kept
            up to date and upgrades will not be possible. Inspect the openshift-cluster-version
            namespace for events or changes to the cluster-version-operator deployment
            or pods to diagnose and repair. {{ with $console_url := "console_url" | query
            }}{{ if ne (len (label "url" (first $console_url ) ) ) 0}} For more information
            refer to {{ label "url" (first $console_url ) }}/k8s/cluster/projects/openshift-cluster-version.{{
            end }}{{ end }}
          summary: Cluster version operator has disappeared from Prometheus target discovery.
        expr: |
          absent(up{job="cluster-version-operator"} == 1)
        for: 10m
        labels:
          severity: critical
      - alert: CannotRetrieveUpdates
        annotations:
          description: Failure to retrieve updates means that cluster administrators will
            need to monitor for available updates on their own or risk falling behind
            on security or other bugfixes. If the failure is expected, you can clear spec.channel
            in the ClusterVersion object to tell the cluster-version operator to not retrieve
            updates. Failure reason {{ with $cluster_operator_conditions := "cluster_operator_conditions"
            | query}}{{range $value := .}}{{if and (eq (label "name" $value) "version")
            (eq (label "condition" $value) "RetrievedUpdates") (eq (label "endpoint" $value)
            "metrics") (eq (value $value) 0.0)}}{{label "reason" $value}} {{end}}{{end}}{{end}}.
            {{ with $console_url := "console_url" | query }}{{ if ne (len (label "url"
            (first $console_url ) ) ) 0}} For more information refer to {{ label "url"
            (first $console_url ) }}/settings/cluster/.{{ end }}{{ end }}
          summary: Cluster version operator has not retrieved updates in {{ $value | humanizeDuration
            }}.
        expr: |
          (time()-cluster_version_operator_update_retrieval_timestamp_seconds) >= 3600 and ignoring(condition, name, reason) cluster_operator_conditions{name="version", condition="RetrievedUpdates", endpoint="metrics", reason!="NoChannel"}
        labels:
          severity: warning
      - alert: UpdateAvailable
        annotations:
          description: For more information refer to 'oc adm upgrade'{{ with $console_url
            := "console_url" | query }}{{ if ne (len (label "url" (first $console_url
            ) ) ) 0}} or {{ label "url" (first $console_url ) }}/settings/cluster/{{ end
            }}{{ end }}.
          summary: Your upstream update recommendation service recommends you update your
            cluster.
        expr: |
          sum by (channel,upstream) (cluster_version_available_updates) > 0
        labels:
          severity: info
    - name: cluster-operators
      rules:
      - alert: ClusterNotUpgradeable
        annotations:
          description: In most cases, you will still be able to apply patch releases.
            Reason {{ with $cluster_operator_conditions := "cluster_operator_conditions"
            | query}}{{range $value := .}}{{if and (eq (label "name" $value) "version")
            (eq (label "condition" $value) "Upgradeable") (eq (label "endpoint" $value)
            "metrics") (eq (value $value) 0.0) (ne (len (label "reason" $value)) 0) }}{{label
            "reason" $value}}.{{end}}{{end}}{{end}} For more information refer to 'oc
            adm upgrade'{{ with $console_url := "console_url" | query }}{{ if ne (len
            (label "url" (first $console_url ) ) ) 0}} or {{ label "url" (first $console_url
            ) }}/settings/cluster/{{ end }}{{ end }}.
          summary: One or more cluster operators have been blocking minor version cluster
            upgrades for at least an hour.
        expr: |
          max by (name, condition, endpoint) (cluster_operator_conditions{name="version", condition="Upgradeable", endpoint="metrics"} == 0)
        for: 60m
        labels:
          severity: info
      - alert: ClusterOperatorDown
        annotations:
          description: The {{ $labels.name }} operator may be down or disabled, and the
            components it manages may be unavailable or degraded.  Cluster upgrades may
            not complete. For more information refer to 'oc get -o yaml clusteroperator
            {{ $labels.name }}'{{ with $console_url := "console_url" | query }}{{ if ne
            (len (label "url" (first $console_url ) ) ) 0}} or {{ label "url" (first $console_url
            ) }}/settings/cluster/{{ end }}{{ end }}.
          summary: Cluster operator has not been available for 10 minutes.
        expr: |
          cluster_operator_up{job="cluster-version-operator"} == 0
        for: 10m
        labels:
          severity: critical
      - alert: ClusterOperatorDegraded
        annotations:
          description: The {{ $labels.name }} operator is degraded because {{ $labels.reason
            }}, and the components it manages may have reduced quality of service.  Cluster
            upgrades may not complete. For more information refer to 'oc get -o yaml clusteroperator
            {{ $labels.name }}'{{ with $console_url := "console_url" | query }}{{ if ne
            (len (label "url" (first $console_url ) ) ) 0}} or {{ label "url" (first $console_url
            ) }}/settings/cluster/{{ end }}{{ end }}.
          summary: Cluster operator has been degraded for 30 minutes.
        expr: |
          (
            cluster_operator_conditions{job="cluster-version-operator", condition="Degraded"}
            or on (name)
            group by (name) (cluster_operator_up{job="cluster-version-operator"})
          ) == 1
        for: 30m
        labels:
          severity: warning
      - alert: ClusterOperatorFlapping
        annotations:
          description: The  {{ $labels.name }} operator behavior might cause upgrades
            to be unstable. For more information refer to 'oc get -o yaml clusteroperator
            {{ $labels.name }}'{{ with $console_url := "console_url" | query }}{{ if ne
            (len (label "url" (first $console_url ) ) ) 0}} or {{ label "url" (first $console_url
            ) }}/settings/cluster/{{ end }}{{ end }}.
          summary: Cluster operator up status is changing often.
        expr: |
          changes(cluster_operator_up{job="cluster-version-operator"}[2m]) > 2
        for: 10m
        labels:
          severity: warning
  openshift-monitoring-telemetry.yaml: |
    groups:
    - name: telemeter.rules
      rules:
      - expr: count({__name__=~"cluster:usage:.*|count:up0|count:up1|cluster_version|cluster_version_available_updates|cluster_operator_up|cluster_operator_conditions|cluster_version_payload|cluster_installer|cluster_infrastructure_provider|cluster_feature_set|instance:etcd_object_counts:sum|ALERTS|code:apiserver_request_total:rate:sum|cluster:capacity_cpu_cores:sum|cluster:capacity_memory_bytes:sum|cluster:cpu_usage_cores:sum|cluster:memory_usage_bytes:sum|openshift:cpu_usage_cores:sum|openshift:memory_usage_bytes:sum|workload:cpu_usage_cores:sum|workload:memory_usage_bytes:sum|cluster:virt_platform_nodes:sum|cluster:node_instance_type_count:sum|cnv:vmi_status_running:count|node_role_os_version_machine:cpu_capacity_cores:sum|node_role_os_version_machine:cpu_capacity_sockets:sum|subscription_sync_total|olm_resolution_duration_seconds|csv_succeeded|csv_abnormal|cluster:kube_persistentvolumeclaim_resource_requests_storage_bytes:provisioner:sum|cluster:kubelet_volume_stats_used_bytes:provisioner:sum|ceph_cluster_total_bytes|ceph_cluster_total_used_raw_bytes|ceph_health_status|job:ceph_osd_metadata:count|job:kube_pv:count|job:ceph_pools_iops:total|job:ceph_pools_iops_bytes:total|job:ceph_versions_running:count|job:noobaa_total_unhealthy_buckets:sum|job:noobaa_bucket_count:sum|job:noobaa_total_object_count:sum|noobaa_accounts_num|noobaa_total_usage|console_url|cluster:network_attachment_definition_instances:max|cluster:network_attachment_definition_enabled_instance_up:max|insightsclient_request_send_total|cam_app_workload_migrations|cluster:apiserver_current_inflight_requests:sum:max_over_time:2m|cluster:alertmanager_integrations:max|cluster:telemetry_selected_series:count|openshift:prometheus_tsdb_head_series:sum|openshift:prometheus_tsdb_head_samples_appended_total:sum|monitoring:container_memory_working_set_bytes:sum|namespace_job:scrape_series_added:topk3_sum1h|namespace_job:scrape_samples_post_metric_relabeling:topk3|monitoring:haproxy_server_http_responses_total:sum|rhmi_status|cluster_legacy_scheduler_policy|cluster_master_schedulable|che_workspace_status|che_workspace_started_total|che_workspace_failure_total|che_workspace_start_time_seconds_sum|che_workspace_start_time_seconds_count|cco_credentials_mode|cluster:kube_persistentvolume_plugin_type_counts:sum|visual_web_terminal_sessions_total|acm_managed_cluster_info|cluster:vsphere_vcenter_info:sum|cluster:vsphere_esxi_version_total:sum|cluster:vsphere_node_hw_version_total:sum|openshift:build_by_strategy:sum|rhods_aggregate_availability|rhods_total_users|instance:etcd_disk_wal_fsync_duration_seconds:histogram_quantile|instance:etcd_mvcc_db_total_size_in_bytes:sum|instance:etcd_network_peer_round_trip_time_seconds:histogram_quantile|instance:etcd_mvcc_db_total_size_in_use_in_bytes:sum|instance:etcd_disk_backend_commit_duration_seconds:histogram_quantile|jaeger_operator_instances_storage_types|jaeger_operator_instances_strategies|jaeger_operator_instances_agent_strategies|appsvcs:cores_by_product:sum|nto_custom_profiles:count",alertstate=~"firing|",quantile=~"0.99|0.99|0.99|"})
        record: cluster:telemetry_selected_series:count
  openshift-monitoring-kubernetes-monitoring-rules.yaml: |
    groups:
    - name: kubernetes-apps
      rules:
      - alert: KubePodCrashLooping
        annotations:
          description: 'Pod {{ $labels.namespace }}/{{ $labels.pod }} ({{ $labels.container
            }}) is in waiting state (reason: "CrashLoopBackOff").'
          summary: Pod is crash looping.
        expr: |
          max_over_time(kube_pod_container_status_waiting_reason{reason="CrashLoopBackOff", namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}[5m]) >= 1
        for: 15m
        labels:
          severity: warning
      - alert: KubePodNotReady
        annotations:
          description: Pod {{ $labels.namespace }}/{{ $labels.pod }} has been in a non-ready
            state for longer than 15 minutes.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/KubePodNotReady.md
          summary: Pod has been in a non-ready state for more than 15 minutes.
        expr: |
          sum by (namespace, pod) (
            max by(namespace, pod) (
              kube_pod_status_phase{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics", phase=~"Pending|Unknown"}
            ) * on(namespace, pod) group_left(owner_kind) topk by(namespace, pod) (
              1, max by(namespace, pod, owner_kind) (kube_pod_owner{owner_kind!="Job"})
            )
          ) > 0
        for: 15m
        labels:
          severity: warning
      - alert: KubeDeploymentGenerationMismatch
        annotations:
          description: Deployment generation for {{ $labels.namespace }}/{{ $labels.deployment
            }} does not match, this indicates that the Deployment has failed but has not
            been rolled back.
          summary: Deployment generation mismatch due to possible roll-back
        expr: |
          kube_deployment_status_observed_generation{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
            !=
          kube_deployment_metadata_generation{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
        for: 15m
        labels:
          severity: warning
      - alert: KubeStatefulSetReplicasMismatch
        annotations:
          description: StatefulSet {{ $labels.namespace }}/{{ $labels.statefulset }} has
            not matched the expected number of replicas for longer than 15 minutes.
          summary: Deployment has not matched the expected number of replicas.
        expr: |
          (
            kube_statefulset_status_replicas_ready{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
              !=
            kube_statefulset_status_replicas{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
          ) and (
            changes(kube_statefulset_status_replicas_updated{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}[10m])
              ==
            0
          )
        for: 15m
        labels:
          severity: warning
      - alert: KubeStatefulSetGenerationMismatch
        annotations:
          description: StatefulSet generation for {{ $labels.namespace }}/{{ $labels.statefulset
            }} does not match, this indicates that the StatefulSet has failed but has
            not been rolled back.
          summary: StatefulSet generation mismatch due to possible roll-back
        expr: |
          kube_statefulset_status_observed_generation{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
            !=
          kube_statefulset_metadata_generation{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
        for: 15m
        labels:
          severity: warning
      - alert: KubeStatefulSetUpdateNotRolledOut
        annotations:
          description: StatefulSet {{ $labels.namespace }}/{{ $labels.statefulset }} update
            has not been rolled out.
          summary: StatefulSet update has not been rolled out.
        expr: |
          (
            max without (revision) (
              kube_statefulset_status_current_revision{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
                unless
              kube_statefulset_status_update_revision{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
            )
              *
            (
              kube_statefulset_replicas{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
                !=
              kube_statefulset_status_replicas_updated{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
            )
          )  and (
            changes(kube_statefulset_status_replicas_updated{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}[5m])
              ==
            0
          )
        for: 15m
        labels:
          severity: warning
      - alert: KubeDaemonSetRolloutStuck
        annotations:
          description: DaemonSet {{ $labels.namespace }}/{{ $labels.daemonset }} has not
            finished or progressed for at least 30 minutes.
          summary: DaemonSet rollout is stuck.
        expr: |
          (
            (
              kube_daemonset_status_current_number_scheduled{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
               !=
              kube_daemonset_status_desired_number_scheduled{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
            ) or (
              kube_daemonset_status_number_misscheduled{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
               !=
              0
            ) or (
              kube_daemonset_updated_number_scheduled{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
               !=
              kube_daemonset_status_desired_number_scheduled{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
            ) or (
              kube_daemonset_status_number_available{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
               !=
              kube_daemonset_status_desired_number_scheduled{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
            )
          ) and (
            changes(kube_daemonset_updated_number_scheduled{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}[5m])
              ==
            0
          )
        for: 30m
        labels:
          severity: warning
      - alert: KubeContainerWaiting
        annotations:
          description: Pod {{ $labels.namespace }}/{{ $labels.pod }} container {{ $labels.container}}
            has been in waiting state for longer than 1 hour.
          summary: Pod container waiting longer than 1 hour
        expr: |
          sum by (namespace, pod, container) (kube_pod_container_status_waiting_reason{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}) > 0
        for: 1h
        labels:
          severity: warning
      - alert: KubeDaemonSetNotScheduled
        annotations:
          description: '{{ $value }} Pods of DaemonSet {{ $labels.namespace }}/{{ $labels.daemonset
            }} are not scheduled.'
          summary: DaemonSet pods are not scheduled.
        expr: |
          kube_daemonset_status_desired_number_scheduled{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
            -
          kube_daemonset_status_current_number_scheduled{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"} > 0
        for: 10m
        labels:
          severity: warning
      - alert: KubeDaemonSetMisScheduled
        annotations:
          description: '{{ $value }} Pods of DaemonSet {{ $labels.namespace }}/{{ $labels.daemonset
            }} are running where they are not supposed to run.'
          summary: DaemonSet pods are misscheduled.
        expr: |
          kube_daemonset_status_number_misscheduled{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"} > 0
        for: 15m
        labels:
          severity: warning
      - alert: KubeJobCompletion
        annotations:
          description: Job {{ $labels.namespace }}/{{ $labels.job_name }} is taking more
            than 12 hours to complete.
          summary: Job did not complete in time
        expr: |
          kube_job_spec_completions{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"} - kube_job_status_succeeded{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}  > 0
        for: 12h
        labels:
          severity: warning
      - alert: KubeJobFailed
        annotations:
          description: Job {{ $labels.namespace }}/{{ $labels.job_name }} failed to complete.
            Removing failed job after investigation should clear this alert.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/KubeJobFailed.md
          summary: Job failed to complete.
        expr: |
          kube_job_failed{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}  > 0
        for: 15m
        labels:
          severity: warning
      - alert: KubeHpaReplicasMismatch
        annotations:
          description: HPA {{ $labels.namespace }}/{{ $labels.horizontalpodautoscaler  }}
            has not matched the desired number of replicas for longer than 15 minutes.
          summary: HPA has not matched descired number of replicas.
        expr: |
          (kube_horizontalpodautoscaler_status_desired_replicas{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
            !=
          kube_horizontalpodautoscaler_status_current_replicas{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"})
            and
          (kube_horizontalpodautoscaler_status_current_replicas{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
            >
          kube_horizontalpodautoscaler_spec_min_replicas{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"})
            and
          (kube_horizontalpodautoscaler_status_current_replicas{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
            <
          kube_horizontalpodautoscaler_spec_max_replicas{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"})
            and
          changes(kube_horizontalpodautoscaler_status_current_replicas{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}[15m]) == 0
        for: 15m
        labels:
          severity: warning
      - alert: KubeHpaMaxedOut
        annotations:
          description: HPA {{ $labels.namespace }}/{{ $labels.horizontalpodautoscaler  }}
            has been running at max replicas for longer than 15 minutes.
          summary: HPA is running at max replicas
        expr: |
          kube_horizontalpodautoscaler_status_current_replicas{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
            ==
          kube_horizontalpodautoscaler_spec_max_replicas{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
        for: 15m
        labels:
          severity: warning
    - name: kubernetes-resources
      rules:
      - alert: KubeCPUOvercommit
        annotations:
          description: Cluster has overcommitted CPU resource requests for Pods by {{
            $value }} CPU shares and cannot tolerate node failure.
          summary: Cluster has overcommitted CPU resource requests.
        expr: |
          sum(namespace_cpu:kube_pod_container_resource_requests:sum{}) - (sum(kube_node_status_allocatable{resource="cpu"}) - max(kube_node_status_allocatable{resource="cpu"})) > 0
          and
          (sum(kube_node_status_allocatable{resource="cpu"}) - max(kube_node_status_allocatable{resource="cpu"})) > 0
        for: 10m
        labels:
          namespace: kube-system
          severity: warning
      - alert: KubeMemoryOvercommit
        annotations:
          description: Cluster has overcommitted memory resource requests for Pods by
            {{ $value }} bytes and cannot tolerate node failure.
          summary: Cluster has overcommitted memory resource requests.
        expr: |
          sum(namespace_memory:kube_pod_container_resource_requests:sum{}) - (sum(kube_node_status_allocatable{resource="memory"}) - max(kube_node_status_allocatable{resource="memory"})) > 0
          and
          (sum(kube_node_status_allocatable{resource="memory"}) - max(kube_node_status_allocatable{resource="memory"})) > 0
        for: 10m
        labels:
          namespace: kube-system
          severity: warning
      - alert: KubeCPUQuotaOvercommit
        annotations:
          description: Cluster has overcommitted CPU resource requests for Namespaces.
          summary: Cluster has overcommitted CPU resource requests.
        expr: |
          sum(kube_resourcequota{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics", type="hard", resource="cpu"})
            /
          sum(kube_node_status_allocatable{resource="cpu"})
            > 1.5
        for: 5m
        labels:
          severity: warning
      - alert: KubeMemoryQuotaOvercommit
        annotations:
          description: Cluster has overcommitted memory resource requests for Namespaces.
          summary: Cluster has overcommitted memory resource requests.
        expr: |
          sum(kube_resourcequota{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics", type="hard", resource="memory"})
            /
          sum(kube_node_status_allocatable{resource="memory",job="kube-state-metrics"})
            > 1.5
        for: 5m
        labels:
          severity: warning
      - alert: KubeQuotaAlmostFull
        annotations:
          description: Namespace {{ $labels.namespace }} is using {{ $value | humanizePercentage
            }} of its {{ $labels.resource }} quota.
          summary: Namespace quota is going to be full.
        expr: |
          kube_resourcequota{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics", type="used"}
            / ignoring(instance, job, type)
          (kube_resourcequota{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics", type="hard"} > 0)
            > 0.9 < 1
        for: 15m
        labels:
          severity: info
      - alert: KubeQuotaFullyUsed
        annotations:
          description: Namespace {{ $labels.namespace }} is using {{ $value | humanizePercentage
            }} of its {{ $labels.resource }} quota.
          summary: Namespace quota is fully used.
        expr: |
          kube_resourcequota{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics", type="used"}
            / ignoring(instance, job, type)
          (kube_resourcequota{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics", type="hard"} > 0)
            == 1
        for: 15m
        labels:
          severity: info
      - alert: KubeQuotaExceeded
        annotations:
          description: Namespace {{ $labels.namespace }} is using {{ $value | humanizePercentage
            }} of its {{ $labels.resource }} quota.
          summary: Namespace quota has exceeded the limits.
        expr: |
          kube_resourcequota{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics", type="used"}
            / ignoring(instance, job, type)
          (kube_resourcequota{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics", type="hard"} > 0)
            > 1
        for: 15m
        labels:
          severity: warning
    - name: kubernetes-storage
      rules:
      - alert: KubePersistentVolumeFillingUp
        annotations:
          description: The PersistentVolume claimed by {{ $labels.persistentvolumeclaim
            }} in Namespace {{ $labels.namespace }} is only {{ $value | humanizePercentage
            }} free.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/KubePersistentVolumeFillingUp.md
          summary: PersistentVolume is filling up.
        expr: |
          (
            kubelet_volume_stats_available_bytes{namespace=~"(openshift-.*|kube-.*|default)",job="kubelet", metrics_path="/metrics"}
              /
            kubelet_volume_stats_capacity_bytes{namespace=~"(openshift-.*|kube-.*|default)",job="kubelet", metrics_path="/metrics"}
          ) < 0.03
          and
          kubelet_volume_stats_used_bytes{namespace=~"(openshift-.*|kube-.*|default)",job="kubelet", metrics_path="/metrics"} > 0
        for: 1m
        labels:
          severity: critical
      - alert: KubePersistentVolumeFillingUp
        annotations:
          description: Based on recent sampling, the PersistentVolume claimed by {{ $labels.persistentvolumeclaim
            }} in Namespace {{ $labels.namespace }} is expected to fill up within four
            days. Currently {{ $value | humanizePercentage }} is available.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/KubePersistentVolumeFillingUp.md
          summary: PersistentVolume is filling up.
        expr: |
          (
            kubelet_volume_stats_available_bytes{namespace=~"(openshift-.*|kube-.*|default)",job="kubelet", metrics_path="/metrics"}
              /
            kubelet_volume_stats_capacity_bytes{namespace=~"(openshift-.*|kube-.*|default)",job="kubelet", metrics_path="/metrics"}
          ) < 0.15
          and
          kubelet_volume_stats_used_bytes{namespace=~"(openshift-.*|kube-.*|default)",job="kubelet", metrics_path="/metrics"} > 0
          and
          predict_linear(kubelet_volume_stats_available_bytes{namespace=~"(openshift-.*|kube-.*|default)",job="kubelet", metrics_path="/metrics"}[6h], 4 * 24 * 3600) < 0
        for: 1h
        labels:
          severity: warning
      - alert: KubePersistentVolumeErrors
        annotations:
          description: The persistent volume {{ $labels.persistentvolume }} has status
            {{ $labels.phase }}.
          summary: PersistentVolume is having issues with provisioning.
        expr: |
          kube_persistentvolume_status_phase{phase=~"Failed|Pending",namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"} > 0
        for: 5m
        labels:
          severity: warning
    - name: kubernetes-system
      rules:
      - alert: KubeClientErrors
        annotations:
          description: Kubernetes API server client '{{ $labels.job }}/{{ $labels.instance
            }}' is experiencing {{ $value | humanizePercentage }} errors.'
          summary: Kubernetes API server client is experiencing errors.
        expr: |
          (sum(rate(rest_client_requests_total{code=~"5.."}[5m])) by (instance, job, namespace)
            /
          sum(rate(rest_client_requests_total[5m])) by (instance, job, namespace))
          > 0.01
        for: 15m
        labels:
          severity: warning
    - name: kubernetes-system-apiserver
      rules:
      - alert: AggregatedAPIErrors
        annotations:
          description: An aggregated API {{ $labels.name }}/{{ $labels.namespace }} has
            reported errors. It has appeared unavailable {{ $value | humanize }} times
            averaged over the past 10m.
          summary: An aggregated API has reported errors.
        expr: |
          sum by(name, namespace)(increase(aggregator_unavailable_apiservice_total[10m])) > 4
        labels:
          severity: warning
      - alert: AggregatedAPIDown
        annotations:
          description: An aggregated API {{ $labels.name }}/{{ $labels.namespace }} has
            been only {{ $value | humanize }}% available over the last 10m.
          summary: An aggregated API is down.
        expr: |
          (1 - max by(name, namespace)(avg_over_time(aggregator_unavailable_apiservice[10m]))) * 100 < 85
        for: 5m
        labels:
          severity: warning
      - alert: KubeAPIDown
        annotations:
          description: KubeAPI has disappeared from Prometheus target discovery.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/KubeAPIDown.md
          summary: Target disappeared from Prometheus target discovery.
        expr: |
          absent(up{job="apiserver"} == 1)
        for: 15m
        labels:
          severity: critical
      - alert: KubeAPITerminatedRequests
        annotations:
          description: The apiserver has terminated {{ $value | humanizePercentage }}
            of its incoming requests.
          summary: The apiserver has terminated {{ $value | humanizePercentage }} of its
            incoming requests.
        expr: |
          sum(rate(apiserver_request_terminations_total{job="apiserver"}[10m]))  / (  sum(rate(apiserver_request_total{job="apiserver"}[10m])) + sum(rate(apiserver_request_terminations_total{job="apiserver"}[10m])) ) > 0.20
        for: 5m
        labels:
          severity: warning
    - name: kubernetes-system-kubelet
      rules:
      - alert: KubeNodeNotReady
        annotations:
          description: '{{ $labels.node }} has been unready for more than 15 minutes.'
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/KubeNodeNotReady.md
          summary: Node is not ready.
        expr: |
          kube_node_status_condition{job="kube-state-metrics",condition="Ready",status="true"} == 0
        for: 15m
        labels:
          severity: warning
      - alert: KubeNodeUnreachable
        annotations:
          description: '{{ $labels.node }} is unreachable and some workloads may be rescheduled.'
          summary: Node is unreachable.
        expr: |
          (kube_node_spec_taint{job="kube-state-metrics",key="node.kubernetes.io/unreachable",effect="NoSchedule"} unless ignoring(key,value) kube_node_spec_taint{job="kube-state-metrics",key=~"ToBeDeletedByClusterAutoscaler|cloud.google.com/impending-node-termination|aws-node-termination-handler/spot-itn"}) == 1
        for: 15m
        labels:
          severity: warning
      - alert: KubeletTooManyPods
        annotations:
          description: Kubelet '{{ $labels.node }}' is running at {{ $value | humanizePercentage
            }} of its Pod capacity.
          summary: Kubelet is running at capacity.
        expr: |
          count by(node) (
            (kube_pod_status_phase{job="kube-state-metrics",phase="Running"} == 1) * on(instance,pod,namespace,cluster) group_left(node) topk by(instance,pod,namespace,cluster) (1, kube_pod_info{job="kube-state-metrics"})
          )
          /
          max by(node) (
            kube_node_status_capacity{job="kube-state-metrics",resource="pods"} != 1
          ) > 0.95
        for: 15m
        labels:
          severity: warning
      - alert: KubeNodeReadinessFlapping
        annotations:
          description: The readiness status of node {{ $labels.node }} has changed {{
            $value }} times in the last 15 minutes.
          summary: Node readiness status is flapping.
        expr: |
          sum(changes(kube_node_status_condition{status="true",condition="Ready"}[15m])) by (node) > 2
        for: 15m
        labels:
          severity: warning
      - alert: KubeletPlegDurationHigh
        annotations:
          description: The Kubelet Pod Lifecycle Event Generator has a 99th percentile
            duration of {{ $value }} seconds on node {{ $labels.node }}.
          summary: Kubelet Pod Lifecycle Event Generator is taking too long to relist.
        expr: |
          node_quantile:kubelet_pleg_relist_duration_seconds:histogram_quantile{quantile="0.99"} >= 10
        for: 5m
        labels:
          severity: warning
      - alert: KubeletPodStartUpLatencyHigh
        annotations:
          description: Kubelet Pod startup 99th percentile latency is {{ $value }} seconds
            on node {{ $labels.node }}.
          summary: Kubelet Pod startup latency is too high.
        expr: |
          histogram_quantile(0.99, sum(rate(kubelet_pod_worker_duration_seconds_bucket{job="kubelet", metrics_path="/metrics"}[5m])) by (instance, le)) * on(instance) group_left(node) kubelet_node_name{job="kubelet", metrics_path="/metrics"} > 60
        for: 15m
        labels:
          severity: warning
      - alert: KubeletClientCertificateRenewalErrors
        annotations:
          description: Kubelet on node {{ $labels.node }} has failed to renew its client
            certificate ({{ $value | humanize }} errors in the last 5 minutes).
          summary: Kubelet has failed to renew its client certificate.
        expr: |
          increase(kubelet_certificate_manager_client_expiration_renew_errors[5m]) > 0
        for: 15m
        labels:
          severity: warning
      - alert: KubeletServerCertificateRenewalErrors
        annotations:
          description: Kubelet on node {{ $labels.node }} has failed to renew its server
            certificate ({{ $value | humanize }} errors in the last 5 minutes).
          summary: Kubelet has failed to renew its server certificate.
        expr: |
          increase(kubelet_server_expiration_renew_errors[5m]) > 0
        for: 15m
        labels:
          severity: warning
      - alert: KubeletDown
        annotations:
          description: Kubelet has disappeared from Prometheus target discovery.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/KubeletDown.md
          summary: Target disappeared from Prometheus target discovery.
        expr: |
          absent(up{job="kubelet", metrics_path="/metrics"} == 1)
        for: 15m
        labels:
          namespace: kube-system
          severity: critical
    - name: k8s.rules
      rules:
      - expr: |
          sum by (cluster, namespace, pod, container) (
            irate(container_cpu_usage_seconds_total{job="kubelet", metrics_path="/metrics/cadvisor", image!=""}[5m])
          ) * on (cluster, namespace, pod) group_left(node) topk by (cluster, namespace, pod) (
            1, max by(cluster, namespace, pod, node) (kube_pod_info{node!=""})
          )
        record: node_namespace_pod_container:container_cpu_usage_seconds_total:sum_irate
      - expr: |
          container_memory_working_set_bytes{job="kubelet", metrics_path="/metrics/cadvisor", image!=""}
          * on (namespace, pod) group_left(node) topk by(namespace, pod) (1,
            max by(namespace, pod, node) (kube_pod_info{node!=""})
          )
        record: node_namespace_pod_container:container_memory_working_set_bytes
      - expr: |
          container_memory_rss{job="kubelet", metrics_path="/metrics/cadvisor", image!=""}
          * on (namespace, pod) group_left(node) topk by(namespace, pod) (1,
            max by(namespace, pod, node) (kube_pod_info{node!=""})
          )
        record: node_namespace_pod_container:container_memory_rss
      - expr: |
          container_memory_cache{job="kubelet", metrics_path="/metrics/cadvisor", image!=""}
          * on (namespace, pod) group_left(node) topk by(namespace, pod) (1,
            max by(namespace, pod, node) (kube_pod_info{node!=""})
          )
        record: node_namespace_pod_container:container_memory_cache
      - expr: |
          container_memory_swap{job="kubelet", metrics_path="/metrics/cadvisor", image!=""}
          * on (namespace, pod) group_left(node) topk by(namespace, pod) (1,
            max by(namespace, pod, node) (kube_pod_info{node!=""})
          )
        record: node_namespace_pod_container:container_memory_swap
      - expr: |
          kube_pod_container_resource_requests{resource="memory",job="kube-state-metrics"}  * on (namespace, pod, cluster)
          group_left() max by (namespace, pod) (
            (kube_pod_status_phase{phase=~"Pending|Running"} == 1)
          )
        record: cluster:namespace:pod_memory:active:kube_pod_container_resource_requests
      - expr: |
          sum by (namespace, cluster) (
              sum by (namespace, pod, cluster) (
                  max by (namespace, pod, container, cluster) (
                    kube_pod_container_resource_requests{resource="memory",job="kube-state-metrics"}
                  ) * on(namespace, pod, cluster) group_left() max by (namespace, pod, cluster) (
                    kube_pod_status_phase{phase=~"Pending|Running"} == 1
                  )
              )
          )
        record: namespace_memory:kube_pod_container_resource_requests:sum
      - expr: |
          kube_pod_container_resource_requests{resource="cpu",job="kube-state-metrics"}  * on (namespace, pod, cluster)
          group_left() max by (namespace, pod) (
            (kube_pod_status_phase{phase=~"Pending|Running"} == 1)
          )
        record: cluster:namespace:pod_cpu:active:kube_pod_container_resource_requests
      - expr: |
          sum by (namespace, cluster) (
              sum by (namespace, pod, cluster) (
                  max by (namespace, pod, container, cluster) (
                    kube_pod_container_resource_requests{resource="cpu",job="kube-state-metrics"}
                  ) * on(namespace, pod, cluster) group_left() max by (namespace, pod, cluster) (
                    kube_pod_status_phase{phase=~"Pending|Running"} == 1
                  )
              )
          )
        record: namespace_cpu:kube_pod_container_resource_requests:sum
      - expr: |
          kube_pod_container_resource_limits{resource="memory",job="kube-state-metrics"}  * on (namespace, pod, cluster)
          group_left() max by (namespace, pod) (
            (kube_pod_status_phase{phase=~"Pending|Running"} == 1)
          )
        record: cluster:namespace:pod_memory:active:kube_pod_container_resource_limits
      - expr: |
          sum by (namespace, cluster) (
              sum by (namespace, pod, cluster) (
                  max by (namespace, pod, container, cluster) (
                    kube_pod_container_resource_limits{resource="memory",job="kube-state-metrics"}
                  ) * on(namespace, pod, cluster) group_left() max by (namespace, pod, cluster) (
                    kube_pod_status_phase{phase=~"Pending|Running"} == 1
                  )
              )
          )
        record: namespace_memory:kube_pod_container_resource_limits:sum
      - expr: |
          kube_pod_container_resource_limits{resource="cpu",job="kube-state-metrics"}  * on (namespace, pod, cluster)
          group_left() max by (namespace, pod) (
           (kube_pod_status_phase{phase=~"Pending|Running"} == 1)
           )
        record: cluster:namespace:pod_cpu:active:kube_pod_container_resource_limits
      - expr: |
          sum by (namespace, cluster) (
              sum by (namespace, pod, cluster) (
                  max by (namespace, pod, container, cluster) (
                    kube_pod_container_resource_limits{resource="cpu",job="kube-state-metrics"}
                  ) * on(namespace, pod, cluster) group_left() max by (namespace, pod, cluster) (
                    kube_pod_status_phase{phase=~"Pending|Running"} == 1
                  )
              )
          )
        record: namespace_cpu:kube_pod_container_resource_limits:sum
      - expr: |
          max by (cluster, namespace, workload, pod) (
            label_replace(
              label_replace(
                kube_pod_owner{job="kube-state-metrics", owner_kind="ReplicaSet"},
                "replicaset", "$1", "owner_name", "(.*)"
              ) * on(replicaset, namespace) group_left(owner_name) topk by(replicaset, namespace) (
                1, max by (replicaset, namespace, owner_name) (
                  kube_replicaset_owner{job="kube-state-metrics"}
                )
              ),
              "workload", "$1", "owner_name", "(.*)"
            )
          )
        labels:
          workload_type: deployment
        record: namespace_workload_pod:kube_pod_owner:relabel
      - expr: |
          max by (cluster, namespace, workload, pod) (
            label_replace(
              kube_pod_owner{job="kube-state-metrics", owner_kind="DaemonSet"},
              "workload", "$1", "owner_name", "(.*)"
            )
          )
        labels:
          workload_type: daemonset
        record: namespace_workload_pod:kube_pod_owner:relabel
      - expr: |
          max by (cluster, namespace, workload, pod) (
            label_replace(
              kube_pod_owner{job="kube-state-metrics", owner_kind="StatefulSet"},
              "workload", "$1", "owner_name", "(.*)"
            )
          )
        labels:
          workload_type: statefulset
        record: namespace_workload_pod:kube_pod_owner:relabel
    - name: kube-scheduler.rules
      rules:
      - expr: |
          histogram_quantile(0.99, sum(rate(scheduler_e2e_scheduling_duration_seconds_bucket{job="scheduler"}[5m])) without(instance, pod))
        labels:
          quantile: "0.99"
        record: cluster_quantile:scheduler_e2e_scheduling_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.99, sum(rate(scheduler_scheduling_algorithm_duration_seconds_bucket{job="scheduler"}[5m])) without(instance, pod))
        labels:
          quantile: "0.99"
        record: cluster_quantile:scheduler_scheduling_algorithm_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.99, sum(rate(scheduler_binding_duration_seconds_bucket{job="scheduler"}[5m])) without(instance, pod))
        labels:
          quantile: "0.99"
        record: cluster_quantile:scheduler_binding_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.9, sum(rate(scheduler_e2e_scheduling_duration_seconds_bucket{job="scheduler"}[5m])) without(instance, pod))
        labels:
          quantile: "0.9"
        record: cluster_quantile:scheduler_e2e_scheduling_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.9, sum(rate(scheduler_scheduling_algorithm_duration_seconds_bucket{job="scheduler"}[5m])) without(instance, pod))
        labels:
          quantile: "0.9"
        record: cluster_quantile:scheduler_scheduling_algorithm_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.9, sum(rate(scheduler_binding_duration_seconds_bucket{job="scheduler"}[5m])) without(instance, pod))
        labels:
          quantile: "0.9"
        record: cluster_quantile:scheduler_binding_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.5, sum(rate(scheduler_e2e_scheduling_duration_seconds_bucket{job="scheduler"}[5m])) without(instance, pod))
        labels:
          quantile: "0.5"
        record: cluster_quantile:scheduler_e2e_scheduling_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.5, sum(rate(scheduler_scheduling_algorithm_duration_seconds_bucket{job="scheduler"}[5m])) without(instance, pod))
        labels:
          quantile: "0.5"
        record: cluster_quantile:scheduler_scheduling_algorithm_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.5, sum(rate(scheduler_binding_duration_seconds_bucket{job="scheduler"}[5m])) without(instance, pod))
        labels:
          quantile: "0.5"
        record: cluster_quantile:scheduler_binding_duration_seconds:histogram_quantile
    - name: node.rules
      rules:
      - expr: |
          topk by(namespace, pod) (1,
            max by (node, namespace, pod) (
              label_replace(kube_pod_info{job="kube-state-metrics",node!=""}, "pod", "$1", "pod", "(.*)")
          ))
        record: 'node_namespace_pod:kube_pod_info:'
      - expr: |
          sum(
            node_memory_MemAvailable_bytes{job="node-exporter"} or
            (
              node_memory_Buffers_bytes{job="node-exporter"} +
              node_memory_Cached_bytes{job="node-exporter"} +
              node_memory_MemFree_bytes{job="node-exporter"} +
              node_memory_Slab_bytes{job="node-exporter"}
            )
          ) by (cluster)
        record: :node_memory_MemAvailable_bytes:sum
    - name: kubelet.rules
      rules:
      - expr: |
          histogram_quantile(0.99, sum(rate(kubelet_pleg_relist_duration_seconds_bucket[5m])) by (instance, le) * on(instance) group_left(node) kubelet_node_name{job="kubelet", metrics_path="/metrics"})
        labels:
          quantile: "0.99"
        record: node_quantile:kubelet_pleg_relist_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.9, sum(rate(kubelet_pleg_relist_duration_seconds_bucket[5m])) by (instance, le) * on(instance) group_left(node) kubelet_node_name{job="kubelet", metrics_path="/metrics"})
        labels:
          quantile: "0.9"
        record: node_quantile:kubelet_pleg_relist_duration_seconds:histogram_quantile
      - expr: |
          histogram_quantile(0.5, sum(rate(kubelet_pleg_relist_duration_seconds_bucket[5m])) by (instance, le) * on(instance) group_left(node) kubelet_node_name{job="kubelet", metrics_path="/metrics"})
        labels:
          quantile: "0.5"
        record: node_quantile:kubelet_pleg_relist_duration_seconds:histogram_quantile
  openshift-monitoring-node-exporter-rules.yaml: |
    groups:
    - name: node-exporter
      rules:
      - alert: NodeFilesystemSpaceFillingUp
        annotations:
          description: Filesystem on {{ $labels.device }} at {{ $labels.instance }} has
            only {{ printf "%.2f" $value }}% available space left and is filling up.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/NodeFilesystemSpaceFillingUp.md
          summary: Filesystem is predicted to run out of space within the next 24 hours.
        expr: |
          (
            node_filesystem_avail_bytes{job="node-exporter",fstype!=""} / node_filesystem_size_bytes{job="node-exporter",fstype!=""} * 100 < 40
          and
            predict_linear(node_filesystem_avail_bytes{job="node-exporter",fstype!=""}[6h], 24*60*60) < 0
          and
            node_filesystem_readonly{job="node-exporter",fstype!=""} == 0
          )
        for: 1h
        labels:
          severity: warning
      - alert: NodeFilesystemSpaceFillingUp
        annotations:
          description: Filesystem on {{ $labels.device }} at {{ $labels.instance }} has
            only {{ printf "%.2f" $value }}% available space left and is filling up fast.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/NodeFilesystemSpaceFillingUp.md
          summary: Filesystem is predicted to run out of space within the next 4 hours.
        expr: |
          (
            node_filesystem_avail_bytes{job="node-exporter",fstype!=""} / node_filesystem_size_bytes{job="node-exporter",fstype!=""} * 100 < 15
          and
            predict_linear(node_filesystem_avail_bytes{job="node-exporter",fstype!=""}[6h], 4*60*60) < 0
          and
            node_filesystem_readonly{job="node-exporter",fstype!=""} == 0
          )
        for: 1h
        labels:
          severity: critical
      - alert: NodeFilesystemAlmostOutOfSpace
        annotations:
          description: Filesystem on {{ $labels.device }} at {{ $labels.instance }} has
            only {{ printf "%.2f" $value }}% available space left.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/NodeFilesystemAlmostOutOfSpace.md
          summary: Filesystem has less than 5% space left.
        expr: |
          (
            node_filesystem_avail_bytes{job="node-exporter",fstype!=""} / node_filesystem_size_bytes{job="node-exporter",fstype!=""} * 100 < 5
          and
            node_filesystem_readonly{job="node-exporter",fstype!=""} == 0
          )
        for: 30m
        labels:
          severity: warning
      - alert: NodeFilesystemAlmostOutOfSpace
        annotations:
          description: Filesystem on {{ $labels.device }} at {{ $labels.instance }} has
            only {{ printf "%.2f" $value }}% available space left.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/NodeFilesystemAlmostOutOfSpace.md
          summary: Filesystem has less than 3% space left.
        expr: |
          (
            node_filesystem_avail_bytes{job="node-exporter",fstype!=""} / node_filesystem_size_bytes{job="node-exporter",fstype!=""} * 100 < 3
          and
            node_filesystem_readonly{job="node-exporter",fstype!=""} == 0
          )
        for: 30m
        labels:
          severity: critical
      - alert: NodeFilesystemFilesFillingUp
        annotations:
          description: Filesystem on {{ $labels.device }} at {{ $labels.instance }} has
            only {{ printf "%.2f" $value }}% available inodes left and is filling up.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/NodeFilesystemFilesFillingUp.md
          summary: Filesystem is predicted to run out of inodes within the next 24 hours.
        expr: |
          (
            node_filesystem_files_free{job="node-exporter",fstype!=""} / node_filesystem_files{job="node-exporter",fstype!=""} * 100 < 40
          and
            predict_linear(node_filesystem_files_free{job="node-exporter",fstype!=""}[6h], 24*60*60) < 0
          and
            node_filesystem_readonly{job="node-exporter",fstype!=""} == 0
          )
        for: 1h
        labels:
          severity: warning
      - alert: NodeFilesystemFilesFillingUp
        annotations:
          description: Filesystem on {{ $labels.device }} at {{ $labels.instance }} has
            only {{ printf "%.2f" $value }}% available inodes left and is filling up fast.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/NodeFilesystemFilesFillingUp.md
          summary: Filesystem is predicted to run out of inodes within the next 4 hours.
        expr: |
          (
            node_filesystem_files_free{job="node-exporter",fstype!=""} / node_filesystem_files{job="node-exporter",fstype!=""} * 100 < 20
          and
            predict_linear(node_filesystem_files_free{job="node-exporter",fstype!=""}[6h], 4*60*60) < 0
          and
            node_filesystem_readonly{job="node-exporter",fstype!=""} == 0
          )
        for: 1h
        labels:
          severity: critical
      - alert: NodeFilesystemAlmostOutOfFiles
        annotations:
          description: Filesystem on {{ $labels.device }} at {{ $labels.instance }} has
            only {{ printf "%.2f" $value }}% available inodes left.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/NodeFilesystemAlmostOutOfFiles.md
          summary: Filesystem has less than 5% inodes left.
        expr: |
          (
            node_filesystem_files_free{job="node-exporter",fstype!=""} / node_filesystem_files{job="node-exporter",fstype!=""} * 100 < 5
          and
            node_filesystem_readonly{job="node-exporter",fstype!=""} == 0
          )
        for: 1h
        labels:
          severity: warning
      - alert: NodeFilesystemAlmostOutOfFiles
        annotations:
          description: Filesystem on {{ $labels.device }} at {{ $labels.instance }} has
            only {{ printf "%.2f" $value }}% available inodes left.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/NodeFilesystemAlmostOutOfFiles.md
          summary: Filesystem has less than 3% inodes left.
        expr: |
          (
            node_filesystem_files_free{job="node-exporter",fstype!=""} / node_filesystem_files{job="node-exporter",fstype!=""} * 100 < 3
          and
            node_filesystem_readonly{job="node-exporter",fstype!=""} == 0
          )
        for: 1h
        labels:
          severity: critical
      - alert: NodeNetworkReceiveErrs
        annotations:
          description: '{{ $labels.instance }} interface {{ $labels.device }} has encountered
            {{ printf "%.0f" $value }} receive errors in the last two minutes.'
          summary: Network interface is reporting many receive errors.
        expr: |
          rate(node_network_receive_errs_total[2m]) / rate(node_network_receive_packets_total[2m]) > 0.01
        for: 1h
        labels:
          severity: warning
      - alert: NodeNetworkTransmitErrs
        annotations:
          description: '{{ $labels.instance }} interface {{ $labels.device }} has encountered
            {{ printf "%.0f" $value }} transmit errors in the last two minutes.'
          summary: Network interface is reporting many transmit errors.
        expr: |
          rate(node_network_transmit_errs_total[2m]) / rate(node_network_transmit_packets_total[2m]) > 0.01
        for: 1h
        labels:
          severity: warning
      - alert: NodeHighNumberConntrackEntriesUsed
        annotations:
          description: '{{ $value | humanizePercentage }} of conntrack entries are used.'
          summary: Number of conntrack are getting close to the limit.
        expr: |
          (node_nf_conntrack_entries / node_nf_conntrack_entries_limit) > 0.75
        labels:
          severity: warning
      - alert: NodeTextFileCollectorScrapeError
        annotations:
          description: Node Exporter text file collector failed to scrape.
          summary: Node Exporter text file collector failed to scrape.
        expr: |
          node_textfile_scrape_error{job="node-exporter"} == 1
        labels:
          severity: warning
      - alert: NodeClockSkewDetected
        annotations:
          description: Clock on {{ $labels.instance }} is out of sync by more than 300s.
            Ensure NTP is configured correctly on this host.
          summary: Clock skew detected.
        expr: |
          (
            node_timex_offset_seconds > 0.05
          and
            deriv(node_timex_offset_seconds[5m]) >= 0
          )
          or
          (
            node_timex_offset_seconds < -0.05
          and
            deriv(node_timex_offset_seconds[5m]) <= 0
          )
        for: 10m
        labels:
          severity: warning
      - alert: NodeClockNotSynchronising
        annotations:
          description: Clock on {{ $labels.instance }} is not synchronising. Ensure NTP
            is configured on this host.
          summary: Clock not synchronising.
        expr: |
          min_over_time(node_timex_sync_status[5m]) == 0
          and
          node_timex_maxerror_seconds >= 16
        for: 10m
        labels:
          severity: warning
      - alert: NodeRAIDDegraded
        annotations:
          description: RAID array '{{ $labels.device }}' on {{ $labels.instance }} is
            in degraded state due to one or more disks failures. Number of spare drives
            is insufficient to fix issue automatically.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/NodeRAIDDegraded.md
          summary: RAID Array is degraded
        expr: |
          node_md_disks_required - ignoring (state) (node_md_disks{state="active"}) > 0
        for: 15m
        labels:
          severity: critical
      - alert: NodeRAIDDiskFailure
        annotations:
          description: At least one device in RAID array on {{ $labels.instance }} failed.
            Array '{{ $labels.device }}' needs attention and possibly a disk swap.
          summary: Failed device in RAID array
        expr: |
          node_md_disks{state="failed"} > 0
        labels:
          severity: warning
      - alert: NodeFileDescriptorLimit
        annotations:
          description: File descriptors limit at {{ $labels.instance }} is currently at
            {{ printf "%.2f" $value }}%.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/NodeFileDescriptorLimit.md
          summary: Kernel is predicted to exhaust file descriptors limit soon.
        expr: |
          (
            node_filefd_allocated{job="node-exporter"} * 100 / node_filefd_maximum{job="node-exporter"} > 70
          )
        for: 15m
        labels:
          severity: warning
      - alert: NodeFileDescriptorLimit
        annotations:
          description: File descriptors limit at {{ $labels.instance }} is currently at
            {{ printf "%.2f" $value }}%.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/NodeFileDescriptorLimit.md
          summary: Kernel is predicted to exhaust file descriptors limit soon.
        expr: |
          (
            node_filefd_allocated{job="node-exporter"} * 100 / node_filefd_maximum{job="node-exporter"} > 90
          )
        for: 15m
        labels:
          severity: critical
    - name: node-exporter.rules
      rules:
      - expr: |
          count without (cpu) (
            count without (mode) (
              node_cpu_seconds_total{job="node-exporter"}
            )
          )
        record: instance:node_num_cpu:sum
      - expr: |
          1 - avg without (cpu, mode) (
            rate(node_cpu_seconds_total{job="node-exporter", mode="idle"}[1m])
          )
        record: instance:node_cpu_utilisation:rate1m
      - expr: |
          (
            node_load1{job="node-exporter"}
          /
            instance:node_num_cpu:sum{job="node-exporter"}
          )
        record: instance:node_load1_per_cpu:ratio
      - expr: |
          1 - (
            node_memory_MemAvailable_bytes{job="node-exporter"}
          /
            node_memory_MemTotal_bytes{job="node-exporter"}
          )
        record: instance:node_memory_utilisation:ratio
      - expr: |
          rate(node_vmstat_pgmajfault{job="node-exporter"}[1m])
        record: instance:node_vmstat_pgmajfault:rate1m
      - expr: |
          rate(node_disk_io_time_seconds_total{job="node-exporter", device=~"mmcblk.p.+|nvme.+|sd.+|vd.+|xvd.+|dm-.+|dasd.+"}[1m])
        record: instance_device:node_disk_io_time_seconds:rate1m
      - expr: |
          rate(node_disk_io_time_weighted_seconds_total{job="node-exporter", device=~"mmcblk.p.+|nvme.+|sd.+|vd.+|xvd.+|dm-.+|dasd.+"}[1m])
        record: instance_device:node_disk_io_time_weighted_seconds:rate1m
      - expr: |
          sum without (device) (
            rate(node_network_receive_bytes_total{job="node-exporter", device!="lo"}[1m])
          )
        record: instance:node_network_receive_bytes_excluding_lo:rate1m
      - expr: |
          sum without (device) (
            rate(node_network_transmit_bytes_total{job="node-exporter", device!="lo"}[1m])
          )
        record: instance:node_network_transmit_bytes_excluding_lo:rate1m
      - expr: |
          sum without (device) (
            rate(node_network_receive_drop_total{job="node-exporter", device!="lo"}[1m])
          )
        record: instance:node_network_receive_drop_excluding_lo:rate1m
      - expr: |
          sum without (device) (
            rate(node_network_transmit_drop_total{job="node-exporter", device!="lo"}[1m])
          )
        record: instance:node_network_transmit_drop_excluding_lo:rate1m
  openshift-kube-apiserver-operator-kube-apiserver-operator.yaml: |
    groups:
    - name: cluster-version
      rules:
      - alert: TechPreviewNoUpgrade
        annotations:
          message: Cluster has enabled tech preview features that will prevent upgrades.
        expr: |
          cluster_feature_set{name!="", namespace="openshift-kube-apiserver-operator"} == 0
        for: 10m
        labels:
          severity: warning
  openshift-cluster-node-tuning-operator-node-tuning-operator.yaml: |
    groups:
    - name: node-tuning-operator.rules
      rules:
      - alert: NTOPodsNotReady
        annotations:
          description: |
            Pod {{ $labels.pod }} is not ready.
            Review the "Event" objects in "openshift-cluster-node-tuning-operator" namespace for further details.
          summary: Pod {{ $labels.pod }} is not ready.
        expr: |
          kube_pod_status_ready{namespace='openshift-cluster-node-tuning-operator', condition='true'} == 0
        for: 30m
        labels:
          severity: warning
      - alert: NTODegraded
        annotations:
          description: The Node Tuning Operator is degraded. Review the "node-tuning"
            ClusterOperator object for further details.
          summary: The Node Tuning Operator is degraded.
        expr: nto_degraded_info == 1
        for: 2h
        labels:
          severity: warning
      - expr: count(nto_profile_calculated_total{profile!~"openshift-node",profile!~"openshift-control-plane",profile!~"openshift"})
        record: nto_custom_profiles:count
  openshift-kube-apiserver-cpu-utilization.yaml: |
    groups:
    - name: control-plane-cpu-utilization
      rules:
      - alert: HighOverallControlPlaneCPU
        annotations:
          message: Given three control plane nodes, the overall CPU utilization may only
            be about 2/3 of all available capacity. This is because if a single control
            plane node fails, the remaining two must handle the load of the cluster in
            order to be HA. If the cluster is using more than 2/3 of all capacity, if
            one control plane node fails, the remaining two are likely to fail when they
            take the load. To fix this, increase the CPU and memory on your control plane
            nodes.
          summary: CPU utilization across all three control plane nodes is higher than
            two control plane nodes can sustain; a single control plane node outage may
            cause a cascading failure; increase available CPU.
        expr: |
          sum(
            100 - (avg by (instance) (rate(node_cpu_seconds_total{mode="idle"}[1m])) * 100)
            AND on (instance) label_replace( kube_node_role{role="master"}, "instance", "$1", "node", "(.+)" )
          )
          /
          count(kube_node_role{role="master"})
          > 60
        for: 10m
        labels:
          namespace: openshift-kube-apiserver
          severity: warning
      - alert: ExtremelyHighIndividualControlPlaneCPU
        annotations:
          message: Extreme CPU pressure can cause slow serialization and poor performance
            from the kube-apiserver and etcd. When this happens, there is a risk of clients
            seeing non-responsive API requests which are issued again causing even more
            CPU pressure. It can also cause failing liveness probes due to slow etcd responsiveness
            on the backend. If one kube-apiserver fails under this condition, chances
            are you will experience a cascade as the remaining kube-apiservers are also
            under-provisioned. To fix this, increase the CPU and memory on your control
            plane nodes.
          summary: CPU utilization on a single control plane node is very high, more CPU
            pressure is likely to cause a failover; increase available CPU.
        expr: |
          100 - (avg by (instance) (rate(node_cpu_seconds_total{mode="idle"}[1m])) * 100) > 90 AND on (instance) label_replace( kube_node_role{role="master"}, "instance", "$1", "node", "(.+)" )
        for: 5m
        labels:
          namespace: openshift-kube-apiserver
          severity: critical
  openshift-cloud-credential-operator-cloud-credential-operator-alerts.yaml: |
    groups:
    - name: CloudCredentialOperator
      rules:
      - alert: CloudCredentialOperatorTargetNamespaceMissing
        annotations:
          description: At least one CredentialsRequest custom resource has specified in
            its .spec.secretRef.namespace field a namespace which does not presently exist.
            This means the Cloud Credential Operator in the openshift-cloud-credential-operator
            namespace cannot process the CredentialsRequest resource. Check the conditions
            of all CredentialsRequests with 'oc get credentialsrequest -A' to find any
            CredentialsRequest(s) with a .status.condition showing a condition type of
            MissingTargetNamespace set to True.
          message: CredentialsRequest(s) pointing to non-existent namespace
          summary: One ore more CredentialsRequest CRs are asking to save credentials
            to a non-existent namespace.
        expr: cco_credentials_requests_conditions{condition="MissingTargetNamespace"}
          > 0
        for: 5m
        labels:
          severity: warning
      - alert: CloudCredentialOperatorProvisioningFailed
        annotations:
          description: While processing a CredentialsRequest, the Cloud Credential Operator
            encountered an issue. Check the conditions of all CredentialsRequets with
            'oc get credentialsrequest -A' to find any CredentialsRequest(s) with a .stats.condition
            showing a condition type of CredentialsProvisionFailure set to True for more
            details on the issue.
          message: CredentialsRequest(s) unable to be fulfilled
          summary: One or more CredentialsRequest CRs are unable to be processed.
        expr: cco_credentials_requests_conditions{condition="CredentialsProvisionFailure"}
          > 0
        for: 5m
        labels:
          severity: warning
      - alert: CloudCredentialOperatorDeprovisioningFailed
        annotations:
          description: While processing a CredentialsRequest marked for deletion, the
            Cloud Credential Operator encountered an issue. Check the conditions of all
            CredentialsRequests with 'oc get credentialsrequest -A' to find any CredentialsRequest(s)
            with a .status.condition showing a condition type of CredentialsDeprovisionFailure
            set to True for more details on the issue.
          message: CredentialsRequest(s) unable to be cleaned up
          summary: One or more CredentialsRequest CRs are unable to be deleted.
        expr: cco_credentials_requests_conditions{condition="CredentialsDeprovisionFailure"}
          > 0
        for: 5m
        labels:
          severity: warning
      - alert: CloudCredentialOperatorInsufficientCloudCreds
        annotations:
          description: The Cloud Credential Operator has determined that there are insufficient
            permissions to process one or more CredentialsRequest CRs. Check the conditions
            of all CredentialsRequests with 'oc get credentialsrequest -A' to find any
            CredentialsRequest(s) with a .status.condition showing a condition type of
            InsufficientCloudCreds set to True for more details.
          message: Cluster's cloud credentials insufficient for minting or passthrough
          summary: Problem with the available platform credentials.
        expr: cco_credentials_requests_conditions{condition="InsufficientCloudCreds"}
          > 0
        for: 5m
        labels:
          severity: warning
      - alert: CloudCredentialOperatorStaleCredentials
        annotations:
          message: 1 or more credentials requests are stale and should be deleted. Check
            the status.conditions on CredentialsRequest CRs to identify the stale one(s).
        expr: cco_credentials_requests_conditions{condition="StaleCredentials"} > 0
        for: 5m
        labels:
          severity: warning
  openshift-monitoring-kube-state-metrics-rules.yaml: |
    groups:
    - name: kube-state-metrics
      rules:
      - alert: KubeStateMetricsListErrors
        annotations:
          description: kube-state-metrics is experiencing errors at an elevated rate in
            list operations. This is likely causing it to not be able to expose metrics
            about Kubernetes objects correctly or at all.
          summary: kube-state-metrics is experiencing errors in list operations.
        expr: |
          (sum(rate(kube_state_metrics_list_total{job="kube-state-metrics",result="error"}[5m]))
            /
          sum(rate(kube_state_metrics_list_total{job="kube-state-metrics"}[5m])))
          > 0.01
        for: 15m
        labels:
          severity: warning
      - alert: KubeStateMetricsWatchErrors
        annotations:
          description: kube-state-metrics is experiencing errors at an elevated rate in
            watch operations. This is likely causing it to not be able to expose metrics
            about Kubernetes objects correctly or at all.
          summary: kube-state-metrics is experiencing errors in watch operations.
        expr: |
          (sum(rate(kube_state_metrics_watch_total{job="kube-state-metrics",result="error"}[5m]))
            /
          sum(rate(kube_state_metrics_watch_total{job="kube-state-metrics"}[5m])))
          > 0.01
        for: 15m
        labels:
          severity: warning
  openshift-monitoring-prometheus-operator-rules.yaml: |
    groups:
    - name: prometheus-operator
      rules:
      - alert: PrometheusOperatorListErrors
        annotations:
          description: Errors while performing List operations in controller {{$labels.controller}}
            in {{$labels.namespace}} namespace.
          summary: Errors while performing list operations in controller.
        expr: |
          (sum by (controller,namespace) (rate(prometheus_operator_list_operations_failed_total{job="prometheus-operator",namespace="openshift-monitoring"}[10m])) / sum by (controller,namespace) (rate(prometheus_operator_list_operations_total{job="prometheus-operator",namespace="openshift-monitoring"}[10m]))) > 0.4
        for: 15m
        labels:
          severity: warning
      - alert: PrometheusOperatorWatchErrors
        annotations:
          description: Errors while performing watch operations in controller {{$labels.controller}}
            in {{$labels.namespace}} namespace.
          summary: Errors while performing watch operations in controller.
        expr: |
          (sum by (controller,namespace) (rate(prometheus_operator_watch_operations_failed_total{job="prometheus-operator",namespace="openshift-monitoring"}[10m])) / sum by (controller,namespace) (rate(prometheus_operator_watch_operations_total{job="prometheus-operator",namespace="openshift-monitoring"}[10m]))) > 0.4
        for: 15m
        labels:
          severity: warning
      - alert: PrometheusOperatorSyncFailed
        annotations:
          description: Controller {{ $labels.controller }} in {{ $labels.namespace }}
            namespace fails to reconcile {{ $value }} objects.
          summary: Last controller reconciliation failed
        expr: |
          min_over_time(prometheus_operator_syncs{status="failed",job="prometheus-operator",namespace="openshift-monitoring"}[5m]) > 0
        for: 10m
        labels:
          severity: warning
      - alert: PrometheusOperatorReconcileErrors
        annotations:
          description: '{{ $value | humanizePercentage }} of reconciling operations failed
            for {{ $labels.controller }} controller in {{ $labels.namespace }} namespace.'
          summary: Errors while reconciling controller.
        expr: |
          (sum by (controller,namespace) (rate(prometheus_operator_reconcile_errors_total{job="prometheus-operator",namespace="openshift-monitoring"}[5m]))) / (sum by (controller,namespace) (rate(prometheus_operator_reconcile_operations_total{job="prometheus-operator",namespace="openshift-monitoring"}[5m]))) > 0.1
        for: 10m
        labels:
          severity: warning
      - alert: PrometheusOperatorNodeLookupErrors
        annotations:
          description: Errors while reconciling Prometheus in {{ $labels.namespace }}
            Namespace.
          summary: Errors while reconciling Prometheus.
        expr: |
          rate(prometheus_operator_node_address_lookup_errors_total{job="prometheus-operator",namespace="openshift-monitoring"}[5m]) > 0.1
        for: 10m
        labels:
          severity: warning
      - alert: PrometheusOperatorNotReady
        annotations:
          description: Prometheus operator in {{ $labels.namespace }} namespace isn't
            ready to reconcile {{ $labels.controller }} resources.
          summary: Prometheus operator not ready
        expr: |
          min by(namespace, controller) (max_over_time(prometheus_operator_ready{job="prometheus-operator",namespace="openshift-monitoring"}[5m]) == 0)
        for: 5m
        labels:
          severity: warning
      - alert: PrometheusOperatorRejectedResources
        annotations:
          description: Prometheus operator in {{ $labels.namespace }} namespace rejected
            {{ printf "%0.0f" $value }} {{ $labels.controller }}/{{ $labels.resource }}
            resources.
          summary: Resources rejected by Prometheus operator
        expr: |
          min_over_time(prometheus_operator_managed_resources{state="rejected",job="prometheus-operator",namespace="openshift-monitoring"}[5m]) > 0
        for: 5m
        labels:
          severity: warning
  openshift-cluster-samples-operator-samples-operator-alerts.yaml: |
    groups:
    - name: SamplesOperator
      rules:
      - alert: SamplesRetriesMissingOnImagestreamImportFailing
        annotations:
          message: |
            Samples operator is detecting problems with imagestream image imports, and the periodic retries of those
            imports are not occurring.  Contact support.  You can look at the "openshift-samples" ClusterOperator object
            for details. Most likely there are issues with the external image registry hosting the images that need to
            be investigated.  The list of ImageStreams that have failing imports are:
            {{ range query "openshift_samples_failed_imagestream_import_info > 0" }}
              {{ .Labels.name }}
            {{ end }}
            However, the list of ImageStreams for which samples operator is retrying imports is:
            retrying imports:
            {{ range query "openshift_samples_retry_imagestream_import_total > 0" }}
               {{ .Labels.imagestreamname }}
            {{ end }}
        expr: sum(openshift_samples_failed_imagestream_import_info) > sum(openshift_samples_retry_imagestream_import_total)
          - sum(openshift_samples_retry_imagestream_import_total offset 30m)
        for: 2h
        labels:
          severity: warning
      - alert: SamplesImagestreamImportFailing
        annotations:
          message: |
            Samples operator is detecting problems with imagestream image imports.  You can look at the "openshift-samples"
            ClusterOperator object for details. Most likely there are issues with the external image registry hosting
            the images that needs to be investigated.  Or you can consider marking samples operator Removed if you do not
            care about having sample imagestreams available.  The list of ImageStreams for which samples operator is
            retrying imports:
            {{ range query "openshift_samples_retry_imagestream_import_total > 0" }}
               {{ .Labels.imagestreamname }}
            {{ end }}
        expr: sum(openshift_samples_retry_imagestream_import_total) - sum(openshift_samples_retry_imagestream_import_total
          offset 30m) > sum(openshift_samples_failed_imagestream_import_info)
        for: 2h
        labels:
          severity: warning
      - alert: SamplesDegraded
        annotations:
          message: |
            Samples could not be deployed and the operator is degraded. Review the "openshift-samples" ClusterOperator object for further details.
        expr: openshift_samples_degraded_info == 1
        for: 2h
        labels:
          severity: warning
      - alert: SamplesInvalidConfig
        annotations:
          message: |
            Samples operator has been given an invalid configuration.
        expr: openshift_samples_invalidconfig_info == 1
        for: 2h
        labels:
          severity: warning
      - alert: SamplesMissingSecret
        annotations:
          message: |
            Samples operator cannot find the samples pull secret in the openshift namespace.
        expr: openshift_samples_invalidsecret_info{reason="missing_secret"} == 1
        for: 2h
        labels:
          severity: warning
      - alert: SamplesMissingTBRCredential
        annotations:
          message: |
            The samples operator cannot find credentials for 'registry.redhat.io'. Many of the sample ImageStreams will fail to import unless the 'samplesRegistry' in the operator configuration is changed.
        expr: openshift_samples_invalidsecret_info{reason="missing_tbr_credential"} ==
          1
        for: 2h
        labels:
          severity: warning
      - alert: SamplesTBRInaccessibleOnBoot
        annotations:
          message: |
            One of two situations has occurred.  Either
            samples operator could not access 'registry.redhat.io' during its initial installation and it bootstrapped as removed.
            If this is expected, and stems from installing in a restricted network environment, please note that if you
            plan on mirroring images associated with sample imagestreams into a registry available in your restricted
            network environment, and subsequently moving samples operator back to 'Managed' state, a list of the images
            associated with each image stream tag from the samples catalog is
            provided in the 'imagestreamtag-to-image' config map in the 'openshift-cluster-samples-operator' namespace to
            assist the mirroring process.
            Or, the use of allowed registries or blocked registries with global imagestream configuration will not allow
            samples operator to create imagestreams using the default image registry 'registry.redhat.io'.
        expr: openshift_samples_tbr_inaccessible_info == 1
        for: 2d
        labels:
          severity: info
  openshift-cluster-machine-approver-machineapprover-rules.yaml: |
    groups:
    - name: cluster-machine-approver.rules
      rules:
      - alert: MachineApproverMaxPendingCSRsReached
        annotations:
          message: max pending CSRs threshold reached.
        expr: |
          mapi_current_pending_csr > mapi_max_pending_csr
        for: 5m
        labels:
          severity: warning
  openshift-monitoring-cluster-monitoring-operator-prometheus-rules.yaml: |
    groups:
    - name: openshift-general.rules
      rules:
      - alert: TargetDown
        annotations:
          description: '{{ printf "%.4g" $value }}% of the {{ $labels.job }}/{{ $labels.service
            }} targets in {{ $labels.namespace }} namespace have been unreachable for
            more than 15 minutes. This may be a symptom of network connectivity issues,
            down nodes, or failures within these components. Assess the health of the
            infrastructure and nodes running these targets and then contact support.'
          summary: Some targets were not reachable from the monitoring server for an extended
            period of time.
        expr: |
          100 * (count(up == 0 unless on (node) max by (node) (kube_node_spec_unschedulable == 1)) BY (job, namespace, service) /
            count(up unless on (node) max by (node) (kube_node_spec_unschedulable == 1)) BY (job, namespace, service)) > 10
        for: 15m
        labels:
          severity: warning
      - alert: HighlyAvailableWorkloadIncorrectlySpread
        annotations:
          description: Workload {{ $labels.namespace }}/{{ $labels.workload }} is incorrectly
            spread across multiple nodes which breaks high-availability requirements.
            Since the workload is using persistent volumes, manual intervention is needed.
            Please follow the guidelines provided in the runbook of this alert to fix
            this issue.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/HighlyAvailableWorkloadIncorrectlySpread.md
          summary: Highly-available workload is incorrectly spread across multiple nodes
            and manual intervention is needed.
        expr: |
          count without (node)
          (
            group by (node, workload, namespace)
            (
              kube_pod_info{node!=""}
              * on(namespace,pod) group_left(workload)
              (
                max by(namespace, pod, workload) (kube_pod_spec_volumes_persistentvolumeclaims_info)
                * on(namespace,pod) group_left(workload)
                (
                  namespace_workload_pod:kube_pod_owner:relabel
                  * on(namespace,workload,workload_type) group_left()
                  (
                    count without(pod) (namespace_workload_pod:kube_pod_owner:relabel{namespace=~"(openshift-.*|kube-.*|default)"}) > 1
                  )
                )
              )
            )
          ) == 1
        for: 1h
        labels:
          severity: warning
    - name: openshift-kubernetes.rules
      rules:
      - expr: sum(rate(container_cpu_usage_seconds_total{container="",pod!=""}[5m])) BY
          (pod, namespace)
        record: pod:container_cpu_usage:sum
      - expr: sum(container_fs_usage_bytes{pod!=""}) BY (pod, namespace)
        record: pod:container_fs_usage_bytes:sum
      - expr: sum(container_memory_usage_bytes{container!=""}) BY (namespace)
        record: namespace:container_memory_usage_bytes:sum
      - expr: sum(rate(container_cpu_usage_seconds_total{container!="POD",container!=""}[5m]))
          BY (namespace)
        record: namespace:container_cpu_usage:sum
      - expr: sum(container_memory_usage_bytes{container="",pod!=""}) BY (cluster) / sum(machine_memory_bytes)
          BY (cluster)
        record: cluster:memory_usage:ratio
      - expr: sum(container_spec_cpu_shares{container="",pod!=""}) / 1000 / sum(machine_cpu_cores)
        record: cluster:container_spec_cpu_shares:ratio
      - expr: sum(rate(container_cpu_usage_seconds_total{container="",pod!=""}[5m])) /
          sum(machine_cpu_cores)
        record: cluster:container_cpu_usage:ratio
      - expr: max without(endpoint, instance, job, pod, service) (kube_node_labels and
          on(node) kube_node_role{role="master"})
        labels:
          label_node_role_kubernetes_io: master
          label_node_role_kubernetes_io_master: "true"
        record: cluster:master_nodes
      - expr: max without(endpoint, instance, job, pod, service) (kube_node_labels and
          on(node) kube_node_role{role="infra"})
        labels:
          label_node_role_kubernetes_io_infra: "true"
        record: cluster:infra_nodes
      - expr: max without(endpoint, instance, job, pod, service) (cluster:master_nodes
          and on(node) cluster:infra_nodes)
        labels:
          label_node_role_kubernetes_io_infra: "true"
          label_node_role_kubernetes_io_master: "true"
        record: cluster:master_infra_nodes
      - expr: cluster:master_infra_nodes or on (node) cluster:master_nodes or on (node)
          cluster:infra_nodes or on (node) max without(endpoint, instance, job, pod, service)
          (kube_node_labels)
        record: cluster:nodes_roles
      - expr: kube_node_labels and on(node) (sum(label_replace(node_cpu_info, "node",
          "$1", "instance", "(.*)")) by (node, package, core) == 2)
        labels:
          label_node_hyperthread_enabled: "true"
        record: cluster:hyperthread_enabled_nodes
      - expr: count(sum(virt_platform) by (instance, type, system_manufacturer, system_product_name,
          baseboard_manufacturer, baseboard_product_name)) by (type, system_manufacturer,
          system_product_name, baseboard_manufacturer, baseboard_product_name)
        record: cluster:virt_platform_nodes:sum
      - expr: |
          sum by(label_beta_kubernetes_io_instance_type, label_node_role_kubernetes_io, label_kubernetes_io_arch, label_node_openshift_io_os_id) (
            (
              cluster:master_nodes
              * on(node) group_left() max by(node)
              (
                kube_node_status_capacity{resource="cpu",unit="core"}
              )
            )
            or on(node) (
              max without(endpoint, instance, job, pod, service)
              (
                kube_node_labels
              ) * on(node) group_left() max by(node)
              (
                kube_node_status_capacity{resource="cpu",unit="core"}
              )
            )
          )
        record: cluster:capacity_cpu_cores:sum
      - expr: |
          clamp_max(
            label_replace(
              sum by(instance, package, core) (
                node_cpu_info{core!="",package!=""}
                or
                # Assume core = cpu and package = 0 for platforms that don't expose core/package labels.
                label_replace(label_join(node_cpu_info{core="",package=""}, "core", "", "cpu"), "package", "0", "package", "")
              ) > 1,
              "label_node_hyperthread_enabled",
              "true",
              "instance",
              "(.*)"
            ) or on (instance, package)
            label_replace(
              sum by(instance, package, core) (
                label_replace(node_cpu_info{core!="",package!=""}
                or
                # Assume core = cpu and package = 0 for platforms that don't expose core/package labels.
                label_join(node_cpu_info{core="",package=""}, "core", "", "cpu"), "package", "0", "package", "")
              ) <= 1,
              "label_node_hyperthread_enabled",
              "false",
              "instance",
              "(.*)"
            ),
            1
          )
        record: cluster:cpu_core_hyperthreading
      - expr: |
          topk by(node) (1, cluster:nodes_roles) * on (node)
            group_right( label_beta_kubernetes_io_instance_type, label_node_role_kubernetes_io, label_node_openshift_io_os_id, label_kubernetes_io_arch,
                         label_node_role_kubernetes_io_master, label_node_role_kubernetes_io_infra)
          label_replace( cluster:cpu_core_hyperthreading, "node", "$1", "instance", "(.*)" )
        record: cluster:cpu_core_node_labels
      - expr: count(cluster:cpu_core_node_labels) by (label_beta_kubernetes_io_instance_type,
          label_node_hyperthread_enabled)
        record: cluster:capacity_cpu_cores_hyperthread_enabled:sum
      - expr: |
          sum by(label_beta_kubernetes_io_instance_type, label_node_role_kubernetes_io)
          (
            (
              cluster:master_nodes
              * on(node) group_left() max by(node)
              (
                kube_node_status_capacity{resource="memory",unit="byte"}
              )
            )
            or on(node)
            (
              max without(endpoint, instance, job, pod, service)
              (
                kube_node_labels
              )
              * on(node) group_left() max by(node)
              (
                kube_node_status_capacity{resource="memory",unit="byte"}
              )
            )
          )
        record: cluster:capacity_memory_bytes:sum
      - expr: sum(1 - rate(node_cpu_seconds_total{mode="idle"}[2m]) * on(namespace, pod)
          group_left(node) node_namespace_pod:kube_pod_info:{pod=~"node-exporter.+"})
        record: cluster:cpu_usage_cores:sum
      - expr: sum(node_memory_MemTotal_bytes{job="node-exporter"} - node_memory_MemAvailable_bytes{job="node-exporter"})
        record: cluster:memory_usage_bytes:sum
      - expr: sum(rate(container_cpu_usage_seconds_total{namespace!~"openshift-.+",pod!="",container=""}[5m]))
        record: workload:cpu_usage_cores:sum
      - expr: cluster:cpu_usage_cores:sum - workload:cpu_usage_cores:sum
        record: openshift:cpu_usage_cores:sum
      - expr: sum(container_memory_working_set_bytes{namespace!~"openshift-.+",pod!="",container=""})
        record: workload:memory_usage_bytes:sum
      - expr: cluster:memory_usage_bytes:sum - workload:memory_usage_bytes:sum
        record: openshift:memory_usage_bytes:sum
      - expr: sum(cluster:master_nodes or on(node) kube_node_labels ) BY (label_beta_kubernetes_io_instance_type,
          label_node_role_kubernetes_io, label_kubernetes_io_arch, label_node_openshift_io_os_id)
        record: cluster:node_instance_type_count:sum
      - expr: |
          sum by(provisioner) (
            topk by (namespace, persistentvolumeclaim) (
              1, kube_persistentvolumeclaim_resource_requests_storage_bytes
            ) * on(namespace, persistentvolumeclaim) group_right()
            topk by(namespace, persistentvolumeclaim) (
              1, kube_persistentvolumeclaim_info * on(storageclass) group_left(provisioner) topk by(storageclass) (1, max by(storageclass, provisioner) (kube_storageclass_info))
            )
          )
        record: cluster:kube_persistentvolumeclaim_resource_requests_storage_bytes:provisioner:sum
      - expr: (sum(node_role_os_version_machine:cpu_capacity_cores:sum{label_node_role_kubernetes_io_master="",label_node_role_kubernetes_io_infra=""}
          or absent(__does_not_exist__)*0)) + ((sum(node_role_os_version_machine:cpu_capacity_cores:sum{label_node_role_kubernetes_io_master="true"}
          or absent(__does_not_exist__)*0) * ((max(cluster_master_schedulable == 1)*0+1)
          or (absent(cluster_master_schedulable == 1)*0))))
        record: workload:capacity_physical_cpu_cores:sum
      - expr: min_over_time(workload:capacity_physical_cpu_cores:sum[5m:15s])
        record: cluster:usage:workload:capacity_physical_cpu_cores:min:5m
      - expr: max_over_time(workload:capacity_physical_cpu_cores:sum[5m:15s])
        record: cluster:usage:workload:capacity_physical_cpu_cores:max:5m
      - expr: |
          sum  by (provisioner) (
            topk by (namespace, persistentvolumeclaim) (
              1, kubelet_volume_stats_used_bytes
            ) * on (namespace,persistentvolumeclaim) group_right()
            topk by (namespace, persistentvolumeclaim) (
              1, kube_persistentvolumeclaim_info * on(storageclass) group_left(provisioner) topk by(storageclass) (1, max by(storageclass, provisioner) (kube_storageclass_info))
            )
          )
        record: cluster:kubelet_volume_stats_used_bytes:provisioner:sum
      - expr: sum by (instance) (apiserver_storage_objects)
        record: instance:etcd_object_counts:sum
      - expr: topk(500, max by(resource) (apiserver_storage_objects))
        record: cluster:usage:resources:sum
      - expr: count(count (kube_pod_restart_policy{type!="Always",namespace!~"openshift-.+"})
          by (namespace,pod))
        record: cluster:usage:pods:terminal:workload:sum
      - expr: sum(max(kubelet_containers_per_pod_count_sum) by (instance))
        record: cluster:usage:containers:sum
      - expr: count(cluster:cpu_core_node_labels) by (label_kubernetes_io_arch, label_node_hyperthread_enabled,
          label_node_openshift_io_os_id,label_node_role_kubernetes_io_master,label_node_role_kubernetes_io_infra)
        record: node_role_os_version_machine:cpu_capacity_cores:sum
      - expr: count(max(cluster:cpu_core_node_labels) by (node, package, label_beta_kubernetes_io_instance_type,
          label_node_hyperthread_enabled, label_node_role_kubernetes_io) ) by ( label_beta_kubernetes_io_instance_type,
          label_node_hyperthread_enabled, label_node_role_kubernetes_io)
        record: cluster:capacity_cpu_sockets_hyperthread_enabled:sum
      - expr: count (max(cluster:cpu_core_node_labels) by (node, package, label_kubernetes_io_arch,
          label_node_hyperthread_enabled, label_node_openshift_io_os_id,label_node_role_kubernetes_io_master,label_node_role_kubernetes_io_infra)
          ) by (label_kubernetes_io_arch, label_node_hyperthread_enabled, label_node_openshift_io_os_id,label_node_role_kubernetes_io_master,label_node_role_kubernetes_io_infra)
        record: node_role_os_version_machine:cpu_capacity_sockets:sum
      - expr: max(alertmanager_integrations{namespace="openshift-monitoring"})
        record: cluster:alertmanager_integrations:max
      - expr: sum by(plugin_name, volume_mode)(pv_collector_total_pv_count)
        record: cluster:kube_persistentvolume_plugin_type_counts:sum
      - expr: sum by(version)(vsphere_vcenter_info)
        record: cluster:vsphere_vcenter_info:sum
      - expr: sum by(version)(vsphere_esxi_version_total)
        record: cluster:vsphere_esxi_version_total:sum
      - expr: sum by(hw_version)(vsphere_node_hw_version_total)
        record: cluster:vsphere_node_hw_version_total:sum
      - expr: |
          sum(
            min by (node) (kube_node_status_condition{condition="Ready",status="true"})
              and
            max by (node) (kube_node_role{role="master"})
          ) == bool sum(kube_node_role{role="master"})
        record: cluster:control_plane:all_nodes_ready
      - alert: ClusterMonitoringOperatorReconciliationErrors
        annotations:
          description: Errors are occurring during reconciliation cycles. Inspect the
            cluster-monitoring-operator log for potential root causes.
          summary: Cluster Monitoring Operator is experiencing unexpected reconciliation
            errors.
        expr: max_over_time(cluster_monitoring_operator_last_reconciliation_successful[5m])
          == 0
        for: 1h
        labels:
          severity: warning
      - alert: AlertmanagerReceiversNotConfigured
        annotations:
          description: Alerts are not configured to be sent to a notification system,
            meaning that you may not be notified in a timely fashion when important failures
            occur. Check the OpenShift documentation to learn how to configure notifications
            with Alertmanager.
          summary: Receivers (notification integrations) are not configured on Alertmanager
        expr: cluster:alertmanager_integrations:max == 0
        for: 10m
        labels:
          namespace: openshift-monitoring
          severity: warning
      - alert: KubeDeploymentReplicasMismatch
        annotations:
          description: Deployment {{ $labels.namespace }}/{{ $labels.deployment }} has
            not matched the expected number of replicas for longer than 15 minutes. This
            indicates that cluster infrastructure is unable to start or restart the necessary
            components. This most often occurs when one or more nodes are down or partioned
            from the cluster, or a fault occurs on the node that prevents the workload
            from starting. In rare cases this may indicate a new version of a cluster
            component cannot start due to a bug or configuration error. Assess the pods
            for this deployment to verify they are running on healthy nodes and then contact
            support.
          runbook_url: https://github.com/openshift/runbooks/blob/master/alerts/cluster-monitoring-operator/KubeDeploymentReplicasMismatch.md
          summary: Deployment has not matched the expected number of replicas
        expr: |
          (((
            kube_deployment_spec_replicas{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
              >
            kube_deployment_status_replicas_available{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}
          ) and (
            changes(kube_deployment_status_replicas_updated{namespace=~"(openshift-.*|kube-.*|default)",job="kube-state-metrics"}[5m])
              ==
            0
          )) * on() group_left cluster:control_plane:all_nodes_ready) > 0
        for: 15m
        labels:
          severity: warning
      - alert: MultipleContainersOOMKilled
        annotations:
          description: Multiple containers were out of memory killed within the past 15
            minutes. There are many potential causes of OOM errors, however issues on
            a specific node or containers breaching their limits is common.
          summary: Containers are being killed due to OOM
        expr: sum(max by(namespace, container, pod) (increase(kube_pod_container_status_restarts_total[12m]))
          and max by(namespace, container, pod) (kube_pod_container_status_last_terminated_reason{reason="OOMKilled"})
          == 1) > 5
        for: 15m
        labels:
          namespace: kube-system
          severity: info
      - expr: avg_over_time((((count((max by (node) (up{job="kubelet",metrics_path="/metrics"}
          == 1) and max by (node) (kube_node_status_condition{condition="Ready",status="true"}
          == 1) and min by (node) (kube_node_spec_unschedulable == 0))) / scalar(count(min
          by (node) (kube_node_spec_unschedulable == 0))))))[5m:1s])
        record: cluster:usage:kube_schedulable_node_ready_reachable:avg5m
      - expr: avg_over_time((count(max by (node) (kube_node_status_condition{condition="Ready",status="true"}
          == 1)) / scalar(count(max by (node) (kube_node_status_condition{condition="Ready",status="true"}))))[5m:1s])
        record: cluster:usage:kube_node_ready:avg5m
      - expr: (max without (condition,container,endpoint,instance,job,service) (((kube_pod_status_ready{condition="false"}
          == 1)*0 or (kube_pod_status_ready{condition="true"} == 1)) * on(pod,namespace)
          group_left() group by (pod,namespace) (kube_pod_status_phase{phase=~"Running|Unknown|Pending"}
          == 1)))
        record: kube_running_pod_ready
      - expr: avg(kube_running_pod_ready{namespace=~"openshift-.*"})
        record: cluster:usage:openshift:kube_running_pod_ready:avg
      - expr: avg(kube_running_pod_ready{namespace!~"openshift-.*"})
        record: cluster:usage:workload:kube_running_pod_ready:avg
    - interval: 30s
      name: kubernetes-recurring.rules
      rules:
      - expr: sum_over_time(workload:capacity_physical_cpu_cores:sum[30s:1s]) + ((cluster:usage:workload:capacity_physical_cpu_core_seconds
          offset 25s) or (absent(cluster:usage:workload:capacity_physical_cpu_core_seconds
          offset 25s)*0))
        record: cluster:usage:workload:capacity_physical_cpu_core_seconds
    - name: openshift-ingress.rules
      rules:
      - expr: sum by (code) (rate(haproxy_server_http_responses_total[5m]) > 0)
        record: code:cluster:ingress_http_request_count:rate5m:sum
      - expr: sum (rate(haproxy_frontend_bytes_in_total[5m]))
        record: cluster:usage:ingress_frontend_bytes_in:rate5m:sum
      - expr: sum (rate(haproxy_frontend_bytes_out_total[5m]))
        record: cluster:usage:ingress_frontend_bytes_out:rate5m:sum
      - expr: sum (haproxy_frontend_current_sessions)
        record: cluster:usage:ingress_frontend_connections:sum
      - expr: sum(max without(service,endpoint,container,pod,job,namespace) (increase(haproxy_server_http_responses_total{code!~"2xx|1xx|4xx|3xx",exported_namespace!~"openshift-.*"}[5m])
          > 0)) / sum (max without(service,endpoint,container,pod,job,namespace) (increase(haproxy_server_http_responses_total{exported_namespace!~"openshift-.*"}[5m])))
          or absent(__does_not_exist__)*0
        record: cluster:usage:workload:ingress_request_error:fraction5m
      - expr: sum (max without(service,endpoint,container,pod,job,namespace) (irate(haproxy_server_http_responses_total{exported_namespace!~"openshift-.*"}[5m])))
          or absent(__does_not_exist__)*0
        record: cluster:usage:workload:ingress_request_total:irate5m
      - expr: sum(max without(service,endpoint,container,pod,job,namespace) (increase(haproxy_server_http_responses_total{code!~"2xx|1xx|4xx|3xx",exported_namespace=~"openshift-.*"}[5m])
          > 0)) / sum (max without(service,endpoint,container,pod,job,namespace) (increase(haproxy_server_http_responses_total{exported_namespace=~"openshift-.*"}[5m])))
          or absent(__does_not_exist__)*0
        record: cluster:usage:openshift:ingress_request_error:fraction5m
      - expr: sum (max without(service,endpoint,container,pod,job,namespace) (irate(haproxy_server_http_responses_total{exported_namespace=~"openshift-.*"}[5m])))
          or absent(__does_not_exist__)*0
        record: cluster:usage:openshift:ingress_request_total:irate5m
    - name: openshift-build.rules
      rules:
      - expr: sum by (strategy) (openshift_build_status_phase_total)
        record: openshift:build_by_strategy:sum
    - name: openshift-monitoring.rules
      rules:
      - expr: sum by (job,namespace) (max without(instance) (prometheus_tsdb_head_series{namespace=~"openshift-monitoring|openshift-user-workload-monitoring"}))
        record: openshift:prometheus_tsdb_head_series:sum
      - expr: sum by(job,namespace) (max without(instance) (rate(prometheus_tsdb_head_samples_appended_total{namespace=~"openshift-monitoring|openshift-user-workload-monitoring"}[2m])))
        record: openshift:prometheus_tsdb_head_samples_appended_total:sum
      - expr: sum by (namespace) (max without(instance) (container_memory_working_set_bytes{namespace=~"openshift-monitoring|openshift-user-workload-monitoring",
          container=""}))
        record: monitoring:container_memory_working_set_bytes:sum
      - expr: topk(3, sum by(namespace, job)(sum_over_time(scrape_series_added[1h])))
        record: namespace_job:scrape_series_added:topk3_sum1h
      - expr: topk(3, max by(namespace, job) (topk by(namespace,job) (1, scrape_samples_post_metric_relabeling)))
        record: namespace_job:scrape_samples_post_metric_relabeling:topk3
      - expr: sum by(exported_service) (rate(haproxy_server_http_responses_total{exported_namespace="openshift-monitoring",
          exported_service=~"alertmanager-main|grafana|prometheus-k8s"}[5m]))
        record: monitoring:haproxy_server_http_responses_total:sum
      - expr: max by (cluster, namespace, workload, pod) (label_replace(label_replace(kube_pod_owner{job="kube-state-metrics",
          owner_kind="ReplicationController"},"replicationcontroller", "$1", "owner_name",
          "(.*)") * on(replicationcontroller, namespace) group_left(owner_name) topk by(replicationcontroller,
          namespace) (1, max by (replicationcontroller, namespace, owner_name) (kube_replicationcontroller_owner{job="kube-state-metrics"})),"workload",
          "$1", "owner_name", "(.*)"))
        labels:
          workload_type: deploymentconfig
        record: namespace_workload_pod:kube_pod_owner:relabel
    - name: openshift-etcd-telemetry.rules
      rules:
      - expr: sum by (instance) (etcd_mvcc_db_total_size_in_bytes{job="etcd"})
        record: instance:etcd_mvcc_db_total_size_in_bytes:sum
      - expr: histogram_quantile(0.99, sum by (instance, le) (rate(etcd_disk_wal_fsync_duration_seconds_bucket{job="etcd"}[5m])))
        labels:
          quantile: "0.99"
        record: instance:etcd_disk_wal_fsync_duration_seconds:histogram_quantile
      - expr: histogram_quantile(0.99, sum by (instance, le) (rate(etcd_network_peer_round_trip_time_seconds_bucket{job="etcd"}[5m])))
        labels:
          quantile: "0.99"
        record: instance:etcd_network_peer_round_trip_time_seconds:histogram_quantile
      - expr: sum by (instance) (etcd_mvcc_db_total_size_in_use_in_bytes{job="etcd"})
        record: instance:etcd_mvcc_db_total_size_in_use_in_bytes:sum
      - expr: histogram_quantile(0.99, sum by (instance, le) (rate(etcd_disk_backend_commit_duration_seconds_bucket{job="etcd"}[5m])))
        labels:
          quantile: "0.99"
        record: instance:etcd_disk_backend_commit_duration_seconds:histogram_quantile
    - name: openshift-sre.rules
      rules:
      - expr: sum(rate(apiserver_request_total{job="apiserver"}[10m])) BY (code)
        record: code:apiserver_request_total:rate:sum
    - name: general.rules
      rules:
      - alert: Watchdog
        annotations:
          description: |
            This is an alert meant to ensure that the entire alerting pipeline is functional.
            This alert is always firing, therefore it should always be firing in Alertmanager
            and always fire against a receiver. There are integrations with various notification
            mechanisms that send a notification when this alert is not firing. For example the
            "DeadMansSnitch" integration in PagerDuty.
          summary: An alert that should always be firing to certify that Alertmanager
            is working properly.
        expr: vector(1)
        labels:
          namespace: openshift-monitoring
          severity: none
    - name: node-network
      rules:
      - alert: NodeNetworkInterfaceFlapping
        annotations:
          description: Network interface "{{ $labels.device }}" changing its up status
            often on node-exporter {{ $labels.namespace }}/{{ $labels.pod }}
          summary: Network interface is often changing its status
        expr: |
          changes(node_network_up{job="node-exporter",device!~"veth.+"}[2m]) > 2
        for: 2m
        labels:
          severity: warning
    - name: kube-prometheus-node-recording.rules
      rules:
      - expr: sum(rate(node_cpu_seconds_total{mode!="idle",mode!="iowait",mode!="steal"}[3m]))
          BY (instance)
        record: instance:node_cpu:rate:sum
      - expr: sum(rate(node_network_receive_bytes_total[3m])) BY (instance)
        record: instance:node_network_receive_bytes:rate:sum
      - expr: sum(rate(node_network_transmit_bytes_total[3m])) BY (instance)
        record: instance:node_network_transmit_bytes:rate:sum
      - expr: sum(rate(node_cpu_seconds_total{mode!="idle",mode!="iowait",mode!="steal"}[5m]))
        record: cluster:node_cpu:sum_rate5m
      - expr: cluster:node_cpu_seconds_total:rate5m / count(sum(node_cpu_seconds_total)
          BY (instance, cpu))
        record: cluster:node_cpu:ratio
    - name: kube-prometheus-general.rules
      rules:
      - expr: count without(instance, pod, node) (up == 1)
        record: count:up1
      - expr: count without(instance, pod, node) (up == 0)
        record: count:up0
  openshift-dns-operator-dns.yaml: |
    groups:
    - name: openshift-dns.rules
      rules:
      - alert: CoreDNSPanicking
        annotations:
          description: '{{ $value }} CoreDNS panics observed on {{ $labels.instance }}'
          summary: CoreDNS panic
        expr: increase(coredns_panics_total[10m]) > 0
        for: 5m
        labels:
          severity: warning
      - alert: CoreDNSHealthCheckSlow
        annotations:
          description: CoreDNS Health Checks are slowing down (instance {{ $labels.instance
            }})
          summary: CoreDNS health checks
        expr: histogram_quantile(.95, sum(rate(coredns_health_request_duration_seconds_bucket[5m]))
          by (instance, le)) > 10
        for: 5m
        labels:
          severity: warning
      - alert: CoreDNSErrorsHigh
        annotations:
          description: CoreDNS is returning SERVFAIL for {{ $value | humanizePercentage
            }} of requests.
          summary: CoreDNS serverfail
        expr: |
          (sum(rate(coredns_dns_responses_total{rcode="SERVFAIL"}[5m]))
            /
          sum(rate(coredns_dns_responses_total[5m])))
          > 0.01
        for: 5m
        labels:
          severity: warning
  openshift-monitoring-prometheus-k8s-thanos-sidecar-rules.yaml: |
    groups:
    - name: thanos-sidecar
      rules:
      - alert: ThanosSidecarPrometheusDown
        annotations:
          description: Thanos Sidecar {{$labels.instance}} cannot connect to Prometheus.
          summary: Thanos Sidecar cannot connect to Prometheus
        expr: |
          thanos_sidecar_prometheus_up{job=~"prometheus-(k8s|user-workload)-thanos-sidecar"} == 0
        for: 1h
        labels:
          severity: warning
      - alert: ThanosSidecarBucketOperationsFailed
        annotations:
          description: Thanos Sidecar {{$labels.instance}} bucket operations are failing
          summary: Thanos Sidecar bucket operations are failing
        expr: |
          sum by (job, instance) (rate(thanos_objstore_bucket_operation_failures_total{job=~"prometheus-(k8s|user-workload)-thanos-sidecar"}[5m])) > 0
        for: 1h
        labels:
          severity: warning
      - alert: ThanosSidecarUnhealthy
        annotations:
          description: Thanos Sidecar {{$labels.instance}} is unhealthy for more than
            {{$value}} seconds.
          summary: Thanos Sidecar is unhealthy.
        expr: |
          time() - max by (job, instance) (thanos_sidecar_last_heartbeat_success_time_seconds{job=~"prometheus-(k8s|user-workload)-thanos-sidecar"}) >= 240
        for: 1h
        labels:
          severity: warning
  openshift-kube-apiserver-kube-apiserver-requests.yaml: |
    groups:
    - name: apiserver-requests-in-flight
      rules:
      - expr: |
          max_over_time(sum(apiserver_current_inflight_requests{apiserver=~"openshift-apiserver|kube-apiserver"}) by (apiserver,requestKind)[2m:])
        record: cluster:apiserver_current_inflight_requests:sum:max_over_time:2m
  openshift-kube-controller-manager-operator-kube-controller-manager-operator.yaml: |
    groups:
    - name: cluster-version
      rules:
      - alert: KubeControllerManagerDown
        annotations:
          message: KubeControllerManager has disappeared from Prometheus target discovery.
        expr: |
          absent(up{job="kube-controller-manager"} == 1)
        for: 15m
        labels:
          severity: critical
      - alert: PodDisruptionBudgetAtLimit
        annotations:
          message: The pod disruption budget is preventing further disruption to pods
            because it is at the minimum allowed level.
        expr: |
          max by(namespace, poddisruptionbudget) (kube_poddisruptionbudget_status_current_healthy == kube_poddisruptionbudget_status_desired_healthy)
        for: 60m
        labels:
          severity: warning
      - alert: PodDisruptionBudgetLimit
        annotations:
          message: The pod disruption budget is below the minimum number allowed pods.
        expr: |
          max by (namespace, poddisruptionbudget) (kube_poddisruptionbudget_status_current_healthy < kube_poddisruptionbudget_status_desired_healthy)
        for: 15m
        labels:
          severity: critical
  openshift-kube-apiserver-audit-errors.yaml: |
    groups:
    - name: apiserver-audit
      rules:
      - alert: AuditLogError
        annotations:
          description: An API Server had an error writing to an audit log.
          summary: |-
            An API Server instance was unable to write audit logs. This could be
            triggered by the node running out of space, or a malicious actor
            tampering with the audit logs.
        expr: |
          sum by (apiserver,instance)(rate(apiserver_audit_error_total{apiserver=~".+-apiserver"}[5m])) / sum by (apiserver,instance) (rate(apiserver_audit_event_total{apiserver=~".+-apiserver"}[5m])) > 0
        for: 1m
        labels:
          namespace: openshift-kube-apiserver
          severity: warning
  openshift-ingress-operator-ingress-operator.yaml: |
    groups:
    - name: openshift-ingress.rules
      rules:
      - alert: HAProxyReloadFail
        annotations:
          message: HAProxy reloads are failing on {{ $labels.pod }}. Router is not respecting
            recently created or modified routes
        expr: template_router_reload_failure == 1
        for: 5m
        labels:
          severity: warning
      - alert: HAProxyDown
        annotations:
          message: HAProxy metrics are reporting that HAProxy is down on pod {{ $labels.namespace
            }} / {{ $labels.pod }}
        expr: haproxy_up == 0
        for: 5m
        labels:
          severity: critical
      - alert: IngressControllerDegraded
        annotations:
          message: |
            The {{ $labels.namespace }}/{{ $labels.name }} ingresscontroller is
            degraded: {{ $labels.reason }}.
        expr: ingress_controller_conditions{condition="Degraded"} == 1
        for: 5m
        labels:
          severity: warning
      - alert: IngressControllerUnavailable
        annotations:
          message: |
            The {{ $labels.namespace }}/{{ $labels.name }} ingresscontroller is
            unavailable: {{ $labels.reason }}.
        expr: ingress_controller_conditions{condition="Available"} == 0
        for: 5m
        labels:
          severity: warning
  openshift-ovn-kubernetes-master-rules.yaml: |
    groups:
    - name: cluster-network-operator-master.rules
      rules:
      - alert: NoRunningOvnMaster
        annotations:
          summary: There is no running ovn-kubernetes master
        expr: |
          absent(up{job="ovnkube-master", namespace="openshift-ovn-kubernetes"} == 1)
        for: 10m
        labels:
          severity: warning
      - alert: NoOvnMasterLeader
        annotations:
          summary: There is no ovn-kubernetes master leader
        expr: |
          max(ovnkube_master_leader) == 0
        for: 10m
        labels:
          severity: warning
      - alert: NorthboundStale
        annotations:
          summary: ovn-kubernetes has not written anything to the northbound database
            for too long
        expr: |
          time() - max(ovn_nb_e2e_timestamp) > 300
        for: 10m
        labels:
          severity: warning
      - alert: SouthboundStale
        annotations:
          summary: ovn-northd has not successfully synced any changes to the southbound
            DB for too long
        expr: |
          max(ovn_nb_e2e_timestamp) - max(ovn_sb_e2e_timestamp) > 120
        for: 10m
        labels:
          severity: warning
      - alert: V4SubnetAllocationThresholdExceeded
        annotations:
          summary: More than 80% of v4 subnets available to assign to the nodes are allocated.
            Current v4 subnet allocation percentage is {{ $value }}
        expr: |
          ovnkube_master_allocated_v4_host_subnets/ovnkube_master_num_v4_host_subnets * 100 > 80
        for: 10m
        labels:
          severity: warning
      - alert: V6SubnetAllocationThresholdExceeded
        annotations:
          summary: More than 80% of the v6 subnets available to assign to the nodes are
            allocated. Current v6 subnet allocation percentage is {{ $value }}
        expr: |
          ovnkube_master_allocated_v6_host_subnets/ovnkube_master_num_v6_host_subnets * 100 > 80
        for: 10m
        labels:
          severity: warning
  openshift-multus-prometheus-k8s-rules.yaml: |
    groups:
    - name: multus-admission-controller-monitor-service.rules
      rules:
      - expr: |
          max  (network_attachment_definition_enabled_instance_up) by (networks)
        record: cluster:network_attachment_definition_enabled_instance_up:max
      - expr: |
          max  (network_attachment_definition_instances) by (networks)
        record: cluster:network_attachment_definition_instances:max
  openshift-machine-api-machine-api-operator-prometheus-rules.yaml: |
    groups:
    - name: machine-without-valid-node-ref
      rules:
      - alert: MachineWithoutValidNode
        annotations:
          message: machine {{ $labels.name }} does not have valid node reference
        expr: |
          (mapi_machine_created_timestamp_seconds unless on(node) kube_node_info) > 0
        for: 60m
        labels:
          severity: warning
    - name: machine-with-no-running-phase
      rules:
      - alert: MachineWithNoRunningPhase
        annotations:
          message: 'machine {{ $labels.name }} is in phase: {{ $labels.phase }}'
        expr: |
          (mapi_machine_created_timestamp_seconds{phase!~"Running|Deleting"}) > 0
        for: 60m
        labels:
          severity: warning
    - name: machine-not-yet-deleted
      rules:
      - alert: MachineNotYetDeleted
        annotations:
          message: machine {{ $labels.name }} has been in Deleting phase for more than
            6 hours
        expr: |
          (mapi_machine_created_timestamp_seconds{phase="Deleting"}) > 0
        for: 360m
        labels:
          severity: warning
    - name: machine-api-operator-metrics-collector-up
      rules:
      - alert: MachineAPIOperatorMetricsCollectionFailing
        annotations:
          message: 'machine api operator metrics collection is failing. For more details:  oc
            logs <machine-api-operator-pod-name> -n openshift-machine-api'
        expr: |
          mapi_mao_collector_up == 0
        for: 5m
        labels:
          severity: critical
    - name: machine-health-check-unterminated-short-circuit
      rules:
      - alert: MachineHealthCheckUnterminatedShortCircuit
        expr: |
          mapi_machinehealthcheck_short_circuit == 1
        for: 30m
        labels:
          severity: warning
```

####################################################################################################################































```
kind: DaemonSet
apiVersion: apps/v1
metadata:
  annotations:
    deprecated.daemonset.template.generation: '1'
  resourceVersion: '18891'
  name: node-exporter
  uid: 6ce01c4d-1183-4ea7-b7d8-74f78c90098b
  creationTimestamp: '2023-01-20T00:15:36Z'
  generation: 1
  managedFields:
    - manager: operator
      operation: Update
      apiVersion: apps/v1
      time: '2023-01-20T00:15:36Z'
      fieldsType: FieldsV1
      fieldsV1:
        'f:metadata':
          'f:annotations':
            .: {}
            'f:deprecated.daemonset.template.generation': {}
          'f:labels':
            .: {}
            'f:app.kubernetes.io/component': {}
            'f:app.kubernetes.io/name': {}
            'f:app.kubernetes.io/part-of': {}
            'f:app.kubernetes.io/version': {}
        'f:spec':
          'f:revisionHistoryLimit': {}
          'f:selector': {}
          'f:template':
            'f:metadata':
              'f:annotations':
                .: {}
                'f:target.workload.openshift.io/management': {}
              'f:labels':
                .: {}
                'f:app.kubernetes.io/component': {}
                'f:app.kubernetes.io/name': {}
                'f:app.kubernetes.io/part-of': {}
                'f:app.kubernetes.io/version': {}
            'f:spec':
              'f:volumes':
                .: {}
                'k:{"name":"metrics-client-ca"}':
                  .: {}
                  'f:configMap':
                    .: {}
                    'f:defaultMode': {}
                    'f:name': {}
                  'f:name': {}
                'k:{"name":"node-exporter-textfile"}':
                  .: {}
                  'f:emptyDir': {}
                  'f:name': {}
                'k:{"name":"node-exporter-tls"}':
                  .: {}
                  'f:name': {}
                  'f:secret':
                    .: {}
                    'f:defaultMode': {}
                    'f:secretName': {}
                'k:{"name":"node-exporter-wtmp"}':
                  .: {}
                  'f:hostPath':
                    .: {}
                    'f:path': {}
                    'f:type': {}
                  'f:name': {}
                'k:{"name":"root"}':
                  .: {}
                  'f:hostPath':
                    .: {}
                    'f:path': {}
                    'f:type': {}
                  'f:name': {}
                'k:{"name":"sys"}':
                  .: {}
                  'f:hostPath':
                    .: {}
                    'f:path': {}
                    'f:type': {}
                  'f:name': {}
              'f:containers':
                'k:{"name":"kube-rbac-proxy"}':
                  'f:image': {}
                  'f:volumeMounts':
                    .: {}
                    'k:{"mountPath":"/etc/tls/client"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                    'k:{"mountPath":"/etc/tls/private"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                  'f:terminationMessagePolicy': {}
                  .: {}
                  'f:resources':
                    .: {}
                    'f:requests':
                      .: {}
                      'f:cpu': {}
                      'f:memory': {}
                  'f:args': {}
                  'f:env':
                    .: {}
                    'k:{"name":"IP"}':
                      .: {}
                      'f:name': {}
                      'f:valueFrom':
                        .: {}
                        'f:fieldRef': {}
                  'f:securityContext':
                    .: {}
                    'f:runAsGroup': {}
                    'f:runAsNonRoot': {}
                    'f:runAsUser': {}
                  'f:terminationMessagePath': {}
                  'f:imagePullPolicy': {}
                  'f:ports':
                    .: {}
                    'k:{"containerPort":9100,"protocol":"TCP"}':
                      .: {}
                      'f:containerPort': {}
                      'f:hostPort': {}
                      'f:name': {}
                      'f:protocol': {}
                  'f:name': {}
                'k:{"name":"node-exporter"}':
                  'f:image': {}
                  'f:volumeMounts':
                    .: {}
                    'k:{"mountPath":"/host/root"}':
                      .: {}
                      'f:mountPath': {}
                      'f:mountPropagation': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/host/sys"}':
                      .: {}
                      'f:mountPath': {}
                      'f:mountPropagation': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/var/node_exporter/textfile"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                  'f:terminationMessagePolicy': {}
                  .: {}
                  'f:workingDir': {}
                  'f:resources':
                    .: {}
                    'f:requests':
                      .: {}
                      'f:cpu': {}
                      'f:memory': {}
                  'f:args': {}
                  'f:terminationMessagePath': {}
                  'f:imagePullPolicy': {}
                  'f:name': {}
              'f:dnsPolicy': {}
              'f:tolerations': {}
              'f:priorityClassName': {}
              'f:serviceAccount': {}
              'f:restartPolicy': {}
              'f:hostPID': {}
              'f:schedulerName': {}
              'f:hostNetwork': {}
              'f:nodeSelector': {}
              'f:terminationGracePeriodSeconds': {}
              'f:initContainers':
                .: {}
                'k:{"name":"init-textfile"}':
                  'f:image': {}
                  'f:volumeMounts':
                    .: {}
                    'k:{"mountPath":"/var/log/wtmp"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                      'f:readOnly': {}
                    'k:{"mountPath":"/var/node_exporter/textfile"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                  'f:terminationMessagePolicy': {}
                  .: {}
                  'f:workingDir': {}
                  'f:resources':
                    .: {}
                    'f:requests':
                      .: {}
                      'f:cpu': {}
                      'f:memory': {}
                  'f:command': {}
                  'f:env':
                    .: {}
                    'k:{"name":"TMPDIR"}':
                      .: {}
                      'f:name': {}
                      'f:value': {}
                  'f:securityContext':
                    .: {}
                    'f:privileged': {}
                    'f:runAsUser': {}
                  'f:terminationMessagePath': {}
                  'f:imagePullPolicy': {}
                  'f:name': {}
              'f:serviceAccountName': {}
              'f:securityContext': {}
          'f:updateStrategy':
            'f:rollingUpdate':
              .: {}
              'f:maxSurge': {}
              'f:maxUnavailable': {}
            'f:type': {}
    - manager: kube-controller-manager
      operation: Update
      apiVersion: apps/v1
      time: '2023-01-20T00:24:28Z'
      fieldsType: FieldsV1
      fieldsV1:
        'f:status':
          'f:currentNumberScheduled': {}
          'f:desiredNumberScheduled': {}
          'f:numberAvailable': {}
          'f:numberReady': {}
          'f:observedGeneration': {}
          'f:updatedNumberScheduled': {}
      subresource: status
  namespace: openshift-monitoring
  labels:
    app.kubernetes.io/component: exporter
    app.kubernetes.io/name: node-exporter
    app.kubernetes.io/part-of: openshift-monitoring
    app.kubernetes.io/version: 1.1.2
spec:
  selector:
    matchLabels:
      app.kubernetes.io/component: exporter
      app.kubernetes.io/name: node-exporter
      app.kubernetes.io/part-of: openshift-monitoring
  template:
    metadata:
      creationTimestamp: null
      labels:
        app.kubernetes.io/component: exporter
        app.kubernetes.io/name: node-exporter
        app.kubernetes.io/part-of: openshift-monitoring
        app.kubernetes.io/version: 1.1.2
      annotations:
        target.workload.openshift.io/management: '{"effect": "PreferredDuringScheduling"}'
    spec:
      nodeSelector:
        kubernetes.io/os: linux
      restartPolicy: Always
      initContainers:
        - resources:
            requests:
              cpu: 1m
              memory: 1Mi
          terminationMessagePath: /dev/termination-log
          name: init-textfile
          command:
            - /bin/sh
            - '-c'
            - >-
              [[ ! -d /node_exporter/collectors/init ]] || find
              /node_exporter/collectors/init -perm /111 -type f -exec {} \;
          env:
            - name: TMPDIR
              value: /tmp
          securityContext:
            privileged: true
            runAsUser: 0
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: node-exporter-textfile
              mountPath: /var/node_exporter/textfile
            - name: node-exporter-wtmp
              readOnly: true
              mountPath: /var/log/wtmp
          terminationMessagePolicy: FallbackToLogsOnError
          image: >-
            quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:70529c34455306e2f964df28eb598bb24a945be9332ee674f142213ee288227a
          workingDir: /var/node_exporter/textfile
      serviceAccountName: node-exporter
      hostPID: true
      schedulerName: default-scheduler
      hostNetwork: true
      terminationGracePeriodSeconds: 30
      securityContext: {}
      containers:
        - resources:
            requests:
              cpu: 8m
              memory: 32Mi
          terminationMessagePath: /dev/termination-log
          name: node-exporter
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: sys
              readOnly: true
              mountPath: /host/sys
              mountPropagation: HostToContainer
            - name: root
              readOnly: true
              mountPath: /host/root
              mountPropagation: HostToContainer
            - name: node-exporter-textfile
              readOnly: true
              mountPath: /var/node_exporter/textfile
          terminationMessagePolicy: FallbackToLogsOnError
          image: >-
            quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:70529c34455306e2f964df28eb598bb24a945be9332ee674f142213ee288227a
          workingDir: /var/node_exporter/textfile
          args:
            - '--web.listen-address=127.0.0.1:9100'
            - '--path.sysfs=/host/sys'
            - '--path.rootfs=/host/root'
            - '--no-collector.wifi'
            - >-
              --collector.filesystem.ignored-mount-points=^/(dev|proc|sys|var/lib/docker/.+|var/lib/kubelet/pods/.+)($|/)
            - '--collector.netclass.ignored-devices=^(veth.*|[a-f0-9]{15})$'
            - '--collector.netdev.device-exclude=^(veth.*|[a-f0-9]{15})$'
            - '--collector.cpu.info'
            - '--collector.textfile.directory=/var/node_exporter/textfile'
            - '--no-collector.cpufreq'
        - resources:
            requests:
              cpu: 1m
              memory: 15Mi
          terminationMessagePath: /dev/termination-log
          name: kube-rbac-proxy
          env:
            - name: IP
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: status.podIP
          securityContext:
            runAsUser: 65532
            runAsGroup: 65532
            runAsNonRoot: true
          ports:
            - name: https
              hostPort: 9100
              containerPort: 9100
              protocol: TCP
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: node-exporter-tls
              mountPath: /etc/tls/private
            - name: metrics-client-ca
              mountPath: /etc/tls/client
          terminationMessagePolicy: FallbackToLogsOnError
          image: >-
            quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:0c304c11986790f446604319dfc29db494b15f5f9fa13f2c436688ec29c92dbe
          args:
            - '--logtostderr'
            - '--secure-listen-address=[$(IP)]:9100'
            - >-
              --tls-cipher-suites=TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305
            - '--upstream=http://127.0.0.1:9100/'
            - '--tls-cert-file=/etc/tls/private/tls.crt'
            - '--tls-private-key-file=/etc/tls/private/tls.key'
            - '--client-ca-file=/etc/tls/client/client-ca.crt'
      serviceAccount: node-exporter
      volumes:
        - name: sys
          hostPath:
            path: /sys
            type: ''
        - name: root
          hostPath:
            path: /
            type: ''
        - name: node-exporter-textfile
          emptyDir: {}
        - name: node-exporter-tls
          secret:
            secretName: node-exporter-tls
            defaultMode: 420
        - name: node-exporter-wtmp
          hostPath:
            path: /var/log/wtmp
            type: File
        - name: metrics-client-ca
          configMap:
            name: metrics-client-ca
            defaultMode: 420
      dnsPolicy: ClusterFirst
      tolerations:
        - operator: Exists
      priorityClassName: system-cluster-critical
  updateStrategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 10%
      maxSurge: 0
  revisionHistoryLimit: 10
status:
  currentNumberScheduled: 5
  numberMisscheduled: 0
  desiredNumberScheduled: 5
  numberReady: 5
  observedGeneration: 1
  updatedNumberScheduled: 5
  numberAvailable: 5





















```



##########################################################################################################################


```
cluster-monitoring-operator

kind: Deployment
apiVersion: apps/v1
metadata:
  annotations:
    deployment.kubernetes.io/revision: '1'
    include.release.openshift.io/self-managed-high-availability: 'true'
    include.release.openshift.io/single-node-developer: 'true'
  resourceVersion: '7685'
  name: cluster-monitoring-operator
  uid: 995fcec4-a5bc-4e99-993b-05e119bb07cd
  creationTimestamp: '2023-01-20T00:10:43Z'
  generation: 1
  managedFields:
    - manager: cluster-version-operator
      operation: Update
      apiVersion: apps/v1
      time: '2023-01-20T00:10:43Z'
      fieldsType: FieldsV1
      fieldsV1:
        'f:metadata':
          'f:annotations':
            .: {}
            'f:include.release.openshift.io/self-managed-high-availability': {}
            'f:include.release.openshift.io/single-node-developer': {}
          'f:labels':
            .: {}
            'f:app': {}
          'f:ownerReferences':
            .: {}
            'k:{"uid":"1df3d224-9ceb-4547-8e5d-53dd9cdcc2a1"}': {}
        'f:spec':
          'f:progressDeadlineSeconds': {}
          'f:replicas': {}
          'f:revisionHistoryLimit': {}
          'f:selector': {}
          'f:strategy':
            'f:rollingUpdate':
              .: {}
              'f:maxSurge': {}
              'f:maxUnavailable': {}
            'f:type': {}
          'f:template':
            'f:metadata':
              'f:annotations':
                .: {}
                'f:target.workload.openshift.io/management': {}
              'f:labels':
                .: {}
                'f:app': {}
            'f:spec':
              'f:volumes':
                .: {}
                'k:{"name":"cluster-monitoring-operator-tls"}':
                  .: {}
                  'f:name': {}
                  'f:secret':
                    .: {}
                    'f:defaultMode': {}
                    'f:optional': {}
                    'f:secretName': {}
                'k:{"name":"telemetry-config"}':
                  .: {}
                  'f:configMap':
                    .: {}
                    'f:defaultMode': {}
                    'f:name': {}
                  'f:name': {}
              'f:containers':
                'k:{"name":"cluster-monitoring-operator"}':
                  'f:image': {}
                  'f:volumeMounts':
                    .: {}
                    'k:{"mountPath":"/etc/cluster-monitoring-operator/telemetry"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                  'f:terminationMessagePolicy': {}
                  .: {}
                  'f:resources':
                    .: {}
                    'f:requests':
                      .: {}
                      'f:cpu': {}
                      'f:memory': {}
                  'f:args': {}
                  'f:env':
                    .: {}
                    'k:{"name":"RELEASE_VERSION"}':
                      .: {}
                      'f:name': {}
                      'f:value': {}
                  'f:terminationMessagePath': {}
                  'f:imagePullPolicy': {}
                  'f:name': {}
                'k:{"name":"kube-rbac-proxy"}':
                  'f:image': {}
                  'f:volumeMounts':
                    .: {}
                    'k:{"mountPath":"/etc/tls/private"}':
                      .: {}
                      'f:mountPath': {}
                      'f:name': {}
                  'f:terminationMessagePolicy': {}
                  .: {}
                  'f:resources':
                    .: {}
                    'f:requests':
                      .: {}
                      'f:cpu': {}
                      'f:memory': {}
                  'f:args': {}
                  'f:terminationMessagePath': {}
                  'f:imagePullPolicy': {}
                  'f:ports':
                    .: {}
                    'k:{"containerPort":8443,"protocol":"TCP"}':
                      .: {}
                      'f:containerPort': {}
                      'f:name': {}
                      'f:protocol': {}
                  'f:name': {}
              'f:dnsPolicy': {}
              'f:tolerations': {}
              'f:priorityClassName': {}
              'f:serviceAccount': {}
              'f:restartPolicy': {}
              'f:schedulerName': {}
              'f:nodeSelector': {}
              'f:terminationGracePeriodSeconds': {}
              'f:serviceAccountName': {}
              'f:securityContext': {}
    - manager: kube-controller-manager
      operation: Update
      apiVersion: apps/v1
      time: '2023-01-20T00:15:50Z'
      fieldsType: FieldsV1
      fieldsV1:
        'f:metadata':
          'f:annotations':
            'f:deployment.kubernetes.io/revision': {}
        'f:status':
          'f:availableReplicas': {}
          'f:conditions':
            .: {}
            'k:{"type":"Available"}':
              .: {}
              'f:lastTransitionTime': {}
              'f:lastUpdateTime': {}
              'f:message': {}
              'f:reason': {}
              'f:status': {}
              'f:type': {}
            'k:{"type":"Progressing"}':
              .: {}
              'f:lastTransitionTime': {}
              'f:lastUpdateTime': {}
              'f:message': {}
              'f:reason': {}
              'f:status': {}
              'f:type': {}
          'f:observedGeneration': {}
          'f:readyReplicas': {}
          'f:replicas': {}
          'f:updatedReplicas': {}
      subresource: status
  namespace: openshift-monitoring
  ownerReferences:
    - apiVersion: config.openshift.io/v1
      kind: ClusterVersion
      name: version
      uid: 1df3d224-9ceb-4547-8e5d-53dd9cdcc2a1
  labels:
    app: cluster-monitoring-operator
spec:
  replicas: 1
  selector:
    matchLabels:
      app: cluster-monitoring-operator
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: cluster-monitoring-operator
      annotations:
        target.workload.openshift.io/management: '{"effect": "PreferredDuringScheduling"}'
    spec:
      nodeSelector:
        kubernetes.io/os: linux
        node-role.kubernetes.io/master: ''
      restartPolicy: Always
      serviceAccountName: cluster-monitoring-operator
      schedulerName: default-scheduler
      terminationGracePeriodSeconds: 30
      securityContext: {}
      containers:
        - resources:
            requests:
              cpu: 1m
              memory: 20Mi
          terminationMessagePath: /dev/termination-log
          name: kube-rbac-proxy
          ports:
            - name: https
              containerPort: 8443
              protocol: TCP
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: cluster-monitoring-operator-tls
              mountPath: /etc/tls/private
          terminationMessagePolicy: FallbackToLogsOnError
          image: >-
            quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:0c304c11986790f446604319dfc29db494b15f5f9fa13f2c436688ec29c92dbe
          args:
            - '--logtostderr'
            - '--secure-listen-address=:8443'
            - >-
              --tls-cipher-suites=TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305
            - '--upstream=http://127.0.0.1:8080/'
            - '--tls-cert-file=/etc/tls/private/tls.crt'
            - '--tls-private-key-file=/etc/tls/private/tls.key'
        - resources:
            requests:
              cpu: 10m
              memory: 75Mi
          terminationMessagePath: /dev/termination-log
          name: cluster-monitoring-operator
          env:
            - name: RELEASE_VERSION
              value: 4.9.49
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: telemetry-config
              mountPath: /etc/cluster-monitoring-operator/telemetry
          terminationMessagePolicy: FallbackToLogsOnError
          image: >-
            quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:6592391364542b5cbe26b7c3b6f4ad8a3b92575fc8ab2c5792705006e063a4fd
          args:
            - '-namespace=openshift-monitoring'
            - '-namespace-user-workload=openshift-user-workload-monitoring'
            - '-configmap=cluster-monitoring-config'
            - '-release-version=$(RELEASE_VERSION)'
            - '-logtostderr=true'
            - '-v=2'
            - >-
              -images=prometheus-operator=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:39f7cb9df010a670797b3d6f95e78890f2142873cd63a2e1c62016abc826d392
            - >-
              -images=prometheus-config-reloader=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:fefe5b4b9f77d57043b24cb3d9113b39926542cf64c122adbaa2316073dd8218
            - >-
              -images=configmap-reloader=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:08c1072e31e6ae0c5888c404c856b699f084629b96b4e47f38be8ac18528e6a5
            - >-
              -images=prometheus=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:bd8457a514f36dc65e31fc50eceaa101eb70464f4d2720760cc26351fe102294
            - >-
              -images=alertmanager=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:b8a67c229abc32d9bd837e8fbfc14c48850a03dce86b2113511df172d9bef0c2
            - >-
              -images=grafana=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:cb2b28433b43f475f6ca1af596a6d3363b0d3ce764e6d230d73802be7574a88f
            - >-
              -images=oauth-proxy=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:ca501a8b8967d45fcb3759d802ec4c99be8c2f5038b7503f9affed6d8ab56d65
            - >-
              -images=node-exporter=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:70529c34455306e2f964df28eb598bb24a945be9332ee674f142213ee288227a
            - >-
              -images=kube-state-metrics=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:b87caa98651032e6c940dcec5541158283b8870a6b6c5c359a3ff79403828be5
            - >-
              -images=openshift-state-metrics=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:0e3748e06ff933d185769b9690b19b8f8ee1ca8937cad1ce44b57fe7554c83f2
            - >-
              -images=kube-rbac-proxy=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:0c304c11986790f446604319dfc29db494b15f5f9fa13f2c436688ec29c92dbe
            - >-
              -images=telemeter-client=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:a0ecb6edb534408d5c4dbbe612bf58a24b82c0b4083733930c58420e1a8ebbec
            - >-
              -images=prom-label-proxy=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:d77f233e2a0c891f971926fab9fac8c8fee1ce90d8e26286d41f3baeb1b07126
            - >-
              -images=k8s-prometheus-adapter=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:3978910d80f86129340ac3351c0f15cdafae006c51d0be596edca39f6ee0f28f
            - >-
              -images=thanos=quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:8268b6cdf5c4f054f77c075e7ce864812e7069ecb4706657ece35e0e1040e452
      serviceAccount: cluster-monitoring-operator
      volumes:
        - name: telemetry-config
          configMap:
            name: telemetry-config
            defaultMode: 420
        - name: cluster-monitoring-operator-tls
          secret:
            secretName: cluster-monitoring-operator-tls
            defaultMode: 420
            optional: true
      dnsPolicy: ClusterFirst
      tolerations:
        - key: node.kubernetes.io/memory-pressure
          operator: Exists
          effect: NoSchedule
        - key: node-role.kubernetes.io/master
          operator: Exists
          effect: NoSchedule
        - key: node.kubernetes.io/unreachable
          operator: Exists
          effect: NoExecute
          tolerationSeconds: 120
        - key: node.kubernetes.io/not-ready
          operator: Exists
          effect: NoExecute
          tolerationSeconds: 120
      priorityClassName: system-cluster-critical
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 25%
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600
status:
  observedGeneration: 1
  replicas: 1
  updatedReplicas: 1
  readyReplicas: 1
  availableReplicas: 1
  conditions:
    - type: Progressing
      status: 'True'
      lastUpdateTime: '2023-01-20T00:14:53Z'
      lastTransitionTime: '2023-01-20T00:10:43Z'
      reason: NewReplicaSetAvailable
      message: >-
        ReplicaSet "cluster-monitoring-operator-74c9bc5dbd" has successfully
        progressed.
    - type: Available
      status: 'True'
      lastUpdateTime: '2023-01-20T00:15:50Z'
      lastTransitionTime: '2023-01-20T00:15:50Z'
      reason: MinimumReplicasAvailable
      message: Deployment has minimum availability.
```
