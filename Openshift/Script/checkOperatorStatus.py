import requests
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# OpenShift API endpoint
API_URL = "https://openshift.example.com/apis/operators.coreos.com/v1alpha1/namespaces/default/clusterserviceversions"

# SMTP settings
SMTP_SERVER = "smtp.example.com"
SMTP_PORT = 587
SMTP_USERNAME = "username"
SMTP_PASSWORD = "password"

# Email settings
EMAIL_FROM = "alert@example.com"
EMAIL_TO = "admin@example.com"
EMAIL_SUBJECT = "OpenShift Operator Update Required"

def check_operator_status():
    response = requests.get(API_URL)
    operators = response.json()['items']

    operators_to_update = []
    for operator in operators:
        if operator['status']['phase'] != 'Succeeded':
            operators_to_update.append(operator['metadata']['name'])

    if operators_to_update:
        send_email(operators_to_update)

def send_email(operators_to_update):
    msg = MIMEMultipart()
    msg['From'] = EMAIL_FROM
    msg['To'] = EMAIL_TO
    msg['Subject'] = EMAIL_SUBJECT

    body = "The following operators require an update or are in error state:\n\n" + "\n".join(operators_to_update)
    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    server.starttls()
    server.login(SMTP_USERNAME, SMTP_PASSWORD)
    text = msg.as_string()
    server.sendmail(EMAIL_FROM, EMAIL_TO, text)
    server.quit()

if __name__ == "__main__":
    check_operator_status()
