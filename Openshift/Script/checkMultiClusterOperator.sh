#!/bin/bash

# SMTP settings
SMTP_SERVER="smtp.example.com"
SMTP_PORT="587"
SMTP_USERNAME="username"
SMTP_PASSWORD="password"

# Email settings
EMAIL_FROM="alert@example.com"
EMAIL_TO="admin@example.com"
EMAIL_SUBJECT="OpenShift Operator Update Required"

# OpenShift clusters
CLUSTERS=("https://cluster1.example.com" "https://cluster2.example.com")

for CLUSTER in ${CLUSTERS[@]}
do
  # Login to the OpenShift cluster
  oc login $CLUSTER --token=YOUR_TOKEN

  # Check operator status
  operators_to_update=()
  for operator in $(oc get csv -o jsonpath='{.items[*].metadata.name}')
  do
    phase=$(oc get csv $operator -o jsonpath='{.status.phase}')
    if [[ "$phase" != "Succeeded" ]]; then
      operators_to_update+=("$operator")
    fi
  done

  # Send email if any operator needs to be updated
  if [ ${#operators_to_update[@]} -ne 0 ]; then
    body="The following operators in $CLUSTER require an update or are in error state:\n\n${operators_to_update[@]}"
    echo -e "Subject: $EMAIL_SUBJECT\n\n$body" | sendmail -S $SMTP_SERVER:$SMTP_PORT -f $EMAIL_FROM -t $EMAIL_TO
  fi
done



######################

##via user password

#!/bin/bash

# SMTP settings
SMTP_SERVER="smtp.example.com"
SMTP_PORT="587"
SMTP_USERNAME="username"
SMTP_PASSWORD="password"

# Email settings
EMAIL_FROM="alert@example.com"
EMAIL_TO="admin@example.com"
EMAIL_SUBJECT="OpenShift Operator Update Required"

# OpenShift clusters and credentials
CLUSTERS=("https://cluster1.example.com" "https://cluster2.example.com")
USERNAMES=("username1" "username2")
PASSWORDS=("password1" "password2")

for i in ${!CLUSTERS[@]}
do
  # Login to the OpenShift cluster
  oc login ${CLUSTERS[$i]} --username=${USERNAMES[$i]} --password=${PASSWORDS[$i]}

  # Check operator status
  operators_to_update=()
  for operator in $(oc get csv -o jsonpath='{.items[*].metadata.name}')
  do
    phase=$(oc get csv $operator -o jsonpath='{.status.phase}')
    if [[ "$phase" != "Succeeded" ]]; then
      operators_to_update+=("$operator")
    fi
  done

  # Send email if any operator needs to be updated
  if [ ${#operators_to_update[@]} -ne 0 ]; then
    body="The following operators in ${CLUSTERS[$i]} require an update or are in error state:\n\n${operators_to_update[@]}"
    echo -e "Subject: $EMAIL_SUBJECT\n\n$body" | sendmail -S $SMTP_SERVER


##########################################################################



##with smtplib
#!/bin/bash

# SMTP settings
SMTP_SERVER="smtp.example.com"
SMTP_PORT="587"
SMTP_USERNAME="username"
SMTP_PASSWORD="password"

# Email settings
EMAIL_FROM="alert@example.com"
EMAIL_TO="admin@example.com"
EMAIL_SUBJECT="OpenShift Operator Update Required"

# OpenShift clusters and credentials
CLUSTERS=("https://cluster1.example.com" "https://cluster2.example.com")
USERNAMES=("username1" "username2")
PASSWORDS=("password1" "password2")

for i in ${!CLUSTERS[@]}
do
  # Login to the OpenShift cluster
  oc login ${CLUSTERS[$i]} --username=${USERNAMES[$i]} --password=${PASSWORDS[$i]}

  # Check operator status
  operators_to_update=()
  for operator in $(oc get csv -o jsonpath='{.items[*].metadata.name}')
  do
    phase=$(oc get csv $operator -o jsonpath='{.status.phase}')
    if [[ "$phase" != "Succeeded" ]]; then
      operators_to_update+=("$operator")
    fi
  done

  # Send email if any operator needs to be updated
  if [ ${#operators_to_update[@]} -ne 0 ]; then
    body="The following operators in ${CLUSTERS[$i]} require an update or are in error state:\n\n${operators_to_update[@]}"
    python -c "
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

msg = MIMEMultipart()
msg['From'] = '$EMAIL_FROM'
msg['To'] = '$EMAIL_TO'
msg['Subject'] = '$EMAIL_SUBJECT'
msg.attach(MIMEText('$body', 'plain'))

server = smtplib.SMTP('$SMTP_SERVER', $SMTP_PORT)
server.starttls()
server.login('$SMTP_USERNAME', '$SMTP_PASSWORD')
text = msg.as_string()
server.sendmail('$EMAIL_FROM', '$EMAIL_TO', text)
server.quit()
"
  fi
done
