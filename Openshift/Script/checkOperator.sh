#!/bin/bash

# SMTP settings
SMTP_SERVER="smtp.example.com"
SMTP_PORT="587"
SMTP_USERNAME="username"
SMTP_PASSWORD="password"

# Email settings
EMAIL_FROM="alert@example.com"
EMAIL_TO="admin@example.com"
EMAIL_SUBJECT="OpenShift Operator Update Required"

# Check operator status
operators_to_update=()
for operator in $(oc get csv -o jsonpath='{.items[*].metadata.name}')
do
  phase=$(oc get csv $operator -o jsonpath='{.status.phase}')
  if [[ "$phase" != "Succeeded" ]]; then
    operators_to_update+=("$operator")
  fi
done

# Send email if any operator needs to be updated
if [ ${#operators_to_update[@]} -ne 0 ]; then
  body="The following operators require an update or are in error state:\n\n${operators_to_update[@]}"
  echo -e "Subject: $EMAIL_SUBJECT\n\n$body" | sendmail -S $SMTP_SERVER:$SMTP_PORT -f $EMAIL_FROM -t $EMAIL_TO
fi
