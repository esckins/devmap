import requests
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# OpenShift API endpoint
API_URL = "https://openshift.example.com/api/v1/plugins"

# SMTP settings
SMTP_SERVER = "smtp.example.com"
SMTP_PORT = 587
SMTP_USERNAME = "username"
SMTP_PASSWORD = "password"

# Email settings
EMAIL_FROM = "alert@example.com"
EMAIL_TO = "admin@example.com"
EMAIL_SUBJECT = "OpenShift Plugin Update Required"

def check_plugin_updates():
    response = requests.get(API_URL)
    plugins = response.json()

    plugins_to_update = []
    for plugin in plugins:
        if not plugin['isUpToDate']:
            plugins_to_update.append(plugin['name'])

    if plugins_to_update:
        send_email(plugins_to_update)

def send_email(plugins_to_update):
    msg = MIMEMultipart()
    msg['From'] = EMAIL_FROM
    msg['To'] = EMAIL_TO
    msg['Subject'] = EMAIL_SUBJECT

    body = "The following plugins require an update:\n\n" + "\n".join(plugins_to_update)
    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    server.starttls()
    server.login(SMTP_USERNAME, SMTP_PASSWORD)
    text = msg.as_string()
    server.sendmail(EMAIL_FROM, EMAIL_TO, text)
    server.quit()

if __name__ == "__main__":
    check_plugin_updates()
