#!/bin/bash

# Kubelet durumunu kontrol et
KUBELET_STATUS=$(systemctl is-active kubelet)

# Önceki durumu al
PREVIOUS_STATUS=$(cat /tmp/kubelet_status)

# Durum değiştiyse e-posta gönder
if [ "$KUBELET_STATUS" != "$PREVIOUS_STATUS" ]; then
    echo "Kubelet durumu değişti. Yeni durum: $KUBELET_STATUS" | mail -s "Kubelet Durumu Değişti" your-email@example.com
fi

# Yeni durumu dosyaya yaz
echo $KUBELET_STATUS > /tmp/kubelet_status
