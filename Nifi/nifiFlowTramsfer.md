**step 1**

```
left click > Create template

fill in the blank about name and description ex. nifi_snampshot_22.0422

after that, click Templates at the top right of the screen 

download -> (xml file) loaded your local.
```


#******************************************************************************#

```
go to new environment,

download flow -> nifi_snampshot_22.0422.xml

that finished. now you can start all flow.
```


#******************************************************************************#

```
if you encounter the error go on :)

like that;
org.apache.nifi.processors.elasticsearch.PutElasticsearch5 is not known to this NiFi instance.

click Summary at the top right of the screen

Type putelastic5 in the filter section and go to the template with the -> sign

delete and go to step 1
```



support > [errorTemplateValidate](https://stackoverflow.com/questions/62096053/apache-nifi-error-the-specified-template-is-not-in-a-valid-format)

```
Saving a flow is a different action than saving a template. The template should be saved by "Create Template" action, which saves it in the templates list of the Nifi instance. Then, it is accessible via the Templates menu item:
Under the templates list, there is an option to download a selected template as an XML file. With that, the template can be exported and uploaded into another Nifi instance correctly without any error.
```
