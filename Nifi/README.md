
### nifi cluster
##


cat /etc/hosts

```

192.168.1.120     nifiappx01 nifiappx01
192.168.1.121     nifiappx02 nifiappx02
192.168.1.122     nifiappx03 nifiappx03

```

```
scp openjdk-14.0.1_linux-x64_bin.tar.gz root@nifiappx01:/nifi

tar xvf apache-zookeeper-3.8.0-bin.tar.gz -C /nifi/
tar xvf openjdk-14.0.1_linux-x64_bin.tar.gz –C /nifi/jdk/
tar xvf nifi-1.15.3-bin.tar.gz -C /nifi/

##yum install java-1.8.0-openjdk
```



```
mv apache-zookeeper-3.8.0-bin zookeeeper-3.8

cd zookeeper-3.8
mkdir data

echo 2 > data/myid
##echo 3 > data/myid #Diğer nodelarda benzer işlemler

cp conf/zoo_sample.cfg conf/zoo.cfg
```


######################################################



vi conf/zoo.cfg


```
#################
cat conf/zoo.cfg | grep -v "#"

tickTime=2000
initLimit=10
syncLimit=5
dataDir=/nifi/zookeeper-3.8/data
clientPort=2181


server.1=192.168.1.120:2888:3888
server.2=0.0.0.0:2888:3888
server.3=192.168.1.122:2888:3888
```



#################################


```
bin/zkServer.sh start

Client port found: 2181. Client address: localhost. Client SSL: false. Error contacting service. It is probably not running.

zkServer.sh start-foreground

netstat -nlp|grep 2181

# lsof -i :2181
COMMAND   PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
java    53927 root   53u  IPv6 936472      0t0  TCP *:eforward (LISTEN)

kill -9 53927

./bin/zkServer.sh start

./bin/zkServer.sh status
ZooKeeper JMX enabled by default
Using config: /opt/zookeeper-3.8/bin/../conf/zoo.cfg
Client port found: 2181. Client address: localhost. Client SSL: false.
Error contacting service. It is probably not running.
```



Port connection
Network layer has tcp nd udp protocol. ldap ex. 389 port

""""same vlan can connection between other hosts.#alll nodes re simple works. otherwise you can problem about connection problems."""

```
netstat -nlp|grep 2181
tcp6       0      0 :::2181                 :::*                    LISTEN      54817/java

telnet 192.168.1.121 2888
Trying 192.168.1.121...
Connected to 192.168.1.121.
```


##zkConnection ok


####################################################
## Jdk configurations;

vi ~/.bashrc

########################################

```
# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

export JAVA_HOME=/nifi/jdk/jdk-14.0.1
export PATH=$PATH:$JAVA_HOME/bin


# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi
```

#########################################

```
source ~/.bashrc

update-alternatives --install /usr/bin/java java /nifi/jdk/jdk-14.0.1/bin/java 100

# java -version
openjdk version "1.8.0_322"
OpenJDK Runtime Environment (build 1.8.0_322-b06)
OpenJDK 64-Bit Server VM (build 25.322-b06, mixed mode)
```


#############################################################################
#Nifi install;

cat /nifi/nifi-1.15.3/conf/zookeeper.properties | grep -v "#"

```
initLimit=10
autopurge.purgeInterval=24
syncLimit=5
tickTime=2000
dataDir=/nifi/zookeeper-3.8/data
autopurge.snapRetainCount=30


server.1=192.168.1.120:2888:3888
server.2=192.168.1.121:2888:3888
server.3=192.168.1.122:2888:3888
```


##############################################################################

#### cat nifi.properties| grep -v "#"

```
nifi.flow.configuration.file=./conf/flow.xml.gz
nifi.flow.configuration.archive.enabled=true
nifi.flow.configuration.archive.dir=./conf/archive/
nifi.flow.configuration.archive.max.time=30 days
nifi.flow.configuration.archive.max.storage=500 MB
nifi.flow.configuration.archive.max.count=
nifi.flowcontroller.autoResumeState=true
nifi.flowcontroller.graceful.shutdown.period=10 sec
nifi.flowservice.writedelay.interval=500 ms
nifi.administrative.yield.duration=30 sec
nifi.bored.yield.duration=10 millis
nifi.queue.backpressure.count=10000
nifi.queue.backpressure.size=1 GB

nifi.authorizer.configuration.file=./conf/authorizers.xml
nifi.login.identity.provider.configuration.file=./conf/login-identity-providers.xml
nifi.templates.directory=./conf/templates
nifi.ui.banner.text=
nifi.ui.autorefresh.interval=30 sec
nifi.nar.library.directory=./lib
nifi.nar.library.autoload.directory=./extensions
nifi.nar.working.directory=./work/nar/
nifi.documentation.working.directory=./work/docs/components

nifi.state.management.configuration.file=./conf/state-management.xml
nifi.state.management.provider.local=local-provider
nifi.state.management.provider.cluster=zk-provider
nifi.state.management.embedded.zookeeper.start=false
nifi.state.management.embedded.zookeeper.properties=./conf/zookeeper.properties

nifi.database.directory=./database_repository
nifi.h2.url.append=;LOCK_TIMEOUT=25000;WRITE_DELAY=0;AUTO_SERVER=FALSE

nifi.repository.encryption.protocol.version=
nifi.repository.encryption.key.id=
nifi.repository.encryption.key.provider=
nifi.repository.encryption.key.provider.keystore.location=
nifi.repository.encryption.key.provider.keystore.password=

nifi.flowfile.repository.implementation=org.apache.nifi.controller.repository.WriteAheadFlowFileRepository
nifi.flowfile.repository.wal.implementation=org.apache.nifi.wali.SequentialAccessWriteAheadLog
nifi.flowfile.repository.directory=./flowfile_repository
nifi.flowfile.repository.checkpoint.interval=20 secs
nifi.flowfile.repository.always.sync=false
nifi.flowfile.repository.retain.orphaned.flowfiles=true

nifi.swap.manager.implementation=org.apache.nifi.controller.FileSystemSwapManager
nifi.queue.swap.threshold=20000

nifi.content.repository.implementation=org.apache.nifi.controller.repository.FileSystemRepository
nifi.content.claim.max.appendable.size=1 MB
nifi.content.repository.directory.default=./content_repository
nifi.content.repository.archive.max.retention.period=7 days
nifi.content.repository.archive.max.usage.percentage=50%
nifi.content.repository.archive.enabled=true
nifi.content.repository.always.sync=false
nifi.content.viewer.url=../nifi-content-viewer/

nifi.provenance.repository.implementation=org.apache.nifi.provenance.WriteAheadProvenanceRepository

nifi.provenance.repository.directory.default=./provenance_repository
nifi.provenance.repository.max.storage.time=30 days
nifi.provenance.repository.max.storage.size=10 GB
nifi.provenance.repository.rollover.time=10 mins
nifi.provenance.repository.rollover.size=100 MB
nifi.provenance.repository.query.threads=2
nifi.provenance.repository.index.threads=2
nifi.provenance.repository.compress.on.rollover=true
nifi.provenance.repository.always.sync=false
nifi.provenance.repository.indexed.fields=EventType, FlowFileUUID, Filename, ProcessorID, Relationship
nifi.provenance.repository.indexed.attributes=
nifi.provenance.repository.index.shard.size=500 MB
nifi.provenance.repository.max.attribute.length=65536
nifi.provenance.repository.concurrent.merge.threads=2


nifi.provenance.repository.buffer.size=100000

nifi.components.status.repository.implementation=org.apache.nifi.controller.status.history.VolatileComponentStatusRepository

nifi.components.status.repository.buffer.size=1440
nifi.components.status.snapshot.frequency=1 min

nifi.status.repository.questdb.persist.node.days=14
nifi.status.repository.questdb.persist.component.days=3
nifi.status.repository.questdb.persist.location=./status_repository

nifi.remote.input.host=
nifi.remote.input.secure=false
nifi.remote.input.socket.port=
nifi.remote.input.http.enabled=true
nifi.remote.input.http.transaction.ttl=30 sec
nifi.remote.contents.cache.expiration=30 secs


nifi.web.war.directory=./lib
nifi.web.http.host=192.168.1.121
nifi.web.http.port=8078
nifi.web.http.network.interface.default=
nifi.web.https.host=
nifi.web.https.port=
nifi.web.https.network.interface.default=
nifi.web.jetty.working.directory=./work/jetty
nifi.web.jetty.threads=200
nifi.web.max.header.size=16 KB
nifi.web.proxy.context.path=
nifi.web.proxy.host=






## this configuration must be same other nodes.
nifi.sensitive.props.key=<key>
nifi.sensitive.props.key.protected=
nifi.sensitive.props.algorithm=NIFI_PBKDF2_AES_GCM_256
nifi.sensitive.props.additional.keys=
nifi.sensitive.props.provider=



nifi.security.autoreload.enabled=false
nifi.security.autoreload.interval=10 secs
nifi.security.keystore=
nifi.security.keystoreType=
nifi.security.keystorePasswd=
nifi.security.keyPasswd=
nifi.security.truststore=
nifi.security.truststoreType=
nifi.security.truststorePasswd=
nifi.security.user.authorizer=
nifi.security.allow.anonymous.authentication=false
nifi.security.user.login.identity.provider=
nifi.security.user.jws.key.rotation.period=
nifi.security.ocsp.responder.url=
nifi.security.ocsp.responder.certificate=

nifi.security.user.oidc.discovery.url=
nifi.security.user.oidc.connect.timeout=5 secs
nifi.security.user.oidc.read.timeout=5 secs
nifi.security.user.oidc.client.id=
nifi.security.user.oidc.client.secret=
nifi.security.user.oidc.preferred.jwsalgorithm=
nifi.security.user.oidc.additional.scopes=
nifi.security.user.oidc.claim.identifying.user=
nifi.security.user.oidc.fallback.claims.identifying.user=

nifi.security.user.knox.url=
nifi.security.user.knox.publicKey=
nifi.security.user.knox.cookieName=hadoop-jwt
nifi.security.user.knox.audiences=

nifi.security.user.saml.idp.metadata.url=
nifi.security.user.saml.sp.entity.id=
nifi.security.user.saml.identity.attribute.name=
nifi.security.user.saml.group.attribute.name=
nifi.security.user.saml.metadata.signing.enabled=false
nifi.security.user.saml.request.signing.enabled=false
nifi.security.user.saml.want.assertions.signed=true
nifi.security.user.saml.message.logging.enabled=false
nifi.security.user.saml.authentication.expiration=12 hours
nifi.security.user.saml.single.logout.enabled=false
nifi.security.user.saml.http.client.truststore.strategy=JDK
nifi.security.user.saml.http.client.connect.timeout=30 secs
nifi.security.user.saml.http.client.read.timeout=30 secs



nifi.cluster.protocol.heartbeat.interval=5 sec
nifi.cluster.protocol.heartbeat.missable.max=8
nifi.cluster.protocol.is.secure=false

nifi.cluster.is.node=true
nifi.cluster.node.address=192.168.1.121
nifi.cluster.node.protocol.port=8076
nifi.cluster.node.protocol.max.threads=50
nifi.cluster.node.event.history.size=25
nifi.cluster.node.connection.timeout=5 sec
nifi.cluster.node.read.timeout=5 sec
nifi.cluster.node.max.concurrent.requests=100
nifi.cluster.firewall.file=
nifi.cluster.flow.election.max.wait.time=5 mins
nifi.cluster.flow.election.max.candidates=

nifi.cluster.load.balance.host=
nifi.cluster.load.balance.port=6342
nifi.cluster.load.balance.connections.per.node=1
nifi.cluster.load.balance.max.thread.count=8
nifi.cluster.load.balance.comms.timeout=30 sec

nifi.zookeeper.connect.string=192.168.1.120:2181,192.168.1.121:2181,192.168.1.122:2181
nifi.zookeeper.connect.timeout=10 secs
nifi.zookeeper.session.timeout=10 secs
nifi.zookeeper.root.node=/nifi
nifi.zookeeper.client.secure=false
nifi.zookeeper.security.keystore=
nifi.zookeeper.security.keystoreType=
nifi.zookeeper.security.keystorePasswd=
nifi.zookeeper.security.truststore=
nifi.zookeeper.security.truststoreType=
nifi.zookeeper.security.truststorePasswd=
nifi.zookeeper.jute.maxbuffer=

nifi.zookeeper.auth.type=
nifi.zookeeper.kerberos.removeHostFromPrincipal=
nifi.zookeeper.kerberos.removeRealmFromPrincipal=

nifi.kerberos.krb5.file=

nifi.kerberos.service.principal=
nifi.kerberos.service.keytab.location=

nifi.kerberos.spnego.principal=
nifi.kerberos.spnego.keytab.location=
nifi.kerberos.spnego.authentication.expiration=12 hours

nifi.variable.registry.properties=

nifi.analytics.predict.enabled=false
nifi.analytics.predict.interval=3 mins
nifi.analytics.query.interval=5 mins
nifi.analytics.connection.model.implementation=org.apache.nifi.controller.status.analytics.models.OrdinaryLeastSquares
nifi.analytics.connection.model.score.name=rSquared
nifi.analytics.connection.model.score.threshold=.90

nifi.monitor.long.running.task.schedule=
nifi.monitor.long.running.task.threshold=


nifi.diagnostics.on.shutdown.enabled=false

nifi.diagnostics.on.shutdown.verbose=false

nifi.diagnostics.on.shutdown.directory=./diagnostics

nifi.diagnostics.on.shutdown.max.filecount=10

nifi.diagnostics.on.shutdown.max.directory.size=10 MB

```


#########################################################


cat state-management.xml

```
#property for cluster nodes.

<property name="Connect String">192.168.1.120:2181,192.168.1.121:2181,192.168.1.122:2181</property>

```

#########################

```
vi state-management
/property name="Connect String"
```


#########################


```
 mkdir state
cd state/
mkdir zookeeper
##ilgili node id

echo 1 > myid
ll
mv myid zookeeper/

#####

bin/nifi.sh install
bin/nifi.sh start
```

```
# bin/nifi.sh status

Java home: /nifi/jdk/jdk-14.0.1
NiFi home: /nifi/nifi-1.15.3

Bootstrap Config File: /nifi/nifi-1.15.3/conf/bootstrap.conf

2022-03-25 12:17:02,650 INFO [main] org.apache.nifi.bootstrap.Command Apache NiFi is currently running, listening to Bootstrap on port 45977, PID=76552


```

#### tail -99f /logs/nifi.app.log

Failed unmarshalling 'CONNECTION_RESPONSE' protocol message from localhost/127.0.0.1:11443 due to: java.net.SocketTimeoutException: Read timed out

####

```
nifi.cluster.node.connection.timeout=30 sec
nifi.cluster.node.read.timeout=10 sec
```

####

or 

###

```
If you are suspecting purely timeout issues, please attempt to tweak the below values in nifi.properties and restart the service:
- nifi.cluster.node.protocol.threads=50 (Default 10)
- nifi.cluster.node.connection.timeout=30 sec (Default 5 sec)
- nifi.cluster.node.read.timeout=30 sec (Default 5 sec)


Please find below a set of configurations that worth tuning on larger clusters based on https://nifi.apache.org/docs/nifi-docs/html/administration-guide.html
The below are some example values for larger clusters (you need to tune it based on your unique setup):
nifi.cluster.node.protocol.threads=70
nifi.cluster.node.protocol.max.threads=100
nifi.zookeeper.session.timeout=30 sec
nifi.zookeeper.connect.timeout=30 sec
nifi.cluster.node.connection.timeout=60 sec
nifi.cluster.node.read.timeout=60 sec
nifi.ui.autorefresh.interval=900 sec
nifi.cluster.protocol.heartbeat.interval=20 sec
nifi.components.status.repository.buffer.size=300
nifi.components.status.snapshot.frequency=5 mins
nifi.cluster.node.protocol.max.threads=120
nifi.cluster.node.protocol.threads=80
nifi.cluster.node.read.timeout=90 sec
nifi.cluster.node.connection.timeout=90 sec
nifi.cluster.node.read.timeout=90 sec
```

###

##problems

---
`nifi PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target`

The InvokeHTTP in NiFi is a client of your API. This means that the InvokeHTTP needs to be able to trust your remote server to ensure it's not connecting to a malicious service. To do this, we need to add the Certificate Authority/Root Authority of the remote service to InvokeHTTP's truststore. The CA required will be shown in the 'Issuer' fields of the service's server certificate.

To get the CA, you can follow this blog: https://daniel.haxx.se/blog/2018/11/07/get-the-ca-cert-for-curl/, and retrieve the CA into a pem file with:

`echo quit | openssl s_client -showcerts -servername server -connect server:443 > cacert.pem`

Then, you can use Keystore Explorer (https://keystore-explorer.org) to create a new truststore file (which is a much easier way to manage key/truststores), and import the certificate cacert.pem into this truststore.

You then configure an SSL Context Service in the InvokeHTTP processor, which references the truststore you created.



----

I tried to follow the below steps. and restarted the NIFI Instance. It started working.


1. echo -n | openssl s_client -connect <server name>:<port> |sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/examplecert.crt

2. keytool -import -trustcacerts -keystore /apps/java/jdk1.8.0_211-amd64/jre/lib/security/cacerts  -storepass changeit -noprompt -alias mito -file /tmp/examplecert.crt

---

Usually the error "unable to find valid certification path to
requested target" means that the truststore used by NiFi is not
trusting the cert presented by NiFi Registry.

You can check this by using keytool to inspect the keystores and truststores.

On the NiFi server, use keytool to list the truststore.jks:

keytool --list -v -keystore truststore.jks

Look for the Owner entry in the output.

On the NiFi Registry side, list the keystore.jks:

keytool --list -v -keystore keystore.jks

Look for the Issuer entry in the output.

The issuer of the NiFi Registry cert, should be the owner of the
trusted cert in NiFI's truststore.


Thx for https://www.datasciencearth.com/apache-nifi-bolum-8-nifi-cluster-kurulumu/


https://alasdairb.com/2021/08/09/building-a-simple-rest-api-in-nifi/

```
building a simple rest api in nifi

Building a simple REST API in NiFi
In a previous post I discussed using REST APIs to enrich records at the time of ingest. This post will cover building the corresponding REST API that I used in that post.

This will be a very simple REST API that exposes a single endpoint GET /api. This endpoint returns a 200 OK response with a small piece of JSON that changes based on the value of a URL parameter param1. Any other path or method will return a 404.

First, we need a web server that accept HTTP requests. For this, we can use the HandleHTTPRequest processor (behind the scenes this includes a Jetty web server).

Add the HandleHTTPRequest and double click it to enter the config window. You can customise the Port it listens on, but I will leave it at the default 80. You must add & enable a HTTP Context Map – simply select Create new service… from the drop down and create a new one with default settings (then enable it). Next is Allowed Paths that controls which paths the API will respond to – this is a regex pattern so can include many paths, but in my case I am only allowing one (/api). Lastly, set all methods except GET to false (e.g. Allow Post, Allow PUT, etc).


That’s all we need to accept HTTP Requests. Hit Apply.

Now, we need to return a response. Add a new HandleHTTPResponse. Set the HTTP Status Code to 200 and for HTTP Context Map select the same service you created for the HandleHTTPRequest. That’s it, hit Apply.


Now connect the Success relationship from HandleHTTPRequest to HandleHTTPResponse. Auto-terminate any other relationships (or add them to funnels if you want).


Start the processors and use CURL to test it.

$ curl localhost/api?param1=test -i
HTTP/1.1 200 OK
Date: Sun, 08 Aug 2021 19:47:54 GMT
Transfer-Encoding: chunked
Server: Jetty(9.4.42.v20210604)
Now that we have a basic Request>Response flow, we want to add some logic in the middle. Let’s quickly demonstrate returning a different response based on a URL parameter.

Add a new RouteOnAttribute to the flow and enter the configuration.

Add a new property with the + icon and name it val1. The value of this property will be:

${http.param.param1:equals('val1')}
Do the same for val2 as below.


Create a copy of the HandleHTTPResponse (click on it, Ctrl+C, Ctrl+V) and modify the config of the copy. Change the HTTP Status Code to 201.


Now connect them all together. The HandleHTTPRequest should feed in to the RouteOnAttribute. From RouteOnAttribute, the val1 relationship should go to the first HandleHTTPResponse with the 200 response code – the val2 relationship goes to the second, with the 201 response code. See the screenshot below.


Start all the processors and test it with cURL. In this cURL we are passing a URL parameter param1 with the values val1 or val2. This parameter becomes an attribute on the FlowFile for this request (http.param.param1). The RouteOnAttribute accesses this attribute and routes based on the value, giving us a 200 response for val1 or a 201 response for val2.

$ curl localhost/api?param1=val1 -i
HTTP/1.1 200 OK
Date: Sun, 08 Aug 2021 19:52:05 GMT
Transfer-Encoding: chunked
Server: Jetty(9.4.42.v20210604)

$ curl localhost/api?param1=val2 -i
HTTP/1.1 201 Created
Date: Sun, 08 Aug 2021 19:52:11 GMT
Transfer-Encoding: chunked
Server: Jetty(9.4.42.v20210604)
In reality, we don’t really want to get different HTTP responses like this. The user of my API (the other NiFi flow) actually wanted some JSON in response. So let’s do that.

We want to return some JSON in the body of the resonse, so let’s add a ReplaceText. As the FlowFile content is currently empty (null), we need to set Replacement Strategy to Always Replace and Evaluation Mode to Entire Text (you will see mime-type errors without these settings).

Lastly, we need to set Replacement Value to the full content we want in our response, which is going to be a bit of JSON.

{
  "result": "you sent val1"
}

We need to do the same thing for val2, so make a copy of the ReplaceText processor and change the JSON to say val2 instead.


We don’t both HandleHTTPResponse‘s anymore, so delete the second one. Now connect the RouteOnAttribute to the 2 ReplaceText‘s and then connect the 2 ReplaceText‘s to the single HandleHTTPResponse.


Start all the processors and test it wth curl.

$ curl localhost/api?param1=val1
{
 "result":"you sent val1"
}
$ curl localhost/api?param1=val2
{
 "result":"you sent val2"
}
We now have a very basic REST API that is able to return different results based on user provided parameters. You can extend this with much more complexity – supporting more paths, allowing more HTTP methods, accepting more parameters, doing more complex routing logic and handling error with appropriate HTTP response codes.

Find the flow definition here if you’d like to import the whole thing.

thx sdairs for this port

```

ex flow file;


![image-1.png](./image-1.png)
