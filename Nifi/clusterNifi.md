## Nifi cluster
##

$ cat /etc/hosts

10.30.30.13     nifix01
10.30.30.15     nifix02
10.30.30.16     nifix03


##### jdk - > https://www.oracle.com/java/technologies/javase-downloads.html

##### zookeeper - > https://zookeeper.apache.org/releases.html 

##### apache nifi - > https://nifi.apache.org/download.html


yum install java-1.8.0-openjdk

[root@nifix01 opt]# cd /bin
[root@nifix01 bin]# java -version
openjdk version "1.8.0_322"
OpenJDK Runtime Environment (build 1.8.0_322-b06)
OpenJDK 64-Bit Server VM (build 25.322-b06, mixed mode)


$ scp apache-zookeeper-3.8.0-bin.tar.gz nifi-1.15.3-bin.tar.gz root@nifix01:/nifi ...

apache-zookeeper-3.8.0-bin.tar.gz                                         100%   13MB  64.2MB/s   00:00
nifi-1.15.3-bin.tar.gz                                                    100% 1479MB 197.3MB/s   00:07
