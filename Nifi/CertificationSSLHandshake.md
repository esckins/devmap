unable to find valid certification path to requested target
keystore truststore is invalid nifi


```
What issue do you get when using openssl s_client ? You should definitely be able to connect using s_client, although you may need to provide -CAfile option to trust the issued server certificate and -cert and -key options to provide a client certificate as below:

openssl s_client -connect nifi.nifi.apache.org:9443 -debug -state -CAfile conf/nifi-cert.pem -cert conf/client.pem -key conf/client.key

As for the handshake_failure issue, this may be because:

The certificate has expired or is invalid
The provided keystore or key password is incorrect and the controller service cannot access the keystore
The server and client cannot agree on a suitable cipher suite during negotiation
Diagnosing with s_client is definitely the correct tool. You can also enable TLS debugging via the bootstrap.conf file with the line:

java.arg.15=-Djavax.net.debug=ssl,handshake

This output will be in logs/nifi-bootstrap.log.
```


```
For all nodes;

openssl s_client -connect <host>:<port> -showcerts
vi CA-1.crt
vi CA-2.crt
keytool -import -alias SSLContext -file CA-1.crt -keystore truststore.jks
keytool -import -alias SSLContext2 -file CA-2.crt -keystore truststore.jks
yes
admin password
Admin123!
chmod 777 truststore.jks

openssl s_client -connect nifi.nifi.apache.org:9443 -debug -state -CAfile conf/nifi-cert.pem -cert conf/client.pem -key conf/client.key

```

![image.png](./image.png)

nifi invokehttp module  ssl sslcontext certificate not add

https://community.cloudera.com/t5/Support-Questions/Is-it-possible-to-provide-options-to-InvokeHTTP-nifi/td-p/236239

https://stackoverflow.com/questions/71374815/the-driver-could-not-establish-a-secure-connection-to-sql-server-by-using-secure
