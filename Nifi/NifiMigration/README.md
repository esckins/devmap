## NifiMigration


https://downloads.apache.org/nifi/


### Upgrade apache nifi 

https://nifi.apache.org/docs/nifi-docs/html/administration-guide.html


create new directory ;

Host Machine - Node 1
|--> opt/
   |--> existing-nifi
   |--> new-nifi
Host Machine - Node 2
|--> opt/
   |--> existing-nifi
   |--> new-nifi
Host Machine - Node 3
|--> opt/
   |--> existing-nifi
   |--> new-nifi


! Save old filepath

systemctl stop nifi


# scp nifi-1.16.3-bin.tar.gz root@nifix01:/nifi root@nifix02:/nifi root@nifix03:/nifi nifi-1.16.3-bin.tar.gz   


open multi exec in mobaxterm


tar -xvf nifi-1.16.3-bin.tar.gz

compare 2 files for nifi.properties  ---> Plugin -> Compare -> Compare
or diff cli in linux

```
Configuration file	Necessary changes
authorizers.xml

Copy the <authorizer>…​</authorizer> configured in the existing authorizers.xml to the new NiFi file.

If you are using the file-provider authorizer, ensure that you copy the users.xml and authorizations.xml files from the existing to the new NiFi.

Configuration best practices recommend creating a separate location outside of the NiFi base directory for storing such configuration files, for example: /opt/nifi/configuration-resources/. If you are storing these files in a separate directory, you do not need to move them. Instead, ensure that the new NiFi is pointing to the same files.

bootstrap-notification-services.xml

Use the existing NiFi bootstrap-notification-services.xml file to update properties in the new NiFi.

bootstrap.conf

Use the existing NiFi bootstrap.conf file to update properties in the new NiFi.

flow.json.gz

If you retained the default location for storing flows (<installation-directory>/conf/), copy flow.json.gz from the existing to the new NiFi base install conf directory. If you stored flows to an external location via nifi.properties, update the property nifi.flow.configuration.file to point there.

If you are encrypting sensitive component properties in your dataflow via the sensitive properties key in nifi.properties, make sure the same key is used when copying over your flow.json.gz. If you need to change the key, see the Migrating a Flow with Sensitive Properties section below.

nifi.properties

Use the existing nifi.properties to populate the same properties in the new NiFi file.

Note: This file contains the majority of NiFi configuration settings, so ensure that you have copied the values correctly.

If you followed NiFi best practices, the following properties should be pointing to external directories outside of the base NiFi installation path.

If the below properties point to directories inside the NiFi base installation path, you must copy the target directories to the new NiFi. Stop your existing NiFi installation before you do this.

nifi.flow.configuration.json.file=

If you have retained the default value (./conf/flow.json.gz), copy flow.json.gz from the existing to the new NiFi base install conf directory.

If you stored flows to an external location, update the property value to point there.

nifi.flow.configuration.archive.dir=

Same applies as above if you want to retain archived copies of the flow.json.gz.

nifi.database.directory=

Best practices recommends that you use an external location for each repository. Point the new NiFi at the same external database repository location.

nifi.flowfile.repository.directory=

Best practices recommends that you use an external location for each repository. Point the new NiFi at the same external flowfile repository location.

Warning: You may experience data loss if flowfile repositories are not accessible to the new NiFi.

nifi.content.repository.directory.default=

Best practices recommends that you use an external location for each repository. Point the new NiFi at the same external content repository location.

Your existing NiFi may have multiple content repos defined. Make sure the exact same property names are used and point to the appropriate matching content repo locations. For example:

nifi.content.repository.directory.content1= nifi.content.repository.directory.content2=

Warning: You may experience data loss if content repositories are not accessible to the new NiFi.

Warning: You may experience data loss if property names are wrong or the property points to the wrong content repository.

nifi.provenance.repository.directory.default=

Best practices recommends that you use an external location for each repository. Point the new NiFi at the same external provenance repository location.

Your existing NiFi may have multiple content repos defined. Make sure the exact same property names are used and point to the appropriate matching provenance repo locations. For example:

nifi.provenance.repository.directory.provenance1= nifi.provenance.repository.directory.provenance2=

Note: You may not be able to query old events if provenance repos are not moved correctly or properties are not updated correctly.

state-management.xml

For the local-provider state provider, verify the location of the local directory.

If you have retained the default location (./state/local), copy the complete directory tree to the new NiFi. The existing NiFi should be stopped if you are copying this directory because it may be constantly writing to this directory while running.

Configuration best practices recommend that you move the state to an external directory like /opt/nifi/configuration-resources/ to facilitate easier upgrading later.

For a NiFi cluster, the cluster-provider ZooKeeper “Connect String" property should be set to the same external ZooKeeper as the existing NiFi installation.

For a NiFi cluster, make sure the cluster-provider ZooKeeper "Root Node" property matches exactly the value used in the existing NiFi.

If you are also setting up a new external ZooKeeper, see the ZooKeeper Migrator section for instructions on how to move ZooKeeper information from one cluster to another and migrate ZooKeeper node ownership.
```



cat state-management.xml | grep -v "#" | wc -l


# systemctl show nifi.service | grep path

find / -name nifi.service

# systemd-analyze --system unit-paths

# bin/nifi.sh install
# bin/nifi.sh start
# bin/nifi.sh status

#--> running currently but old version !

The Flow Controller is initializing the Data Flow.

---

```
[centos@ip-172-30-0-249 system]$ cat /etc/systemd/system/nifi.service 
[Unit]
Description=Apache NiFi
After=network.target

[Service]
#Type=forking
Type=service
#User=nifi
#Group=nifi
ExecStart=/opt/nifi-latest/bin/nifi.sh start
ExecStop=/opt/nifi-latest/bin/nifi.sh stop
ExecRestart=/opt/nifi-latest/bin/nifi.sh restart 

[Install]
WantedBy=multi-user.target
```

```
cat zookeeper.properties

initLimit=10
autopurge.purgeInterval=24
syncLimit=5
tickTime=2000
#dataDir=./state/zookeeper
dataDir=./nifi/zookeeper-3.8/data
autopurge.snapRetainCount=30

server.1=10.30.10.10:2888:3888
server.2=10.30.10.12:2888:3888
server.3=10.30.10.11:2888:3888
```




bin/nifi.sh start for 1 node
This node is currently not connected to the cluster. Any modifications to the data flow made here will not replicate across the cluster.

```
systemctl daemon-reload
# service nifi start  --> ubuntuoracle
#systemctl start nifi  --> centos

journalctl -xe
```

```
systemctl daemon-reload

bin/nifi.sh install
Service nifi installed

bin/nifi.sh status
service nifi status
```
note: update logging.xml file for your log management in c/nifi/conf/
---

node by node upgrade for nifi.

I did that and it worked, at the end of the day I created a service and updated the relevant fields.

good luck


![image.png](./image.png)

![image-1.png](./image-1.png)
