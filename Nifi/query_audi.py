GET /security-auditlog-*/_search
{
  "aggs": {
    "2": {
      "terms": {
        "field": "audit_request_effective_user.keyword",
        "order": {
          "_count": "desc"
        },
        "size": 50
      },
      "aggs": {
        "3": {
          "terms": {
            "field": "audit_category.keyword",
            "order": {
              "_count": "desc"
            },
            "size": 50
          }
        }
      }
    }
  },
  "size": 0,
  "stored_fields": [
    "*"
  ],
  "script_fields": {},
  "docvalue_fields": [
    {
      "field": "@timestamp",
      "format": "date_time"
    }
  ],
  "_source": {
    "excludes": []
  },
  "query": {
    "bool": {
      "must": [],
      "filter": [
        {
          "bool": {
            "filter": [
              {
                "bool": {
                  "should": [
                    {
                      "exists": {
                        "field": "audit_request_effective_user.keyword"
                      }
                    }
                  ],
                  "minimum_should_match": 1
                }
              },
              {
                "bool": {
                  "should": [
                    {
                      "exists": {
                        "field": "audit_category.keyword"
                      }
                    }
                  ],
                  "minimum_should_match": 1
                }
              }
            ]
          }
        },
        {
          "range": {
            "@timestamp": {
              "gte": "now-1d",
              "lte": "now",
              "format": "strict_date_optional_time"
            }
          }
        }
      ],
      "should": [],
      "must_not": []
    }
  }
}
