for n in $(kubectl get -o=name pvc,configmap,serviceaccount,secret,ingress,service,deployment,statefulset,hpa,job,cronjob)
do
    mkdir -p $(dirname $n)
    kubectl get -o=yaml $n > $n.yaml
done

. generate-yaml.sh

for n in $(kubectl get -o=name -n devops pvc,configmap,serviceaccount,secret,ingress,service,deployment,statefulset,hpa,job,cronjob)
do
    mkdir -p $(dirname $n)
    kubectl get -o=yaml -n devops $n > $n.yaml
done

####
$ tree

kubectl get all

kubectl get service hellworldexample-helloworld -n default -o yaml > service.yaml 

kubectl get service --all-namespaces -o yaml  > all-service.yaml


kubectl get deployment myreleasename-helloworld -n default -o yaml > deployment.yaml

kubectl get deploy --all-namespaces -o yaml  > all-deployment.yaml


R's : https://jhooq.com/get-yaml-for-deployed-kubernetes-resources/



$ oc get kafka my-cluster -o json |jq '.spec'
{
  "entityOperator": {
    "topicOperator": {
      "reconciliationIntervalSeconds": 90
    },
    "userOperator": {
      "reconciliationIntervalSeconds": 120
    }
  },
  "kafka": {
    "config": {
      "log.message.format.version": "2.5",
      "message.max.bytes": 1000000,
      "offsets.topic.replication.factor": 3,
      "transaction.state.log.min.isr": 2,
      "transaction.state.log.replication.factor": 3
    },
    "listeners": {
      "plain": {
        "authentiation": {
          "type": "scram-sha-512"
        }
      },
      "tls": {
        "authentiation": {
          "type": "tls"
        }
      }
    },
    "replicas": 3,
    "storage": {
      "type": "ephemeral"
    },
    "version": "2.5.0"
  },
  "zookeeper": {
    "replicas": 3,
    "storage": {
      "type": "ephemeral"
    }
  }
}

