
change kubernetes.ndjson file metricbeat* to kubernetes* w/notepadd+ and import kibana for dashboards.

```
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: metricbeat-config
  namespace: kube-system
  labels:
    k8s-app: metricbeat
data:
  metricbeat.yml: |-
    metricbeat.modules:
      - module: kubernetes
        period: 60s
        enabled: true
        metricsets:
          - node
          - system
          - pod
          - container
          - volume
        hosts: ["https://10.30.30.10:10250",".."]
        #hosts: ["localhost:10255"]
        bearer_token_file: /var/run/secrets/kubernetes.io/serviceaccount/token
        #bearer_token_file: /var/run/secrets/kubernetes.io/serviceaccount/metricbeat-token-w8lnh
        #bearer_token_file: /var/run/secrets/kubernetes.io/serviceaccount/kube-state-metrics-token-6gtnp
        ssl.verification_mode: "none"
      - module: system
        period: 60s
        metricsets:
          - cpu
          - load
          - memory
          - network
          - process
          - process_summary
          #- core
          #- diskio
          #- socket
          #- users
        enabled: true
        processes: ['.*']
        process.include_top_n:
          by_cpu: 5      # include top 5 processes by CPU
          by_memory: 5   # include top 5 processes by memory

      - module: system
        period: 15m
        metricsets:
          - filesystem
          - fsstat
        processors:
        - drop_event.when.regexp:
            system.filesystem.mount_point: '^/(sys|cgroup|proc|dev|etc|host|lib)($|/)'

    output.elasticsearch:
      hosts: ['192.168.83.44:9200','192.168.182.37:9200']
    #setup.kibana:
    #  host: "kibana-internal.default.svc.cluster.local:5601"
    output.elasticsearch.index: "kubernetes-%{+yyyy.MM.dd}"
    setup.template.name: "kubernetes"
    setup.template.pattern: "kubernetes-*"
    setup.ilm.enabled: false
```




```
---
kind: DaemonSet
apiVersion: apps/v1
metadata:
  name: metricbeat
  namespace: kube-system
  labels:
    k8s-app: metricbeat
  annotations:
    deprecated.daemonset.template.generation: '2'
  managedFields:
    - manager: dashboard
      operation: Update
      apiVersion: apps/v1
      time: '2022-03-12T14:54:42Z'
      fieldsType: FieldsV1
      fieldsV1:
        f:metadata:
          f:annotations:
            .: {}
            f:deprecated.daemonset.template.generation: {}
          f:labels:
            .: {}
            f:k8s-app: {}
        f:spec:
          f:revisionHistoryLimit: {}
          f:selector: {}
          f:template:
            f:metadata:
              f:labels:
                .: {}
                f:k8s-app: {}
            f:spec:
              f:containers:
                k:{"name":"metricbeat"}:
                  .: {}
                  f:args: {}
                  f:env:
                    .: {}
                    k:{"name":"ELASTICSEARCH_HOST"}:
                      .: {}
                      f:name: {}
                      f:value: {}
                    k:{"name":"ELASTICSEARCH_PASSWORD"}:
                      .: {}
                      f:name: {}
                      f:value: {}
                    k:{"name":"ELASTICSEARCH_PORT"}:
                      .: {}
                      f:name: {}
                      f:value: {}
                    k:{"name":"ELASTICSEARCH_USERNAME"}:
                      .: {}
                      f:name: {}
                      f:value: {}
                  f:image: {}
                  f:imagePullPolicy: {}
                  f:name: {}
                  f:resources:
                    .: {}
                    f:limits:
                      .: {}
                      f:memory: {}
                    f:requests:
                      .: {}
                      f:cpu: {}
                      f:memory: {}
                  f:securityContext:
                    .: {}
                    f:runAsUser: {}
                  f:terminationMessagePath: {}
                  f:terminationMessagePolicy: {}
                  f:volumeMounts:
                    .: {}
                    k:{"mountPath":"/etc/metricbeat.yml"}:
                      .: {}
                      f:mountPath: {}
                      f:name: {}
                      f:readOnly: {}
                      f:subPath: {}
                    k:{"mountPath":"/hostfs/proc"}:
                      .: {}
                      f:mountPath: {}
                      f:name: {}
                      f:readOnly: {}
                    k:{"mountPath":"/hostfs/sys/fs/cgroup"}:
                      .: {}
                      f:mountPath: {}
                      f:name: {}
                      f:readOnly: {}
              f:dnsPolicy: {}
              f:hostNetwork: {}
              f:restartPolicy: {}
              f:schedulerName: {}
              f:securityContext: {}
              f:serviceAccount: {}
              f:serviceAccountName: {}
              f:terminationGracePeriodSeconds: {}
              f:volumes:
                .: {}
                k:{"name":"cgroup"}:
                  .: {}
                  f:hostPath:
                    .: {}
                    f:path: {}
                    f:type: {}
                  f:name: {}
                k:{"name":"config"}:
                  .: {}
                  f:configMap:
                    .: {}
                    f:defaultMode: {}
                    f:name: {}
                  f:name: {}
                k:{"name":"data"}:
                  .: {}
                  f:hostPath:
                    .: {}
                    f:path: {}
                    f:type: {}
                  f:name: {}
                k:{"name":"proc"}:
                  .: {}
                  f:hostPath:
                    .: {}
                    f:path: {}
                    f:type: {}
                  f:name: {}
          f:updateStrategy:
            f:rollingUpdate:
              .: {}
              f:maxSurge: {}
              f:maxUnavailable: {}
            f:type: {}
    - manager: kube-controller-manager
      operation: Update
      apiVersion: apps/v1
      time: '2022-05-02T17:51:09Z'
      fieldsType: FieldsV1
      fieldsV1:
        f:status:
          f:currentNumberScheduled: {}
          f:desiredNumberScheduled: {}
          f:numberAvailable: {}
          f:numberMisscheduled: {}
          f:numberReady: {}
          f:observedGeneration: {}
          f:updatedNumberScheduled: {}
      subresource: status
spec:
  selector:
    matchLabels:
      k8s-app: metricbeat
  template:
    metadata:
      creationTimestamp: null
      labels:
        k8s-app: metricbeat
    spec:
      volumes:
        - name: proc
          hostPath:
            path: /proc
            type: ''
        - name: cgroup
          hostPath:
            path: /sys/fs/cgroup
            type: ''
        - name: config
          configMap:
            name: metricbeat-config
            defaultMode: 384
        - name: data
          hostPath:
            path: /var/lib/metricbeat-data
            type: DirectoryOrCreate
      containers:
        - name: metricbeat
          image: harbor.esckin/devops/metricbeat:7.17.1
          args:
            - '-c'
            - /etc/metricbeat.yml
            - '-e'
          env:
            - name: ELASTICSEARCH_HOST
              value: elasticsearch.default.svc.cluster.local
            - name: ELASTICSEARCH_PORT
              value: '9300'
            - name: ELASTICSEARCH_USERNAME
              value: admin
            - name: ELASTICSEARCH_PASSWORD
              value: admin
          resources:
            limits:
              memory: 200Mi
            requests:
              cpu: 100m
              memory: 100Mi
          volumeMounts:
            - name: config
              readOnly: true
              mountPath: /etc/metricbeat.yml
              subPath: metricbeat.yml
            - name: proc
              readOnly: true
              mountPath: /hostfs/proc
            - name: cgroup
              readOnly: true
              mountPath: /hostfs/sys/fs/cgroup
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          imagePullPolicy: IfNotPresent
          securityContext:
            runAsUser: 0
      restartPolicy: Always
      terminationGracePeriodSeconds: 30
      dnsPolicy: ClusterFirstWithHostNet
      serviceAccountName: metricbeat
      serviceAccount: metricbeat
      hostNetwork: true
      securityContext: {}
      schedulerName: default-scheduler
  updateStrategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 1
      maxSurge: 0
  revisionHistoryLimit: 10
status:
  currentNumberScheduled: 5
  numberMisscheduled: 0
  desiredNumberScheduled: 5
  numberReady: 5
  observedGeneration: 2
  updatedNumberScheduled: 5
  numberAvailable: 5
```



















```
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: metricbeat-setup-config
  namespace: kube-system
  labels:
    k8s-app: metricbeat
    kubernetes.io/cluster-service: 'true'
  managedFields:
    - manager: dashboard
      operation: Update
      apiVersion: v1
      time: '2022-04-27T22:45:28Z'
      fieldsType: FieldsV1
      fieldsV1:
        f:data:
          .: {}
          f:metricbeat.yml: {}
        f:metadata:
          f:labels:
            .: {}
            f:k8s-app: {}
            f:kubernetes.io/cluster-service: {}
data:
  metricbeat.yml: |-
    metricbeat.config.modules:
      path: ${path.config}/modules.d/*.yml
      reload.enabled: false
    setup.template.settings:
      index.number_of_shards: 8
      index.codec: best_compression
    output.elasticsearch:
      hosts: ['192.168.83.44:9200','192.168.182.37:9200']
    output.elasticsearch.index: "kubernetes-%{+yyyy.MM.dd}"
    setup.template.name: "kubernetes"
    setup.template.pattern: "kubernetes-*"
    setup.ilm.enabled: false
```



```
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: metricbeat-deployment-config
  namespace: kube-system
  labels:
    k8s-app: metricbeat
    kubernetes.io/cluster-service: 'true'
data:
  metricbeat.yml: |-
    metricbeat.modules:
      # This module requires `kube-state-metrics` up and running under `kube-system` namespace
      - module: kubernetes
        metricsets:
          - state_node
          - state_deployment
          - state_replicaset
          - state_pod
          - state_container
          - state_daemonset
          - state_statefulset
          - state_job
          - state_cronjob
          - state_resourcequota
          - state_service
          - state_persistentvolume
          - state_persistentvolumeclaim
          - state_storageclass
          - event
        period: 10s
        hosts: ["kube-state-metrics:8080"]
        add_metadata: true

    output.elasticsearch:
      hosts: ['192.168.83.44:9200','192.168.182.37:9200']
    output.elasticsearch.index: "kubernetes-%{+yyyy.MM.dd}"
    setup.template.name: "kubernetes"
    setup.template.pattern: "kubernetes-*"
    setup.ilm.enabled: false
```


```
kind: Deployment
apiVersion: apps/v1
metadata:
  name: metricbeat
  namespace: kube-system
  labels:
    k8s-app: metricbeat
    kubernetes.io/cluster-service: 'true'
  annotations:
    deployment.kubernetes.io/revision: '2'
  managedFields:
    - manager: dashboard
      operation: Update
      apiVersion: apps/v1
      time: '2022-03-12T15:06:20Z'
      fieldsType: FieldsV1
      fieldsV1:
        f:metadata:
          f:labels:
            .: {}
            f:k8s-app: {}
            f:kubernetes.io/cluster-service: {}
        f:spec:
          f:progressDeadlineSeconds: {}
          f:revisionHistoryLimit: {}
          f:selector: {}
          f:strategy:
            f:rollingUpdate:
              .: {}
              f:maxSurge: {}
              f:maxUnavailable: {}
            f:type: {}
          f:template:
            f:metadata:
              f:labels:
                .: {}
                f:k8s-app: {}
                f:kubernetes.io/cluster-service: {}
            f:spec:
              f:containers:
                k:{"name":"metricbeat"}:
                  .: {}
                  f:args: {}
                  f:env:
                    .: {}
                    k:{"name":"ELASTICSEARCH_HOST"}:
                      .: {}
                      f:name: {}
                      f:value: {}
                    k:{"name":"ELASTICSEARCH_PASSWORD"}:
                      .: {}
                      f:name: {}
                      f:value: {}
                    k:{"name":"ELASTICSEARCH_PORT"}:
                      .: {}
                      f:name: {}
                      f:value: {}
                    k:{"name":"ELASTICSEARCH_USERNAME"}:
                      .: {}
                      f:name: {}
                      f:value: {}
                  f:image: {}
                  f:imagePullPolicy: {}
                  f:name: {}
                  f:resources:
                    .: {}
                    f:limits:
                      .: {}
                      f:memory: {}
                    f:requests:
                      .: {}
                      f:cpu: {}
                      f:memory: {}
                  f:securityContext:
                    .: {}
                    f:runAsUser: {}
                  f:terminationMessagePath: {}
                  f:terminationMessagePolicy: {}
                  f:volumeMounts:
                    .: {}
                    k:{"mountPath":"/etc/metricbeat.yml"}:
                      .: {}
                      f:mountPath: {}
                      f:name: {}
                      f:readOnly: {}
                      f:subPath: {}
              f:dnsPolicy: {}
              f:restartPolicy: {}
              f:schedulerName: {}
              f:securityContext: {}
              f:serviceAccount: {}
              f:serviceAccountName: {}
              f:terminationGracePeriodSeconds: {}
              f:volumes:
                .: {}
                k:{"name":"config"}:
                  .: {}
                  f:configMap:
                    .: {}
                    f:defaultMode: {}
                    f:name: {}
                  f:name: {}
    - manager: kube-controller-manager
      operation: Update
      apiVersion: apps/v1
      time: '2022-05-02T15:30:39Z'
      fieldsType: FieldsV1
      fieldsV1:
        f:metadata:
          f:annotations:
            .: {}
            f:deployment.kubernetes.io/revision: {}
        f:status:
          f:availableReplicas: {}
          f:conditions:
            .: {}
            k:{"type":"Available"}:
              .: {}
              f:lastTransitionTime: {}
              f:lastUpdateTime: {}
              f:message: {}
              f:reason: {}
              f:status: {}
              f:type: {}
            k:{"type":"Progressing"}:
              .: {}
              f:lastTransitionTime: {}
              f:lastUpdateTime: {}
              f:message: {}
              f:reason: {}
              f:status: {}
              f:type: {}
          f:observedGeneration: {}
          f:readyReplicas: {}
          f:replicas: {}
          f:updatedReplicas: {}
      subresource: status
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: metricbeat
  template:
    metadata:
      creationTimestamp: null
      labels:
        k8s-app: metricbeat
        kubernetes.io/cluster-service: 'true'
    spec:
      volumes:
        - name: config
          configMap:
            name: metricbeat-deployment-config
            defaultMode: 384
      containers:
        - name: metricbeat
          image: harbor.esckin/devops/metricbeat:7.17.1
          args:
            - '-c'
            - /etc/metricbeat.yml
            - '-e'
          env:
            - name: ELASTICSEARCH_HOST
              value: elasticsearch.default.svc.cluster.local
            - name: ELASTICSEARCH_PORT
              value: '9300'
            - name: ELASTICSEARCH_USERNAME
              value: logstash
            - name: ELASTICSEARCH_PASSWORD
              value: logstash
          resources:
            limits:
              memory: 200Mi
            requests:
              cpu: 100m
              memory: 100Mi
          volumeMounts:
            - name: config
              readOnly: true
              mountPath: /etc/metricbeat.yml
              subPath: metricbeat.yml
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          imagePullPolicy: IfNotPresent
          securityContext:
            runAsUser: 0
      restartPolicy: Always
      terminationGracePeriodSeconds: 30
      dnsPolicy: ClusterFirst
      serviceAccountName: metricbeat
      serviceAccount: metricbeat
      securityContext: {}
      schedulerName: default-scheduler
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 25%
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600
status:
  observedGeneration: 103
  replicas: 1
  updatedReplicas: 1
  readyReplicas: 1
  availableReplicas: 1
```

```
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: kube-state-metrics
  namespace: kube-system
  labels:
    app.kubernetes.io/component: exporter
    app.kubernetes.io/name: kube-state-metrics
    app.kubernetes.io/version: 2.4.2
  annotations:
    deployment.kubernetes.io/revision: '1'
  managedFields:
    - manager: dashboard
      operation: Update
      apiVersion: apps/v1
      time: '2022-04-14T13:31:49Z'
      fieldsType: FieldsV1
      fieldsV1:
        f:metadata:
          f:labels:
            .: {}
            f:app.kubernetes.io/component: {}
            f:app.kubernetes.io/name: {}
            f:app.kubernetes.io/version: {}
        f:spec:
          f:progressDeadlineSeconds: {}
          f:revisionHistoryLimit: {}
          f:selector: {}
          f:strategy:
            f:rollingUpdate:
              .: {}
              f:maxSurge: {}
              f:maxUnavailable: {}
            f:type: {}
          f:template:
            f:metadata:
              f:labels:
                .: {}
                f:app.kubernetes.io/component: {}
                f:app.kubernetes.io/name: {}
                f:app.kubernetes.io/version: {}
            f:spec:
              f:automountServiceAccountToken: {}
              f:containers:
                k:{"name":"kube-state-metrics"}:
                  .: {}
                  f:image: {}
                  f:imagePullPolicy: {}
                  f:livenessProbe:
                    .: {}
                    f:failureThreshold: {}
                    f:httpGet:
                      .: {}
                      f:path: {}
                      f:port: {}
                      f:scheme: {}
                    f:initialDelaySeconds: {}
                    f:periodSeconds: {}
                    f:successThreshold: {}
                    f:timeoutSeconds: {}
                  f:name: {}
                  f:ports:
                    .: {}
                    k:{"containerPort":8080,"protocol":"TCP"}:
                      .: {}
                      f:containerPort: {}
                      f:name: {}
                      f:protocol: {}
                    k:{"containerPort":8081,"protocol":"TCP"}:
                      .: {}
                      f:containerPort: {}
                      f:name: {}
                      f:protocol: {}
                  f:readinessProbe:
                    .: {}
                    f:failureThreshold: {}
                    f:httpGet:
                      .: {}
                      f:path: {}
                      f:port: {}
                      f:scheme: {}
                    f:initialDelaySeconds: {}
                    f:periodSeconds: {}
                    f:successThreshold: {}
                    f:timeoutSeconds: {}
                  f:resources: {}
                  f:securityContext:
                    .: {}
                    f:allowPrivilegeEscalation: {}
                    f:capabilities:
                      .: {}
                      f:drop: {}
                    f:readOnlyRootFilesystem: {}
                    f:runAsUser: {}
                  f:terminationMessagePath: {}
                  f:terminationMessagePolicy: {}
              f:dnsPolicy: {}
              f:nodeSelector: {}
              f:restartPolicy: {}
              f:schedulerName: {}
              f:securityContext: {}
              f:serviceAccount: {}
              f:serviceAccountName: {}
              f:terminationGracePeriodSeconds: {}
    - manager: kube-controller-manager
      operation: Update
      apiVersion: apps/v1
      time: '2022-04-28T01:44:03Z'
      fieldsType: FieldsV1
      fieldsV1:
        f:metadata:
          f:annotations:
            .: {}
            f:deployment.kubernetes.io/revision: {}
        f:status:
          f:availableReplicas: {}
          f:conditions:
            .: {}
            k:{"type":"Available"}:
              .: {}
              f:lastTransitionTime: {}
              f:lastUpdateTime: {}
              f:message: {}
              f:reason: {}
              f:status: {}
              f:type: {}
            k:{"type":"Progressing"}:
              .: {}
              f:lastTransitionTime: {}
              f:lastUpdateTime: {}
              f:message: {}
              f:reason: {}
              f:status: {}
              f:type: {}
          f:observedGeneration: {}
          f:readyReplicas: {}
          f:replicas: {}
          f:updatedReplicas: {}
      subresource: status
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: kube-state-metrics
  template:
    metadata:
      creationTimestamp: null
      labels:
        app.kubernetes.io/component: exporter
        app.kubernetes.io/name: kube-state-metrics
        app.kubernetes.io/version: 2.4.2
    spec:
      containers:
        - name: kube-state-metrics
          image: harbor.esckin/devops/kube-state-metrics:v2.4.2
          ports:
            - name: http-metrics
              containerPort: 8080
              protocol: TCP
            - name: telemetry
              containerPort: 8081
              protocol: TCP
          resources: {}
          livenessProbe:
            httpGet:
              path: /healthz
              port: 8080
              scheme: HTTP
            initialDelaySeconds: 5
            timeoutSeconds: 5
            periodSeconds: 10
            successThreshold: 1
            failureThreshold: 3
          readinessProbe:
            httpGet:
              path: /
              port: 8081
              scheme: HTTP
            initialDelaySeconds: 5
            timeoutSeconds: 5
            periodSeconds: 10
            successThreshold: 1
            failureThreshold: 3
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          imagePullPolicy: IfNotPresent
          securityContext:
            capabilities:
              drop:
                - ALL
            runAsUser: 65534
            readOnlyRootFilesystem: true
            allowPrivilegeEscalation: false
      restartPolicy: Always
      terminationGracePeriodSeconds: 30
      dnsPolicy: ClusterFirst
      nodeSelector:
        kubernetes.io/os: linux
      serviceAccountName: kube-state-metrics
      serviceAccount: kube-state-metrics
      automountServiceAccountToken: true
      securityContext: {}
      schedulerName: default-scheduler
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 25%
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600
status:
  observedGeneration: 116
  replicas: 1
  updatedReplicas: 1
  readyReplicas: 1
  availableReplicas: 1

```


