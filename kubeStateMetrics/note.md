
```
PUT /_template/index_defaults 
{
  "template": "*", 
  "settings": {
    "number_of_shards": 4
  }
}
```

vi metricbeat.reference.yml

```
setup.template.settings:
  index.number_of_shards: 4
  index.codec: best_compression
```

---


Stack Management -> Saved Objects -> Export -> Include related objects

..go to other kibana

Import -> upload exports.ndjson -> Done
