The below powershell can be used to trigger a webhook URL for slack. Inside the powershell you can dynamically get variables from powershell using the json notation that is used.


```
'''
$json = @"
{
    "text": "I am inside $($Env:ComputerName)"
}
"@


if (-not((Get-Service -Name "Appinfo").Status -eq "Running") -or -not((Get-Service -Name "Dhcp").Status -eq "Running")) 
{ 
curl -X POST -H 'Content-type: application/json' --data $json https://hooks.slack.com/services/XXXX/XXXX/XXXX 
}
'''
```

