```
trigger:
  branches:
    include:
      - main

pr:
  branches:
    include:
      - '*'

pool:
  vmImage: 'ubuntu-latest'

steps:
- task: KubernetesManifest@0
  inputs:
    action: 'deploy'
    kubernetesServiceConnection: '<kubernetes-service-connection-name>'
    manifests: '**/*.yaml'
    containers: 'all'
    outputVariable: 'kubectlOutput'
  displayName: 'Deploy to Kubernetes'

- script: |
    echo "$(kubectlOutput)" | while IFS= read -r line; do
      if [[ $line == *"deployment.apps"* ]]; then
        deployment=$(echo "$line" | awk '{print $1}')
        echo "Restarting deployment: $deployment"
        kubectl rollout restart deployment "$deployment"
      fi
    done
  displayName: 'Restart Kubernetes Pods'
```


```
Yukarıdaki pipeline, aşağıdaki adımları içerir:

trigger: Bu bölüm, pipeline'ın hangi tetiklemelerle çalışacağını belirler. Örnekte, "main" branch ve tüm pull request'ler için tetikleme yapılmıştır.
pool: Bu bölüm, pipeline'ın hangi sanal makine görüntüsünde (VM Image) çalışacağını belirler. Örnekte, "ubuntu-latest" VM Image'ı kullanılmıştır.
steps: Bu bölüm, pipeline'ın adımlarını içerir.
İlk adımda, KubernetesManifest@0 görevi kullanılarak Kubernetes'e dağıtım yapılır. Bu adımda, <kubernetes-service-connection-name> ile belirtilen Kubernetes hizmet bağlantısı kullanılır. manifests parametresi, Kubernetes YAML dosyalarının yer aldığı dizini belirtir. containers parametresi, podları yeniden başlatmak için tüm konteynerleri seçer. outputVariable parametresi ise kubectlOutput adında bir değişken oluşturur ve kubectl çıktısını bu değişkene aktarır.
İkinci adımda, bir bash betiği kullanılarak kubectlOutput değişkeni okunur ve her satır için yeniden başlatılması gereken podlar tespit edilir. Bu adımda, sadece "deployment.apps" türündeki kaynaklar yeniden başlatılır. Eğer başka kaynakları da yeniden başlatmak isterseniz, betiği buna göre düzenlemeniz gerekebilir.
Pipeline'ın çalışması için, <kubernetes-service-connection-name> yerine Kubernetes hizmet bağlantısı adını belirtmelisiniz. Ayrıca, YAML dosyasını kaydettikten sonra pipeline'ı başlatmak için "Save and Run" düğmesine tıklayabilirsiniz.

Bu pipeline, belirli bir Kubernetes hizmet bağlantısı kullanarak Kubernetes kümelenizdeki podları yeniden başlatır. Diğer ayarları ihtiyaçlarınıza göre değiştirebilir veya ölçeklendirilebilir
```


