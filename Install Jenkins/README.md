Resource;

[offline install](https://www.jenkins.io/doc/book/installing/offline/)


[install_jenkins](https://www.jenkins.io/doc/book/installing/kubernetes/)


[digitalOc](https://www.digitalocean.com/community/tutorials/how-to-install-jenkins-on-kubernetes)

[helm deploy](https://faun.pub/deploying-and-scaling-jenkins-on-kubernetes-2cd4164720bd)


[openshift jenkins](https://computingforgeeks.com/how-to-install-jenkins-server-on-kubernetes-openshift/)


```
$ alias k=kubectl

$ k create namespace jenkins

$ k get namespaces

```


--Deployment

```
kind: Deployment
apiVersion: apps/v1
metadata:
  name: jenkins
  namespace: jenkins
spec:
  replicas: 1
  selector:
    matchLabels:
      app: jenkins
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: jenkins
    spec:
      volumes:
        - name: jenkins-vol
          emptyDir: {}
      containers:
        - name: jenkins
          image: *repo*/uty/jenkinsimage:1.0
          ports:
            - name: http-port
              containerPort: 8080
              protocol: TCP
            - name: jnlp-port
              containerPort: 50000
              protocol: TCP
          resources: {}
          volumeMounts:
            - name: jenkins-vol
              mountPath: /var/jenkins_vol
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          imagePullPolicy: IfNotPresent
      restartPolicy: Always
      terminationGracePeriodSeconds: 30
      dnsPolicy: ClusterFirst
      securityContext: {}
      schedulerName: default-scheduler
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 25%
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600

```

```
$ kubectl create -f jenkins-deployment.yaml -n jenkins
$ kubectl get deployments -n jenkins


--Servicves

```
kind: Service
apiVersion: v1
metadata:
  name: jenkins
  namespace: jenkins
spec:
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 8080
      nodePort: 30000
  selector:
    app: jenkins
  clusterIP: 10.30.30.10
  clusterIPs:
    - 10.30.30.10
  type: NodePort
  sessionAffinity: None
  externalTrafficPolicy: Cluster
  ipFamilies:
    - IPv4
  ipFamilyPolicy: SingleStack
  internalTrafficPolicy: Cluster
status:
  loadBalancer: {}

```

$ kubectl -n jenkins port-forward <pod_name> 8080:8080
Forwarding from 127.0.0.1:8080 -> 8080
Forwarding from [::1]:8080 -> 8080

$ kubectl get pods -n jenkins
$ kubectl get services -n jenkins


$ kubectl logs <pod_name> -n jenkins

$ jsonpath="{.data.jenkins-admin-password}"
$ kubectl get secret -n jenkins jenkins -o jsonpath=$jsonpath


This may also be found at:
cat /var/jenkins_home/secrets/initialAdminPassword
```




--Unlock jenkins

```
Go to the pod and cd /var/jenkins_home/secrets/initialAdminPassword

[offline unlock](https://www.jenkins.io/doc/book/installing/offline/)

change url and Jenkins is ready!
```










