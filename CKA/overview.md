https://killer.sh/cka practice mock

https://www.cncf.io/training/certification/cka/
https://kubernetes.io/docs/home/

---

# Create a new Namespace
kubectl create namespace my-namespace

# Deploy a new Pod
kubectl run my-pod --image=nginx --namespace=my-namespace
# Expose the Pod as a Service
kubectl expose pod my-pod --port=80 --namespace=my-namespace
# Scale the Deployment
kubectl scale deployment my-deployment --replicas=3 --namespace=my-namespace
# Check Pod logs
kubectl logs my-pod --namespace=my-namespace
# Describe a resource
kubectl describe pod my-pod --namespace=my-namespace

https://kubernetes.io/docs/concepts/services-networking/

https://kubernetes.io/docs/tasks/debug/
