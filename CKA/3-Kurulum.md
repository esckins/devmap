- Minikube

```
$ curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-darwin-amd64

$sudo rpm -Uvh minikube-latest.x86_64.rpm

$ chmod +x minikube

$ sudo mv minikube /usr/local/bin

$ minikube start

$ kubectl get nodes

$ minikube kubectl -- get po -A

$ alias kubectl="minikube kubectl --"

$ minikube dashboard
```


[minikube](https://minikube.sigs.k8s.io/docs/start/)



---

- kubeadm

https://www.umitdemirtas.com/kubernetes-lab01-4-20/

https://devopscube.com/setup-kubernetes-cluster-kubeadm/

https://www.digitalocean.com/community/tutorials/how-to-create-a-kubernetes-cluster-using-kubeadm-on-ubuntu-20-04
