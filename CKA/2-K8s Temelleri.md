Başlıca Kubernetes; container orkestrasyonu sağlar. Container dagıtımı, scale işlemleri ile ha çözümleri sağlayan api destegi,job,cronjob, database, loadbalance yönetimini tek elden yapılabilecek opensource bir platform.

Yunancada geminin kaptanı anlamına gelir, kısaca k8s olarak nitelendirilebilir.

Mikroservis mimarı çagında dağıtım yönetimi konusunda eşsizdir. yapılandırma bilgileri jsonda tutular, config dosyaları genelde yml formatta yazılır. K8s sistem programlamada güçlü olan go diliyle yazılmıştır.


![KubeFlow](Asset/kubflow.png)
