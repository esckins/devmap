https://killercoda.com/ 

**# Devopsmeta**
bcc vs cc
[CNCF Cloud Native](https://landscape.cncf.io/)

https://devopscube.com/setup-kubernetes-cluster-kubeadm/


[kube-monitoring vs](https://github.com/bibinwilson/kubernetes-prometheus)

[kube-monitoring-docs](https://devopscube.com/setup-prometheus-monitoring-on-kubernetes/)

[CKA](https://fatihboy.com/ckad-sinav-tavsiyeleri-ve-ipuclari/)

# İçindekiler
- [ ] ++
- [Giriş](#giri%c5%9f)
- [Mikroservis Nedir?](#mikroservis-nedir)
- [Kubernetes Nedir?](#kubernetes-nedir)
  - [Temel Kubernetes Bileşenleri](#temel-kubernetes-bilesenleri)
- [ne Nedir?](#ne-nedir)

:information_source: &nbsp; :bar_chart: &nbsp; :books: &nbsp; :warning: &nbsp; :busts_in_silhouette: &nbsp; :pencil: &nbsp;


# Giriş
  Bu yazının amacı; kubernetes ve mikroservis mimarisini anlamak. Her şey yolunda giderse alternatif çözüm ve kolaylıklar oluşturmak.

---
: when 1 = 1 #:)
---
Mikroservis mimarisine geçmeden önce bu aşamaya nereden geldik sokak agzıyla bahsetmek gerekir.
21.yy başlarında fiziksel sunucuların hayatımıza girmesiyle birlikte geliştirilen kod parçacıkları ürün haline dönüşüp halka arz ediliyordu.
Burada bahsedilen ürün; sosyal medya, e-ticaret, mobil, websitesi, retail kanallar, local uygulamalar vs vs ..

_Örnegin Bu siteye nasıl bağlandınız ?_  [Server - Client ](https://madooei.github.io/cs421_sp20_homepage/client-server-app/)


En başta 1 sunucu - 1 işletim sistemi (os) -1 uygulama (app) çalışıyordu. İhtiyaç dogrultusunda yeni bir uygulama istenirse extra bir fiziksel sunucuya ihtiyaç duyuluyordu. Siparis süresi, maaliyet, bakım süreçleri,paylaşım, esneklik gibi dezavantajları neticesinde hypervisor katmanı çözüm olarak sunuldu. Böylece bir fiziksel sunucu üzerinde birden çok sanal sunucu(vm) oluşturulabildi.

![sdas](Asset/vm-phy.PNG)

Görüldügü üzere izolasyon saglanmış gibi duruyor. Linux, windows, macos gibi sistemler kurulabilse bile bu sistemler extra yük (disk ,runtime, cpu) oluşturuyor.

Kapsamlı anlatım için  [Hyper-V arch](https://serhatakinci.com/hyper-v-mimarisi-architecture.htm)

Peki uygulama birçok organdan oluşuyor ve kullanıcı sayısı fazlaysa ? Bu sorunlara çözüm olarak serivs yönetimi monolitic,service oriented arch. gibi mimariler [SOA prensibi](http://devnot.com/2015/soa-service-oriented-architecture-nedir/) , trafik yönetimi için load balancer gibi [LB yapısı](https://www.vargonen.com/blog/load-balancing-nedir-load-balancer-nasil-hangi-durumlarda-kullanilir/) çözümler üretildi.

Günümüzde datacenter'ı onprime'da olan birçok firma farklı uygulamalar geliştirse de  java -> Linux weblogic-tomcat, oraclesql , C# -> Windows iis, sql-server yapısında karmaşık sistemleri yönetiyorlar. Gömülü sistem, savunma sanayi tarafında ise C ağırlıklı ürünler geliştiriliyor.

IT sektöründe dataların güvenliği, deployment sayıların azlıgı, işlem yükünün handle edilebilmesi, sorunların bilinmesi gibi alışkanlıklar, BDDK, BTK gibi denetci kurumların kuralları cloud provider'a soru işareti gözüyle bakılmaktadır.

Kullanıcı sayısının, datanın ve iş yükünün artması sonucunda mikroservis alt yapısı oluşması ve orkestrasyon /infrastructure as a code kullanıyor ya da kullanacaktır. Artan iş yükü, karmaşıklık, yönetim ve scale işlemleri monolitic yapının tarihe karışacağını gösteriyor. Elbette Microservis tüm problemleri çözmeyecektir. Hatta bazı durumlarda monolitic uygulamalar çok daha avantajlıdır.

# mikroservis-nedir
- **Micro - servis**
  - Temel anlamda **Sadece tek bir amaca hizmet eden birbirlerinden bağımsız servislerdir**. Soa mimarisi üzerine inşa edilmiştir. Inbound ve outbound kanallardan oluşan bir hizmet sağladığınızı düşünelim. Müşteriler ile connection call center, telesales, mobil application, web site, sms, push notification,retail gibi kanallardan kurulduktan sonra ürünü satın almak istedigi durumu varsayalım. Monolitik bir uygulamaya sahipseniz; ödeme sistemleri servislerinde bir problem olması durumunda tüm akış hataya ugrayacak ve sadece tarifelere bakmak isteyen yeni gelen müşterilerde bu durumdan etkilenecekti. Soa yapısıyla bu problem nispeten çözüldü. Applicationda ürünler sayfası açılmasa bile satın alma işlemi gerçekleştiren müşteriler bu durumdan etkilenmeyecektir.
  Uber'ın SOA yolculugunu anlatan yazısı : [uber](https://eng.uber.com/service-oriented-architecture/)


- Peki soa dururken neden mikroservice kullanayım ? 

Microservis mimarisi birbirinden bağımsız olarak geliştirilen, kücük, tek bir uygulama geliştirmektedir. SOA ise servisler arası ortak bir iletişim mekanizması kuran bağımlı uygulama hizmetlerinden oluşur. 5 servis güçlü 100 vm ile müşterilerin teklif görüntülemesi ve outputları ilgili katmanlara aktaran bir yapıda oldugunuzu varsayalım. Bu servisler ise 100 vm'i işlevlerine göre ayırmış ve sadece orada çalıştıgını varsayalım. Peki gece sms atmayan process'in çalıştıgını sunucular boşta yatıyor ve sadece gün içinde çalışıyor. Dolayısıyla bu sunucuları ben efektif bir şekilde kullanamıyorum. Bu sunucular gece eventlerinde mobil app tarafında geliştirilen uygulamalara destek olabilseydi, artan yükü kolayca handle edebilirdim. Cluster'a extra sunucu eklememe gerek kalmazdı. 

`Ufak bir not: Özellikle yazılım geliştiriciler için garbage collector kavramı ve sunucu loglarında görülen .gc uzantılı çöp toplayıcısı: GC sizin için memory allocation ve deallocation işlemlerini yapar. Gereksiz nesneler, degişkenler gereksiz yer kaplayabilir ve memory leak'e neden olabilir. Yapısı; heap belleğe bakıp, kullanılan objelerin tespit edilmesi ve referans edilmeyenlerin silinmesi üzerine kuruludur. Böylece bellekte boş yer açılmış olur.`


Detaylı link;


[Mikroservice + -](https://gokhana.medium.com/microservice-mimarisi-nedir-microservice-mimarisine-giri%C5%9F-948e30cf65b1)

[microservice vs soa](https://tr.sawakinome.com/articles/software/difference-between-microservices-and-soa-3.html)

[GC](https://tugrulbayrak.medium.com/jvm-garbage-collector-nedir-96e76b6f6239)

# kubernetes-nedir

Google tarafından geliştirilen ardından open source'a açılan container(pod) uygulamaların yönetimi ve orkestrasyonu için standart. 21.yy  ilk çeyreginde container teknolojisiyle birlikte linux çekirdeginde namespaces, control groups özelligi eklendi ve linux container ,image kavramları hayatımıza entegre oldu. Docker {Built, ship, run everywhere} ile image kavramıyla tanıştık. İsminden anlaşılabileceği gibi resimler geçmiş anıları bize yansıtır ve değişmez. Bir development life cycle'ında geliştirilmesi istenilen kod'un business isteklerine göre sprinte alınmasıyla başlar. Bu süreçte developer hangi servislerde çalıcagından, sunucu load miktarından, prod'da defect / fraud olmamasından tutunda Dev/Preprod/Prod ortamlarında aktif olarak operasyon, product owner, test hatta quality eng. ile birlikte çalışır. Bu süreçleri kayıt altına almak içinde change management onaylarının dahil oldugu bir süreç düşünelim. Hal böyle olunca çok fazla zaman kaybı ve karmaşıklık oluşuyor. Bu kadar işlemden geçmesine ragmen prod deployment sonrası patladıgı caseler görülüyor ve def, incident, etkilenen kitle müdahalesi, gelir kaybı, rollback süreci gibi disaster durumlar meydana geliyor. Cevap ise alt ortamlarda çalışıyor fakat prod'da patladı. İşte Docker dev tarafı için enviroment bagımsız bir platform , ops için ise high available, version upgrade ve rollback kolaylıgı saglıyor. Containerların yönetimi konusunda zayıf kalınca docker swarm, kubernetes, rancher, openshift gibi farklı şirketlerin pazara yayıldıgını gözlemledik. 

Yazıya devam etmeden önce aşağıdaki linkleri okumanızı tavsiye ederim.

[Docker ve temelleri](https://medium.com/batech/docker-nedir-docker-kavramlar%C4%B1-avantajlar%C4%B1-901b37742ee0)

[Docker vs Kubernetes](https://azure.microsoft.com/en-us/topic/kubernetes-vs-docker/)

[Container vs vm](https://tr.linkedin.com/pulse/container-nedir-sanalla%C5%9Ft%C4%B1rma-ile-farklar%C4%B1-nelerdir-ahmet-ke%C3%A7eciler)

K__ubernete__s == k8s 8'in espirisi aradaki karakter uzunlugundan geliyor.

![KubeAdv](Asset/kubeadv.PNG)

![KubeAdv](Asset/kubeadv2.PNG)

![KubeUser](Asset/kubeusers.png)

![K8S Architecure](Asset/kubearch.png)


![K8S workflow](Asset/workflow.PNG)

# temel-kubernetes-bilesenleri

K8s'in en temel bileşeni(hücre) podlardır. Clusterda çalışan processlerdir.

Imagelar => Dağıtılmaya hazır source kod. Uygulamanın başka sunucularda rahatlıkla kullanılmayı saglar
Container => image'ların çalışmasıyla birbirinden izole çalışabilen yapıdır.

Linux namespace ve controller tarafından kolayca containerlar ayrılabilir. Örnegin bir namespace içinde izleme uygulamanız bulunurken digerinde java uygulaması çalışabilir. Bu uygulamalar birbirinden tamamen bagımsızdır.
Pod => Container isolasyonu saglar. K8s scale işlemi için podları baz alır.
Genelde bir pod içinde bir container çalışması önerilir ( Nedeni ise containerların aynı network portunu kullanmak istemesidir).Her podun kendine has ip'si vardır. Processlerin en önemli özelliği scale out edilebilmesi ve soyutlamadır.

Bir pod dogar, büyür ve ölür. Başlamaya çalışırken crash olabilir. Bu nedenle pod içinde data saklanmaması önerilir. Data saklamak için en ideal çözüm cloud storage alanlarıdır. Alternatif olarak ceph gibi depolama sistemleri de kullanılabilir.

Cluster => n tane sunucunun oluşturduğu küme.

K8s, master ve worker nodelardan oluşur.
Master Node => Üzerinde pod olmayan, workerları yöneten ve high available için kullanılır. İçinde Kube-api server çalışır. Rest komutlarıyla apiserver'a emirler verilir. etcd verilen komutları w/r görevi görür ( db gibi düşünülebilir ) Kontrolleri yöneten sisteme ise controller manager denir. Kube scheduler ise timer rolü görür.
Worker Node => Master'ın atadıgı görevi işletir. Node'lar içinde ise kubelet(agent) çalışır. Kube proxy ise network ile ilgili, diger nodelar arası haberleşmeyi saglar. Container runtime kubernetes için farklı türleri de mevcut olmasına karşın genelde docker'dır. 
api-server => Master node'un isteklerini yönlendirir.
etcd => Componentleri key-value biçiminde saklar, w/r db
Controller manager => Containerların statuslerini kontrol eder, oluşturur, başlatır.
kube proxy => Her pod'un kendine has ip'si vardır. servis altındaki podlara ip adresi saglar, lb için ortam hazırlar.
kube scheduler => Scheduler işlemlerini gerçekleştir.

[Kubernetes doc](https://kubernetes.io/docs/tutorials/kubernetes-basics/)


Localde bir kubernetes ortamı kurmak için bir virtual box üzerine minikube kurulabilir ya da kubernetes sitesinde komutları ve kavramlar için lab ortamında test edebilirsiniz. Minikube size master ve worker node'u saglar. Genelde test amaçlı geliştirmeler minikube üzerinde denenir. kurulum için yeterli altyapı ve driver varsa minikube start demek yeterli oluyor. Container runtime için docker kurulu olması gerekir. Alt yapınıza göre istenilen yapı farklı yöntemler ile install edilebilir.

practice makes perfect. https://lnkd.in/dNdPgmXy

[Docker install](https://docs.docker.com/engine/install/))

[Minikube start](https://minikube.sigs.k8s.io/docs/start/)

```
minikube start
minikube status
minikube dashboard

#kubectl; kubernetes için komut seti arabirimidir. kubectl [command] [TYPE] [NAME] [flags] docker komutunun karşılıgı gibi düşünülebilir. kubectl'de install edilmeli !
kubectl version

```
[Kubectl command cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)

> minikube delete


-- kubectl ile terminalde calıstırırsak hakkında bilgilere ulaşabiliriz.

![Kubectl commands](Asset/kubectlCommand.jpg)

> alias k=kubectl



> complete -F __start_kubectl k

```
kubectl version

# yaml => Deklerasyon dosyalarıdır, Bir yaml dosyasına istenilen özelllikler belirtilir ve kube api-server aracılıgıyla master node'lara iletilir. Master node ise worker node üzerinde bu kaynagı uygular.

# pod yaratma /imperative way: 
kubectl run pod1 - image=seckiinba/proje:1.0.0
kubectl port-forward pod1 8000:80
#localhost:8000 yazarak sonuca ulasılabilir.
kubectl delete pod pod1
#pod yaratma /declarative way:
#.yml dosyasıyla birlikte creat veya apply ile oluşturabiliriz.
kubectl apply -f pod1.yml 
kubectl port-forward pod1 8000:80
#bu yöntem sayesinde version upgrade gibi durumlar yönetilebilir.
#ilgili yml file da image:kısmında versiyon degiştirilip reply deyince upgrade edilebilir.
kubectl get pods -w
kubectl get podst #- watch ile çalışan podlar izlenebilir
kubectl logs pod1 #ile loglar görüntülenebilir
kubectl exec -i -t pod1 # bash çalıştırılabilir,exit ile çıkalabilir.
#declarative programing en önemli avantajı birden çok pod yönetilebilmesidir.
kubectl get pods - all -namespaces
kubectl get nodes


Kustomize, farklı amaçlar için kullanılacak template YAML dosyaları oluşturmamızı ve orijinal YAML dosyalarına hiç dokunmadan environment değişkenleri ve konfigürasyonları değiştirmemizi sağlar.

https://medium.com/trendyol-tech/kustomize%C4%B1n-g%C3%BCc%C3%BCnden-niye-faydalanmayal%C4%B1m-ki-d144a98b7e3d

kubectl create -k <kustomization_directory>/
kubectl create --kustomize <kustomization_directory>/
kubectl apply -k <kustomization_directory>/
kubectl apply --kustomize <kustomization_directory>/
kubectl get -k <kustomization_directory>/
kubectl get --kustomize <kustomization_directory>/
kubectl describe -k <kustomization_directory>/
kubectl describe --kustomize <kustomization_directory>/
kubectl delete -k <kustomization_directory>/
kubectl delete --kustomize <kustomization_directory>/
//kustomize build 
./kustomize build  <kustomization_directory>/

https://cigdemkadakoglu.medium.com/kustomize-kubernetes-43301a5c05ac#:~:text=Kustomize%20Nedir%3F,de%C4%9Fi%C5%9Fkenleri%20ve%20konfig%C3%BCrasyonlar%C4%B1%20de%C4%9Fi%C5%9Ftirmemizi%20sa%C4%9Flar.

[Docdebug-application-cluster](https://kubernetes.io/docs/tasks/debug-application-cluster/_print/)

***

kubectl create ns ns2
kubevtl deletenamespace ns2
kubectl config set-context minikube - namespace=ns2
kubectl config view

#replication controllerda 3 olarak belirlendigi için bir pod ölse ya da manuel silinse dahi farklı bir tane otomatik olarak oluşturulur.

kubectl get pods
kubectldelete pod rc2-sdasd
kubectl get pods
#rc komutlarını degiştirmek için kubectl delete rc rc2
kubectl get pods ile zamanla silindigini görebilirsini.
kubectl get pods - watch

***

kubectl scale rc rc2 - replicas=1
#2 tanesini otomatik olarak öldürüyor.
kubectl exec -i -t rc2-cbkrm bash # ile çalışan pod içine giriyoruz.
curl http://localhost
exit
kubectl get all
kubectl delete ns ns2



***
alias kcl=kubectl

$kcl expose deployment your-deployment --type=LoadBalancer --name=your-service

$kcl get services your-service

```


**K8s Dinamikleri**

> Namespace

aynı ortam içinde iki tane aynı isimde pod deployment vs calısma imkanı yoktur. 
testini yapılmasını istedigimiz uygulama farklı bir namespacede calısabilir. böylece birebir olabilir.

kubectl cluster-info dump

kubectl get namespaces

default -> varsayılan namespaces; aksi belirtilmedigi sürece hepsini burada olusturur.

kube-system -> cluster kaynaklarının olusturuldugu ns dir.

kube-node-lease -> her node'a ait lease(belirli hakların belirli bir süre verilmesidir). örn. dchp; ilgili ip havuzunu belirli bir süre kiralar.
örn restart aldıgında sunucu yeni ip alsın gibi.

kubelet'i (ajan; pod ve nodelar hkk. bilgileri control plane' gönderir) heartbeat sinyali gönderir.

kube-public -> belirli kaynakların tüm clusterda görünür ve okunur olması için kullanılır. cluster kullanımı için ayrılmıstır.


kubectl config view

kubernetes config get-contexts


> Service
 Podlar arası iletişim kurarken yardımcı olur ve aynı zamanda yazılan kodların k8s ile dışardan erişilmesi sağlayabilir.
Bakım ya da version upgrade zamanlarında podlar ortam bagımsız olması ve kolay yönetilebilmesi gerekiyor. K8sde dolayısıyla distribude olması gerekir.
Bir pod down oldugunda yeni bir ipde pod oluşturulur fakat bu müdahaleyi daha konforlu ve best practice yapıda gerçekleşmesi gerekir (zero downtime)
Service source gibi davranıp destinationlarla haberlesmesi saglanır ve podlar arasında köprü gibi davranır. Müdahale geregini ortadan kaldırır

> Label
Her bir nesnenin metada kısmında verilebilecek key value şeklinde tanımlama yapmayı sağlar.
Service ya da podlara etiket verilebilir. Olmasa da olur ama kritiktir. Aynı service baglı podlar için farklı türde bir pod ve version eklenirse hata sayınız da artacaktır.
Service'e verilen label sonucunda şart saglanınca eşleşme saglanır. Dolayısılya etiketlerin aynı olması gerekir yoksa eşleşme service-pod arası gerçekmez.

label; bu replicaset hangi podları yönetecek sorusunu cevaplar. hespi birbiriyle match etmelidir.

> selector

filtrelemedir. MatchLabel ise bu şarta uymalıdır. replicanın anahtarıdır.

> template

pod'un anahtarlardır. yml içinde labellar aynı olmalıdır. 


> Replication Controller
Podların scale edilebilmesinden sorumludur. Kaç kopya var ya da olmasını istiyorsun gibi soruların cevabıdır. 

> Kind
ReplicationController'dan oluşan işlemin türünü belirten birimdir.
_kubectl rc -o wide_


Kubernetes servis tipleri; nodeport, loadbalancer, clusterIP, Ingress ,externalname
[Service types](https://kubernetesturkey.com/k8s-servis-tipleri/)

_Clusterip varsayılan servis tipidir, Sadece cluster içinde erişilebilir_

porta gelen trafigi yönlendirmek için serviceler kullanılır. 

kubectl expose deployment hello-world-deploy --type NodePort --port 3000

kubectl get deployment,svc -o wide

Dış dünyadan buraya erişmek için; worker nodelardan birinin ipsini alıp port ile erişim yapılabilir.

bir diger türü ise kind: Service ile gerçekleştirmekir.



> ClusterIP_service.yaml

```
apiVersion: v1
kind: Service
metadata:  
  name: my-internal-service
spec:
  selector:    
    app: my-app
  type: ClusterIP
  ports:  
  - name: http
    port: 80
    targetPort: 80
    protocol: TCP

```
_nodeport tipi dış trafigi dogrudan içeri almanın ilkel hali._

> Nodeport_service.yaml

```
apiVersion: v1
kind: Service
metadata:  
  name: my-nodeport-service
spec:
  selector:    
    app: my-app
  type: NodePort
  ports:  
  - name: http
    port: 80
    targetPort: 80
    nodePort: 30036
    protocol: TCP
```

_LoadBalancer tipi servisi internete sunmanın standart yoludur. Bulut saglayıcılarda bu tip ayarlandıgında servis için bir lb saglar_

> lb_servis.yaml

```
apiVersion: v1
kind: Service
metadata:
  name: my-loadbalancer-service
spec:
  selector:
    app: my-app
  ports:
    - protocol: TCP
      port: 80
      targetPort: 7377
  clusterIP: 10.0.177.244
  type: LoadBalancer
status:
  loadBalancer:
    ingress:
    - ip: 192.0.3.77
```

_ExternalName DNS adı ile eşleşirler. Eşleşmeyi spec.externalName ile belirtilir_

> ExternalName_servis.yaml

```
apiVersion: v1
kind: Service
metadata:
  name: my-externalname-service

spec:
  type: ExternalName
  externalName: database.kubernetesturkey.com
```

_Ingress bir servis tipi degildir. Gelen isteklerin servislere ulaşmasını saglayan rule'lardır. Birçok ingress türü mevcuttur, Birden çok servisin önünde durur ve cluster'a akıllı bir yönlendirici (lb) görevi görür._

> L7HttpIngress.yaml

```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: my-ingress
spec:
  backend:
    serviceName: other
    servicePort: 8080
  rules:
  - host: foo.kubernetesturkey.com
    http:
      paths:
      - backend:
          serviceName: foo
          servicePort: 8080
  - host: kubernetesturkey.com
    http:
      paths:
      - path: /bar/*
        backend:
          serviceName: bar
          servicePort: 8080
```

**Kubernetes Dağıtımları**

Kubectl Api istegi yaparken bilgileri json'a döndürür. Gerekli alanları ve istenen durumları açıklayan nesneler belirtilmelidir.

> ReplicaSet

Pod sayısını belirtilen X degerinde sabitleyen birimdir. Pod sayısı azaldıgında auto sayıyı denkleştirir. Replicaset == Replica controller (pod management). Default oalrak rolling updates, rollback yapabilir.

> Deployment

Uygulama denilen yere karşılık gelir.

Blue green: örnegin 2 kopya version1 calısırken version upgrade için 2 kopya v2 konuluyor. İstenilen versionlar ayaga kalktıktan sonra V1 ler kill ediliyor ve trafik v2'ye yönlendiriliyor. Teoride kesinti vermemis sayılıyorsunuz. 4 kopya aynı anda calıstıgı case'de cpu memory sorunları olmasın diye rolling update (deployment stratejisi) geliştiriyor. Böylece 1 artır 1 azalt mantıgıyla deployment edilebiliyor. Dezavantajı ise aynı anda 2 versiyonu da uygulamanın desteklemesi gerekmesidir. Canary ise bekleyerek update gerçekleştiriyor. Yani önce belirli bir kısmı update ediyor ve monitoring sonrası hata yoksa t anından itibaren yeni versiyona geçilebilir gibi bir alt yapı saglıyor.

K8s artısı ise yogunlugun fazla oldugu ve deployment sayılarının arttıgı durumlarda automated scalling özelligi olması. Bu durumlarda örnegin ay başlarında uygulamanın kopya sayısının dinamik olarak ayarlanabilmesini saglıyor.


Deployment default olarak rollback ve rolling updates saglıyor. Aşağıdaki komut ile önceki versiona kolayca geçebilirsiniz.

`kubectl roolout undo deployment/my-deployment (- to-revision 5)`

> deployment.yaml

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-app-deployment
  labels:
    app: my-app
  annotations:
    kubernetes.io/change-cause: ${CHANGE_CAUSE}
spec:
  replicas: 3
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
      - name: api
        image: xxxxxxxxxxxx.dkr.ecr.us-west-2.amazonaws.com/unicorn/my-app:${IMAGE_TAG}
        ports:
        - containerPort: 80

```

_Nginx deployment_

```
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2 # tells deployment to run 2 pods matching the template
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```

> Statefulset

dB gibi state(stabil) tutmak istenilen uygulamalar için önerilir. Kubernetes PersistentVolume ile datayı kalıcı tutabiliyor. Persist edilen veri ile pod ayrı node'a schedule olursa veri pod tarafından erişilemez duruma gelir. Cluster olarak çalışan yapılarda bu cok olası bir durumdur.

Deployment aksine podlar random yaratılmaz. İdentityleri verilen isme göre xyz-1 xyz-2 xyz-3 şeklinde devam eder.Podlar ayaga kalkarken sıraya göre gerçekleşecegi bilinir. Silerken de tersten (3-2-1) başlar.
Örnegin jdbc driver ile databaselerden data cekmek vs işlemleri için statefull oluşturulması gerekir. Burada jdbc connection ve port bilgileri kontrol edilmeli. Sorguların trigger'a takılmaması için statefullsets tanımı yapılır.
Kafka topicleri için ise deploymentlar oluşturulabilir. 

Deployment her yeni pod ayağa kalktıgında random isimde pod namelerden oluşur fakat statefullset için sıralı şekilde devam eder. Replica sayısına göre 0-1-2.. -n tane podlar oluşur.

Kubernetesde config olustururken kind vb gibi bilgilerin oluşturulması gerekir. Boşluk, tab, \ karakterleri önemlidir.

> Volume

Container diskleri hayalidir. Pod herhangi bir şekilde ölebilir ve diskte bulunan her şey kaybolur. K8s volumeları pod tarafından erişilebilen ve yeniden başlatmalarda veri kaybetmeyen klasorlerdir. 
kabaca 5 gruba ayrılır.

- local türler
- Dosya paylaşım türleri (nfs)
- Dağıtık dosya sistemi türleri (Cephfs, glusterfs)
- Cloud provider (awsElasticBlockStore, AzureDisk, AzureFile ..)
- Amaca yönelik (configMap, secret, gitRepo)


> Persistent Volume

Containerlar üzerindeki dosyalar persist edilmedigi için container açılıp kapanması sonrasında data kaybolur. Bu nedenle container'a yazılan veri bir yerde depolanması gerekir. Pod crash olursa yeni bir pod kubelet tarafından tekrar ayaga kalkar ama container içerisinde veri kaybolur. PV nesnesi ise PV Claim (pvc) aracılıgı ile kullanılabilmektedir. 

Persistent Volume cluster içerisinde kurulan herhangi bir storage kullanacak şekilde yaratılabilir. Örnek NFS, iSCSI, host path..
Oluşturulan PersistenVolume ilgili Pod veya Deploymentlara Persistent volume Claim ile mount edilir.
mount => disk partisyonunun dosya sistemine eklenme işlemi

ex:

apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  annotations:
  name: jenkins-pvc
  namespace: jenkins-dev
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 80Gi
  storageClassName: pd-ssd
  volumeMode: Filesystem

[JenkinsinK8s](https://medium.com/@vishal.sharma./running-jenkins-in-kubernetes-cluster-with-persistent-volume-da6584edc126)

Volume aslında bir dosyadan olusur. Veriyi içinde nasıl barındırıldıgına göre volume türüne göre degişir.
En temel veri bilimi emptydir (boş dizin)dir. Bir pod boş bir dizin ile olusturuldugunda atandıgı node üzerinde bir dosya olusturup ilgili container'i mount eder. Varlıgı pod hayatta kalana kadar devam eder.

hostpath ise podun calıstıgı node üzerinde belirli bir dosyayı/klasoru volume olarak monte etmek için kullanılır. Dikkatli kullanılmalıdır.
güvenlik açıgı yaratabilir, dogrudan node'un dosya sistemine erişimi oldugu için genelde podun sadece okuma izni ile monte edilmesi önerilir.

Bir diger volume türü ise bulut saglayıcıların (aws elastic block store, googlecloud persistent disk, azuredisk) gibi veri depolama sistemi vardır.

Son olarak NFS (Network veri depolama) sistemleri kullanılabilir.

persistent volume kalıcı bir veri deposu olusturur, ihtiyac olan uygulamar için eklenebilir.


####################################################################################################################
#
#
#
####################################################################################################################


**Networking (türleri),Ingress**

Her pod her ip sınıfından en fazla bir tane olmak üzere kendi ip adresini alır. Olusturulan pod ile diger nodelardaki haberlesme otomatik oluyor. yani port erişimi vs gerek kalmıyor. Servis konusu ile farklıdır. Service tiplerinde pod içinde çalışan uygulamanın dış dünyaya açılabilmesini sağlamaktı. Dockerda durum ise belirli bir port üzerinden calısması gerekiyordu. yani pat (port adress translation)
Nat ise network adress translation ; dış dünyayı iç dünyaya baglamak gibi düşünülebilir.

kubernetes cni ( container network interface ) saglıyor. Bunun haricindeki implementasyon size baglıdır.
cncf önderliginde standartlar belirlenmektedir. linux tarafında network akışına bakmak faydalı olabilir.

![K8sNetwork](Asset/kubernetesNetwork.PNG)

Gelen trafigin belirli bir pod üzerinde çalışmasını isterseniz; policy yazılması gerekiyor. Kolaylıklar için wave, calicco, cilium gibi network pluginleri vardır. En cok kullanılan calicco'dur

####################################################################################################################
#
#  https://www.youtube.com/watch?v=eiteGaTy8DQ&list=PLp2nIigfaaYsx4JxCKNlCFJX1ghkCGV9s&index=7&ab_channel=HamzaG%C3%9CRCAN
#
####################################################################################################################

**Service Mesh**



Microservislerin birbirleriyle haberlesmesini saglayan bir alt yapıdır. Opensource olarak istio baz alinabilir.
Microservisler api / Application programming interface ile haberlesebiliyorlardi. 

servisler arasi iletişimi bir adet proxy tanımlasak ve bu da şifreleme, authentication, load balance, circuit breaker, monitoring vs içinde kullanılan bir yapı kullanılma durumudur.

proxy -> sidecar proxy denir, bu yapıya ise service mesh denir.

2 ana yapıdan olusur temelde.
1--> control plane: kullanılacak apiler için policyler olusturulan alan..
2--> data plane: sidecar proxy --sepet proxy


istio, consul, linkerd .. gibi cözümler mevcuttur.



[redhatServiceMesh](https://www.redhat.com/en/topics/microservices/what-is-a-service-mesh)
```
A service mesh, like the open source project Istio, is a way to control how different parts of an application share data with one another. Unlike other systems for managing this communication, a service mesh is a dedicated infrastructure layer built right into an app. This visible infrastructure layer can document how well (or not) different parts of an app interact, so it becomes easier to optimize communication and avoid downtime as an app grows.

Each part of an app, called a "service," relies on other services to give users what they want. If a user of an online retail app wants to buy something, they need to know if the item is in stock. So, the service that communicates with the company's inventory database needs to communicate with the product webpage, which itself needs to communicate with the user’s online shopping cart. To add business value, this retailer might eventually build a service that gives users in-app product recommendations. This new service will communicate with a database of product tags to make recommendations, but it also needs to communicate with the same inventory database that the product page needed—it’s a lot of reusable, moving parts.

Modern applications are often broken down in this way, as a network of services each performing a specific business function. In order to execute its function, one service might need to request data from several other services. But what if some services get overloaded with requests, like the retailer’s inventory database? This is where a service mesh comes in—it routes requests from one service to the next, optimizing how all the moving parts work together.











how does it work_?

any architecture have always needed rules to specify how requests get from point A to point B.
Proxies are a familiar concept in enterprise IT—if you are accessing this webpage from a work computer, there’s a good chance you just used one:

```
![Proxy](Asset/image_2022-07-05_164842.png)

1. As your request for this page went out, it was first received by your company’s web proxy…
2. After passing the proxy’s security measure, it was sent to the server that hosts this page…
3. Next, this page was returned to the proxy and again checked against its security measures…
4. And then it was finally sent from the proxy to you.


`In a service mesh, requests are routed between microservices through proxies in their own infrastructure layer. For this reason, individual proxies that make up a service mesh are sometimes called "sidecars," since they run alongside each service, rather than within them. Taken together, these "sidecar" proxies—decoupled from each service—form a mesh network.`


![sidecarMicroservice](Asset/image_2022-07-05_165536.png)

`For example, If a given service fails, a service mesh can collect data on how long it took before a retry succeeded. As data on failure times for a given service aggregates, rules can be written to determine the optimal wait time before retrying that service, ensuring that the system does not become overburdened by unnecessary retries.`


```
With a service mesh:

Developers can focus on adding business value, instead of connecting services.
Distributed tracing of requests through Jaeger presents a visible infrastructure layer alongside services, so problems are easier to recognize and diagnose.
Apps are more resilient to downtime, since a service mesh can reroute requests away from failed services.
Performance metrics can suggest ways to optimize communication in the runtime environment.
Start planning for the future—experiment with a service mesh on Red Hat® OpenShift® Service Mesh. Experience a uniform way to connect, manage, and observe microservices-based applications with behavioral insight into—and control of—the networked microservices in your service mesh. OpenShift Service Mesh is available (at no cost) for Red Hat OpenShift.
```






**Helm (Package Manager)**


** Faydalı Bilgiler **

---
Kubernetes üzerinde uygulamalarınızın başka uygulamalar ile aynı node üzerinde çalışmasını istiyorsanız ne yapmanız gerekiyor?

Node labellerine ve node üzerinde çalışan podlara göre scheduling kuralları nasıl yazılır?

Affinity ve Anti-Affinity konu anlatımı için [mstryoda](https://mstryoda.github.io/kubernetes-kitap/#/affinity-anti-affinity)

**Özet : **
1. Bir uygulama veya config dosyası yazıldı. api v1
2. Ardından uygulama container yapıda oluşturuldu
3. K8s'de çalışması için pod yaratıldı. Direk pod olarak da replicaset/replication controllerda ya da Deployment / Statefulset üzerinden oluşturabiliriz.

```
kind: ConfigMap
apiVersion: v1
metadata:
  name: my-app
  namespace: default
data:
  APP_ENV: 'staging'
  SOME_CONFIG: 'hello'

```

> Görüldügü gibi kubernetesi console üzerinden yönetmek hayli zor. Bu nedenle kubernetes, rancher gibi arayüzler ile manage edilebilir.


[Kubernetes install with ansible /kubespray](https://kubernetes.io/docs/setup/production-environment/tools/kubespray/)

[Kubernetes install with ansible /kubespray2](https://erdembas.dev/2020/12/high-availability-kubernetes-cluster-kurulumu/)

[Kubernetes install with ansible /kubespray3 video](https://www.youtube.com/watch?v=Hh9V_BgelgY)

**Overview**

| Cluster | Açıklama |
| ------ | ------ |
| Namespaces | * Aynı amaca hizmet eden ortak alan * |
| Nodes | * Master & Worker nodes * |
| Persistent Volumes | * Kalıcı depolamayı yönetmesi ve kullanması için API ile erişim sağlayan Pv alt sistemini kullanır. * |
| Storage Classes | * Dinamik depolama sağlayan kubernetes * |


![KubeDashboard](Asset/Kubernetes_Dashboard1.png)





| Workloads | Açıklama |
| ------ | ------ |
| Cron Jobs | * Aynı amaca hizmet eden ortak alan * |
| Daemon Sets | * Master & Worker nodes * |
| Deployments | * Kalıcı depolamayı yönetmesi ve kullanması için API ile erişim sağlayan Pv alt sistemini kullanır. * |
| Jobs | * Dinamik depolama sağlayan kubernetes * |
| Pods | * Dinamik depolama sağlayan kubernetes * |
| Replica Sets | * Dinamik depolama sağlayan kubernetes * |
| Replication Controllers | * Dinamik depolama sağlayan kubernetes * |
| Stateful Sets | * Dinamik depolama sağlayan kubernetes * |




![KubeDashboard - 2](Asset/Kubernetes_Dashboard2.png)




# ne-nedir

> Projenin amacı Kafka sh ile data flow -> Kubernetes ile container yönetimi -> jenkins ile ci/cd -> Grafana'da monitoring -> Pagerduty ile oncall alert oluşturan bir devops çözümü sağlamak.

[Best repo](https://github.com/bregman-arie/devops-exercises)

[best_practice](https://learnk8s.io/production-best-practices)

[best repo 2](https://github.com/learnk8s)

[Ml](https://gitlab.com/tech-marketing/devops-platform/devops-platform-mlflow/-/blob/master/examples/credit_card_fraud.ipynb)

[trendyol](https://inframetrics.trendyol.com/d/gP0c_Sc7z/inframetrics?orgId=1&kiosk)

| TO DO | IN PROGRESS | DONE |
| ------ | ------ | ------ |
| Prometheus-Grafana | Check | OK |
| nkins | Proxy | * |
| Pagerduty-oncall | ** | * |
| K8s | K8s lab | OK |
| Kafka | Kafka-scripts | Prod (OK) |




![FlowCYCLE](Postmortem/Cycle.drawio.png)

https://www.tldraw.com/


[cka rehberi](https://medium.com/devopsturkiye/certified-kubernetes-administrator-cka-rehberi-d356a0892056)

[kubernetesKomutPratikBilgi](https://www.veribilimiokulu.com/pratik-bilgiler-ve-komutlar-11-kubernetes/)

https://medium.com/devopsturkiye/kiali-ile-microservicelerin-trafi%C4%9Fini-g%C3%B6rselle%C5%9Ftirin-8430e8835fbd


[docker save-export](https://serkankaya.net/docker/docker-ile-duzenlenmis-imagei-ve-containeri-kaydetme-ve-yuklemetar-dosyasi-olarak/)



```
docker image ls
docker save bitnami/kubectl:2022 -o bitnamikubectl.gz
## other host;
docker load -i bitnamikubectl.gz

eger çalışan bir container varsa export ile ziplenebilir.

docker run --name myBitnami bitnami/kubectl:2022
docker export myBitnami -o bitnamikubectl.tar
```
https://js.wiki/



user tanımlama;

$devmapsuser
    1  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    2  mkdir $HOME/.kube
    3  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    4  sudo chown $(id -u):$(id -g) $HOME/.kube/config
    5  kubectl get nodes


disable firewalld
   45  systemctl status firewalld.service
   46  systemctl disable --now firewalld
   47  systemctl status firewalld.service
   48  history
   49  cat /etc/chrony.conf
   50  cat /etc/selinux/config
   51  setenforce 0
   52  passwd root




Nameserver limits exceeded" err="Nameserver limits were exceeded, some nameservers have been omitted, the applied nameserver line is:



In short, you have too many entries in /etc/resolv.conf.

This is a known issue https://kubernetes.io/docs/tasks/administer-cluster/dns-debugging-resolution/#known-issues:

Some Linux distributions (e.g. Ubuntu), use a local DNS resolver by default (systemd-resolved). Systemd-resolved moves and replaces /etc/resolv.conf with a stub file that can cause a fatal forwarding loop when resolving names in upstream servers. This can be fixed manually by using kubelet’s --resolv-conf flag to point to the correct resolv.conf (With systemd-resolved, this is /run/systemd/resolve/resolv.conf). kubeadm (>= 1.11) automatically detects systemd-resolved, and adjusts the kubelet flags accordingly.

Also

Linux’s libc is impossibly stuck (see this bug from 2005) with limits of just 3 DNS nameserver records and 6 DNS search records. Kubernetes needs to consume 1 nameserver record and 3 search records. This means that if a local installation already uses 3 nameservers or uses more than 3 searches, some of those settings will be lost. As a partial workaround, the node can run dnsmasq which will provide more nameserver entries, but not more search entries. You can also use kubelet’s --resolv-conf flag.

If you are using Alpine version 3.3 or earlier as your base image, DNS may not work properly owing to a known issue with Alpine. Check here for more information.

You possibly could change that in the Kubernetes code, but I'm not sure about the functionality. As it's set to that value for purpose.

Code can be located here

```
const (
    // Limits on various DNS parameters. These are derived from
    // restrictions in Linux libc name resolution handling.
    // Max number of DNS name servers.
    MaxDNSNameservers = 3
    // Max number of domains in search path.
    MaxDNSSearchPaths = 6
    // Max number of characters in search path.
    MaxDNSSearchListChars = 256
)
```


```
$ kubectl get namespaces --show-labels
NAME                   STATUS   AGE   LABELS
default                Active   27d   kubernetes.io/metadata.name=default
kube-node-lease        Active   27d   kubernetes.io/metadata.name=kube-node-lease
kube-public            Active   27d   kubernetes.io/metadata.name=kube-public
kube-system            Active   27d   kubernetes.io/metadata.name=kube-system
kubernetes-dashboard   Active   27d   kubernetes.io/metadata.name=kubernetes-dashboard

kubectl config view





---
My variation on this for ubuntu and including microk8s:

# k8s:
alias k=kubectl
alias mk=microk8s.kubectl
#alias kubectl=microk8s.kubectl # Use the apt installed kubectl...
alias helm3=microk8s.helm3
alias md=microk8s.docker

unset KUBECONFIG

# k8s default context:
[ -e ~/.kube/config ] && export KUBECONFIG=$HOME:/.kube/config

# If we have microk8s; generate a context for it
which microk8s.config 2>/dev/null > /dev/null && microk8s.config > ~/.kube/contexts/microk8s.yaml

# We put other contexts in ~/.kube/contexts
for c in $(IFS=$'\n' find ~/.kube/contexts -type f -name "*.yaml")
do
export KUBECONFIG=$c:$KUBECONFIG
done


########################################################################################

########################################################################################
Invalid x509 certificate for kubernetes master


Unable to connect to the server: x509: certificate is valid for 10.96.0.1, 10.161.233.80, not 114.215.201.87



One option is to tell kubectl that you don't want the certificate to be validated. Obviously this brings up security issues but I guess you are only testing so here you go:

kubectl --insecure-skip-tls-verify --context=employee-context get pods
The better option is to fix the certificate. Easiest if you reinitialize the cluster by running kubeadm reset on all nodes including the master and then do

kubeadm init --apiserver-cert-extra-sans=114.215.201.87
It's also possible to fix that certificate without wiping everything, but that's a bit more tricky. Execute something like this on the master as root:

rm /etc/kubernetes/pki/apiserver.*
kubeadm init phase certs all --apiserver-advertise-address=0.0.0.0 --apiserver-cert-extra-sans=10.161.233.80,114.215.201.87
docker rm `docker ps -q -f 'name=k8s_kube-apiserver*'`
systemctl restart kubelet


---

rm /etc/kubernetes/pki/apiserver.*
kubeadm alpha phase certs all --apiserver-advertise-address=0.0.0.0 --apiserver-cert-extra-sans=10.161.233.80,114.215.201.87
docker rm -f `docker ps -q -f 'name=k8s_kube-apiserver*'`
systemctl restart kubelet
Also whould be better to add dns name into --apiserver-cert-extra-sans for avoid issues like this in next time.

---

rm /etc/kubernetes/pki/apiserver.*
kubeadm init phase certs all --apiserver-advertise-address=0.0.0.0 --apiserver-cert-extra-sans=114.215.201.87
docker rm -f `docker ps -q -f 'name=k8s_kube-apiserver*'`
systemctl restart kubelet


---

Issue cause: Your configs at $HOME/.kube/ are present with your old IP address.

Try running,

rm $HOME/.kube/* -rf
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config

---

rm /etc/kubernetes/pki/apiserver.*
kubeadm alpha phase certs all --apiserver-advertise-address=0.0.0.0 --apiserver-cert-extra-sans=51.158.75.136
docker rm -f `docker ps -q -f 'name=k8s_kube-apiserver*'`
systemctl restart kubelet


---
kubeadm init phase certs all


kubectl get nodes -o yaml | grep -- "- address:"

kubectl describe configmaps -n mla | grep -i -A5 -B5 "session"

   2  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    3  mkdir $HOME/.kube
    4  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    5   sudo chown $(id -u):$(id -g) $HOME/.kube/config



kubectl --version
kubectl version
kubeadm -h
kubeadm token
kubeadm token -h
kubeadm list
kubeadm token create --print-join-command
kubeadm init phase upload-certs --upload-certs
kubeadm init phase upload-certs --experimental-upload-certs
kubeadm alpha certs certificate-key
kubeadm certs certificate-key
vim /etc/sudoers
systemctl status kubelet
systemctl status containerd
cd /etc/containerd/
vim config.toml
cat /run/containerd/containerd.sock
cd /run/containerd/
cat containerd.sock
cd /run/containerd/
cd runc/
cd k8s.io/
cd /var/lib/kubelet/
vim kubeadm-flags.env
kubectl -n kube-system get cm kubeadm-config -o yaml
cd /var/lib/kubelet/
vim kubeadm-flags.env
vim config.yaml
kubeadm init phase upload-certs --upload-certs
kubeadm token create --print-join-command


```

