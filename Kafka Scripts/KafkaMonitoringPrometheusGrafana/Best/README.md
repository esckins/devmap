```
wget https://github.com/lightbend/kafka-lag-exporter/releases/download/v0.7.1/kafka-lag-exporter-0.7.1.zip
unzip kafka-lag-exporter-0.7.1.zip
rm -rf kafka-lag-exporter-0.7.1.zip
```


**Configure Kafka Lag exporter**

Kafka Lag exporter is non-intrusive in nature – meaning it does not require any changes to be done to your Kafka setup. Some of the configuration to get going is the given below.

> Kafka Broker addresses – Required
> Endpoint port number – Default – 8080. Prometheus server will scrape this port.
> Poll interval – Default – 30 seconds. How often does the lag monitor will poll the Kafka cluster.
> Whitelist of metrics – If you want to scrape only specific metrics.

In addition these simple configuration parameters there is a nice list of parameters available here.

You can now start the Lag monitor using the following command

`./kafka-lag-exporter -Dconfig.file=/home/ec2-user/kafka-lag-exporter-0.6.2/bin/application.conf`


application.conf
```
kafka-lag-exporter {
  reporters.prometheus.port = 7076
  clusters = [
    {
      name = "vkf"
      bootstrap-brokers = "kafkax01:9600,kafkax02:9600,kafkax03:9600"
      labels = {
        location = "ny"
        zone = "us-east"
      }
    }
  ]
}

```

**Query Kafka lag metrics using PromQL**

The metrics from Kafka lag exporter can be queried like any other metrics. Type in kafka_consumergroup_group_lag metric.


![image.png](./image.png)

add to service via nohup

```
vi /etc/systemd/system/kafka.service
[Unit]
Requires=zookeeper.service
After=zookeeper.service

[Service]
Type=simple
User=kafka

Environment="KAFKA_OPTS=-javaagent:/opt/kafka/libs/jmx_prometheus_javaagent-0.16.1.jar=7075:/opt/kafka/config/jmx_exporter.yml"

nohup /opt/kafka/kafka-lag-exporter-0.7.1/bin/kafka-lag-exporter -Dconfig.file=/opt/kafka/kafka-lag-exporter-0.7.1/bin/application.conf > /kafka/kafka-lag.log 2>&1


ExecStart=/bin/sh -c '/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties > /opt/kafka/kafka.log 2>&1'
ExecStop=/opt/kafka/bin/kafka-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target


crontab -l

NOT WORKING

crontab -e
#### kafka lag exporter

*/1 * * * * /opt/kafka/kafka-lag-exporter-0.7.1/bin/kafka-lag-exporter -Dconfig.file=/opt/kafka/kafka-lag-exporter-0.7.1/bin/application.conf > /dev/null 2>&1

jobs -l


```

$$$ in docker
```
#!/bin/bash

PROJECT_ROOT="$(dirname $0)"

cd ${PROJECT_ROOT}

function run() {
    docker-compose down
    docker-compose up -d
}

run

cd - > /dev/null
```


or u can create script;
```
vi lag_exporter.sh

#!/bin/sh

#/opt/kafka/bin/testKafka/bin

./kafka-lag-exporter -Dconfig.file=/opt/kafka/bin/testKafka/bin/application.conf > /dev/null 2>&1
```
connection with prometheus & kafka ports
telnet 10.30.30.10 7076

![image-1.png](./image-1.png)


Prometheus- status - targets;
sum(kafka_consumergroup_group_lag)

![image-2.png](./image-2.png)

Grafana;

![image-3.png](./image-3.png)


**Usefull Link**


[seglo](https://github.com/seglo/kafka-lag-exporter#run-standalone)

[cloudwalkerLag](https://www.cloudwalker.io/2020/06/07/monitoring-kafka-with-prometheus/)

[Kafka Exporter RedHat](https://access.redhat.com/documentation/en-us/red_hat_amq/7.5/html/using_amq_streams_on_rhel/assembly-kafka-exporter-str#con-metrics-kafka-exporter-grafana-str)


[exampleConfig](https://github.com/prometheus/jmx_exporter/tree/master/example_configs)

[k8sMonitoring](https://computingforgeeks.com/install-grafana-on-kubernetes-for-monitoring/)

