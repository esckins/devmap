https://github.com/confluentinc/jmx-monitoring-stacks/tree/6.1.0-post/jmxexporter-prometheus-grafana


https://www.confluent.io/blog/monitor-kafka-clusters-with-prometheus-grafana-and-confluent/
+++
best link;

https://www.confluent.io/blog/monitor-kafka-clusters-with-prometheus-grafana-and-confluent/

https://grafana.com/docs/grafana-cloud/integrations/integrations/integration-kafka/
```
vi /opt/kafka/config/jmx_exporter.yml


lowercaseOutputName: true

rules:
# Special cases and very specific rules
- pattern : kafka.server<type=(.+), name=(.+), clientId=(.+), topic=(.+), partition=(.*)><>Value
  name: kafka_server_$1_$2
  type: GAUGE
  labels:
    clientId: "$3"
    topic: "$4"
    partition: "$5"
- pattern : kafka.server<type=(.+), name=(.+), clientId=(.+), brokerHost=(.+), brokerPort=(.+)><>Value
  name: kafka_server_$1_$2
  type: GAUGE
  labels:
    clientId: "$3"
    broker: "$4:$5"
- pattern : kafka.coordinator.(\w+)<type=(.+), name=(.+)><>Value
  name: kafka_coordinator_$1_$2_$3
  type: GAUGE

# Generic per-second counters with 0-2 key/value pairs
- pattern: kafka.(\w+)<type=(.+), name=(.+)PerSec\w*, (.+)=(.+), (.+)=(.+)><>Count
  name: kafka_$1_$2_$3_total
  type: COUNTER
  labels:
    "$4": "$5"
    "$6": "$7"
- pattern: kafka.(\w+)<type=(.+), name=(.+)PerSec\w*, (.+)=(.+)><>Count
  name: kafka_$1_$2_$3_total
  type: COUNTER
  labels:
    "$4": "$5"
- pattern: kafka.(\w+)<type=(.+), name=(.+)PerSec\w*><>Count
  name: kafka_$1_$2_$3_total
  type: COUNTER

- pattern: kafka.server<type=(.+), client-id=(.+)><>([a-z-]+)
  name: kafka_server_quota_$3
  type: GAUGE
  labels:
    resource: "$1"
    clientId: "$2"

- pattern: kafka.server<type=(.+), user=(.+), client-id=(.+)><>([a-z-]+)
  name: kafka_server_quota_$4
  type: GAUGE
  labels:
    resource: "$1"
    user: "$2"
    clientId: "$3"

# Generic gauges with 0-2 key/value pairs
- pattern: kafka.(\w+)<type=(.+), name=(.+), (.+)=(.+), (.+)=(.+)><>Value
  name: kafka_$1_$2_$3
  type: GAUGE
  labels:
    "$4": "$5"
    "$6": "$7"
- pattern: kafka.(\w+)<type=(.+), name=(.+), (.+)=(.+)><>Value
  name: kafka_$1_$2_$3
  type: GAUGE
  labels:
    "$4": "$5"
- pattern: kafka.(\w+)<type=(.+), name=(.+)><>Value
  name: kafka_$1_$2_$3
  type: GAUGE

# Emulate Prometheus 'Summary' metrics for the exported 'Histogram's.
#
# Note that these are missing the '_sum' metric!
- pattern: kafka.(\w+)<type=(.+), name=(.+), (.+)=(.+), (.+)=(.+)><>Count
  name: kafka_$1_$2_$3_count
  type: COUNTER
  labels:
    "$4": "$5"
    "$6": "$7"
- pattern: kafka.(\w+)<type=(.+), name=(.+), (.+)=(.*), (.+)=(.+)><>(\d+)thPercentile
  name: kafka_$1_$2_$3
  type: GAUGE
  labels:
    "$4": "$5"
    "$6": "$7"
    quantile: "0.$8"
- pattern: kafka.(\w+)<type=(.+), name=(.+), (.+)=(.+)><>Count
  name: kafka_$1_$2_$3_count
  type: COUNTER
  labels:
    "$4": "$5"
- pattern: kafka.(\w+)<type=(.+), name=(.+), (.+)=(.*)><>(\d+)thPercentile
  name: kafka_$1_$2_$3
  type: GAUGE
  labels:
    "$4": "$5"
    quantile: "0.$6"
- pattern: kafka.(\w+)<type=(.+), name=(.+)><>Count
  name: kafka_$1_$2_$3_count
  type: COUNTER
- pattern: kafka.(\w+)<type=(.+), name=(.+)><>(\d+)thPercentile
  name: kafka_$1_$2_$3
  type: GAUGE
  labels:
    quantile: "0.$4"

```




```
##add jar file
libs]# ll |grep jmx
-rw-r--r-- 1 root  root    469645 May  5 15:11 jmx_prometheus_javaagent-0.16.1.jar
################
```


```
vi /opt/kafka/bin/kafka-server-start.sh

export KAFKA_OPTS=' -javaagent:/opt/kafka/libs/jmx_prometheus_javaagent-0.16.1.jar=7075:/opt/kafka/config/jmx_exporter.yml'

################
```

```
vi /etc/systemd/system/kafka.service
##add environment
Environment="KAFKA_OPTS=-javaagent:/opt/kafka/libs/jmx_prometheus_javaagent-0.16.1.jar=7075:/opt/kafka/config/jmx_exporter.yml"

```

```
sudo ss -tunelp | grep 7075
tcp    LISTEN     0      3      [::]:7075               [::]:*                   users:(("java",pid=62882,fd=87)) uid:15001 ino:61573385 sk:74f <->

curl -v http://esckins:7075/metric
```
```
Note
Prometheus must have access to jmx_exporter’s port. We advise you to keep jmx_exporter ports inaccessible from any other external hosts in order to have a more secure infrastructure setup.
```



Resources;

https://computingforgeeks.com/monitor-apache-kafka-with-prometheus-and-grafana/

https://techoverloadeddiar.me/blog_posts/data-science/monitoring-kafka-using-prometheus-on-a-grafana-dashboard/
https://github.com/LiferaySavvy/kafka-monitoring/blob/master/kafka_broker.yml

https://www.robustperception.io/reloading-prometheus-configuration

https://github.com/YasuniChamodya/Monitoring-Kafka


--Zabbix plugin
https://blog.zabbix.com/configuring-grafana-with-zabbix/8007/

https://grafana.com/grafana/plugins/alexanderzobnin-zabbix-app/?tab=installation

--problem ; dash. not found, --> wa solution; manual install with json file in github source


![PostgresqlConn.png](./image.png)

![ZabbixConn.png](./image-1.png)

![ZabbixUserCreate.png](./image-3.png)

![ZabbixUserRole.png](./image-4.png)

![zabbixServer.png](./image-2.png)


