![Kafka](Asset/kafkaProducer-Consumer.PNG)

[bashKafka](https://github.com/framiere/kafka-in-bash)

For latency; Linkedin Burrow

https://github.com/framiere/kafka-in-bash

https://www.lionbloggertech.com/setting-up-burrow-dashboard-for-kafka-lag-monitoring/

https://dzone.com/articles/kafka-monitoring-with-burrow

https://community.cloudera.com/t5/Community-Articles/Monitoring-Kafka-with-Burrow-Part-1/ta-p/245987

https://github.com/linkedin/Burrow#:~:text=Burrow%20is%20a%20monitoring%20companion,of%20those%20consumers%20on%20demand.

https://github.com/linkedin/Burrow


```
# telnet localhost 2181
Trying ::1...
Connected to localhost.
Escape character is '^]'.
stats
Zookeeper version: 3.4.14-4c25d480e66aadd371de8bd2fd8da255ac140bcf, built on 03/06/2019 16:18 GMT
Clients:
 /10.30.30.10:35377[1](queued=0,recved=18857,sent=18857)
 /0:0:0:0:0:0:0:1:40765[0](queued=0,recved=1,sent=0)

Latency min/avg/max: 0/0/3533
Received: 4447122
Sent: 4448143
Connections: 2
Outstanding: 0
Zxid: 0x1e00267b74
Mode: follower
Node count: 596
Connection closed by foreign host.
```

```
echo stat | nc <zookeeper ip> 2181
echo mntr | nc <zookeeper ip> 2181
echo isro  | nc <zookeeper ip> 2181

# echo "ruok" | nc localhost 2181 ; echo
imok
```
```
telnet hostname 2181                                                                                                                                
Trying ::1...
Connected to localhost.
Escape character is '^]'.
srvr
Zookeeper version: 3.7.0-e3704b390a6697bfdf4b0bef79e3da7a4f6bac4b, built on 2021-03-17 09:46 UTC
Latency min/avg/max: 0/0.0/0
Received: 3
Sent: 2
Connections: 1
Outstanding: 0
Zxid: 0x0
Mode: standalone
Node count: 5
Connection closed by foreign host.
```


[zkApache info](https://zookeeper.apache.org/doc/r3.1.2/zookeeperAdmin.html#sc_zkCommands)

./zkServer.sh status
[for more info ibm](https://developer.ibm.com/technologies/analytics/)


```
# cat /etc/systemd/system/zookeeper.service
[Unit]
Requires=network.target remote-fs.target
After=network.target remote-fs.target

[Service]
Type=simple
User=kafka
ExecStart=/opt/kafka/bin/zookeeper-server-start.sh /opt/kafka/config/zookeeper.properties
ExecStop=/opt/kafka/bin/zookeeper-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
```


```
auto.create.topics.enable=false

https://stackoverflow.com/questions/36441768/how-to-create-topics-in-apache-kafka
```




---

server.properties.
```
broker.id=i
listeners=PLAINTEXT://kafkax0i:9600
num.network.threads=3
num.io.threads=8
socket.send.buffer.bytes=102400
socket.receive.buffer.bytes=102400
socket.request.max.bytes=104857600

log.dirs=/kafka
num.partitions=1
num.recovery.threads.per.data.dir=1
offsets.topic.replication.factor=3
transaction.state.log.replication.factor=1
transaction.state.log.min.isr=1

log.retention.hours=8

log.segment.bytes=1073741824

log.retention.check.interval.ms=300000


zookeeper.connect=kafkaxi:2181,kafkaxi+1:2181,kafkaxi+2:2181
zookeeper.connection.timeout.ms=6000
group.initial.rebalance.delay.ms=0
delete.topic.enable = true
auto.create.topics.enable = false
```
