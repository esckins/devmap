#!/bin/bash
cd /usr/share/kafka/kafka_2.11-2.4.1/ || exit 1
lag=$(./bin/kafka-consumer-groups.sh --bootstrap-server localhost:9092 --describe --group my_group 2>/dev/null|grep -v GROUP|awk 'NR>1{num+=$6}END{print num}')
echo "$lag"
if [ "$lag" -gt 10 ];then
  Echo "lag value is too large"
  #Or mail or send_ ding_ MSG, self setting
fi
