#!/bin/python3
#!usr/bin/python3

import os
import re
from elasticsearch import Elasticsearch
import datetime
import numpy as np


now = datetime.datetime.now()
es = Elasticsearch(
    ['elasticx01'],
    scheme="http",
    port=9200,
    http_auth=('admin', 'esckins')
)
index = ["metricbeat*"]
for indexname in index:
  query_5m = {
  "aggs": {
    "2": {
      "terms": {
        "field": "kafka.topic.name",
        "order": {
          "1": "desc"
        },
        "size": 5
      },
      "aggs": {
        "1": {
          "max": {
            "field": "kafka.consumergroup.offset"
          }
        }
      }
    }
  },
  "size": 0,
  "_source": {
    "excludes": []
  },
  "stored_fields": [
    "*"
  ],
  "script_fields": {},
  "query": {
    "bool": {
      "must": [],
      "filter": [
        {
          "match_all": {}
        },
        {
          "range": {
            "@timestamp": {
              "format": "strict_date_optional_time",
              #"gte": "now-5m",
              #"lte": "now-2m"
              "gte": "2022-02-02T13:25:00.000Z",
              "lte": "2022-02-02T13:30:00.000Z"
            }
          }
        }
      ],
      "should": [],
      "must_not": []
    }
  }
}
  query_5m_2 = {
  "aggs": {
    "2": {
      "terms": {
        "field": "kafka.topic.name",
        "order": {
          "1": "desc"
        },
        "size": 5
      },
      "aggs": {
        "1": {
          "max": {
            "field": "kafka.partition.offset.newest"
          }
        }
      }
    }
  },
  "size": 0,
  "_source": {
    "excludes": []
  },
  "stored_fields": [
    "*"
  ],
  "script_fields": {},
  "query": {
    "bool": {
      "must": [],
      "filter": [
        {
          "match_all": {}
        },
        {
          "range": {
            "@timestamp": {
              "format": "strict_date_optional_time",
              #"gte": "now-5m",
              #"lte": "now-2m"
              "gte": "2022-02-02T13:25:00.000Z",
              "lte": "2022-02-02T13:30:00.000Z"
            }
          }
        }
      ],
      "should": [],
      "must_not": []
    }
  }
}
  #res_5m = es.count(index=indexname,body=query_5m)
  #print(f'result is: {res_5m} test')
  result = es.search(index=indexname, body=query_5m)
  #print("query hits:",result)
  #print("query hits:",len(result["aggregations"]["buckets"]))
  #print("query hits:",result["aggregations"]["2"]["buckets"])
  #b=result["aggregations"]["2"]["buckets"][i]["1"]["value"]

  b=result["aggregations"]["2"]["buckets"]
  print(type(b))
  print("#########################################")
  #i=0
  res_value_total = []
  res_key_total = []
  for i in range(0,len(b)):
    res_value=b[i]["1"]["value"]
    res_value_total.append(res_value)
    res_key=b[i]["key"]
    res_key_total.append(res_key)
  print(res_value_total)
  print(res_key_total)
  res_value_total = np.array(res_value_total)



  res_2 = es.search(index=indexname, body=query_5m_2)
  print("#########################################")
  a=res_2["aggregations"]["2"]["buckets"]
  res_value_total2 = []
  res_key_total2 = []
  for j in range(0,len(a)):
    res_value_2=a[j]["1"]["value"]
    res_value_total2.append(res_value_2)
    res_key=b[j]["key"]
    res_key_total2.append(res_key)
  print(res_value_total2)
  #print()
  #print(res_key_total2)


  res_value_total2 = np.array(res_value_total2)
  difference_of_values = res_value_total2 - res_value_total
  difference_of_values = list(difference_of_values)
  zip_result = zip(res_key_total, difference_of_values)
  dict_result = dict(zip_result)


"""
  print()
  print(dict_result)
  print()
  #for i in range(len(difference_of_value))
  for key, value in dict_result.items():
    if value > 0:
       print(key, '-->', value)
"""

####################### Mail for 5 minute lag #################################
  attachment = r'/esckins/mail/kafka_latency.txt'
  filesize = os.path.getsize(attachment)
  print(filesize)


  with open(attachment, "a") as file:
    if filesize != 0:
        print('file is empty')
        file.truncate(0)
    else:
        print('File is full')

  for key, value in dict_result.items():
    if value > 300000000000:
        print(key, '-->', value)
        with open(attachment, "a") as file:
            file.write(str(key) + ' --> ' + str(value) + '\n')
  #os.system('''echo " Kafka latency alarms." | mailx -s "Kafka_latency" -a /esckins/mail/kafka_latency.txt  -r kafka@esckins.com.tr HarunOzgur.TISKAYA@esckins.com mail@esckins.com''')
        os.system(" echo ' ### Kafka latency alarm ###\n\n Ekteki topicler için lag oluşmustur(5dk üzeridir & birimi -> ns).\n Sistem Log Analitiği Servisini kontrol ediniz..\n **Bu mail hosts sunucusu, kafka_latency_3.py den atılmaktadır**\n\n BR' | mailx -a /esckins/mail/kafka_latency.txt -v -r 'kafka@esckins.com.tr' -s 'Latency Alert' -S smtp='mailrelay.esckins.intra:25' mail@esckins.com ")
  ### Powered by Harun Ozgur Tiskaya









"""
############################################################


#for z in range(10):
#  val = random.randint(0,99)
#  print(val, end = ' ' )

class Matrix:
    def __init__(self, nrows, ncols):
        self.matrix = [[0] * ncols for i in range(nrows)]

    def __getitem__(self, index):
        return self.matrix[index[0]][index[1]]

    def __setitem__(self, index, val):
        self.matrix[index[0]][index[1]] = val

    def __str__(self):
        s = ''
        for i in range(len(self.matrix)):
            for k in range(len(self.matrix[0])):
                if k != 0:
                    s += ' '
                s += str(self.matrix[i][k])
            s += '\n'

        return s

m = Matrix(5, 5)

for i in range(5):
    for k in range(5):
        print(m[i,k], end=' ')
    print()

print()
print(m)


###########################################################






#os.system ("echo 'legacy ${topic} ' | mail -s 'Check legacy' -r kafkax@esckins.com.tr mail@esckins.com")

echo "Text area" | mailx -v -r "kafka@esckins.com" -s "Predictive_Test" -S smtp="mailrelay.esckins.intra:25" mail@esckins.com

"""
