storage.tsdb.retention.size="500GB"

https://levelup.gitconnected.com/federating-prometheus-effectively-4ccd51b2767b


```
scrape_configs:
  - job_name: 'advanced-federation'
    scrape_interval: 20s
    scrape_timeout: 20s
    scheme: http
    metrics_path: /federate
    honor_labels: true
    metric_relabel_configs:
      - source_labels: [id]
        regex: '^static-agent$'
        action: drop    
    params:
      match[]:
        - '{__name__=~"kube_.*|node_.*|container_.*"}'
    static_configs:
      - targets: ['child-prometheus1:31090']
      - targets: ['child-prometheus2:31091']
```


[DOC](https://devopscube.com/setup-prometheus-monitoring-on-kubernetes/)


[Prometheus promql](https://awesome-prometheus-alerts.grep.to/rules.html)


[prometheusArch.](https://lyz-code.github.io/blue-book/devops/prometheus/prometheus_architecture/)

[grafanaOfficial](https://grafana.com/)

[Prometheus--](https://www.opensourceforu.com/2017/04/prometheus/)

[sensuGrafana](https://sensu.io/blog/visualizing-sensu-go-data-in-grafana)


[MicrometerTrendyol](https://medium.com/trendyol-tech/micrometer-ile-prometheus-metrikleri-oluşturma-808abde7fda3)


[windowsExporter](https://github.com/prometheus-community/windows_exporter/releases)

[grafanaWindowsExporter](https://grafana.com/grafana/dashboards/14694)

```
Usage
To use this dashboard you must install windows exporter from here and after that you do not have to do any other thing. dashboard will show what you saw in snapshots.

How you can install windows_eporter to collect data for this dashboard:
Download latest version of windows_exporter from here (download .msi file)
Open up a command prompt with administrator privileges (Run as administrator)
Change your directory where you've downloaded .msi file at
Run below command :
msiexec /i windows_exporter-0.16.0-amd64.msi ENABLED_COLLECTORS="ad,adfs,cache,cpu,cpu_info,cs,container,dfsr,dhcp,dns,fsrmquota,iis,logical_disk,logon,memory,msmq,mssql,netframework_clrexceptions,netframework_clrinterop,netframework_clrjit,netframework_clrloading,netframework_clrlocksandthreads,netframework_clrmemory,netframework_clrremoting,netframework_clrsecurity,net,os,process,remote_fx,service,tcp,time,vmware" TEXTFILE_DIR="C:\custom_metrics" LISTEN_PORT="9115"


Add your VM in to your prometheus server (prometheus.yml file)
```
https://community.chocolatey.org/packages/prometheus-windows-exporter.install#ansible

---

https://igboie.medium.com/kubernetes-ci-cd-with-github-github-actions-and-argo-cd-36b88b6bda64

https://www.youtube.com/watch?v=XE_mAhxZpwU&t=8s&ab_channel=Thetips4you

https://www.youtube.com/watch?v=V4ApOm37XCU&ab_channel=Metricfire

https://open.spotify.com/episode/7oogqbPeJrOvtAI1hCC0fk
