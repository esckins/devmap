file_sd_config .json file create dynamic


```
prometheus.yml
global:
  scrape_interval:     15s
  evaluation_interval: 15s 
scrape_configs:  
  - job_name: 'test'
    file_sd_configs:
      - files:
        - sample_generated_targets.json
```

```
sample_generated_targets.json
[
  {
    "targets": ["127.0.0.1:21500"],
    "labels": {
      "__metrics_path__": "/metrics/testitem1"
    }
  },
  {
    "targets": ["127.0.0.1:21500"],
    "labels": {
      "__metrics_path__": "/metrics/testitem2"
    }
  },
  {
    "targets": ["127.0.0.1:21500"],
    "labels": {
      "__metrics_path__": "/metrics/testitem3"
    }
  },
  {
    "targets": ["127.0.0.1:21500"],
    "labels": {
      "__metrics_path__": "/metrics/testitem4"
    }
  }
]
```









```
- job_name: 'instances-service-discovery'
    file_sd_configs:
      - files:
          - targets.json
        refresh_interval: 5s


```

$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

```
https://github.com/sapcc/atlas

Ironic Nodes
discoveries:
      ironic:
        refresh_interval: 600 #How often the discovery should check for new/updated nodes.
        targets_file_name: "ironic.json" #Name of the file to write the nodes to.
        os_auth: # Openstack auth
          auth_url: openstack auth url
          user: openstack user
          password: os user pw
          user_domain_name: openstack user_domain_name
          project_name: openstack project_name
          domain_name: openstack domain_name
+


Netbox API
DCIM-Devices
netbox:
    refresh_interval: 600 # How often the discovery should check for new/updated devices.
    targets_file_name: "netbox.json"  #Name of the file to write the devices to.
    netbox_host: "netbox_host_url"
    netbox_api_token: "netbox_api_token"
    dcim:
      devices: #Array of device queries
        - custom_labels: #Use to add custom labels to the target
            anyLabel: "anyValue"
            job: "job_name"
          target: 1 #Query Parameters: Any parameters the netbox api ([netbox_url]/api/dcim/devices/) accepts.
          role: "role_name"
          manufacturer: "cisco"
          region: "de1"
          status: "1"
        - custom_labels: ....


Virtualization-VMs
netbox:
    refresh_interval: 600 # How often the discovery should check for new/updated devices.
    targets_file_name: "netbox.json"  #Name of the file to write the devices to.
    netbox_host: "netbox_host_url"
    netbox_api_token: "netbox_api_token"
    virtualization:
      vm: #Array of vms queries
        - custom_labels: #Use to add custom labels to the target
            anyLabel: "anyValue"
            job: "job_name"
          target: 1 #Query Parameters: Any parameters the netbox api ([netbox_url]/api/virtualization/virtual-machines/",) accepts.
          manufacturer: "cisco"
          region: "de1"
          tag: "tag_name"
        - custom_labels: ....

NAME:
   atlas - discovers custom services, enriches them with metadata labels and writes them to a file or Kubernetes configmap
 USAGE:
   atlas [global options]
 VERSION:
   0.1.7
 COMMANDS:
     help, h  Shows a list of commands or help for one command
 GLOBAL OPTIONS:
  - OS_PROM_CONFIGMAP_NAME: name of the configmap, where the discovered nodes should be written to.
  - K8S_NAMESPACE: name of the K8s namespace atlas is running in.
  - K8S_REGION: name of the k8s region atlas is running in
  - LOG_LEVEL: log level atlas should use:
    - "debug"
    - "error"
    - "warn"
    - "info"
```

######################################################

It doesn't seem to be supported (at the moment?). At least until multi-config files is supported.

What worked for me is to use Ansible to maintain the config file (add/remove jobs via blockinfile) and reload the service by sending a SIGHUP (as suggested there: https://github.com/prometheus/prometheus/issues/2041).

As an example, here is the Ansible task:

```
- name: Append new job
  notify: Reload Prometheus
  blockinfile:
    path: /home/prometheus/prometheus/prometheus.yml
    marker: "# {mark} ANSIBLE MANAGED BLOCK - My new job"
    block: |
      # Must be 2-space indented
        - job_name: "my job"
          file_sd_configs:
            - files:
              - "/home/prometheus/prometheus/my_group_of_servers.yml"
          metrics_path: "/my_metrics"


Here's the handler:

- name: Reload Prometheus
  systemd:
    name: prometheus
    state: reloaded

You also need to add reload support to systemd service file:   

ExecReload=/bin/kill -HUP $MAINPID
```

resource:https://stackoverflow.com/questions/65933597/dynamically-add-job-to-a-prometheus-configuration
