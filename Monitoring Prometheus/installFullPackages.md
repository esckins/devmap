---


#################### Prometheus Install ####################

```
https://prometheus.io/download/

https://prometheus.io/docs/instrumenting/exporters/

https://www.yusufsezer.com.tr/prometheus/

```

```
curl -L -O https://github.com/prometheus/prometheus/releases/download/v2.34.0/prometheus-2.30.0.linux-amd64.tar.gz
###scp -r prometheus root@esckins01:/home

tar -xvzf prometheus-2.34.0.linux-amd64.tar.gz

cd prometheus-2.34.0.linux-amd64

./prometheus

ts=2022-03-29T12:52:06.626Z caller=main.go:755 level=error msg="Unable to start web listener" err="listen tcp 0.0.0.0:9090: bind: address already in use"
```


```
vi prometheus.yml


    static_configs:
      - targets: ["localhost:9091"]
```

```
./prometheus --web.listen-address=:9010 &

lsof -i :9090
sudo kill -9 <pid>


systemctl stop cockpit
systemctl stop cockpit.socket

http://esckinsx01:9090/
```

############################

```
cp -r . /usr/local/bin/prometheus
cat << EOF > /etc/systemd/system/prometheus.service
[Unit]
Description=Prometheus Service
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/prometheus/prometheus --config.file=/usr/local/bin/prometheus/prometheus.yml

[Install]
WantedBy=multi-user.target
EOF

service prometheus start

service prometheus status
```

## in webportal --> graph --> up --> execute


####

```
vi prometheus.yml
global: # global conf
	scrape_interval: 15s # verileri alma süresi
	evaluation_interval: 15s # kuralların işleme süresi

rule_files: # kurallar
	# - "first.rules"
	# - "second.rules"

scrape_configs: # verilerin alınacağı yer
	- job_name: prometheus
		static_configs:
		- targets: ['localhost:9090']

```

####
#for new resource, add the end of yml file.

```
- job_name: node_exporter
  scrape_interval: 1m
  scrape_timeout: 1m
  	static_configs:
	  - targets: ['localhost:9100']

#https://prometheus.io/docs/instrumenting/exporters/

curl -L -O https://github.com/prometheus/node_exporter/releases/download/v1.3.1/node_exporter-1.3.1.linux-amd64.tar.gz
# scp -r node_exporter-1.3.1.linux-amd64.tar.gz root@esckinsx01:/home/prometheus


tar -xvzf node_exporter-1.3.1.linux-amd64.tar.gz

cd node_exporter-1.3.1.linux-amd64/
./node_exporter

esckins:9100/metrics
```

###service for nodeexporter;

```
cp -r node_exporter /usr/local/bin

cp -r node_exporter /usr/local/bin
cat << EOF > /etc/systemd/system/node-exporter.service
[Unit]
Description=Prometheus Node Exporter Service
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
EOF


service node-exporter start

service node-exporter status
```


## add Node Exporter addresses to prometheus conf.

## go to Status>Command-Line Flags

```
--config.file	/usr/local/bin/prometheus/prometheus.yml

vi /usr/local/bin/prometheus/prometheus.yml
## add to end of file
  - job_name: 'node-exporter'
    static_configs:
    - targets: ['localhost:9100']

## or you can add like that;
cat << EOF >> /usr/local/bin/prometheus/prometheus.yml
  - job_name: 'node-exporter'
    static_configs:
    - targets: ['localhost:9100']
EOF

## you can change time for scrape with scrape_interval line

```

```
service prometheus restart

--> graph -> node_ -> node_uname_info like that node_disk_info


node_disk_info{device="dm-0", instance="localhost:9100", job="node-exporter", major="253", minor="0"}

```

## prometheus rules;

#PromQL query;

```
cat <<EOF > /usr/local/bin/prometheus/rules.yml
groups:
- name: memory_usage_10dk
  rules: 
  - record: memory_usage_10dk
    expr: (1 - avg(irate(node_cpu_seconds_total{mode="idle"}[10m])) by (instance)) * 100
EOF
```

#check file with promtool 

```
./promtool check rules rules.yml

./promtool check rules /usr/local/bin/prometheus/kural.yml
Checking /usr/local/bin/prometheus/kural.yml
  SUCCESS: 2 rules found



add rules file in prometheus.yml

rule_files:
   - "rules.yml"
```

```
#vi /usr/local/bin/prometheus/prometheus.yml

rule_files:
  - "/usr/local/bin/prometheus/kural.yml"
  # - "first_rules.yml"
  # - "second_rules.yml"



service prometheus restart

Status>Rules can access memory_usages
```


```
## Alert Manager ;

cat <<EOF >> /usr/local/bin/prometheus/rules.yml
- name: uyarilar
  rules:
    - alert: KaynakKontrol
      expr: up == 0
      for: 1m
      labels:
        severity: critical
      annotations:
        summary: "[{{ $labels.instance }}] ulaşılamıyor. "
        description: "[{{ $labels.instance }}] kaynağına 1 dakikadır ulaşılamıyor."
EOF

service node-exporter stop
service node-exporter start

```

```
curl -L -O https://github.com/prometheus/alertmanager/releases/download/v0.22.2/alertmanager-0.22.2.linux-amd64.tar.gz
tar -xvzf alertmanager-0.22.2.linux-amd64.tar.gz
cd alertmanager-0.22.2.linux-amd64


cp -r . /usr/local/bin/alertmanager
cat << EOF > /etc/systemd/system/alertmanager.service
[Unit]
Description=Prometheus Alert Manager
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/alertmanager/alertmanager --config.file=/usr/local/bin/alertmanager/alertmanager.yml

[Install]
WantedBy=multi-user.target
EOF

```

## send mail

```
receivers:
- name: 'email'
  email_configs:
  - send_resolved: true
    to: esckins@mail.com
    from: admin@admin.com
    smarthost: smtp.gmail.com:587
    auth_username: "admin@admin.com"
    auth_identity: "admin@admin.com"
    auth_password: "****"

## webapi
receivers:
- name: 'web'
  webhook_configs:
  - url: 'http://esckins.com:5001/'

## checking

./amtool check-config alertmanager.yml
```

```
# ./amtool check-config alertmanager.yml
Checking 'alertmanager.yml'  SUCCESS
Found:
 - global config
 - route
 - 1 inhibit rules
 - 1 receivers
 - 0 templates


service alertmanager start

service alertmanager status
Process: 17112 ExecStart=/usr/local/bin/alertmanager/alertmanager --config.file=/usr/local/bin/alertmanager/alertmanager.yml (code=exited, status=203/EXEC)

vi /etc/systemd/system/alertmanager.service


[Unit]
Description=AlertManager Server Service
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=root
Group=root
ExecStart=/etc/prometheus/exporter/alertmanager/alertmanager \
 --config.file="/etc/prometheus/exporter/alertmanager/alertmanager.yml" \
 --storage.path="/etc/prometheus/exporter/alertmanager/data/" \
 --web.listen-address=0.0.0.0:9093 \
 --web.external-url="http://localhost:9093/alertmanager" \
 --log.level=debug \

Restart=always

[Install]
WantedBy=multi-user.target

####

[Service]
Type=simple
ExecStart=/usr/local/bin/alertmanager/alertmanager-0.24.0.linux-amd64/alertmanager --config.file=/usr/local/bin/alertmanager/alertmanager-0.24.0.linux-amd64/alertmanager.yml



systemctl daemon-reload

systemctl status alertmanager.service
● alertmanager.service - Prometheus Alert Manager
   Loaded: loaded (/etc/systemd/system/alertmanager.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2022-04-04 14:03:28 +03; 3s ago
 Main PID: 20398 (alertmanager)

esckinsx01:9093
```

-- entegration with alertmanager and prometheus

```
vi prometheus.yml
alerting:
  alertmanagers:
  - static_configs:
    - targets:
       - alertmanager:9093


# service node-exporter restart
Redirecting to /bin/systemctl restart node-exporter.service
# service prometheus restart
Redirecting to /bin/systemctl restart prometheus.service


you can connection grafana.

https://grafana.com/grafana/dashboards/10242
download json --> import dash. after prometheus connection save&tets

## thx a lot https://www.yusufsezer.com.tr/prometheus/
```







