#!/bin/bash

set -e
set -o pipefail

prometheus_format=${DEVICEMAPPER_PROMETHEUS:-true} # influxdb format otherwise

function format_labels {
    first=true
    echo "$*" | tr , "\\n" | while read -r l; do
        label_name=${l%%=*}
        label_value=${l##*=}
        label_name=${label_name,,} # make label names lower case
        label_value=${label_value//\"} # remove quotes "
        if [[ -z "$label_value" ]]; then # empty labels
            label_value="-"
        elif [[ "$label_value" =~ " " ]]; then # labels with spaces
            label_value="${label_value// /\ }" # escape spaces
        fi
        if [[ "$first" == true ]]; then
            first=false
        else
	    # add comma before all labels except first
            output+=,
        fi
        if [[ "$prometheus_format" == "true" ]]; then
            label_value="\"$label_value\""
        fi
        output+="$label_name=$label_value"
        echo "$output"
    done
}

lsblk -nbpP -o SIZE,NAME,KNAME,PKNAME,PARTLABEL,STATE,TYPE,FSTYPE,MOUNTPOINT | sort -u | sed -r 's|" ([^=]+=")|",\L\1|g' | while IFS=, read -r size raw_labels; do
    size=${size/SIZE=}
    size=${size//\"} # remove quotes "
    labels=$(format_labels "$raw_labels" | tail -1)
    if [[ "$prometheus_format" == "true" ]]; then
        echo "devicemapper_size{$labels} $size"
    else
        echo "devicemapper,$labels size=$size"
    fi
done




##script to scrape metrics from devicemapper, useful to use as metadata source


# devicemapper.sh | grep -v -F /dev/loop
devicemapper_size{name="/dev/mapper/kubuntu--vg-swap_1",kname="/dev/dm-2",pkname="/dev/dm-0",partlabel="-",state="running",type="lvm",fstype="swap",mountpoint="-"} 1027604480
devicemapper_size{name="/dev/mapper/kubuntu--vg-root",kname="/dev/dm-1",pkname="/dev/dm-0",partlabel="-",state="running",type="lvm",fstype="ext4",mountpoint="/"} 509771513856
devicemapper_size{name="/dev/mapper/sda3_crypt",kname="/dev/dm-0",pkname="/dev/sda3",partlabel="-",state="running",type="crypt",fstype="LVM2_member",mountpoint="-"} 510802264064
devicemapper_size{name="/dev/sda3",kname="/dev/sda3",pkname="/dev/sda",partlabel="-",state="-",type="part",fstype="crypto_LUKS",mountpoint="-"} 510804361216
devicemapper_size{name="/dev/sda",kname="/dev/sda",pkname="-",partlabel="-",state="running",type="disk",fstype="-",mountpoint="-"} 512110190592
devicemapper_size{name="/dev/sda1",kname="/dev/sda1",pkname="/dev/sda",partlabel="EFI System Partition",state="-",type="part",fstype="vfat",mountpoint="/boot/efi"} 536870912
devicemapper_size{name="/dev/sda2",kname="/dev/sda2",pkname="/dev/sda",partlabel="-",state="-",type="part",fstype="ext4",mountpoint="/boot"} 767557632


