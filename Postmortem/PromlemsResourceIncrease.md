[stackTolerate](https://stackoverflow.com/questions/59765994/0-3-nodes-are-available-1-nodes-had-taints-that-the-pod-didnt-tolerate-2-in)

kubectl taint nodes --all node-role.kubernetes.io/master-

###############################################################################

Problem:
0/10 nodes are available: 3 node(s) had taint {node-role.kubernetes.io/master: }, that the pod didn't tolerate, 7 Insufficient cpu.


kubectl get limitrange -o=yaml

kubectl get po --namespace devops

kubectl describe po - for one of the existing pods, to check which node it's running on

kubectl get nodes

kubectl describe node - to see the CPU usage for the node being used by the existing pod, as below:

Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource                       Requests      Limits
  --------                       --------      ------
  cpu                            1010m (93%)   4 (210%)


kubectl get deployments --namespace devops

kubectl delete deployment ....


kubectl describe pod frontend | grep -A 9999999999 Events


***

kubectl describe nodes devopsworker11


```
Name:            e2e-test-node-pool-4lw4
[ ... lines removed for clarity ...]
Capacity:
 cpu:                               2
 memory:                            7679792Ki
 pods:                              110
Allocatable:
 cpu:                               1800m
 memory:                            7474992Ki
 pods:                              110
[ ... lines removed for clarity ...]
Non-terminated Pods:        (5 in total)
  Namespace    Name                                  CPU Requests  CPU Limits  Memory Requests  Memory Limits
  ---------    ----                                  ------------  ----------  ---------------  -------------
  kube-system  fluentd-gcp-v1.38-28bv1               100m (5%)     0 (0%)      200Mi (2%)       200Mi (2%)
  kube-system  kube-dns-3297075139-61lj3             260m (13%)    0 (0%)      100Mi (1%)       170Mi (2%)
  kube-system  kube-proxy-e2e-test-...               100m (5%)     0 (0%)      0 (0%)           0 (0%)
  kube-system  monitoring-influxdb-grafana-v4-z1m12  200m (10%)    200m (10%)  600Mi (8%)       600Mi (8%)
  kube-system  node-problem-detector-v0.1-fj7m3      20m (1%)      200m (10%)  20Mi (0%)        100Mi (1%)
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  CPU Requests    CPU Limits    Memory Requests    Memory Limits
  ------------    ----------    ---------------    -------------
  680m (34%)      400m (20%)    920Mi (11%)        1070Mi (13%)
```



Name:               devopsworker11
Roles:              <none>
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=devopsworker11
                    kubernetes.io/os=linux
Annotations:        kubeadm.alpha.kubernetes.io/cri-socket
CreationTimestamp:  Thu, 25 Jun 2020 17:49:28 +0300
Taints:             <none>
Unschedulable:      false
Lease:
  HolderIdentity:  devopsworker11
  AcquireTime:     <unset>
  RenewTime:       Mon, 28 Feb 2022 00:07:21 +0300
Conditions:
  Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
  ----                 ------  -----------------                 ------------------                ------                       -------
  NetworkUnavailable   False   Wed, 06 Oct 2021 15:10:56 +0300   Wed, 06 Oct 2021 15:10:56 +0300   CalicoIsUp                   Calico is running on this node
  MemoryPressure       False   Mon, 28 Feb 2022 00:03:11 +0300   Wed, 06 Oct 2021 15:10:20 +0300   KubeletHasSufficientMemory   kubelet has sufficient memory available
  DiskPressure         False   Mon, 28 Feb 2022 00:03:11 +0300   Tue, 22 Feb 2022 09:46:22 +0300   KubeletHasNoDiskPressure     kubelet has no disk pressure
  PIDPressure          False   Mon, 28 Feb 2022 00:03:11 +0300   Wed, 06 Oct 2021 15:10:20 +0300   KubeletHasSufficientPID      kubelet has sufficient PID available
  Ready                True    Mon, 28 Feb 2022 00:03:11 +0300   Wed, 06 Oct 2021 15:10:30 +0300   KubeletReady                 kubelet is posting ready status
Addresses:
  InternalIP:  #.#.#.#
  Hostname:    devopsworker11
Capacity:
  cpu:                40
  ephemeral-storage:  102390Mi
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             395993416Ki
  pods:               110
Allocatable:
  cpu:                40
  ephemeral-storage:  96627326817
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             395891016Ki
  pods:               110
System Info:
  Machine ID:                 #
  System UUID:                #
  Boot ID:                    #
  Kernel Version:             #.x86_64
  OS Image:                   # 1.7
  Operating System:           linux
  Architecture:               amd64
  Container Runtime Version:  docker://#
  Kubelet Version:            v#
  Kube-Proxy Version:         v#
Non-terminated Pods:          (20 in total)
  Namespace                   Name                                       CPU Requests  CPU Limits  Memory Requests  Memory Limits  AGE
  ---------                   ----                                       ------------  ----------  ---------------  -------------  ---
  kube-system                 calico-node-mx94c                          250m (0%)     0 (0%)      0 (0%)           0 (0%)         612d
  kube-system                 kube-proxy-92ft8                           0 (0%)        0 (0%)      0 (0%)           0 (0%)         612d
  devops                      jdbc-anfaxorclx01-0                        0 (0%)        0 (0%)      2Gi (0%)         2Gi (0%)       55d
  devops                      jdbc-bkysgway-0                            0 (0%)        0 (0%)      0 (0%)           0 (0%)         139d
  devops                      jdbc-gwayexception-0            0 (0%)        0 (0%)      0 (0%)           0 (0%)         57d
  devops                      jdbc-oshar-0                  0 (0%)        0 (0%)      0 (0%)           0 (0%)         52d
  devops                      kafka-iis-1-8569fdf84c-7nlvf               3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       48d
  devops                      kafka-iis-1-8569fdf84c-nrsnd               3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       41d
  devops                      kafka-iis-2-c9c86899f-2s5ff                3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       6d13h
  devops                      kafka-iis-2-c9c86899f-tkqn6                3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       5d13h
  devops                      kafka-iis-3-7dcd88c44c-fdt8v               3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       11d
  devops                      kafka-oraclemetricbeat-58df7b48d8-swws2    2 (5%)        2 (5%)      4Gi (1%)         4Gi (1%)       144d
  devops                      kafka-trace-1-5564649565-qpzgl             3 (7%)        3 (7%)      4Gi (1%)         4Gi (1%)       6d15h
  devops                      kafka-trace-1-5564649565-v6dbx             3 (7%)        3 (7%)      4Gi (1%)         4Gi (1%)       42d
  devops                      kafka-trace-2-765bfbd7b5-2dlcf             3 (7%)        3 (7%)      4Gi (1%)         4Gi (1%)       5d13h
  devops                      kafka-trace-2-765bfbd7b5-hcw2p             3 (7%)        3 (7%)      4Gi (1%)         4Gi (1%)       5d13h
  devops                      kafka-trace-3-85cd54949c-m4jjv             3 (7%)        3 (7%)      4Gi (1%)         4Gi (1%)       5d12h
  devops                      kafka-trace-4-79584c99bf-8tc49             3 (7%)        3 (7%)      4Gi (1%)         4Gi (1%)       41m
  devops                      kafka-trace-4-79584c99bf-dzmk5             3 (7%)        3 (7%)      4Gi (1%)         4Gi (1%)       41m
  devops                      ytu-deployment-5844f96d8d-j89p2            0 (0%)        0 (0%)      0 (0%)           0 (0%)         57d
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource           Requests      Limits
  --------           --------      ------
  cpu                38250m (95%)  38 (95%)
  memory             64Gi (16%)    64Gi (16%)
  ephemeral-storage  0 (0%)        0 (0%)
  hugepages-1Gi      0 (0%)        0 (0%)
  hugepages-2Mi      0 (0%)        0 (0%)
Events:              <none>

kubectl describe pod simmemleak-hra99

kubectl describe pod kafka-trace-4-79584c99bf-dzmk5 --namespace devops

```
Name:                           simmemleak-hra99
Namespace:                      default
Image(s):                       saadali/simmemleak
Node:                           kubernetes-node-tf0f/10.30.30.10
Labels:                         name=simmemleak
Status:                         Running
Reason:
Message:
IP:                             10.66.66.10
Containers:
  simmemleak:
    Image:  saadali/simmemleak:latest
    Limits:
      cpu:          100m
      memory:       50Mi
    State:          Running
      Started:      Tue, 07 Jul 2019 12:54:41 -0700
    Last State:     Terminated
      Reason:       OOMKilled
      Exit Code:    137
      Started:      Fri, 07 Jul 2019 12:54:30 -0700
      Finished:     Fri, 07 Jul 2019 12:54:33 -0700
    Ready:          False
    Restart Count:  5
Conditions:
  Type      Status
  Ready     False
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  42s   default-scheduler  Successfully assigned simmemleak-hra99 to kubernetes-node-tf0f
  Normal  Pulled     41s   kubelet            Container image "saadali/simmemleak:latest" already present on machine
  Normal  Created    41s   kubelet            Created container simmemleak
  Normal  Started    40s   kubelet            Started container simmemleak
  Normal  Killing    32s   kubelet            Killing container with id ead3fb35-5cf5-44ed-9ae1-488115be66c6: Need to kill Pod
```



https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/



Solved:

resource increase for worker nodes




```
You can run below command to remove the taint from master node and then you should be able to deploy your pod on that node

kubectl taint nodes  mildevkub020 node-role.kubernetes.io/master-
kubectl taint nodes  mildevkub040 node-role.kubernetes.io/master-



```

```
for node in $(kubectl get nodes --selector='node-role.kubernetes.io/master' | awk 'NR>1 {print $1}' ) ; do   kubectl taint node $node node-role.kubernetes.io/master- ; done



```
```

Remove the taints on the master so that you can schedule pods on it.

kubectl taint nodes --all node-role.kubernetes.io/master-
It should return the following.

node/<your-hostname> untainted
```




################################################################################
