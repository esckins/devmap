

Kubernetes-Dashboard Error: ImagePullBackOff

$ docker image ls -a
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get "http://%2Fvar%2Frun%2Fdocker.sock/v1.40/images/json?all=1": dial unix /var/run/docker.sock: connect: permission denied

---
If imagePullPolicy is set to Always, Kubernetes will always pull the image from the Repository. 
With IfNotPresent, Kubernetes will only pull the image when it does not already exist on the node. 
While with imagePullPolicy set to Never, Kubernetes will never pull the image. 
---

kubectl delete pods kubernetes-dashboard-64686c4bf9-4f724  dashboard-metrics-scraper-779f5454cb-gwskv -n kubernetes-dashboard

kubectl get pods -n kubernetes-dashboard

kubectl get services -n kubernetes-dashboard


kubectl logs --tail 500 kubernetes-dashboard -n kubernetes-dashboard
kubectl logs --tail 500 kubernetes-dashboard-64686c4bf9-82cqr -n kubernetes-dashboard



kubectl edit service kubernetes-dashboard -n kubernetes-dashboard

############################################################################
kubectl edit deployment kubernetes-dashboard -n kubernetes-dashboard
############################################################################

kubectl logs --tail 500 kubernetes-dashboard-64686c4bf9-82cqr -n kubernetes-dashboard
kubectl get pods -n kubernetes-dashboard



uname -a
kubectl version
kubectl cluster-info
kubectl get nodes -o wide
docker image ls -a

############################################################################
imagePullPolicy change always -> IfNotPresent
############################################################################

$ kubectl get pods -n kubernetes-dashboard
NAME                                         READY   STATUS    RESTARTS   AGE
dashboard-metrics-scraper-779f5454cb-hgc9z   0/1     Evicted   0          184d
dashboard-metrics-scraper-779f5454cb-mwt86   1/1     Running   0          19m
dashboard-metrics-scraper-779f5454cb-q587n   0/1     Evicted   0          425d
kubernetes-dashboard-c87459bdf-4gbv8         1/1     Running   0          23s
