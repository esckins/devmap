```
~]$ kubectl describe job deployment-restart-1657079700 --namespace devops
Name:                     deployment-restart-1657079700
Namespace:                devops
Selector:                 controller-uid=da2212434-8238-4a2d-b7a3-9581235474
Labels:                   controller-uid=da223424-8218-4a2d-b7a3-958asd1275474
                          job-name=deployment-restart-1657079700
Annotations:              <none>
Controlled By:            CronJob/deployment-restart
Parallelism:              1
Completions:              1
Start Time:               Wed, 06 Jul 2022 07:01:08 +0300
Active Deadline Seconds:  600s
Pods Statuses:            0 Running / 0 Succeeded / 1 Failed
Pod Template:
  Labels:           controller-uid=da212334-8238-4e2d-b7a3-9123124asd5474
                    job-name=deployment-restart-1657079700
  Service Account:  deployment-restart
  Containers:
   kubectl:
    Image:      harbor.esckin.intra/uty/bitnami/kubectl:2022
    Port:       <none>
    Host Port:  <none>
    Command:
      kubectl
      rollout
      restart
      StatefulSet/jdbc-bireyselesckin
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:           <none>
```



`Note: If your job has restartPolicy = "OnFailure", keep in mind that your container running the Job will be terminated once the job backoff limit has been reached. This can make debugging the Job’s executable more difficult. We suggest setting restartPolicy = "Never" when debugging the Job or using a logging system to ensure output from failed Jobs is not lost inadvertently.`


--- failed image,

https://www.datree.io/resources/kubernetes-error-codes-failed-to-pull-image

statefulset.apps/jdbc-bireyselatmvakif restarted

## add trigger
