```
$ kubectl get nodes -o wide
NAME           STATUS     ROLES    AGE    VERSION   INTERNAL-IP    EXTERNAL-IP   OS-IMAGE                  KERNEL-VERSION                     CONTAINER-RUNTIME
kubex10   Ready      master   2y9d   v1.18.3   10.30.30.10    <none>        Oracle Linux Server 7.9   4.14.35-2047.513.2.el7uek.x86_64   docker://19.3.11
kubex11   Ready      master   2y8d   v1.18.3   10.30.30.11    <none>        Oracle Linux Server 7.9   4.14.35-2047.513.2.el7uek.x86_64   docker://19.3.11
kubex12   Ready      master   2y8d   v1.18.3   10.30.30.12    <none>        Oracle Linux Server 7.9   4.14.35-2047.513.2.el7uek.x86_64   docker://19.3.11
kubex10   Ready      <none>   2y8d   v1.18.3   10.30.30.13   <none>        Oracle Linux Server 7.7   4.14.35-1902.3.2.el7uek.x86_64     docker://19.3.1
kubex11   Ready      <none>   2y8d   v1.18.3   10.30.30.14   <none>        Oracle Linux Server 7.7   4.14.35-1902.3.2.el7uek.x86_64     docker://19.3.1
kubex12   Ready      <none>   2y8d   v1.18.3   10.30.30.15   <none>        Oracle Linux Server 7.7   4.14.35-1902.3.2.el7uek.x86_64     docker://19.3.1
kubex13   Ready      <none>   2y8d   v1.18.3   10.30.30.16   <none>        Oracle Linux Server 7.7   4.14.35-1902.3.2.el7uek.x86_64     docker://19.3.1
kubexx14   Ready      <none>   2y8d   v1.18.3   10.30.30.17   <none>        Oracle Linux Server 7.7   4.14.35-1902.3.2.el7uek.x86_64     docker://19.3.1
kubex15   Ready      <none>   2y8d   v1.18.3   10.30.30.18   <none>        Oracle Linux Server 7.7   4.14.35-1902.3.2.el7uek.x86_64     docker://19.3.1
kubex17   NotReady   <none>   2y8d   v1.18.3   10.30.30.19   <none>        Oracle Linux Server 7.7   4.14.35-1902.3.2.el7uek.x86_64     docker://19.3.1
```

```
$ journalctl -u kubelet
Hint: You are currently not seeing messages from other users and the system.
      Users in the 'systemd-journal' group can see all messages. Pass -q to
      turn off this notice.
No journal files were opened due to insufficient permissions.
```


```
$ kubectl describe node kubex17
Name:               kubex17
Roles:              <none>
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=kubex17
                    kubernetes.io/os=linux
Annotations:        kubeadm.alpha.kubernetes.io/cri-socket: /var/run/dockershim.sock
                    node.alpha.kubernetes.io/ttl: 0
                    projectcalico.org/IPv4Address: 10.30.30.19/24
                    projectcalico.org/IPv4IPIPTunnelAddr: 10.99.99.0
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Thu, 25 Jun 2020 18:05:00 +0300
Taints:             node.kubernetes.io/unreachable:NoExecute
                    node.kubernetes.io/unreachable:NoSchedule
Unschedulable:      false
Lease:
  HolderIdentity:  isdevopskbwlx17
  AcquireTime:     <unset>
  RenewTime:       Thu, 30 Jun 2022 10:53:19 +0300
Conditions:
  Type                 Status    LastHeartbeatTime                 LastTransitionTime                Reason              Message
  ----                 ------    -----------------                 ------------------                ------              -------
  NetworkUnavailable   False     Tue, 21 Jun 2022 10:13:08 +0300   Tue, 21 Jun 2022 10:13:08 +0300   CalicoIsUp          Calico is running on this node
  MemoryPressure       Unknown   Thu, 30 Jun 2022 10:52:11 +0300   Thu, 30 Jun 2022 11:16:18 +0300   NodeStatusUnknown   Kubelet stopped posting node status.
  DiskPressure         Unknown   Thu, 30 Jun 2022 10:52:11 +0300   Thu, 30 Jun 2022 11:16:18 +0300   NodeStatusUnknown   Kubelet stopped posting node status.
  PIDPressure          Unknown   Thu, 30 Jun 2022 10:52:11 +0300   Thu, 30 Jun 2022 11:16:18 +0300   NodeStatusUnknown   Kubelet stopped posting node status.
  Ready                Unknown   Thu, 30 Jun 2022 10:52:11 +0300   Thu, 30 Jun 2022 11:16:18 +0300   NodeStatusUnknown   Kubelet stopped posting node status.
Addresses:
  InternalIP:  10.30.30.19
  Hostname:    kubex17
Capacity:
  cpu:                40
  ephemeral-storage:  102390Mi
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             395993416Ki
  pods:               110
Allocatable:
  cpu:                40
  ephemeral-storage:  96627326817
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             395891016Ki
  pods:               110
System Info:
  Machine ID:                 c06aa56fsd63asdsadd00103de2
  System UUID:                31233237-31235-5x43-3114-317735992a12E
  Boot ID:                    b123fd0-ffd0-4esds-812317-91234d46fcd604
  Kernel Version:             4.14.35-1902.3.2.el7uek.x86_64
  OS Image:                   Oracle Linux Server 7.7
  Operating System:           linux
  Architecture:               amd64
  Container Runtime Version:  docker://19.3.1
  Kubelet Version:            v1.18.3
  Kube-Proxy Version:         v1.18.3
Non-terminated Pods:          (17 in total)
  Namespace                   Name                                CPU Requests  CPU Limits  Memory Requests  Memory Limits  AGE
  ---------                   ----                                ------------  ----------  ---------------  -------------  ---
  kube-system                 calico-node-8xlzv                   250m (0%)     0 (0%)      0 (0%)           0 (0%)         2y8d
  kube-system                 kube-proxy-kpq6x                    0 (0%)        0 (0%)      0 (0%)           0 (0%)         2y8d
  devops                         jdbc-itsm-0                         0 (0%)        0 (0%)      0 (0%)           0 (0%)         4d23h
  devops                         kafka-exception-6888cf5889-tw888    2 (5%)        2 (5%)      4Gi (1%)         4Gi (1%)       4d18h
  devops                         kafka-fulltext-ffc799b87-vtb27      2 (5%)        2 (5%)      4Gi (1%)         4Gi (1%)       9d
  devops                         kafka-iis-1-8569fdf84c-gs7sp        3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       12d
  devops                         kafka-iis-2-c9c86899f-65z6s         3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       12d
  devops                         kafka-iis-2-c9c86899f-jgh96         3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       12d
  devops                         kafka-iis-2-c9c86899f-xwpct         3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       12d
  devops                         kafka-iis-2-c9c86899f-zvlr6         3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       12d
  devops                         kafka-iis-3-7dcd88c44c-n2t7h        3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       12d
  devops                         kafka-iis-5-796d66bd67-gp7bz        3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       3d23h
  devops                         kafka-iis-5-796d66bd67-hfgcz        3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       5d19h
  devops                         kafka-iis-5-796d66bd67-jxzpf        3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       5d19h
  devops                         kafka-iis-5-796d66bd67-lmsl2        3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       5d19h
  devops                         kafka-iis-5-796d66bd67-ppmzr        3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       5d19h
  devops                         kafka-openshift-7fbb7c6747-s56w9    2 (5%)        2 (5%)      4Gi (1%)         4Gi (1%)       10d
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource           Requests      Limits
  --------           --------      ------
  cpu                39250m (98%)  39 (97%)
  memory             78Gi (20%)    78Gi (20%)
  ephemeral-storage  0 (0%)        0 (0%)
  hugepages-1Gi      0 (0%)        0 (0%)
  hugepages-2Mi      0 (0%)        0 (0%)
Events:              <none>
```

```
$ kubectl get pods -n kube-system


  355  du -sh *
  356  cd /opt/
  357  du -sh *
  358  df -h
  359  docker images
  360  docker ps
  361  ls -la
  362  cd cni/
  363  ls -la
  364  systemctl restart docker
  365  df -h
  366  systemctl stop kubelet
  367  systemctl start kubelet
  368  systemctl status kubelet
  369  logout
  370  reboot
  371  systemctl status docker
  372  clear
  373  systemctl status kubernetes
  374  clear
  375  systemctl status kubelet
  376  systemctl start kubelet
  377  systemctl status kubelet
  378  systemctl status chronyd
  379  systemctl start chronyd
  380  systemctl status kubelet
  381  clear
  382  systemctl status nfs-lock
  383  systemctl start nfs-lock
  384  journalctl -xe
  385  systemctl status rpcbind.service
  386  systemctl start rpcbind.service
  387  systemctl enable rpcbind.service
  388  systemctl start rpcbind.service
  389  systemctl status rpcbind.service
  390  systemctl enable nfs-lock
  391  systemctl start nfs-lock
  392  systemctl status nfs-lock
  393  systemctl status nfs-idmap
  394  systemctl start nfs-idmap
  395  systemctl status nfs-idmap
  396  systemctl status centrify-sshd
  397  service sshd status
  398  systemctl status chronyd
  399  systemctl status chronyd -l

```



####


```
$ kubectl describe node kubex15
Name:               kubex15
Roles:              <none>
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=isdevopskbwlx15
                    kubernetes.io/os=linux
Annotations:        kubeadm.alpha.kubernetes.io/cri-socket: /var/run/dockershim.sock
                    node.alpha.kubernetes.io/ttl: 0
                    projectcalico.org/IPv4Address: 10.30.69.185/24
                    projectcalico.org/IPv4IPIPTunnelAddr: 10.96.206.192
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Thu, 25 Jun 2020 18:04:09 +0300
Taints:             <none>
Unschedulable:      false
Lease:
  HolderIdentity:  kubex15
  AcquireTime:     <unset>
  RenewTime:       Mon, 04 Jul 2022 10:59:14 +0300
Conditions:
  Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
  ----                 ------  -----------------                 ------------------                ------                       -------
  NetworkUnavailable   False   Wed, 06 Oct 2021 14:52:07 +0300   Wed, 06 Oct 2021 14:52:07 +0300   CalicoIsUp                   Calico is running on this node
  MemoryPressure       False   Mon, 04 Jul 2022 10:56:22 +0300   Wed, 06 Oct 2021 14:51:33 +0300   KubeletHasSufficientMemory   kubelet has sufficient memory available
  DiskPressure         False   Mon, 04 Jul 2022 10:56:22 +0300   Tue, 17 May 2022 21:55:57 +0300   KubeletHasNoDiskPressure     kubelet has no disk pressure
  PIDPressure          False   Mon, 04 Jul 2022 10:56:22 +0300   Wed, 06 Oct 2021 14:51:33 +0300   KubeletHasSufficientPID      kubelet has sufficient PID available
  Ready                True    Mon, 04 Jul 2022 10:56:22 +0300   Thu, 30 Jun 2022 10:47:08 +0300   KubeletReady                 kubelet is posting ready status
Addresses:
  InternalIP:  10.30.30.15
  Hostname:    kubexx15
Capacity:
  cpu:                40
  ephemeral-storage:  102390Mi
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             395993416Ki
  pods:               110
Allocatable:
  cpu:                40
  ephemeral-storage:  96627326817
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             395891016Ki
  pods:               110
System Info:
  Machine ID:                 c06as56fsd63asdsadd00103de0
  System UUID:                31313137-3135-5A43-3334-313932421224A
  Boot ID:                    07554ae7-597d-46d7-afa1-6160842231a2
  Kernel Version:             4.14.35-1902.3.2.el7uek.x86_64
  OS Image:                   Oracle Linux Server 7.7
  Operating System:           linux
  Architecture:               amd64
  Container Runtime Version:  docker://19.3.1
  Kubelet Version:            v1.18.3
  Kube-Proxy Version:         v1.18.3
Non-terminated Pods:          (31 in total)
  Namespace                   Name                                       CPU Requests  CPU Limits  Memory Requests  Memory Limits  AGE
  ---------                   ----                                       ------------  ----------  ---------------  -------------  ---
  kube-system                 calico-node-kpjl9                          250m (0%)     0 (0%)      0 (0%)           0 (0%)         2y8d
  kube-system                 kube-proxy-qmwdr                           0 (0%)        0 (0%)      0 (0%)           0 (0%)         2y8d
  devops                         exalidraw-6d59f59db9-dmgxg                 0 (0%)        0 (0%)      0 (0%)           0 (0%)         81d
  devops                         heartbeat-5fb775667f-n4wfz                 0 (0%)        0 (0%)      0 (0%)           0 (0%)         10d
  devops                         hsm-deployment-9966484b4-x4zgf             2 (5%)        2 (5%)      4Gi (1%)         4Gi (1%)       160d
  devops                         jdbc-donanimmetrik-0                       0 (0%)        0 (0%)      0 (0%)           0 (0%)         270d
  devops                         jdbc-donanimmetriktamadevopsma-0              0 (0%)        0 (0%)      0 (0%)           0 (0%)         270d
  devops                         jdbc-fraud-0                               0 (0%)        0 (0%)      0 (0%)           0 (0%)         89d
  devops                         jdbc-ips-0                                 0 (0%)        0 (0%)      4Gi (1%)         4Gi (1%)       17d
  devops                         jdbc-kredivakif-swjournal1-0               0 (0%)        0 (0%)      4Gi (1%)         4Gi (1%)       171d
  devops                         jdbc-kredivakif-swjournal2-0               0 (0%)        0 (0%)      4Gi (1%)         4Gi (1%)       168d
  devops                         jdbc-mobil-0                               0 (0%)        0 (0%)      0 (0%)           0 (0%)         46d
  devops                         jdbc-oracle-stats-2-0                      0 (0%)        0 (0%)      2Gi (0%)         2Gi (0%)       143d
  devops                         kafka-fulltext-ffc799b87-ztcrf             2 (5%)        2 (5%)      4Gi (1%)         4Gi (1%)       16d
  devops                         kafka-iis-1-8569fdf84c-g4p2b               3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       3d23h
  devops                         kafka-iis-3-7dcd88c44c-jszhp               3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       12d
  devops                         kafka-iis-4-58dc75d475-nggvv               3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       12d
  devops                         kafka-iis-5-796d66bd67-wvxjs               3 (7%)        3 (7%)      6Gi (1%)         6Gi (1%)       5d19h
  devops                         kafka-openshift-7fbb7c6747-ch2dg           2 (5%)        2 (5%)      4Gi (1%)         4Gi (1%)       10d
  devops                         kafka-oraclemetricbeat-58df7b48d8-w27fx    2 (5%)        2 (5%)      4Gi (1%)         4Gi (1%)       270d
  devops                         kafka-osmetric-5bccc7d494-fgx49            2 (5%)        2 (5%)      4Gi (1%)         4Gi (1%)       111d
  devops                         kafka-osmetric-5bccc7d494-gskdz            2 (5%)        2 (5%)      4Gi (1%)         4Gi (1%)       111d
  devops                         kafka-osmetric-5bccc7d494-zdq7k            2 (5%)        2 (5%)      4Gi (1%)         4Gi (1%)       111d
  devops                         kafka-swift-54fd6ddb47-wxdzd               2 (5%)        2 (5%)      4Gi (1%)         4Gi (1%)       167d
  devops                         kafka-trace-1-7b4b6cd4fb-mdscw             3 (7%)        3 (7%)      4Gi (1%)         4Gi (1%)       48d
  devops                         kafka-trace-2-6d55b676dd-m62rq             3 (7%)        3 (7%)      4Gi (1%)         4Gi (1%)       27d
  devops                         kafka-trace-4-6d7fcc9dc6-h5xz7             3 (7%)        3 (7%)      4Gi (1%)         4Gi (1%)       27d
  devops                         kafka-unicahttplog-5c49567459-8tq5h        0 (0%)        0 (0%)      0 (0%)           0 (0%)         270d
  devops                         kibanatest-859975cd8b-9l2pv                0 (0%)        0 (0%)      0 (0%)           0 (0%)         152d
  devops                         uygulama-error-7b47b96c46-6znmx            1 (2%)        1 (2%)      2Gi (0%)         2Gi (0%)       156d
  devops                         ytu-deployment-5844f96d8d-9knhp            0 (0%)        0 (0%)      0 (0%)           0 (0%)         24d
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource           Requests      Limits
  --------           --------      ------
  cpu                38250m (95%)  38 (95%)
  memory             84Gi (22%)    84Gi (22%)
  ephemeral-storage  0 (0%)        0 (0%)
  hugepages-1Gi      0 (0%)        0 (0%)
  hugepages-2Mi      0 (0%)        0 (0%)
Events:              <none>
```

```
kubectl get pod -n devops | grep Evicted
kubectl get pod -n devops | grep Evicted | awk '{print $1}' | xargs kubectl delete pod -n devops

```

