for n in $(kubectl get -o=name pvc,configmap,serviceaccount,secret,ingress,service,deployment,statefulset,hpa,job,cronjob)
do
    mkdir -p $(dirname $n)
    kubectl get -o=yaml $n > $n.yaml
done

. generate-yaml.sh


####

kubectl get all

kubectl get service hellworldexample-helloworld -n default -o yaml > service.yaml 

kubectl get service --all-namespaces -o yaml  > all-service.yaml


kubectl get deployment myreleasename-helloworld -n default -o yaml > deployment.yaml

kubectl get deploy --all-namespaces -o yaml  > all-deployment.yaml


R's : https://jhooq.com/get-yaml-for-deployed-kubernetes-resources/
