**What is Kubeflow?**

The Kubeflow project is dedicated to making Machine Learning easy to set up with Kubernetes, portable and scalable. The goal is not to recreate other services, but to provide a straightforward way for spinning up best of breed OSS solutions. Kubernetes is an open-source platform for automating deployment, scaling, and management of containerised applications.

Because Kubeflow relies on Kubernetes, it runs wherever Kubernetes runs such as bare-metal servers, or cloud providers such as Google. Details of the project can be found at https://github.com/kubeflow/kubeflow

**Kubeflow Components**

Kubeflow has three core components.

_TF Job Operator and Controller_: Extension to Kubernetes to simplify deployment of distributed TensorFlow workloads. By using an Operator, Kubeflow is capable of automatically configuring the master, worker and parameterized server configuration. Workloads can be deployed with a TFJob.

_TF Hub_: Running instances of JupyterHub, enabling you to work with Jupyter Notebooks.

_Model Server_: Deploying a trained TensorFlow models for clients to access and use for future predictions.

These three models will be used to deploy different workloads in the following steps.


**Deploying Kubeflow**

With Kubeflow being an extension to Kubernetes, all the components need to be deployed to the platform.

The team have provided an installation script which uses Ksonnet to deploy Kubeflow to an existing Kubernetes cluster. Ksonnet requires a valid Github token. The following can be used within Katacoda. Run the command to set the required environment variable.

`master $ export GITHUB_TOKEN=99510f2ccf40e496d1e97dbec9f31cb16770b884`


Once installed, you can run the installation script:


```
master $ export KUBEFLOW_VERSION=0.2.5
master $ curl https://raw.githubusercontent.com/kubeflow/kubeflow/v${KUBEFLOW_VERSION}/scripts/deploy.sh | bash
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1957  100  1957    0     0  21023      0 --:--:-- --:--:-- --:--:-- 21271
```

```
master $ kubectl get pods
NAME                                                           READY   STATUS             RESTARTS   AGE
admission-webhook-bootstrap-stateful-set-0                     1/1     Running            0          5m38s
admission-webhook-deployment-64cb96ddbf-s4fcs                  1/1     Running            0          4m56s
application-controller-stateful-set-0                          1/1     Running            0          5m41s
argo-ui-778676df64-kl7bg                                       1/1     Running            0          5m38s
centraldashboard-cc58bf567-srmwf                               1/1     Running            0          5m8s
jupyter-web-app-deployment-89789fd5-qpjbl                      1/1     Running            0          5m36s
katib-controller-6b789b6cb5-62qjt                              1/1     Running            1          5m30s
katib-db-manager-64f548b47c-bn55z                              0/1     CrashLoopBackOff   3          5m30s
katib-mysql-57884cb488-pbrl8                                   0/1     Pending            0          5m30s
katib-ui-5c5cc6bd77-2wshh                                      1/1     Running            0          5m29s
kfserving-controller-manager-0                                 2/2     Running            1          5m33s
metacontroller-0                                               1/1     Running            0          5m39s
metadata-db-76c9f78f77-rt9z9                                   0/1     Pending            0          5m36s
metadata-deployment-674fdd976b-q265b                           0/1     Running            0          5m35s
metadata-envoy-deployment-5688989bd6-6qqcm                     1/1     Running            0          5m35s
metadata-grpc-deployment-5579bdc87b-bpnfd                      0/1     CrashLoopBackOff   5          5m35s
metadata-ui-9b8cd699d-jrq4l                                    1/1     Running            0          5m34s
minio-755ff748b-z275j                                          0/1     Pending            0          5m28s
ml-pipeline-79b4f85cbc-j4s25                                   1/1     Running            0          5m28s
ml-pipeline-ml-pipeline-visualizationserver-5fdffdc5bf-5sqzj   1/1     Running            0          5m26s
ml-pipeline-persistenceagent-645cb66874-rnqlw                  1/1     Running            2          5m27s
ml-pipeline-scheduledworkflow-6c978b6b85-z9nx4                 1/1     Running            0          5m26s
ml-pipeline-ui-6995b7bccf-lh5tj                                1/1     Running            0          5m27s
ml-pipeline-viewer-controller-deployment-8554dc7b9f-f484m      1/1     Running            0          5m27s
mysql-598bc897dc-cg4sf                                         0/1     Pending            0          5m27s
notebook-controller-deployment-7db57b9ccf-fcpjc                1/1     Running            0          5m34s
profiles-deployment-5d87dd4f87-cpllk                           2/2     Running            0          5m26s
pytorch-operator-5fd5f94bdd-r24qf                              1/1     Running            0          5m34s
seldon-controller-manager-679fc777cd-wmgkj                     1/1     Running            0          5m25s
spark-operatorcrd-cleanup-rw598                                0/2     Completed          0          5m36s
spark-operatorsparkoperator-c7b64b87f-qj78l                    1/1     Running            0          5m36s
spartakus-volunteer-6b767c8d6-blkqg                            1/1     Running            0          5m31s
tensorboard-6544748d94-hvp5x                                   1/1     Running            0          5m31s
tf-job-operator-7d7c8fb8bb-c9sl8                               1/1     Running            0          5m31s
workflow-controller-945c84565-dgvr6                            1/1     Running            0          5m38s
```

**Create Persistent Volume and Services**

To ensure Kubeflow runs successfully, deploy the following extensions.

```
kubectl apply -f ~/kubeflow/katacoda.yaml

master $ cat ~/kubeflow/katacoda.yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: volname
spec:
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
    - ReadOnlyMany
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Recycle
  hostPath:
    path: /data/
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: tf-hub-lb-katacoda
  name: tf-hub-lb-katacoda
spec:
  type: LoadBalancer
  ports:
  - name: hub
    port: 80
    protocol: TCP
    targetPort: 8000
  selector:
    app: tf-hub
  externalIPs:
  - 10.0.0.5
  - 10.0.0.7
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: centraldashboard-katacoda
  name: centraldashboard-katacoda
spec:
  type: LoadBalancer
  ports:
  - port: 8082
    protocol: TCP
    targetPort: 8082
  selector:
    app: centraldashboard
  externalIPs:
  - 10.0.0.5
  - 10.0.0.7
---
apiVersion: v1
kind: Service
metadata:
  labels:
    service: ambassador
  name: ambassador-katacoda
spec:
  type: LoadBalancer
  ports:
  - name: ambassador
    port: 30080
    protocol: TCP
    targetPort: 80
  selector:
    service: ambassador
  externalIPs:
  - 10.0.0.5
  - 10.0.0.7
```

This will create the LoadBalancer and Persistent Volume required by Kubeflow. This will vary based on your environment.


**Example TensorFlow Application**


The main Kubeflow capability is to easily deploy TensorFlow code that had been packaged as a Docker Image.

In this step, we'll deploy our first TensorFlow workload that performs a matrix multiplication across the defined workers and parameter servers.

You can see the main execution code-snippet below:

```
for job_name in cluster_spec.keys():
  for i in range(len(cluster_spec[job_name])):
    d = "/job:{0}/task:{1}".format(job_name, i)
    with tf.device(d):
      a = tf.constant(range(width * height), shape=[height, width])
      b = tf.constant(range(width * height), shape=[height, width])
      c = tf.multiply(a, b)
      results.append(c)
```

The complete example can be viewed at https://github.com/tensorflow/k8s/tree/master/examples/tf_sample



**Deploy TensorFlow Job (TFJob)**

TfJob provides a Kubeflow custom resource that makes it easy to run distributed or non-distributed TensorFlow jobs on Kubernetes. The TFJob controller takes a YAML specification for a master, parameter servers, and workers to help run distributed computation.

A Custom Resource Definition (CRD) provides the ability to create and manage TF Jobs in the same fashion as built-in Kubernetes resources. Once deployed, the CRD can configure the TensorFlow job, allowing users to focus on machine learning instead of infrastructure.

**Create TFJob Deployment Definition**

To deploy the TensorFlow workload described in the previous step, Kubeflow needs a TFJob definition. In this scenario, you can view it by running cat example.yaml

The definition defines three components:

Master: Each job must have one master. The master will coordinate training operations execution between workers.

Worker: A job can have 0 to N workers. Each worker process runs the same model, providing parameters for processing to a Parameter Server.

PS: A job can have 0 to N parameter servers. Parameter server enables you to scale your model across multiple machines.

More information can be found at https://www.tensorflow.org/deploy/distributed



```
master $ cat example.yaml
apiVersion: "kubeflow.org/v1alpha2"
kind: "TFJob"
metadata:
  name: "example-job"
spec:
  tfReplicaSpecs:
    Master:
      replicas: 1
      restartPolicy: Never
      template:
        spec:
          containers:
            - name: tensorflow
              image: gcr.io/tf-on-k8s-dogfood/tf_sample:dc944ff
    Worker:
      replicas: 1
      restartPolicy: Never
      template:
        spec:
          containers:
            - name: tensorflow
              image: gcr.io/tf-on-k8s-dogfood/tf_sample:dc944ff
    PS:
      replicas: 2
      restartPolicy: Never
      template:
        spec:
          containers:
            - name: tensorflow
              image: gcr.io/tf-on-k8s-dogfood/tf_sample:dc944ffmaster $ 
```


**Deploying TFJob**

The TFJob can be deployed by running kubectl apply -f example.yaml

By deploying the job, Kubernetes will schedule the workloads for execution across the available nodes. As part of the deployment, Kubeflow will configure TensorFlow with the required settings allowing the different components to communicate.

The next step will explain the Job and how to access the results.

```
master $ cat example.yaml 
apiVersion: "kubeflow.org/v1alpha2"
kind: "TFJob"
metadata:
  name: "example-job"
spec:
  tfReplicaSpecs:
    Master:
      replicas: 1
      restartPolicy: Never
      template:
        spec:
          containers:
            - name: tensorflow
              image: gcr.io/tf-on-k8s-dogfood/tf_sample:dc944ff
    Worker:
      replicas: 1
      restartPolicy: Never
      template:
        spec:
          containers:
            - name: tensorflow
              image: gcr.io/tf-on-k8s-dogfood/tf_sample:dc944ff
    PS:
      replicas: 2
      restartPolicy: Never
      template:
        spec:
          containers:
            - name: tensorflow
              image: gcr.io/tf-on-k8s-dogfood/tf_sample:dc944ffmaster $ 
```

**View Job Progress and Results**


The status of TensorFlow jobs can be viewed via kubectl get tfjob. Once the TensorFlow job has been completed, the master is marked as successful. Keep running the job command to see when it finishes.

The master is responsible for coordinating the execution and aggregating the results. Under the covers, the completed workloads can be listed using kubectl get pods | grep Completed

In this example, the results are outputted to STDOUT, viewable using kubectl logs.

```
master $ kubectl get pods | grep Completed
spark-operatorcrd-cleanup-rw598                                0/2     Completed   0          11m
```


The command below will output the results:

```
master $ kubectl logs $(kubectl get pods | grep Completed | tr -s ' ' | cut -d ' ' -f 1)
Error from server (BadRequest): a container name must be specified for pod spark-operatorcrd-cleanup-rw598, choose one of: [delete-sparkapp-crd delete-scheduledsparkapp-crd]
```

You will see the results from the execution of the workload on the master, worker and parameter servers.

**Deploy JupyterHub**

The second key component of Kubeflow is the ability to run Jupyter Notebooks via JupyterHub. Jupyter Notebook is the classic data science tool to run inline scripts and code snippets while documenting the process in the browser.

With Kubeflow the JupyterHub is deployed onto the Kubernetes cluster. You can find there the Load Balancer IP address using kubectl get svc


```
master $ kubectl get svc
NAME                                           TYPE           CLUSTER-IP       EXTERNAL-IP                  PORT(S)             AGE
admission-webhook-service                      ClusterIP      10.99.243.199    <none>                       443/TCP             13m
ambassador-katacoda                            LoadBalancer   10.98.237.224    10.0.0.5,10.0.0.5,10.0.0.7   30080:31639/TCP     6m28s
application-controller-service                 ClusterIP      10.102.135.32    <none>                       443/TCP             13m
argo-ui                                        NodePort       10.103.170.17    <none>                       80:32051/TCP        13m
centraldashboard                               ClusterIP      10.98.183.111    <none>                       80/TCP              13m
centraldashboard-katacoda                      LoadBalancer   10.109.33.92     10.0.0.5,10.0.0.5,10.0.0.7   8082:32208/TCP      6m28s
jupyter-web-app-service                        ClusterIP      10.103.93.47     <none>                       80/TCP              13m
katib-controller                               ClusterIP      10.102.196.190   <none>                       443/TCP,8080/TCP    13m
katib-db-manager                               ClusterIP      10.97.158.136    <none>                       6789/TCP            13m
katib-mysql                                    ClusterIP      10.106.40.149    <none>                       3306/TCP            13m
katib-ui                                       ClusterIP      10.105.193.139   <none>                       80/TCP              13m
kfserving-controller-manager-metrics-service   ClusterIP      10.102.26.47     <none>                       8443/TCP            13m
kfserving-controller-manager-service           ClusterIP      10.102.231.147   <none>                       443/TCP             13m
kfserving-webhook-server-service               ClusterIP      10.97.87.57      <none>                       443/TCP             12m
metadata-db                                    ClusterIP      10.108.53.33     <none>                       3306/TCP            13m
metadata-envoy-service                         ClusterIP      10.98.231.135    <none>                       9090/TCP            13m
metadata-grpc-service                          ClusterIP      10.103.214.57    <none>                       8080/TCP            13m
metadata-service                               ClusterIP      10.101.201.100   <none>                       8080/TCP            13m
metadata-ui                                    ClusterIP      10.99.46.208     <none>                       80/TCP              13m
minio-service                                  ClusterIP      10.98.82.252     <none>                       9000/TCP            13m
ml-pipeline                                    ClusterIP      10.102.93.176    <none>                       8888/TCP,8887/TCP   13m
ml-pipeline-ml-pipeline-visualizationserver    ClusterIP      10.107.84.5      <none>                       8888/TCP            13m
ml-pipeline-tensorboard-ui                     ClusterIP      10.98.174.46     <none>                       80/TCP              13m
ml-pipeline-ui                                 ClusterIP      10.106.229.86    <none>                       80/TCP              13m
mysql                                          ClusterIP      10.109.195.162   <none>                       3306/TCP            13m
notebook-controller-service                    ClusterIP      10.96.75.9       <none>                       443/TCP             13m
profiles-kfam                                  ClusterIP      10.98.181.57     <none>                       8081/TCP            13m
pytorch-operator                               ClusterIP      10.103.162.38    <none>                       8443/TCP            13m
seldon-webhook-service                         ClusterIP      10.108.50.44     <none>                       443/TCP             13m
tensorboard                                    ClusterIP      10.108.182.168   <none>                       9000/TCP            13m
tf-hub-lb-katacoda                             LoadBalancer   10.100.93.150    10.0.0.5,10.0.0.5,10.0.0.7   80:31598/TCP        6m29s
tf-job-operator                                ClusterIP      10.102.146.249   <none>                       8443/TCP            13m
```

**Open Jupyter Hub**

Via Katacoda, you can access the browser interface at the following link https://a4fbd1ff5e1e4d2c9233ee8208f64a9a-167772165-80-shadow05.environments.katacoda.com or using the terminal Jupyterhub tab. To access the JupyterHub use the username admin and a blank password in the login form.

To deploy a notebook, a new server has to be started. Kubeflow is using internally the
gcr.io/kubeflow-images-public/tensorflow-1.8.0-notebook-cpu:v0.2.1

Docker Image as default. After accessing the JupyterHub, you can click Start My server button.

The server launcher allows you to configure additional options, such as resource requirements. In this case, accept the defaults and click Spawn to start the server. Now you can see the contents of the Docker image that you can navigate, extend and work with Jupyter Notebooks.

Under the covers, this will Spawn a new Kubernetes Pod called jupyter-admin for managing the server. View this using kubectl get pods jupyter-admin


**Working with Jupyter Notebook**

JupyterHub can now be accessed via the pod. You can now work with the environment seamlessly. For example to create a new notebook, select the New dropdown, and select the Python 3 kernel as shown below.


It's now possible to create code snippets. To start working with TensorFlow, paste the code below to the first cell and run it.

```
from __future__ import print_function

import tensorflow as tf

hello = tf.constant('Hello TensorFlow!')
s = tf.Session()
print(s.run(hello))
```


**Deploy Trained Model Server**

The Kubeflow tf-serving provides the template for serving a TensorFlow model. This can be customised and deployed by using Ksonnet and defining the parameters based on your model.

Using environment variables, we're defining the names and path of where our trained model is located.

```
MODEL_COMPONENT=model-server
MODEL_NAME=inception
MODEL_PATH=/serving/inception-export
```


Using Ksonnet, it's possible to extend the Kubeflow tf-serving component to match the requirements for the model.


```
cd ~/kubeflow_ks_app
ks generate tf-serving ${MODEL_COMPONENT} --name=${MODEL_NAME}
ks param set ${MODEL_COMPONENT} modelPath $MODEL_PATH

ks param set ${MODEL_COMPONENT} modelServerImage katacoda/tensorflow_serving
```

The parameters defined can be viewed via ks param list


This provides a script that can be deployed to the environment and make our model available to clients.

You can deploy the template to the defined Kubernetes cluster.


```
master $ ks apply default -c ${MODEL_COMPONENT}

```


**Image Classification**

In this example, we use the pre-trained Inception V3 model. It's the architecture trained on ImageNet dataset. The ML task is image classification while the model server and its clients being handled by Kubernetes.

To use the published model, you need to set up the client. This can be achieved the same way as other jobs. The YAML file for deploying the client is cat ~/model-client-job.yaml. To deploy it use the following command:

`kubectl apply -f ~/model-client-job.yaml`

To see the status of the model-client-job run:

`kubectl get pods`

The command below will output the classification results for the Katacoda logo.

`kubectl logs $(kubectl get pods | grep Completed | tail -n1 |  tr -s ' ' | cut -d ' ' -f 1)`

More information on serving models via Kubernetes can be found at https://github.com/kubeflow/kubeflow/tree/master/components/k8s-model-server

