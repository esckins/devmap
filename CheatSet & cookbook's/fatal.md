https://komodor.com/learn/how-to-fix-kubernetes-node-not-ready-error/


[ada@ihost10 deployment.apps]$ kubectl describe node host11
Name:               host11
Roles:              master
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=host11
                    kubernetes.io/os=linux
                    node-role.kubernetes.io/master=
Annotations:        kubeadm.alpha.kubernetes.io/cri-socket: /var/run/dockershim.sock
                    node.alpha.kubernetes.io/ttl: 0
                    projectcalico.org/IPv4Address: 10.30.69.57/24
                    projectcalico.org/IPv4IPIPTunnelAddr: 10.96.128.192
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Thu, 25 Jun 2020 17:39:41 +0300
Taints:             node.kubernetes.io/unreachable:NoExecute
                    node-role.kubernetes.io/master:NoSchedule
                    node.kubernetes.io/unreachable:NoSchedule
Unschedulable:      false
Lease:
  HolderIdentity:  host11
  AcquireTime:     <unset>
  RenewTime:       Mon, 13 Feb 2023 19:13:54 +0300
Conditions:
  Type                 Status    LastHeartbeatTime                 LastTransitionTime                Reason              Message
  ----                 ------    -----------------                 ------------------                ------              -------
  NetworkUnavailable   False     Wed, 09 Nov 2022 00:34:51 +0300   Wed, 09 Nov 2022 00:34:51 +0300   CalicoIsUp          Calico is running on this node
  MemoryPressure       Unknown   Mon, 13 Feb 2023 19:10:40 +0300   Mon, 13 Feb 2023 19:14:38 +0300   NodeStatusUnknown   Kubelet stopped posting node status.
  DiskPressure         Unknown   Mon, 13 Feb 2023 19:10:40 +0300   Mon, 13 Feb 2023 19:14:38 +0300   NodeStatusUnknown   Kubelet stopped posting node status.
  PIDPressure          Unknown   Mon, 13 Feb 2023 19:10:40 +0300   Mon, 13 Feb 2023 19:14:38 +0300   NodeStatusUnknown   Kubelet stopped posting node status.
  Ready                Unknown   Mon, 13 Feb 2023 19:10:40 +0300   Mon, 13 Feb 2023 19:14:38 +0300   NodeStatusUnknown   Kubelet stopped posting node status.
Addresses:
  InternalIP:  10.30.69.57
  Hostname:    host11
Capacity:
  cpu:                8
  ephemeral-storage:  15186Mi
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             16125692Ki
  pods:               110
Allocatable:
  cpu:                8
  ephemeral-storage:  14331307599
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             16023292Ki
  pods:               110
System Info:
  Machine ID:                1111111111111111
  System UUID:                1111111111111111
  Boot ID:                   1111111111111111
  Kernel Version:             4.14.15-2047.518.4.el7uek.x86_64
  OS Image:                   Oracle Linux Server 7.9
  Operating System:           linux
  Architecture:               amd64
  Container Runtime Version:  docker://19.3.11
  Kubelet Version:            v1.18.3
  Kube-Proxy Version:         v1.18.3
Non-terminated Pods:          (7 in total)
  Namespace                   Name                                    CPU Requests  CPU Limits  Memory Requests  Memory Limits  AGE
  ---------                   ----                                    ------------  ----------  ---------------  -------------  ---
  kube-system                 calico-node-9mk6g                       250m (3%)     0 (0%)      0 (0%)           0 (0%)         2y246d
  kube-system                 coredns-66bff467f8-jqrrl                100m (1%)     0 (0%)      70Mi (0%)        170Mi (1%)     508d
  kube-system                 etcd-host11                       0 (0%)        0 (0%)      0 (0%)           0 (0%)         2y246d
  kube-system                 kube-apiserver-host11             250m (3%)     0 (0%)      0 (0%)           0 (0%)         2y246d
  kube-system                 kube-controller-manager-host11    200m (2%)     0 (0%)      0 (0%)           0 (0%)         2y246d
  kube-system                 kube-proxy-ktf9c                        0 (0%)        0 (0%)      0 (0%)           0 (0%)         2y246d
  kube-system                 kube-scheduler-host11             100m (1%)     0 (0%)      0 (0%)           0 (0%)         2y246d
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource           Requests    Limits
  --------           --------    ------
  cpu                900m (11%)  0 (0%)
  memory             70Mi (0%)   170Mi (1%)
  ephemeral-storage  0 (0%)      0 (0%)
  hugepages-1Gi      0 (0%)      0 (0%)
  hugepages-2Mi      0 (0%)      0 (0%)
Events:              <none>















● kubelet.service - kubelet: The Kubernetes Node Agent
   Loaded: loaded (/usr/lib/systemd/system/kubelet.service; enabled; vendor preset: disabled)
  Drop-In: /usr/lib/systemd/system/kubelet.service.d
           └─10-kubeadm.conf
   Active: activating (auto-restart) (Result: exit-code) since Mon 2023-02-27 14:25:12 +03; 2s ago
     Docs: https://kubernetes.io/docs/
  Process: 115608 ExecStart=/usr/bin/kubelet $KUBELET_KUBECONFIG_ARGS $KUBELET_CONFIG_ARGS $KUBELET_KUBEADM_ARGS $KUBELET_EXTRA_ARGS (code=exited, status=255)
 Main PID: 115608 (code=exited, status=255)

Feb 27 14:25:12 host11 systemd[1]: Unit kubelet.service entered failed state.
Feb 27 14:25:12 host11 systemd[1]: kubelet.service failed.



journalctl -u kubelet


failed to load Kubelet config file /var/lib/kubelet/config.yaml, error failed to read


$ kubectl get pods -n kube-system


