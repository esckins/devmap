# kubernetes-cheat-sheet

[resource](https://github.com/DenizParlak/kubernetes-cheat-sheet/blob/master/README.md)

[repo's](https://github.com/learnk8s/kubernetes-production-best-practices/blob/master/notes/kubernetes-best-practices.md)

Nodes

1-) Node'ların listelenmesi

kubectl get nodes ve(ya) kubectl get no

Not: Diğer örneklerde de göreceğiniz gibi, bazı parametreler varsayılan olarak kısaltmalara sahiptir. İki kullanımın output olarak herhangi bir farkı yoktur.


Aynı komut "-o wide" parametresiyle birlikte kullanıldığında Internal-External IP, işletim sistemi, çekirdek versiyonu ve Docker Runtime bilgileri de gösterilir.



"--show-labels" parametresi ile label bilgileri görüntülenebilir. Bu parametreden aldığınız sonucu "--selector" parametresi ile birlikte kullanarak "label" bazlı listeleme yapabilirsiniz.



Listeme işlemlerinde JSON bazlı filtrelemeler de yapılabilmektedir. Örneğin sadece Internal IP bilgilerini getirmek için:

kubectl get nodes -o jsonpath='{.items[*].status.addresses[?(@.type=="InternalIP")].address}'

gibi bir kullanım mümkündür.

2-) Node'lar hakkında bilgi alma

kubectl describe nodes ve(ya) kubectl describe no

Herhangi bir node adı belirtilmediğinde mevcut cluster üzerinde çalışan tüm node'lar sırasıyla listelenir. Argüman olarak bir node adı verildiğinde ise o node'a ait detaylı bilgiler görüntülenir.



Bir node hakkında çok daha ayrıntılı bilgi alabilmek için "get" ile birlikte "-o yaml" parametresi de kullanılabilir. Bu şekilde bilgiler JSON formatında aktarılır.

kubectl get no -o yaml



3-) Node üzerinde değişiklik yapma

kubectl edit no node_adı



Pods
1-) Yeni Pod oluşturma

kubectl create -f dosya_adı ve(ya) kubectl apply -f dosya_adı

### Not: create "Imperative", apply "Declarative" mantığıyla çalışır.

Pod oluşturulurken mevcut YAML dosyası kullanlabileceği gibi "run" ve belirleyici diğer parametreler ile birlikte de aynı işlem gerçekleştirilebilir.

kubectl run pod_adı --image=redis



2-) Pod'ların listelenmesi

alias k=kubectl

Toplu pod silmek için

kubectl get pod -n esckins | grep Evicted | awk '{print $1}' | xargs kubectl delete pod -n esckins

force ;
$ kubectl delete pod kafka-iis-2-c9c86899f-kqlcr --force --grace-period=0 --namespace=esckin
warning: Immediate deletion does not wait for confirmation that the running resource has been terminated. The resource may continue to run on the cluster indefinitely.
pod "kafka-iis-2-c9c86899f-kqlcr" force deleted

kubectl scale -n esckin deployment kafka-iis-4 --replicas=6

kubectl get pods ve(ya) kubectl get po



"-o wide" kullanımı pod'lar için de geçerlidir.



"--show-labels" kullanımı burada da geçerlidir. Label bilgisine göre "-l" parametresi ile filtreleme yapılabilir.



3-) Pod'lar hakkında bilgi alma

kubectl describe pods ve(ya) kubectl describe po



Pod adı belirtilmediği sürece tüm pod'ların bilgisi listelenir. Özellikle troubleshooting gerektiren duruesckinsrda ilk başvurulan komutlardan birisidir.

4-) Pod'ların silinmesi

kubectl delete pods ve(ya) kubectl delete po

Çoklu silmeler için Pod isimleri aralarında boşluk bırakalarak tanıesckinsnabilir.



5-) Pod üzerinde değişiklik yapma

kubectl edit po pod_adı

6-) Pod'u interaktif modda başlatma

kubectl run -i --tty busybox --image=busybox -- sh



7-) Yeni başlatılan Pod'un özelliklerini YAML formatında dosyaya aktarma

kubectl run pod_adı --image=alpine -o yaml > my_pod.yaml



8-) Pod üzerinde komut çalıştırma

kubectl exec pod_adı -- komut



9-) Pod'a Probe kullanımı ekleme

Üç çeşit Probe vardır:

-Liveness Probe: Pod istenildiği gibi çalışıyor mu kontrol edilir. Hata kodu alınırsa container silinir ve yeniden başlatılır.

-Readiness Probe: Pod trafik almaya uygun mu kontrol edilir. Hata kodu dönerse bu Pod'a trafik yönlendirmez.

-Startup Probe: Pod'un içerisindeki uygulama istenildiği şekilde çalışıyor mu kontrol edilir.

Üç çeşit Probe yöntemi vardır:

```

-ExecAction

apiVersion: v1
kind: Pod
metadata: exec-probe
spec:
  containers:
  - name: exec-pod
    image: nginx
    args:
    - /bin/sh
    - -c
    - touch /tmp/deneme.txt; sleep 600
    livenessProbe:
      exec:
        command:
        - cat 
        - /tmp/deneme.txt
      initialDelaySeconds: 5
      periodSeconds: 5



-HTTPGet

apiVersion: v1
kind: Pod
metadata:
  name: httget-probe
spec:
  containers:
  - name: probe-pod
    image: httpd:2.4
    livenessProbe:
      httppGet:
        path: /index.html
        port: 80
      initialDelaySeconds: 5
      periodSeconds: 5


-TCPSocket

apiVersion: v1
kind: Pod
metadata:
  name: tcpsocket-probe
spec:
  containers:
  - name: tcpsocket-pod
    image: nginx
    ports:
    - containerPort: 80
    livenessProbe:
      tcpSocket:
        port: 80
      initialDelaySeconds: 5
      periodSeconds: 5


```
Namespaces
1-) Yeni namespace oluşturma

kubectl create namespace namespace_adı



2-) Namespace'lerin listelenmesi

kubectl get namespaces ve(ya) kubectl get ns



"-o yaml" parametresi namespace'ler için de aktiftir.

3-) Namespace'ler hakkında bilgi alma

kubectl describe namespaces ve(ya) kubectl describe ns



4-) Namespace'lerin silinmesi

kubectl delete namespace namespace_adı ve(ya) kubectl delete ns namespace_adı

Deployments
1-) Yeni Deployment oluşturma

kubectl create deployment deployment_adı --image=imaj_adı



2-) Deployment'ların listelenmesi

kubectl get deployments ve(ya) kubectl get deploy



Kullanılan Docker imajları ve "selector" bilgisi "-o wide" ile görüntülenebilir.



"-o yaml" parametresi aynı şekilde Deployment'lar için de kullanılabilir.

3-) Deployment'lar hakkında bilgi alma

kubectl describe deployments ve(ya) kubectl describe deploy

Yine troubleshoot duruesckinsrında kullanılan başlıca komutlardan birisidir.



4-) Deployment'ların silinmesi

kubectl delete deployments deployment_adı ve(ya) kubectl delete deploy deployment_adı

Services
1-) Yeni Service oluşturma

kubectl create service loadbalancer mylb --tcp="80:8080"

Ana şablon "kubectl create service" ile başlar, sonrasında service tipi belirtilmelidir. Bu örnekte "loadbalancer" tipi seçilmiştir. Diğer tipler:

clusterip
externalname
nodeport
Sonrasında service adı ve port tanıesckinsnır.



2-) Service'lerin listelenmesi

kubectl get services ve(ya) kubectl get svc



"-o wide" parametresi "selector" bilgisini getirir. "-o yaml" kullanımı servisler için de geçerlidir.

"--show-labels" parametresi ile servise atanan label bilgileri de görüntülenebilir.



3-) Service'ler hakkında bilgi alma

kubectl describe services service_adı ve(ya) kubectl describe svc service_adı



4-) Service üzerinde değişiklik yapma

kubectl edit service ve(ya) kubectl edit svc



5-) Service'lerin silinmesi

kubectl delete services service_adı ve(ya) kubectl delete svc service_adı

DaemonSet
1-) DaemonSet'lerin listelenmesi

kubectl get daemonset ve(ya) kubectl get ds



2-) DaemonSet hakkında bilgi alma

kubectl describe daemonset ve(ya) kubectl describe ds

3-) DaemonSet üzerinde değişiklik yapma

kubectl edit daemonset ve(veya) kubectl edit ds



4-) DaemonSet'lerin silinmesi

kubectl delete daemonset daemonsets_adı ve(ya) kubectl delete ds daemonsets_adı

PersistentVolume
1-) PersistentVolume'ların listelenmesi

kubectl get persistentvolume ve(ya) kubectl get pv



2-) PersistentVolume hakkında bilgi alma

kubectl describe persistentvolume ve(ya) kubectl describe pv



3-) PersistentVolume'lerin silinmesi

kubectl delete persistentvolume persistentvolume_adı ve(ya) kubectl delete pv persistentvolume_adı

PersistentVolumeClaim
1-) PersistentVolumeClaim'ların listelenmesi

kubectl get persistentvolumeclaim ve(ya) kubectl get pvc



2-) PersistentVolumeClaim'ler hakkında bilgi alma

kubectl describe persistentvolumeclaim ve(ya) kubectl describe pvc



3-) PersistentVolumeClaim'lerin silinmesi

kubectl delete persistentvolumeclaim persistentvolumeclaim_adı ve(ya) kubectl delete pvc persistentvolumeclaim_adı

ReplicaSets
1-) ReplicaSets listelenmesi

kubectl get replicasets ve(ya) kubectl get rs



2-) ReplicaSets hakkında bilgi alma

kubectl describe replicasets ve(ya) kubectl describe rs



3-) ReplicaSet'lerin silinmesi

kubectl delete replicaset(s) replicaset_adı ve(ya) kubectl delete rs replicaset_adı

ConfigMaps
1-) ConfigMap'lerin listelenmesi

kubectl get configmaps ve(ya) kubectl get cm



2-) ConfigMap'ler hakkında bilgi alma

kubectl describe configmaps configmap_adı ve(ya) kubectl describe cm configmap_adı



Secrets
1-) Secret'ların listelenmesi

kubectl get secret ve(ya) kubectl get secrets



Roles
1-) Rollerin listelenmesi

kubectl get roles



Events
1-) Event'ların listelenmesi

kubectl get events ve(ya) kubectl get ev 



2-) Event'ların anlık takip edilmesi

kubectl get events -w ve(ya) kubectl get ev -w 

# Logs

1-) Pod loglarının görüntülenmesi

kubectl logs pod_adı


kubectl logs my-pod > my-pod-logs.txt

kubectl logs my-pod | grep search-expression

kubectl logs job/my-job

kubectl logs deployment/my-deployment


$ kubectl apply -f /root/course/es.yaml
kubectl logs es | grep "mode \[basic\] - valid"
kubectl logs kibana|grep "Status changed from yellow to green"


kubectl describe po -l k8s-app=filebeat-dynamic -n kube-system
kubectl get po -l k8s-app=kube-state-metrics -n kube-system

2-) Son X satır log'ların listelenmesi

kubectl logs --tail=X pod_adı



3-) Son X saatlik - X dakikalık log'ların listelenmesi

kubectl logs --since=X pod_adı

Saat bazlı listelemede "h" kullanılır,

Dakika bazlı listelemede "m" kullanılır.



4-) Belirli bir selector'a sahip pod'lara ait log'ların listelenmesi

kubectl logs --selector=selector_kriterleri

Örneğin label olarak run=main_svc belirtilmiş pod'larınızın tümünden log çıktısı almak için

kubectl logs --selector=run=main_svc

yazabilirsiniz.

ServiceAccounts
1-) ServiceAccount'ların görüntülenmesi

kubectl get serviceaccounts ve(ya) kubectl get sa



2-) Bilgilerin .yaml formatında alınması

kubectl get serviceaccounts -o yaml ve(ya) kubectl get sa -o yaml



Genel Bilgiler ve İpuçları
1-) Component'lar farklı namespace'ler altında oluşturulmuş olabilir, listelemeler varsayılan olarak "default" namespace'i baz alınarak yapılır. Bu tarz duruesckinsrda "-n" parametresi ile hedef namespace belirtilir.



2-) Tüm yapıların listelenmesi

kubectl get all



3-) Cluster hakkında temel bilgi alma

kubectl cluster-info



4-) cat komutu ile çoklu component oluşturma

apiVersion: v1
kind: Pod
metadata:
  name: nightw
spec:
  containers:
  - name: nightw
    image: oraclelinux
    args:
    - sleep
    - "1000"
---
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu
spec:
  containers:
  - name: ubuntu
    image: ubuntu
    args:
    - sleep
    - "1700"
EOF
5-) Çoklu component silme

kubectl delete pod,svc mypod mysrv

6-) PersistentVolume'ların boyuta göre listelenmesi

kubectl get pv --sort-by=.spec.capacity.storage



7-) Ölçeklendirmenin güncellenmesi

kubectl scale --replicas=X component_tipi/component_adı

Resimdeki örnekte 1 replica olarak çalışan deployment 3 replica şeklinde güncellenmiştir.



8-) Container log'larını görüntüleme

kubectl logs pod_adı -c container_adı



9-) Mevcut Pod'a label ekleme

kubectl label pod pod_adı mein=kampf



10-) Bash için otomatik komut tanıesckinsmanın ayarlanması

source <(kubectl completion bash

Not: Komutun çalışması için bash-completion paketinin yüklü olması gerekmektedir.

İşlemin kalıcı olması adına .bashrc dosyasına yazmak için:

echo "source <(kubectl completion bash)" >> ~/.bashrc

11-) Aynı anda birden çok kubeconfig dosyasının kullanılması

config ve config2 isimli dosyalarımız olduğunu varsayalım.

export KUBECONFIG=~/.kube/config:~/.kube/config2














*** OTHER ***








kubectl describe pod metrics-server-555aa8b6a6-tcchv -n kube-system


kubectl get ds --all-namespaces

NAMESPACE     NAME          DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR                 AGE
kube-system   calico-node   10        10        10      10           10          beta.kubernetes.io/os=linux   567d
kube-system   kube-proxy    10        10        10      10           10          kubernetes.io/os=linux        567d

alias k=kubectl

k get nodes -o wide

k version --short



```
alias k=kubectl

k get nodes

k get servers

kubectl apply -f cluster.yaml

k get servers ## allocated (true)

k get serverbindings -owide ## (serverclass)

k get metalmachines -owide

k get machines

kubectl describe clusters ## Status
```


```
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: python-test-code-configmap
data:
  entrypoint.sh: |-
    #!/usr/bin/env python

    import math

    while True:
      x = 0.0001
      for i in range(1000000):
        x = x + math.sqrt(x)
        print(x)
      print("OK!")

---

kubernetes --- autoscale 


apiVersion: apps/v1
kind: Deployment
metadata:
  name: constant-load-deployment-test
spec:
  selector:
    matchLabels:
      run: python-constant-load-test
  replicas: 1
  template:
    metadata:
      labels:
        run: python-constant-load-test
    spec:
      containers:
        - name: python-runtime
          image: python:alpine3.15
          resources:
            limits:
              cpu: 50m
            requests:
              cpu: 20m
          command:
            - /bin/entrypoint.sh
          volumeMounts:
            - name: python-test-code-volume
              mountPath: /bin/entrypoint.sh
              readOnly: true
              subPath: entrypoint.sh
      volumes:
        - name: python-test-code-volume
          configMap:
            defaultMode: 0700
            name: python-test-code-configmap

https://www.digitalocean.com/community/tutorials/how-to-configure-kubernetes-horizontal-pod-autoscaler-using-metrics-server



with script;

#!/usr/bin/env sh

echo
echo "[INFO] Starting load testing in 10s..."
sleep 10
echo "[INFO] Working (press Ctrl+C to stop)..."
kubectl run -i --tty load-generator \
    --rm \
    --image=busybox \
    --restart=Never \
    -n hpa-external-load \
    -- /bin/sh -c "while sleep 0.001; do wget -q -O- http://quote; done" > /dev/null 2>&1
echo "[INFO] Load testing finished."

kubectl get hpa -n hpa-external-load -w



alias k=kubectl


kubectl get nodes -o wide
 du -hsx * | sort -rh |head -20

$ kubectl cluster-info
Kubernetes master is running at https://10.10.30.31:6131
KubeDNS is running at https://10.10.30.31:6131/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.

kubectl get pod -n esckins | grep Evicted | awk '{print $1}' | xargs kubectl delete pod -n esckins



kubectl get hpa

kubectl get hpa -n esckins
kubectl describe hpa kafka-trace-1 -n esckins
kubectl edit ...
kubectl delete hpa kafka-trace-1 -n esckins


kubectl autoscale ---

 kubectl autoscale deployment kafka-trace-4 --cpu-percent=90 --memory-percent=90 --min=5 --max=8 -n esckins

Error: unknown flag: --memory-percent
See 'kubectl autoscale --help' for usage.

$ kubectl get hpa php-apache-hpa -o yaml | grep -i apiversion
apiVersion: autoscaling/v1

$ kubectl delete hpa php-apache-hpa
horizontalpodautoscaler.autoscaling “php-apache-hpa” deleted

$ kubectl autoscale --help | grep -i memory
$ kubectl explain hpa | grep -i autoscaling
VERSION: autoscaling/v1

####Looks like memory is something unknown by the “kubectl autoscale” command or by “kubectl explain hpa”.

$ kubectl api-resources | grep -i -e kind -e hpa
NAME SHORTNAMES APIGROUP NAMESPACED KIND
horizontalpodautoscalers hpa autoscaling true HorizontalPodAutoscaler
$ kubectl api-versions | grep -i autoscaling
autoscaling/v1
autoscaling/v2beta1
autoscaling/v2beta2


$ kubectl expose deployment php-apache — name=php-apache-service — port=80
service/php-apache-service exposed
$ kubectl get service | grep -i -e php -e name
NAME TYPE CLUSTER-IP EXTERNAL-IP PORT(S) AGE
php-apache-service ClusterIP 10.0.213.56 <none> 80/TCP 52s


$ kubectl run -i — tty load-generator — rm — image=busybox — restart=Never — /bin/sh -c “while sleep 0.01; do wget -q -O- http://php-apache-service; done”
If you don’t see a command prompt, try pressing enter.
OK!OK!OK!OK!OK!OK!OK!OK!OK!OK!OK!OK!OK!OK!OK!OK!OK!OK!OK!OK
[…]

$ kubectl get hpa -w

k autoscale deployment kafka-trace-3 --cpu-percent=90 --memory-percent=95 --min=5 --max=9 -n esckins


 kubectl autoscale deployment kafka-trace-4 --cpu-percent=90 --min=5 --max=9 -n esckins

  kubectl get hpa --all-namespaces
  kubectl get --raw /apis/metrics.k8s.io/v1beta1

  wget metrics.k8s.io
  kubectl get --raw "/apis/
  kubectl edit hpa kafka-esc -n esckins
  kubectl edit hpa kafka-esc -n esckins

  kubectl exec etcd-master -n kube-system -- sh -c "ETCDCTL_API=3 etcdctl get / --prefix --keys-only --limit=10 --cacert /etc/kubernetes/pki/etcd/ca.crt --cert /etc/kubernetes/pki/etcd/server.crt  --key /etc/kubernetes/pki/etcd/server.key"
  ps -aux | grep kube-apiserver

kubectl top pod

kubectl set resources deployment nginx -c=nginx --limits=cpu=100m,memory=64Mi

$kubectl describe hpa
kubectl logs -f metric-server -n kube-system

Syntaxerror on line 6 of this yaml. It needs to be - --kubelet-insecure-tls and not - --kubectl-insecure-tls

spec:
  containers:
  - args:
    - --cert-dir=/tmp
    - --secure-port=4443
    - --kubectl-insecure-tls
    - --kubelet-preferred-address-types=InternalIP
    - --kubelet-use-node-status-port
    - --metric-resolution=15s
    image: k8s.gcr.io/metrics-server/metrics-server:v0.6.1
    imagePullPolicy: IfNotPresent


NAME               REFERENCE                     TARGETS         MINPODS   MAXPODS   REPLICAS   AGE
kafka-trace-1      Deployment/kafka-trace-1      23%/90%         6         12        6          2y47d
kafka-trace-2      Deployment/kafka-trace-2      22%/90%         6         12        6          2y47d
kafka-trace-3      Deployment/kafka-trace-3      26%/90%         5         9         5          6m30s

edit ;


k edit hpa kafka-trace-3 -n esckins

spec:
  maxReplicas: 12
  minReplicas: 6
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: kafka-trace-3
  targetCPUUtilizationPercentage: 90
status:
  currentCPUUtilizationPercentage: 26
  currentReplicas: 5
  desiredReplicas: 5

:wq!

$ k autoscale deployment kafka-iis-5 --cpu-percent=90 --min=5 --max=9 -n esckins
horizontalpodautoscaler.autoscaling/kafka-iis-5 autoscaled


kubectl get pod -n esckins | grep Evicted | awk '{print $1}' | xargs kubectl delete pod -n esckins

```

What does "Does not have minimum availability" in k8s mean?
---

kubectl describe pods -n namespace 


I provided the correct roles and then it worked. Roles:

storage
object
viewer
---

$ kubectl describe pods kafka-iis-4-6594bcdccf-xl6pw -n esckin


Events:
  Type     Reason            Age        From               Message
  ----     ------            ----       ----               -------
  Warning  FailedScheduling  <unknown>  default-scheduler  0/10 nodes are available: 3 node(s) had taint {node-role.kubernetes.io/master: }, that the pod didn't tolerate, 7 Insufficient cpu.
  Warning  FailedScheduling  <unknown>  default-scheduler  0/10 nodes are available: 3 node(s) had taint {node-role.kubernetes.io/master: }, that the pod didn't tolerate, 7 Insufficient cpu.



---

just want to throw out an example of it not being the CPU / mem.

I remade a cluster, which included a new node pool, nodes, etc. Then tried re-deploying some of the pods that were on the previous cluster. 3 of them were working, and one kept showing "Does not have minimum availability".

It turns out, the pod in question's deployment file had spec.template.spec.nodeName hard-coded to one of the nodes from the previous cluster. In the new cluster, that node does not exist, so it failed.

Review your k8s deployment file (.yaml or .json) if you are running into this issue and the other solutions don't work. Maybe even service files as well.


kubectl get pods -n kube-system -o jsonpath="{.items[*].spec.containers[*].image}" |\
> tr -s '[[:space:]]' '\n' |\
> sort |\
> uniq -c
      1 calico/kube-controllers:v3.8.2
     10 calico/node:v3.8.2
      3 docker.io/alpine:3.12
      2 k8s.gcr.io/coredns:1.6.7
      3 k8s.gcr.io/etcd:3.4.3-0
      3 k8s.gcr.io/kube-apiserver:v1.18.3
      3 k8s.gcr.io/kube-controller-manager:v1.18.3
     10 k8s.gcr.io/kube-proxy:v1.18.3
      3 k8s.gcr.io/kube-scheduler:v1.18.3
      1 k8s.gcr.io/metrics-server-amd64:v0.3.6



#####

`alias kubectl="kubecolor"`

https://github.com/alperen-selcuk/kubecolor

If you use your .bash_profile on more than one computer (e.g. synced via git) that might not all have kubecolor installed, you can avoid breaking kubectl like so:

`command -v kubecolor >/dev/null 2>&1 && alias kubectl="kubecolor"`

Manually via go command
go install github.com/hidetatz/kubecolor/cmd/kubecolor@latest
go get -u github.com/hidetatz/kubecolor/cmd/kubecolor


--
kubecolor --context=your_context get pods -o json
alias kubectl="kubecolor"

bash
# autocomplete for kubecolor

complete -o default -F __start_kubectl kubecolor

If you are using an alias like k="kubecolor", then just change above like:

complete -o default -F __start_kubectl k





```
kubectl get pod -n esckin | grep Evicted | awk '{print $1}' | xargs kubectl delete pod -n esckin
delete pods --

kubectl get pod -n esckin | grep Terminating | awk '{print $1}' | xargs kubectl delete pod -n esckin


kubectl get pod -n esckin | grep deployment-restart | awk '{print $1}' | xargs kubectl delete pod -n esckin
pod "deployment-restart-1679543400-7nlqq" deleted
pod "deployment-restart-1679543700-kdtdb" deleted
pod "deployment-restart-manual-59k-v62lv" deleted
pod "deployment-restart-manual-8m5-t8tlj" deleted
pod "deployment-restart-manual-8nh-td2vc" deleted
pod "deployment-restart-manual-g52-5mmlq" deleted
pod "deployment-restart-manual-jxk-tgdlz" deleted
pod "deployment-restart-manual-krg-bpvsw" deleted
pod "deployment-restart-manual-ll7-8h5dl" deleted
pod "deployment-restart-manual-pl5-zdhvz" deleted
```


