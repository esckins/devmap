#install helm
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

#install keda

helm repo add kedacore https://kedacore.github.io/charts
helm install keda kedacore/keda

---
apiVersion: keda.k8s.io/v1alpha1
kind: ScaledObject
metadata:
  name: my-scaledobject
spec:
  scaleTargetRef:
    deploymentName: my-deployment
  triggers:
  - type: periodic
    metadata:
      interval: "30s"
    scaleMetric:
      type: cpu
      resourceName: my-service
      targetAverageUtilization: 75
#  minReplicaCount: 2
#  maxReplicaCount: 5
---
apiVersion: keda.k8s.io/v1alpha1
kind: ScaledObject
metadata:
  name: my-scaledobject
spec:
  scaleTargetRef:
    deploymentName: my-deployment
  triggers:
  - type: httptrigger
    metadata:
      method: POST
      url: http://my-service/users
    scaleMetric:
      type: httprate
      resourceName: my-service
      targetAverageValue: 10
      maxScaleOutRate: 5
      minScaleInRate: 1
      cooldownPeriod: "30s"


apiVersion: keda.k8s.io/v1alpha1
kind: ScaledObject
metadata:
  name: my-scaledobject
spec:
  scaleTargetRef:
    deploymentName: my-deployment
  triggers:
  - type: mqtrigger
    metadata:
      queueName: my-queue
      connectionString: "amqp://user:password@localhost:5672/vhost"
      authMode: plain
    scaleMetric:
      type: messagecount
      resourceName: my-queue
      targetAverageValue: 1000
      maxScaleOutRate: 5
      minScaleInRate: 1
      cooldownPeriod: "30s"


apiVersion: keda.k8s.io/v1alpha1
kind: ScaledObject
metadata:
  name: my-scaledobject
spec:
  scaleTargetRef:
    deploymentName: my-deployment
  triggers:
  - type: iotdevice
    metadata:
      connectionString: "HostName=my-iot-hub.azure-devices.net;DeviceId=my-device;SharedAccessKey=XYZ"
    scaleMetric:
      type: devicetelemetry
      resourceName: my-device
      targetAverageValue: 75
      metricName: "temperature"
      maxScaleOutRate: 5
      minScaleInRate: 1
      cooldownPeriod: "30s"


apiVersion: keda.k8s.io/v1alpha1
kind: ScaledObject
metadata:
  name: my-scaledobject
spec:
  scaleTargetRef:
    deploymentName: my-deployment
  triggers:
  - type: dbpoller
    metadata:
      connectionString: "Server=my-server;Database=my-db;User Id=my-user;Password=my-password;"
      query: "SELECT COUNT(*) FROM my-table"
      pollInterval: "30s"
    scaleMetric:
      type: custom
      resourceName: my-table
      targetAverageValue: 100000
      maxScaleOutRate: 5
      minScaleInRate: 1
      cooldownPeriod: "30s"


apiVersion: keda.k8s.io/v1alpha1
kind: ScaledObject
metadata:
  name: my-scaledobject
spec:
  scaleTargetRef:
    deploymentName: my-deployment
  triggers:
  - type: fsnotify
    metadata:
      directoryPath: "/path/to/my/directory"
      fileType: .csv
    scaleMetric:
      type: filecount
      resourceName: my-directory
      targetAverageValue: 10000
      maxScaleOutRate: 5
      minScaleInRate: 1
      cooldownPeriod: "30s"

#resources: https://sezer.in/keda-kurulum-ve-kullanim/




 oc get pods -n keda
NAME                                     READY   STATUS    RESTARTS   AGE
keda-metrics-apiserver-f57cbb4b5-7z8wt   1/1     Running   0          23h
keda-olm-operator-58bd9bcd65-xqq9w       1/1     Running   0          2d23h
keda-operator-78b964d4cd-xjv47           1/1     Running   0          23h



$ oc apply -f - <<EOF
apiVersion: v1
kind: ConfigMap
metadata:
  name: cluster-monitoring-config
  namespace: openshift-monitoring
data:
  config.yaml: |
    enableUserWorkload: true
EOF


# oc project keda
# oc create serviceaccount thanos
serviceaccount/thanos created
[root@isocpibsld01 keda]# oc describe serviceaccount thanos
Name:                thanos
Namespace:           keda
Labels:              <none>
Annotations:         <none>
Image pull secrets:  thanos-dockercfg-pcrbs
Mountable secrets:   thanos-dockercfg-pcrbs
Tokens:              thanos-token-rft59
Events:              <none>

#  secret=$(oc get secret | grep thanos-token | head -n 1 | awk '{ print $1 }')
[root@host01 keda]# echo $secret
thanos-token-rft59



oc process TOKEN="$secret" -f - <<EOF | oc apply -f -
apiVersion: template.openshift.io/v1
kind: Template
parameters:
- name: TOKEN
objects:
- apiVersion: keda.sh/v1alpha1
  kind: TriggerAuthentication
  metadata:
    name: keda-trigger-auth-prometheus
  spec:
    secretTargetRef:
    - parameter: bearerToken
      name: \${TOKEN}
      key: token
    - parameter: ca
      name: \${TOKEN}
      key: ca.crt
EOF

triggerauthentication.keda.sh/keda-trigger-auth-prometheus created


$ oc apply -f - <<EOF
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: thanos-metrics-reader
rules:
- apiGroups:
  - ""
  resources:
  - pods
  - nodes
  verbs:
  - get
- apiGroups:
  - metrics.k8s.io
  resources:
  - pods
  - nodes
  verbs:
  - get
  - list
  - watch
- apiGroups:
  - ""
  resources:
  - namespaces
  verbs:
  - get
EOF
role.rbac.authorization.k8s.io/thanos-metrics-reader created


oc adm policy add-role-to-user thanos-metrics-reader -z thanos --role-namespace=openshift-ingress-operator
error: role binding in namespace "keda" can't reference role in different namespace "openshift-ingress-operator"

oc adm policy add-cluster-role-to-user cluster-reader -z thanos -n kube-system
clusterrole.rbac.authorization.k8s.io/cluster-reader added: "thanos"

oc adm policy -n openshift-ingress-operator add-cluster-role-to-user cluster-monitoring-view -z thanos
Warning: ServiceAccount 'thanos' not found
clusterrole.rbac.authorization.k8s.io/cluster-monitoring-view added: "thanos"


oc get ingresscontroller/default -o yaml | grep replicas:
replicas: 2


$ oc apply -f - <<EOF
apiVersion: keda.sh/v1alpha1
kind: ScaledObject
metadata:
  name: ingress-scaler
spec:
  scaleTargetRef:
    apiVersion: operator.openshift.io/v1
    kind: IngressController
    name: default
    envSourceContainerName: ingress-operator
  minReplicaCount: 1
  maxReplicaCount: 20
  cooldownPeriod: 1
  pollingInterval: 1
  triggers:
  - type: prometheus
    metricType: AverageValue
    metadata:
      serverAddress: https://thanos-querier.openshift-monitoring.svc.cluster.local:9091 ##Change
      namespace: openshift-ingress-operator
      metricName: 'kube-node-role'
      threshold: '1'
      query: 'sum(kube_node_role{role="worker",service="kube-state-metrics"})'
      authModes: "bearer"
    authenticationRef:
      name: keda-trigger-auth-prometheus
EOF

scaledobject.keda.sh/ingress-scaler created


   scaleTargetRef:
     apiVersion: operator.openshift.io/v1
     kind: IngressController
     name: default
     envSourceContainerName: ingress-operator


---



#oc get ingresscontroller/default -o yaml -n openshift-ingress-operator
apiVersion: operator.openshift.io/v1
kind: IngressController
metadata:
  creationTimestamp: "2023-08-06T20:21:23Z"
  finalizers:
  - ingresscontroller.operator.openshift.io/finalizer-ingresscontroller
  generation: 4
  name: default
  namespace: openshift-ingress-operator
  resourceVersion: "15478815"
  uid: 1196177d-29cb-46ff-96b2-1bb87b1d4397
spec:
  clientTLS:
    clientCA:
      name: ""
    clientCertificatePolicy: ""
  defaultCertificate:
    name: custom-apps-secret
  httpCompression: {}
  httpEmptyRequestsPolicy: Respond
  httpErrorCodePages:
    name: ""
  nodePlacement:
    nodeSelector:
      matchLabels:
        node-role.kubernetes.io/infra: ""
    tolerations:
    - effect: NoSchedule
      key: node-role.kubernetes.io/infra
      value: reserved
    - effect: NoExecute
      key: node-role.kubernetes.io/infra
      value: reserved
    - effect: NoSchedule
      key: node.ocs.openshift.io/storage
      value: "true"
  replicas: 3
  tuningOptions: {}
  unsupportedConfigOverrides: null
status:
  availableReplicas: 3
  conditions:
  - lastTransitionTime: "2023-08-06T20:21:26Z"
    reason: Valid
    status: "True"
    type: Admitted
  - lastTransitionTime: "2023-08-23T16:26:33Z"
    status: "True"
    type: PodsScheduled
  - lastTransitionTime: "2023-08-23T16:27:05Z"
    message: The deployment has Available status condition set to True
    reason: DeploymentAvailable
    status: "True"
    type: DeploymentAvailable
  - lastTransitionTime: "2023-08-23T16:27:05Z"
    message: Minimum replicas requirement is met
    reason: DeploymentMinimumReplicasMet
    status: "True"
    type: DeploymentReplicasMinAvailable
  - lastTransitionTime: "2023-08-23T16:27:35Z"
    message: All replicas are available
    reason: DeploymentReplicasAvailable
    status: "True"
    type: DeploymentReplicasAllAvailable
  - lastTransitionTime: "2023-08-06T20:21:26Z"
    message: The configured endpoint publishing strategy does not include a managed
      load balancer
    reason: EndpointPublishingStrategyExcludesManagedLoadBalancer
    status: "False"
    type: LoadBalancerManaged
  - lastTransitionTime: "2023-08-06T20:21:26Z"
    message: No DNS zones are defined in the cluster dns config.
    reason: NoDNSZones
    status: "False"
    type: DNSManaged
  - lastTransitionTime: "2023-08-23T16:27:05Z"
    status: "True"
    type: Available
  - lastTransitionTime: "2023-08-06T20:21:26Z"
    status: "False"
    type: Progressing
  - lastTransitionTime: "2023-08-23T16:27:05Z"
    status: "False"
    type: Degraded
  - lastTransitionTime: "2023-08-06T20:21:26Z"
    message: IngressController is upgradeable.
    reason: Upgradeable
    status: "True"
    type: Upgradeable
  - lastTransitionTime: "2023-08-17T09:03:25Z"
    message: Canary route checks for the default ingress controller are successful
    reason: CanaryChecksSucceeding
    status: "True"
    type: CanaryChecksSucceeding
  domain: apps.ocp-sandbox.vakifbank.intra
  endpointPublishingStrategy:
    hostNetwork:
      httpPort: 80
      httpsPort: 443
      protocol: TCP
      statsPort: 1936
    type: HostNetwork
  observedGeneration: 4
  selector: ingresscontroller.operator.openshift.io/deployment-ingresscontroller=default
  tlsProfile:
    ciphers:
    - ECDHE-ECDSA-AES138-ACA-SHA256
...

